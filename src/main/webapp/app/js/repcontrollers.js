'use strict';

angular
		.module('crenx.repcontrollers', [])
		.controller(
				'RepController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q) {
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						nombre : null,
						apellidoPaterno : null
					};
					$scope.cobranza = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					// $scope.ruta = null;
					$scope.emptyForm = true;
					var filterTextTimeout;
					$scope.$watch('ruta', function(val) {
						if (filterTextTimeout)
							$timeout.cancel(filterTextTimeout);
						filterTextTimeout = $timeout(function() {
							loadData();
						}, 250); // delay 250 ms
					})

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								// loadData();
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							}

							else {
								loadFilteredTree(e.val, source)

							}
						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						if (selectControl == "#ruta") {
							$(selectControl).append(
									$("<option></option>").val("null").text(
											"Seleccione uno"));
						}
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function loadData() {

					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0) {
							$scope.emptyForm = false;
						} else
							$scope.emptyForm = true;
						return $scope.emptyForm;

					}
					$scope.consulta = function loadDataConsulta() {
						if (!isEmptyForm()) {
							apiService.post('api/movil/creditosPorRuta/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}

					}
					function lcoationLoadCompleted(result) {
						$scope.cobranza = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.generaReporteCobranza = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteCobranza',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteCobranzaExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}
					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = 'Reporte Cobranza';
						} else
							alert("No se encontraron datos");

					}

					loadOrgTree();

				})
		.controller(
				'CajaController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q, $modal, $log) {
					// Permisos
					var permisos = $rootScope.repository.loggedUser.permissions;
					$scope.permisoCG = ($.inArray('OBJ_CAN_GASTOS', permisos) > -1) ? true
							: false;
					$scope.permisoCT = ($.inArray('OBJ_CAN_TRANSFER', permisos) > -1) ? true
							: false;
					$scope.emptyForm = true;
					$scope.emptyDatos = false;

					$scope.movimientosCaja = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.idTipoMov = [];
					$scope.idTipoOperacion = [];
					$scope.obs = '';
					$rootScope.obs = '';
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'idTipoMov') {
								$scope.filters.idTipoMov = null;
								$scope.filters.idTipoMov = e.val;
							} else if (source == 'idTipoOperacion') {
								$scope.filters.idTipoOperacion = null;
								$scope.filters.idTipoOperacion = e.val;
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
						prepareListItems("#idTipoOperacion", 'Tipo Operación');
						prepareListItems("#idTipoMov", 'Tipo Movimiento');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}
					function loadCatalogos() {
						apiService.post('/api/catalogo/tiposMovimiento', null,
								function(result) {
									$scope.idTipoMov = result.data;
									$("#idTipoMov").empty();
									$.each($scope.idTipoMov, function(key,
											value) {
										$("#idTipoMov").append(
												$("<option></option>").val(
														value.idCatalogo).text(
														value.nombre));
									});
								}, locationLoadFailed);
					}

					function loadTipoOperacion() {
						apiService.post('/api/catalogo/tiposOperacion', null,
								function(result) {
									$scope.idTipoOperacion = result.data;
									$("idTipoOperacion").empty();
									$.each($scope.idTipoOperacion, function(
											key, value) {
										$("#idTipoOperacion").append(
												$("<option></option>").val(
														value.idCatalogo).text(
														value.nombre));

									});
								}, locationLoadFailed);
					}
					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'idTipoMov' || nivel == 'idTipoOperacion')
							return;
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0) {
							$scope.emptyForm = false;
						} else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}

					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/movil/movimientosCaja/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}

					function lcoationLoadCompleted(result) {
						$scope.movimientosCaja = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal,
						tipoReporte : null,
						idTipoMov : null,
						idTipoOperacion : null
					};
					$scope.generaReporteCaja = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteCaja',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteCajaExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											if (data.data.byteLength == 0) {
												$scope.emptyDatos = true;
												return;
											}

											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}
					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Caja";
						} else
							$scope.emptyDatos = true;

					}

					$rootScope.eliminar = function(idMovimiento, tipoOperacion,
							modal_id, modal_size, modal_backdrop) {
						$scope.idMovimiento = idMovimiento;
						$rootScope.operacion = "Cancelar " + tipoOperacion;
						$scope.aux = tipoOperacion;
						$rootScope.obs = "";
						$rootScope.fechaEmi = "";

						$rootScope.currentModal = $modal
								.open({
									templateUrl : 'EliminarModal',
									size : modal_size,
									backdrop : typeof modal_backdrop == 'undefined' ? true
											: modal_backdrop
								});
					};

					$rootScope.cancelMovCaja = function(item, event) {
						var fondeoVO = {};
						fondeoVO.idMC = $scope.idMovimiento;
						fondeoVO.descripcion = this.obs;
						fondeoVO.fechaEmision = this.fechaEmi;
						fondeoVO.idDispositivo = "WebApp";

						if ($scope.aux == 'Desembolso por gasto') {
							apiService
									.post(
											'/api/org/cancelarGasto',
											fondeoVO,
											function(result) {
												toastr
														.success(
																"Se canceló satisfactoriamente el movimiento",
																"Cancelar movimiento",
																opts);
												$rootScope.currentModal.close();
												apiService
														.post(
																'api/movil/movimientosCaja/',
																$scope.filters,
																lcoationLoadCompleted,
																locationLoadFailed);
											}, serviceError);

						} else if ($scope.aux == 'Ingreso por transferencia'
								|| $scope.aux == 'Egreso por transferencia') {
							apiService
									.post(
											'/api/org/cancelarTransferencia',
											fondeoVO,
											function(result) {
												toastr
														.success(
																"Se canceló satisfactoriamente la transferencia",
																"Cancelar transferencia",
																opts);
												$rootScope.currentModal.close();
												apiService
														.post(
																'api/movil/movimientosCaja/',
																$scope.filters,
																lcoationLoadCompleted,
																locationLoadFailed);
											}, serviceError);

						}
					}
					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					// Loading AJAX Content
					$scope.openAjaxModal = function(modal_id, url_location) {
						$rootScope.currentModal = $modal
								.open({
									templateUrl : modal_id,
									resolve : {
										ajaxContent : function($http) {
											return $http
													.get(url_location)
													.then(
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml(response.data);
															},
															function(response) {
																$rootScope.modalContent = $sce
																		.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
															});
										}
									}
								});

						$rootScope.modalContent = $sce
								.trustAsHtml('Modal content is loading...');
					}
					// End From UIModalsCtrl

					loadOrgTree();
					loadCatalogos();
					loadTipoOperacion();
				})
		.controller(
				'ConcentradoCajaController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q, $modal, $log) {

					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.idTipoMov = [];
					$scope.idTipoOperacion = [];
					$scope.selectedCredito = false;
					$scope.emptyForm = true;
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'idTipoMov') {
								$scope.filters.idTipoMov = null;
								$scope.filters.idTipoMov = e.val;
							} else if (source == 'idTipoOperacion') {
								$scope.filters.idTipoOperacion = null;
								$scope.filters.idTipoOperacion = e.val;
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
						prepareListItems("#idTipoOperacion", 'Tipo Operación');
						prepareListItems("#idTipoMov", 'Tipo Movimiento');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'idTipoMov' || nivel == 'idTipoOperacion')
							return;
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0)
							$scope.emptyForm = false;
						else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}
					function loadData() {
						if (!isEmptyForm()) {
							$scope.selectedCaja = false;
							apiService.post('api/movil/concentradoCaja/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}

					function lcoationLoadCompleted(result) {
						$scope.cajas = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal,
						tipoReporte : null,
						idTipoMov : null,
						idTipoOperacion : null
					};
					$scope.generaReporteCaja = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post(
										'report/reporteCajaConcentrado',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteCajaConcentradoExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}
					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Caja Concentrado";
						} else
							alert("No se encontraron datos");

					}

					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					// CÓDIGO NUEVO
					$scope.cajaSelected = function(caja) {
						$scope.nombreUnidad = caja.nombre;
						$scope.fechaInicio = caja.fechaC;
						$scope.saldoI = caja.saldoInicial;
						$scope.saldoF = caja.saldoFinal;

						$scope.filtersDetalle = {
							divisiones : $scope.filters.divisiones,
							regiones : $scope.filters.regiones,
							gerencias : $scope.filters.gerencias,
							rutas : $scope.filters.rutas,
							fechaInicial : caja.fechaC,
							fechaFinal : caja.fechaC,
							tipoReporte : null,
							idTipoMov : null,
							idTipoOperacion : null
						};

						apiService.post('api/movil/movimientosCaja/',
								$scope.filtersDetalle, function(result) {
									$scope.movimientosCaja = result.data;
									$scope.selectedCaja = true;
								}, locationLoadFailed);

						/*
						 * $scope.producto = credito.producto;
						 * $scope.estatusCredito = credito.estatusCredito;
						 * $scope.saldo = credito.saldo; $scope.plazo =
						 * credito.plazo; $scope.creditoItem = credito;
						 * $scope.cliente = cliente;
						 * 
						 * if ($scope.selectedCredito) { $scope.selectedCredito =
						 * false return; }
						 * apiService.post('/api/cliente/pagos/',
						 * credito.idCredito, function(result) {
						 * $scope.tablaAmortizacion = result.data;
						 * $scope.selectedCredito = true; if (credito.saldo>0)
						 * $scope.habilitarPago = true; else
						 * $scope.habilitarPago = false;
						 *  }, locationLoadFailed);
						 * 
						 */
					}

					loadOrgTree();

				})

		.controller(
				'TraspasosRepController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q, $modal, $log) {

					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.idTipoMov = [];
					$scope.idTipoOperacion = [];
					$scope.selectedCredito = false;
					$scope.emptyForm = true;
					$scope.emptyDatos = false;
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'idTipoMov') {
								$scope.filters.idTipoMov = null;
								$scope.filters.idTipoMov = e.val;
							} else if (source == 'idTipoOperacion') {
								$scope.filters.idTipoOperacion = null;
								$scope.filters.idTipoOperacion = e.val;
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta Origen');
						prepareListItems("#idTipoOperacion", 'Tipo Operación');
						prepareListItems("#idTipoMov", 'Tipo Movimiento');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'idTipoMov' || nivel == 'idTipoOperacion')
							return;
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0)
							$scope.emptyForm = false;
						else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}
					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/movil/reporteTraspasos/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}

					function lcoationLoadCompleted(result) {
						$scope.traspasos = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal,
						tipoReporte : null,
						idTipoMov : null,
						idTipoOperacion : null
					};
					$scope.generaReporteCaja = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteTraspasos',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteTraspasosExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											if (data.data.byteLength == 0) {
												$scope.emptyDatos = true;
												return;
											}
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}
					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Traspasos";
						} else
							$scope.emptyDatos = true;

					}

					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					loadOrgTree();

				})
		.controller(
				'ClientesRecomprasRepController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q, $modal, $log) {

					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.idTipoMov = [];
					$scope.idTipoOperacion = [];
					$scope.selectedCredito = false;
					$scope.emptyForm = true;

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'idTipoMov') {
								$scope.filters.idTipoMov = null;
								$scope.filters.idTipoMov = e.val;
							} else if (source == 'idTipoOperacion') {
								$scope.filters.idTipoOperacion = null;
								$scope.filters.idTipoOperacion = e.val;
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta Origen');
						prepareListItems("#idTipoOperacion", 'Tipo Operación');
						prepareListItems("#idTipoMov", 'Tipo Movimiento');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'idTipoMov' || nivel == 'idTipoOperacion')
							return;
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0)
							$scope.emptyForm = false;
						else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}
					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/cliente/reporteRecompras/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}

					function lcoationLoadCompleted(result) {
						$scope.traspasos = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal,
						tipoReporte : null,
						idTipoMov : null,
						idTipoOperacion : null
					};
					$scope.generaReporteCaja = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteRecompra',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteRecompraExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											if (data.data.byteLength == 0) {
												$scope.emptyDatos = true;
												return;
											}
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}
					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Recompra";
						} else
							$scope.emptyDatos = true;

					}

					function serviceError(response) {
						toastr.error(response.data.sourceMessage,
								response.data.errMessage, opts);
					}
					var opts = {
						"closeButton" : true,
						"debug" : false,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "300",
						"hideDuration" : "1000",
						"timeOut" : "5000",
						"extendedTimeOut" : "1000",
						"showEasing" : "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
					};

					loadOrgTree();

				})
		.controller(
				'ResumenController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q) {
					$scope.movimientosCaja = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.emptyForm = true;

					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					$scope.reporteResumen = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteResumen',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}

						}

					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteResumenExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}

					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Resumen";
						} else
							alert("No se encontraron datos");

					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0) {
							$scope.emptyForm = false;
						} else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}

					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/indicadores/resumen/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}
					function lcoationLoadCompleted(result) {
						$scope.itemsResumen = result.data;
						/*
						 * Activar para manejo de filtros var table =
						 * $("#resumen").dataTable().yadcf([ {column_number :
						 * 0}, {column_number : 1, filter_type: 'text'},
						 * {column_number : 2, filter_type: 'text'},
						 * {column_number : 3, filter_type: 'range_number'},
						 * {column_number : 4, filter_type: 'range_number'},
						 * {column_number : 5, filter_type: 'range_number'},
						 * {column_number : 6, filter_type: 'range_number'}, ]);
						 * $("#resumen").DataTable( { dom: 'Bfrtip', buttons: [
						 * 'copy', 'csv', 'excel', 'pdf', 'print' ] } );
						 */

					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal
					};

					loadOrgTree();

				})
		.controller(
				'VisitasController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $q) {
					$scope.Visitas = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.emptyForm = true;
					$scope.emptyDatos = false;
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					$scope.reporteVisitas = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteVisitas',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download();
							}
						}
					}

					function download() {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						$http
								.post('report/reporteVisitasExcel',
										$scope.filters, {
											responseType : "arraybuffer"
										})
								.then(
										function(data) {
											if (data.data.byteLength == 0) {
												$scope.emptyDatos = true;
												return;
											}

											var type = data
													.headers('Content-Type');
											var disposition = data
													.headers('Content-Disposition');
											if (disposition) {
												var match = disposition
														.match(/.*filename=\"?([^;\"]+)\"?.*/);
												if (match[1])
													defaultFileName = match[1];
											}
											defaultFileName = defaultFileName
													.replace(/[<>:"\/\\|?*]+/g,
															'_');
											var blob = new Blob([ data.data ],
													{
														type : type
													});
											saveAs(blob, defaultFileName);
											deferred.resolve(defaultFileName);
										}, function error(data) {
											console.log(data.data);
										});
						return deferred.promise;
					}

					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Visitas";
						} else
							$scope.emptyDatos = true;
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0)
							$scope.emptyForm = false;
						else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}
					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/visitas/datos/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}
					}

					$scope.reload = function() {
						loadData();
					}
					function lcoationLoadCompleted(result) {
						$scope.Visitas = result.data;
					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal
					};

					loadOrgTree();

				})
		.controller(
				'EstadoCarteraController',
				function($scope, $http, $rootScope, apiService, $stateParams,
						$state, $timeout, $sce, $q) {
					$scope.movimientosCaja = [];
					// Estos catálogos se deben llenar aplicando los filtros del
					// usuario, una sola vez
					$scope.divisiones = [];
					$scope.regiones = [];
					$scope.gerencias = [];
					$scope.rutas = [];
					$scope.cliente = {};
					$scope.fechaActual = new Date();
					$scope.emptyForm = true;
					function formatDate(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						return [ year, month, day ].join('-');
					}

					function addDays(date, days) {
						var result = new Date(date);
						result.setDate(result.getDate() + days);
						return result;
					}

					function prepareListItems(control, placeHolderVal) {
						$(control).select2({
							placeholder : placeHolderVal,
							allowClear : true
						}).on(
								'select2-open',
								function() {
									// Adding Custom Scrollbar
									$(this).data('select2').results.addClass(
											'overflow-hidden')
											.perfectScrollbar();
								}).on('change', function(e) {
							var source = $(this).context.id;
							loadFilteredTree(e.val, source)
							if (source == 'ruta') {
								$scope.filters.rutas = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.rutas.push({
										id : e.val[i],
										name : "no importa"
									})
								}
							} else if (source == 'gerencia') {
								$scope.filters.gerencias = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.gerencias.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'region') {
								$scope.filters.regiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.regiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							} else if (source == 'division') {
								$scope.filters.divisiones = [];
								for (var i = 0; i < e.val.length; i++) {
									$scope.filters.divisiones.push({
										id : e.val[i],
										name : "no importa"
									})
								}
								loadFilteredTree(e.val, source)
							}

						});
					}

					jQuery(document).ready(function($) {
						prepareListItems("#division", 'División');
						prepareListItems("#region", 'Región');
						prepareListItems("#gerencia", 'Gerencia');
						prepareListItems("#ruta", 'Ruta');
					});

					function addSelectItems(selectControl, listaItems) {
						$(selectControl).empty();
						$.each(listaItems, function(key, value) {
							$(selectControl).append(
									$("<option></option>").val(value.id).text(
											value.name));
						});

					}
					function loadOrgTree() {
						$scope.orgId = $rootScope.repository.loggedUser.idTitOrg == null ? "null"
								: $rootScope.repository.loggedUser.idTitOrg;
						// Preguntamos si es suplente
						if ($scope.orgId == null || $scope.orgId == "null") {
							$scope.orgId = $rootScope.repository.loggedUser.idsupOrg == null ? "null"
									: $rootScope.repository.loggedUser.idsupOrg;
						}
						apiService.post('/api/org/filteredTree', $scope.orgId,
								function(result) {
									$scope.divisiones = result.data.divisiones;
									$scope.regiones = result.data.regiones;
									$scope.gerencias = result.data.gerencias;
									$scope.rutas = result.data.rutas;
									addSelectItems("#division",
											$scope.divisiones);
									addSelectItems("#region", $scope.regiones);
									addSelectItems("#gerencia",
											$scope.gerencias);
									addSelectItems("#ruta", $scope.rutas);
								}, locationLoadFailed);

					}

					function loadFilteredTree(filters, nivel) {
						var filtros = {};
						if (nivel == 'division') {
							filtros.tipoNodo = 'D';
						}
						if (nivel == 'region') {
							filtros.tipoNodo = 'R';
						}
						if (nivel == 'gerencia') {
							filtros.tipoNodo = 'G';
						}
						if (nivel == 'ruta') {
							filtros.tipoNodo = 'RR';
						}
						filtros.filtros = filters;
						apiService.post('/api/org/limitedTree', filtros,
								function(result) {
									if (nivel == 'division') {
										addSelectItems("#region",
												result.data.regiones);
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'region') {
										addSelectItems("#gerencia",
												result.data.gerencias);
										addSelectItems("#ruta",
												result.data.rutas);
									}
									if (nivel == 'gerencia') {
										addSelectItems("#ruta",
												result.data.rutas);
									}
								}, locationLoadFailed);
					}

					function loadData() {
						if (!isEmptyForm()) {
							apiService.post('api/indicadores/cartera/',
									$scope.filters, lcoationLoadCompleted,
									locationLoadFailed);
						}

					}

					$scope.reloadx = function() {
						loadData();
					}
					$scope.generaReporteCartera = function(tipo) {
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteCartera',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download(1);
							}
						}
					}

					$scope.generaReporteDetalleCartera = function(tipo) {
						$scope.fechaActual = new Date();
						if (!isEmptyForm()) {
							if (tipo == 'html') {
								apiService.post('report/reporteDetalleCartera',
										$scope.filters, lcoationLoadCompleted2,
										locationLoadFailed);
							} else if (tipo == 'excel') {
								download(2);
							}

						}
					}

					function isEmptyForm() {
						if ($scope.filters.divisiones.length != 0
								|| $scope.filters.gerencias.length != 0
								|| $scope.filters.regiones.length != 0
								|| $scope.filters.rutas.length != 0) {
							$scope.emptyForm = false;
						} else
							$scope.emptyForm = true;
						return $scope.emptyForm;
					}

					function download(tipo) {
						var defaultFileName = 'Report.xls';
						var self = this;
						var deferred = $q.defer();
						if (tipo == 1) {
							$http
									.post('report/reporteCarteraExcel',
											$scope.filters, {
												responseType : "arraybuffer"
											})
									.then(
											function(data) {
												var type = data
														.headers('Content-Type');
												var disposition = data
														.headers('Content-Disposition');
												if (disposition) {
													var match = disposition
															.match(/.*filename=\"?([^;\"]+)\"?.*/);
													if (match[1])
														defaultFileName = match[1];
												}
												defaultFileName = defaultFileName
														.replace(
																/[<>:"\/\\|?*]+/g,
																'_');
												var blob = new Blob(
														[ data.data ], {
															type : type
														});
												saveAs(blob, defaultFileName);
												deferred
														.resolve(defaultFileName);
											}, function error(data) {
												console.log(data.data);
											});

						} else
							(tipo == 2)
						{
							$http
									.post('report/reporteDetalleCarteraExcel',
											$scope.filters, {
												responseType : "arraybuffer"
											})
									.then(
											function(data) {
												var type = data
														.headers('Content-Type');
												var disposition = data
														.headers('Content-Disposition');
												if (disposition) {
													var match = disposition
															.match(/.*filename=\"?([^;\"]+)\"?.*/);
													if (match[1])
														defaultFileName = match[1];
												}
												defaultFileName = defaultFileName
														.replace(
																/[<>:"\/\\|?*]+/g,
																'_');
												var blob = new Blob(
														[ data.data ], {
															type : type
														});
												saveAs(blob, defaultFileName);
												deferred
														.resolve(defaultFileName);
											}, function error(data) {
												console.log(data.data);
											});

						}
						return deferred.promise;
					}

					function lcoationLoadCompleted2(result) {
						if (result.data != "") {
							var myWindow = window.open("", "", "");
							myWindow.document.write(result.data);
							myWindow.document.title = "Reporte de Cartera";
						} else
							alert("No se encontraron datos");

					}
					function lcoationLoadCompleted(result) {
						$scope.itemsResumen = result.data;
						/*
						 * Activar para manejo de filtros var table =
						 * $("#resumen").dataTable().yadcf([ {column_number :
						 * 0}, {column_number : 1, filter_type: 'text'},
						 * {column_number : 2, filter_type: 'text'},
						 * {column_number : 3, filter_type: 'range_number'},
						 * {column_number : 4, filter_type: 'range_number'},
						 * {column_number : 5, filter_type: 'range_number'},
						 * {column_number : 6, filter_type: 'range_number'}, ]);
						 * $("#resumen").DataTable( { dom: 'Bfrtip', buttons: [
						 * 'copy', 'csv', 'excel', 'pdf', 'print' ] } );
						 */

					}

					function locationLoadFailed(response) {
						alert(response);
					}

					$scope.fechaInicial = formatDate(addDays(Date(), -1));
					$scope.fechaFinal = formatDate(addDays(Date(), 1));
					$scope.filters = {
						divisiones : [],
						regiones : [],
						gerencias : [],
						rutas : [],
						fechaInicial : $scope.fechaInicial,
						fechaFinal : $scope.fechaFinal
					};

					loadOrgTree();

					$scope.showDetail = function() {
						apiService
								.post('api/indicadores/carteraDetalle/',
										$scope.filters, showDetalle,
										locationLoadFailed);
						$scope.showDetalle = true;

					}
					function showDetalle(result) {
						$scope.creditosVO = result.data;
					}
				});
