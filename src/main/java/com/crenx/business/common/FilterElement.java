package com.crenx.business.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Especifica el metadato del nombre de la propiedad de un documento
 * @author JCHR
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FilterElement {

	/**
	 * @return
	 */
	public String name();
}
