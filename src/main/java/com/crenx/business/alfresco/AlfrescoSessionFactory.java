package com.crenx.business.alfresco;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException;
import org.springframework.util.StringUtils;

/**
 * Fabrica de Sesiones de Alfresco
 * 
 * @author JCHR
 *
 */
public class AlfrescoSessionFactory {

	private static Map<String, Session> sessions = new HashMap<String, Session>();
	// alfresco/api/-default-/public/cmis/versions/1.1/atom
	private static final String CONTEXT_URL_ATOM = "/alfresco/api/-default-/cmis/versions/1.1/atom";
	private static final String CONTEXT_ACL_SERVICE = "/alfresco/cmisws/ACLService?wsdl";
	private static final String CONTEXT_DISCOVERY_SERVICE = "/alfresco/cmisws/DiscoveryService?wsdl";
	private static final String CONTEXT_MULTIFILING_SERVICE = "/alfresco/cmisws/MultiFilingService?wsdl";
	private static final String CONTEXT_NAVIGATION_SERVICE = "/alfresco/cmisws/NavigationService?wsdl";
	private static final String CONTEXT_OBJECT_SERVICE = "/alfresco/cmisws/ObjectService?wsdl";
	private static final String CONTEXT_POLICY_SERVICE = "/alfresco/cmisws/PolicyService?wsdl";
	private static final String CONTEXT_RELATIONSHIP_SERVICE = "/alfresco/cmisws/RelationshipService?wsdl";
	private static final String CONTEXT_REPOSITORY_SERVICE = "/alfresco/cmisws/RepositoryService?wsdl";
	private static final String CONTEXT_VERSIONING_SERVICE = "/alfresco/cmisws/VersioningService?wsdl";

	/**
	 * Contructor que no permite la instanciaciar
	 */
	private AlfrescoSessionFactory() {
		//empty
	}

	/**
	 * Crea una session tipo ATOM
	 * 
	 * @param repositoryId
	 * @param context
	 * @return
	 * @throws CmisConnectionException
	 */
	public static Session createSessionAtomPub(final String repositoryId, final AlfrescoConnectContext context)
			throws CmisConnectionException {
		String url = null;
		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(SessionParameter.USER, context.getUsername());
		parameters.put(SessionParameter.PASSWORD, context.getPwd());
		try {
			url = buildURL(context, CONTEXT_URL_ATOM);
		} catch (MalformedURLException exception) {
			throw new CmisConnectionException("Could not connect to the Alfresco Server", exception);
		}
		parameters.put(SessionParameter.ATOMPUB_URL, url);
		parameters.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		parameters.put(SessionParameter.COMPRESSION, "true");
		parameters.put(SessionParameter.CACHE_TTL_OBJECTS, "0");
		return createSession(parameters, repositoryId);
	}

	/**
	 * Crea una session tipo SOAP
	 * 
	 * @param repositoryId
	 * @param context
	 * @return
	 * @throws CmisConnectionException
	 */
	public static Session createSessionSoap(final String repositoryId, final AlfrescoConnectContext context)
			throws CmisConnectionException {
		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(SessionParameter.USER, context.getUsername());
		parameters.put(SessionParameter.PASSWORD, context.getPwd());
		parameters.put(SessionParameter.BINDING_TYPE, BindingType.WEBSERVICES.value());

		try {
			parameters.put(SessionParameter.WEBSERVICES_ACL_SERVICE, buildURL(context, CONTEXT_ACL_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_DISCOVERY_SERVICE,
					buildURL(context, CONTEXT_DISCOVERY_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_MULTIFILING_SERVICE,
					buildURL(context, CONTEXT_MULTIFILING_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_NAVIGATION_SERVICE,
					buildURL(context, CONTEXT_NAVIGATION_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_OBJECT_SERVICE, buildURL(context, CONTEXT_OBJECT_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_POLICY_SERVICE, buildURL(context, CONTEXT_POLICY_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_RELATIONSHIP_SERVICE,
					buildURL(context, CONTEXT_RELATIONSHIP_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_REPOSITORY_SERVICE,
					buildURL(context, CONTEXT_REPOSITORY_SERVICE));
			parameters.put(SessionParameter.WEBSERVICES_VERSIONING_SERVICE,
					buildURL(context, CONTEXT_VERSIONING_SERVICE));
		} catch (MalformedURLException exception) {
			throw new CmisConnectionException("Could not connect to the Alfresco Server", exception);
		}

		return createSession(parameters, repositoryId);
	}

	/**
	 * Crea un Session con los par�metros especificados. Si el nombre de
	 * repositorio es null o vacio genera una conexi�n a Root.
	 * 
	 * @param parameters
	 * @param repositoryName
	 * @return
	 * @throws CmisConnectionException
	 */
	private static Session createSession(final Map<String, String> parameters, final String repositoryId)
			throws CmisConnectionException {
		final SessionFactory sessionFactory = SessionFactoryImpl.newInstance();		 
		Session session = null;
		if (StringUtils.hasText(repositoryId)) {
			parameters.put(SessionParameter.REPOSITORY_ID, repositoryId);
			if (sessions.get(repositoryId) == null) {
				session = sessionFactory.createSession(parameters);
				sessions.put(repositoryId, session);
			} else {
				session = sessions.get(repositoryId);
			}
		}else{
			session = sessionFactory.createSession(parameters);
		}
		return session;
	}

	/**
	 * Crea una representaci�n de URL a partir de los datos del contexto y la urlContexto
	 * 
	 * @param context
	 * @param urlContext es el target resource
	 * @return
	 * @throws MalformedURLException
	 */
	private static String buildURL(final AlfrescoConnectContext context, final String urlContext)
			throws MalformedURLException {
		return new URL("http", context.getHost(), context.getPort(), CONTEXT_URL_ATOM).toExternalForm();
	}
}
