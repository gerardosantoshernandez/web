package com.crenx.business.alfresco;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisUnauthorizedException;

/**
 * Clase de utilerias para el manejo de CMIS en Alfresco
 * 
 * @author JCHR
 *
 */
class AlfrescoUtils {

	/**
	 * Obtiene un CMIS Object por el nombre desde un Folder específico
	 * 
	 * @param session
	 *            Session CMIS
	 * @param parentFolder
	 *            Folder donde se va a realizar la búsqueda
	 * @param objectName.
	 *            Si el objeto no se encuentra null es el valor a regresar
	 * @return
	 */
	public static CmisObject getObjectByPath(final Session session, final String path) {
		CmisObject object = null;
		try {
			object = session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException nfe0) {
			// Nothing to do, object does not exist
		}
		return object;
	}

	/**
	 * Obtiene un CMIS Object por el nombre desde un Folder específico
	 * 
	 * @param session
	 *            Session CMIS
	 * @param parentFolder
	 *            Folder donde se va a realizar la búsqueda
	 * @param objectName.
	 *            Si el objeto no se encuentra null es el valor a regresar
	 * @return
	 */
	public static CmisObject getObject(final Session session, final Folder parentFolder, final String objectName) {
		CmisObject object = null;
		try {
			String path = parentFolder.getPath();
			if (!path.endsWith("/")) {
				path += "/";
			}
			path += objectName;
			object = session.getObjectByPath(path);
		} catch (CmisObjectNotFoundException nfe0) {
			// Nothing to do, object does not exist
		}
		return object;
	}

	/**
	 * Obtiene la ruta de un Documento
	 * 
	 * @param document
	 *            Documento a consultar
	 * @return
	 */
	public static String getDocumentPath(final Document document) {
		String path = getParentFolderPath(document);
		if (!path.endsWith("/")) {
			path += "/";
		}
		path += document.getName();
		return path;
	}

	/**
	 * Obtiene la ruta del Folder que contiene el Documento
	 *
	 * @param document
	 *            Documento a consultar
	 * @return Retorna la ruta. Si el Documetno no esta contenido entonces
	 *         regresa null
	 */
	public static String getParentFolderPath(final Document document) {
		Folder parentFolder = getDocumentFirstParentFolder(document);
		return parentFolder.getPath();
	}

	/**
	 * Obtiene el primer Folder donde se encuentra contenido el Documento
	 *
	 * @param document
	 *            Documento a consultar
	 * @return p
	 */
	public static Folder getDocumentFirstParentFolder(final Document document) {
		// Get all the parent folders (could be more than one if multi-filed)
		final List<Folder> parentFolders = document.getParents();
		Folder result = null;
		// Checar si el Documetno esta contenido en un Folder
		if (parentFolders.size() > 0) {
			result = parentFolders.get(0);
		}
		return result;
	}

	/**
	 * Crea un folder a partir de un Folder padre. Si parent es null, el folder
	 * padre por default sera el Folder root del Repositorio
	 * 
	 * @param session
	 * @param parent
	 * @param folderName
	 * @return
	 */
	public static Folder createFolder(final Session session, final Folder parent, final String folderName) {
		final Folder parentFolder = (parent == null) ? session.getRootFolder() : parent;
		// Revisa permisos de creación
		if (parentFolder.getAllowableActions().getAllowableActions().contains(Action.CAN_CREATE_FOLDER) == false) {
			throw new CmisUnauthorizedException(
					"Current user does not have permission to create a sub-folder in " + parentFolder.getPath());
		}
		// Revisa si el folder ya existe
		Folder newFolder = (Folder) getObject(session, parentFolder, folderName);
		if (newFolder == null) {
			Map<String, Object> newFolderProps = new HashMap<String, Object>();
			newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			newFolderProps.put(PropertyIds.NAME, folderName);
			newFolder = parentFolder.createFolder(newFolderProps);
		}
		return newFolder;
	}

}
