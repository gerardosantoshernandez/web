package com.crenx.br;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.crenx.business.common.ActorCredito;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.Perfil;
import com.crenx.data.domain.entity.ReglaCredito;
import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.PropiedadesDocRepository;
import com.crenx.data.domain.repository.ReglaCreditoRepository;
import com.crenx.data.domain.repository.TablaAmortizacionRepository;
import com.crenx.data.domain.vo.AmortizacionVO;
import com.crenx.data.domain.vo.CreditoBRVO;
import com.crenx.data.domain.vo.DocumentoVO;
import com.crenx.data.domain.vo.FiltroDocumentosVO;
import com.crenx.data.domain.vo.TablaAmortizacionVO;
import com.crenx.services.CatalogoServices;
import com.crenx.services.CreditoServices;
import com.crenx.services.DocumentoServices;
import com.crenx.services.EmpleadoServices;
import com.crenx.services.SeguridadServices;
import com.google.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@Service
public class CreditoBRImpl {

	@Autowired
	private CreditoRepository creditoRepository;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private CreditoServices credServices;
	@Autowired
	private TablaAmortizacionRepository taRepository;
    @Autowired
    private ReglaCreditoRepository regCredRepository;
	@Autowired
	PagoRepository pagoRepository;
	@Autowired
	PropiedadesDocRepository docPropRepository;
	@Autowired
	private SeguridadServices segServices;
	@Autowired
	private EmpleadoServices empServices;
	@Autowired
	private DocumentoServices doctoServices;
	@Autowired
	private Environment env;

	/*
	 * Verificar niveles de autorización
	 */
	public CreditoBRVO estatusNuevoCredito(Credito credito, Usuario usuario, double monto, String idCodeudor)
	{	
		CreditoBRVO result = new CreditoBRVO();
		if (credito.getIdCredito()!=null)
		{
			if (pagoRepository.findByCreditoIdCredito(credito.getIdCredito()).size()>0)
			{
				Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
				result.setEstatusCredito(status);
				result.setMotivoRechazo("No puede modificar el crédito cuando ya tiene pagos aplicados");
				result.setCrearCredito(false);
				return result;				
			}else
				result.setCorreccion(true);				
		}
		
		//Regla de documentos
		if (! validaDocumentos(credito, result))
		{			
			Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			result.setEstatusCredito(status);			
			result.setCrearCredito(false);
			return result;			
		}
		
		int activos = (int)catServices.getParametroByName("CREDITOS_ACTIVOS").getValorNum();
		int creditosActivos = creditosActivos(credito.getCliente().getIdCliente(), credito.getIdCredito());		
		int creditosPendAut = creditosPorEstatus(credito.getCliente().getIdCliente(), credito.getIdCredito(), "CRED_PENDIENTE");
		int creditosPendEnt = creditosPorEstatus(credito.getCliente().getIdCliente(), credito.getIdCredito(), "CRED_PENDIENTE_ENT");
		Catalogo statusActivo = catServices.getCatalogoCveInterna("CRED_PAGADO");

		List<Credito> pagados = creditoRepository.findByClienteIdClienteAndEstatusCreditoIdCatalogoOrderByFechaEmisionDesc(credito.getCliente().getIdCliente(), statusActivo.getIdCatalogo());
		boolean autorizarPrimerCredito = (pagados.isEmpty());
		if (creditosActivos>=activos)
		{
			Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			result.setEstatusCredito(status);
			result.setMotivoRechazo(String.format("No puede tener mas de %d créditos activos", activos));
			result.setCreditosActivos(creditosActivos);
			result.setCrearCredito(false);
			return result;
		}
		if ( (creditosPendAut+creditosPendEnt)>0 )
		{
			Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			result.setEstatusCredito(status);
			result.setMotivoRechazo(String.format("Ya tiene %d créditos pendientes de autorizar o entregar", activos));
			result.setCreditosActivos(creditosActivos);
			result.setCrearCredito(false);
			return result;			
		}
		String reglaRecompra = reglaRecompra(credito);
		if (reglaRecompra!=null)
		{
			Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			result.setEstatusCredito(status);
			result.setMotivoRechazo(String.format("No cumple con la política de recompra 81% fecha último vencimiento %s ", reglaRecompra));
			result.setCreditosActivos(creditosActivos);
			result.setCrearCredito(false);
			return result;
			
		}
		boolean requiereAval = true;
		double maximo = catServices.getParametroByName("TOPE_AVAL").getValorNum();
		if (monto>maximo)
		{
			requiereAval = true;
			result.setMotivoRechazo("Requiere Co-Deudor");
			if (idCodeudor==null)
			{
				Catalogo status = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
				result.setEstatusCredito(status);
				result.setCrearCredito(false);
				return result;				
			}				
		}
		//FIXME: Regresar a PErfil 
		double maximoMonto=montoAutorizadoCredito(usuario.getNombreUsuario());
		String cveEstatus = "CRED_ACTIVO";
		//FIXME: Implementar esta validación
		autorizarPrimerCredito = false;
		if (monto>maximoMonto || autorizarPrimerCredito)
		{
			cveEstatus = "CRED_PENDIENTE";
			result.setMotivoRechazo("Requiere Autorización");
		}
		Catalogo status = catServices.getCatalogoCveInterna(cveEstatus);
		result.setRequiereAval(requiereAval);
		result.setEstatusCredito(status);
		result.setCrearCredito(true);
		return result;
	}

	public int creditosPorEstatus(String idCliente, String idCredito, String estatus)
	{
		String idEstaus = catServices.getCatalogoCveInterna(estatus).getIdCatalogo();
		Collection<Credito> creds = Lists.newArrayList(creditoRepository.findByClienteIdClienteAndEstatusCreditoIdCatalogoOrderByFechaEmisionDesc(idCliente,idEstaus));
		int total = creds.size();
		for (Credito theCredit: creds)
		{
			if (theCredit.getIdCredito().equals(idCredito))
			{
				total--;
			}
		}
		return total;
		
	}
	public int creditosActivos(String idCliente, String idCredito)
	{
		return creditosPorEstatus(idCliente, idCredito, "CRED_ACTIVO");
	}

	private double redondea(double valor, int precision) {
		double powerUp = Math.pow(10, precision);
		double tempVal = valor * powerUp;
		tempVal = Math.round(tempVal);
		tempVal = tempVal / powerUp;
		return tempVal;
	}

	public AmortizacionVO calculaTablaAmortizacion(double iva, double pagoMillar, double capital, int duracion,
			double tasaNominal, double comision) {
		tasaNominal = tasaNominal / 100;
		comision = comision / 100;

		AmortizacionVO result = new AmortizacionVO();
		Collection<TablaAmortizacionVO> tabla = new ArrayList<TablaAmortizacionVO>();

		double importeComision = redondea((capital * comision), 0);
		double ivaComision = redondea((importeComision * iva), 0);
		double totalComision = redondea((importeComision + ivaComision), 0);
		result.setComision(importeComision);
		result.setIvaComision(ivaComision);
		result.setTotalComision(totalComision);

		double pagoFijoPactado = pagoMillar * (capital / 1000);
		double saldo = capital;
		double diferenciaRedondeo = 0;
		double aportacionCapital = 0;
		double totalInterees = 0;
		double totalIva = 0;

		double pagoFijoCalculado = capital
				* ((tasaNominal * Math.pow(1 + tasaNominal, duracion)) / (Math.pow(1 + tasaNominal, duracion) - 1));
		double tasaEfectivaDiaria = tasaNominal;
		double pagoSinAjustar = pagoFijoCalculado;
		pagoFijoCalculado = redondea(pagoFijoCalculado, 0);

		result.setPagoFijo(pagoFijoCalculado);

		for (int i = 1; i <= duracion; i++) {
			TablaAmortizacionVO oneItem = new TablaAmortizacionVO();
			oneItem.setPagoFijo(pagoFijoCalculado);
			oneItem.setInteres(redondea((saldo * tasaEfectivaDiaria) / (1 + iva), 2));
			oneItem.setIvaInteres(redondea(oneItem.getInteres() - (oneItem.getInteres() / (1 + iva)), 2));
			if (i == duracion) {
				pagoSinAjustar = saldo;
				oneItem.setAmortizacion(saldo);
			} else
				oneItem.setAmortizacion(redondea((pagoSinAjustar - oneItem.getInteres() - oneItem.getIvaInteres()), 2));

			diferenciaRedondeo += (pagoFijoCalculado - pagoSinAjustar);
			saldo -= oneItem.getAmortizacion();
			saldo = redondea(saldo, 2);
			oneItem.setSaldo(saldo);
			tabla.add(oneItem);
			totalInterees += oneItem.getInteres();
			aportacionCapital += oneItem.getAmortizacion();
			totalIva += oneItem.getIvaInteres();
		}
		result.setTablaAmoritzacion(tabla);
		return result;
	}

	public AmortizacionVO calculaTablaAmortizacion(double iva, double pagoMillar, double capital, int duracion,
			double tasaNominal, double comision, boolean interesFijo) {
		tasaNominal = tasaNominal / 100;
		comision = comision / 100;

		AmortizacionVO result = new AmortizacionVO();
		Collection<TablaAmortizacionVO> tabla = new ArrayList<TablaAmortizacionVO>();

		double importeComision = redondea((capital * comision), 2);
		double ivaComision = redondea((importeComision * iva), 2);
		double totalComision = redondea((importeComision + ivaComision), 2);
		result.setComision(importeComision);
		result.setIvaComision(ivaComision);
		result.setTotalComision(totalComision);

		double pagoFijoPactado = pagoMillar * (capital / 1000);
		//FIXME: Solo para Migracioón
		pagoFijoPactado= pagoMillar;
		double saldo = capital;
		double diferenciaRedondeo = 0;
		double aportacionCapital = 0;
		double totalInterees = 0;
		double totalIva = 0;

		double pagoCapital = capital / duracion;
		double interes = (capital * tasaNominal) / duracion;
		double ivaInteres =interes - (interes/(1 + iva));
		interes = interes- ivaInteres;
		pagoCapital = redondea(pagoCapital,2);
		interes = redondea(interes, 2);
		ivaInteres = redondea(ivaInteres, 2);
		double pagoFijoCalculado = pagoCapital + interes + ivaInteres;
		
		//FIXME: Solo para migración
		pagoFijoCalculado = pagoFijoPactado;
		pagoCapital = pagoFijoPactado - (interes + ivaInteres);
		double pagoSinAjustar = pagoFijoCalculado;
		pagoFijoCalculado = redondea(pagoFijoCalculado, 0);

		result.setPagoFijo(pagoFijoCalculado);

		for (int i = 1; i <= duracion; i++) {
			TablaAmortizacionVO oneItem = new TablaAmortizacionVO();
			oneItem.setPagoFijo(pagoFijoCalculado);
			oneItem.setInteres(interes);
			oneItem.setIvaInteres(ivaInteres);
			if (i == duracion) {
				pagoSinAjustar = saldo;
				oneItem.setAmortizacion(saldo);
			} else
				oneItem.setAmortizacion(redondea((pagoSinAjustar - oneItem.getInteres() - oneItem.getIvaInteres()), 2));

			diferenciaRedondeo += (pagoFijoCalculado - pagoSinAjustar);
			saldo -= oneItem.getAmortizacion();
			saldo = redondea(saldo, 2);
			oneItem.setSaldo(saldo);
			tabla.add(oneItem);
			totalInterees += oneItem.getInteres();
			aportacionCapital += oneItem.getAmortizacion();
			totalIva += oneItem.getIvaInteres();
		}
		result.setTablaAmoritzacion(tabla);
		return result;
	}

	// FIXME: Integrar el pago del primer abono con la comisión
	public AmortizacionVO calculaAmortizacionBAD(double iva, double pagoMillar, double capital, int duracion,
			double tasaNominal, double comision) {
		tasaNominal = tasaNominal / 100;
		comision = comision / 100;
		AmortizacionVO amortizacion = new AmortizacionVO();
		Collection<TablaAmortizacionVO> tabla = new ArrayList<TablaAmortizacionVO>();
		double pagoFijoPactado = pagoMillar * (capital / 1000);
		double saldo = capital;
		double diferenciaRedondeo = 0;
		double aportacionCapital = 0;
		double totalInterees = 0;
		double totalIva = 0;

		double montoComision = redondea((capital * comision), 0);
		double ivaComision = redondea((montoComision * iva), 0);
		double totalComision = montoComision + ivaComision;

		amortizacion.setMontoComision(montoComision);
		amortizacion.setIvaComision(ivaComision);
		amortizacion.setTotalComision(totalComision);
		TablaAmortizacionVO comisionItem = new TablaAmortizacionVO();

		comisionItem.setNumPago(0);
		comisionItem.setAmortizacion(montoComision);
		comisionItem.setInteres(0);
		comisionItem.setIvaInteres(ivaComision);
		comisionItem.setPagoFijo(totalComision);
		tabla.add(comisionItem);

		double pagoFijoCalculado = capital
				* ((tasaNominal * Math.pow(1 + tasaNominal, duracion)) / (Math.pow(1 + tasaNominal, duracion) - 1));
		double tasaEfectivaDiaria = tasaNominal;
		double pagoSinAjustar = pagoFijoCalculado;
		pagoFijoCalculado = redondea(pagoFijoCalculado, 0);

		for (int i = 1; i <= duracion; i++) {
			TablaAmortizacionVO oneItem = new TablaAmortizacionVO();
			oneItem.setPagoFijo(pagoFijoPactado);
			oneItem.setInteres(redondea((saldo * tasaEfectivaDiaria) / (1 + iva), 2));
			oneItem.setIvaInteres(redondea(oneItem.getInteres() - (oneItem.getInteres() / (1 + iva)), 2));
			if (i == duracion) {
				pagoSinAjustar = saldo;
				oneItem.setAmortizacion(saldo);
			} else
				oneItem.setAmortizacion(redondea((pagoSinAjustar - oneItem.getInteres() - oneItem.getIvaInteres()), 2));

			diferenciaRedondeo += (pagoFijoCalculado - pagoSinAjustar);
			saldo -= oneItem.getAmortizacion();
			saldo = redondea(saldo, 2);
			oneItem.setSaldo(saldo);
			tabla.add(oneItem);
			totalInterees += oneItem.getInteres();
			aportacionCapital += oneItem.getAmortizacion();
			totalIva += oneItem.getIvaInteres();
		}
		amortizacion.setTablaAmoritzacion(tabla);
		return amortizacion;
	}
	

	/*
	 * La regla utiliza el último crédito pagado
	 * No debe haber créditos activos
	 */
	public String reglaRecompra(Credito credito)
	{
		String retVal = null;
		List<Credito> creditos = credServices.creditosPorEstatus(credito.getCliente().getIdCliente(), "CRED_PAGADO");
		if (creditos.size()==0)
			return retVal;
		Credito ultCredito = creditos.get(0);
		double plazoRegla = ultCredito.getPlazo() * 0.81;
		int plazoRound = (int)plazoRegla;
		List<TablaAmortizacion> tabla = taRepository.findByCreditoIdCreditoAndNoPago(ultCredito.getIdCredito(), plazoRound);
		TablaAmortizacion tablaItem = tabla.get(0);
		if (credito.getFechaEmision().before(tablaItem.getFechaVencimiento()))
		{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			retVal =  df.format(tablaItem.getFechaVencimiento());
		}
		return retVal;
	}

	
	/**
	 * Valida los documentos necesarios de un crédito
	 * 
	 * @param credito
	 * @param brvo
	 * @return
	 */
	public boolean validaDocumentos(final Credito credito, final CreditoBRVO brvo) {
		boolean valid = true;
		final Cliente cliente = credito.getCliente();
		final Cliente codeudor = credito.getCoDeudor();
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		filtro.setIdCliente(cliente.getIdCliente());
		// Se obtienen los documentos pertenecientes al cliente
		final List<DocumentoVO> doctosCliente = doctoServices.obtenerDetalleDocumentos(filtro);
		// Se obtienen los documentos requeridos del cliente
		try {
			validarDoctosRequeridos(ActorCredito.CLIENTE, doctosCliente);
			// revisa los documentos del codeudor
			if (codeudor != null) {
				filtro.setIdCliente(codeudor.getIdCliente());
				final List<DocumentoVO> doctosCodeudor = doctoServices.obtenerDetalleDocumentos(filtro);
				validarDoctosRequeridos(ActorCredito.CODEUDOR, doctosCodeudor);
			}
		} catch (NoSuchElementException exception) {
			brvo.setMotivoRechazo(exception.getMessage());
			valid = false;
		}
		return valid;
	}
		
	/**
	 * Obtiene los documentos que son requeridos y que no se encuentran en
	 * doctos
	 * 
	 * @param actor
	 * @param doctos
	 * @return
	 */
	public void validarDoctosRequeridos(final ActorCredito actor, final List<DocumentoVO> doctos)
			throws NoSuchElementException {
		List<String> faltantes = new ArrayList<String>();
		final String configDoctosReq = env.getProperty("documentos.requeridos." + actor.name().toLowerCase());
		if (StringUtils.isNotBlank(configDoctosReq)) {
			final String[] doctosReq = configDoctosReq.split(",");
			faltantes = Lists.newArrayList(doctosReq);			
			for (DocumentoVO docto : doctos) {
				faltantes.remove(docto.getTipoDocumento());
			}
		}
		if (CollectionUtils.isNotEmpty(faltantes)) {
			final String error = String.format(
					"El %s no cuenta con su expediente completo, faltan los siguientes docs: %s",
					actor.name().toLowerCase(), StringUtils.join(faltantes, ","));
			throw new NoSuchElementException(error);
		}
	}
	
	public double montoAutorizadoCredito(String userName)
	{
		Usuario usuario = segServices.getPrincipal(userName);
		Perfil perfil = null;
		try {
			perfil = empServices.loadPerfil(usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		List<String> roles = perfil.getRoles();
		List<ReglaCredito> topes = regCredRepository.findByRolLlaveNegocioIn(roles);
		double maximoMonto=0;
		for (ReglaCredito oneRule : topes)
		{
			double maxItem = oneRule.getMaximo();
			maximoMonto = maximoMonto>maxItem?maximoMonto:maxItem;			
		}
		return maximoMonto;
	}
	
}
