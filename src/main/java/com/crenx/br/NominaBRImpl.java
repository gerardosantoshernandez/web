package com.crenx.br;

import org.springframework.stereotype.Service;

@Service
public class NominaBRImpl {

	public static String CARGO_CYC="12";
	
	public boolean calculaPorSalario(String cargo) 
	{
		boolean returnVal = false;
		if (!cargo.equals(CARGO_CYC))
			returnVal = true;
		return returnVal;
	}
	
	public double factorCobranza(double porcentaje)
	{
		if (porcentaje>=100)
			return 1;
		if (porcentaje>=91 && porcentaje <100)
			return 0.75;
		if (porcentaje>=86 && porcentaje <91)
			return 0.60;
		if (porcentaje>=81 && porcentaje <86)
			return 0.50;
		return 0.4;
	}

	public double factorNormalidad(double porcentaje)
	{
		if (porcentaje>=96)
			return 2.5;
		if (porcentaje>=90 && porcentaje <96)
			return 2.25;
		if (porcentaje>=86 && porcentaje <90)
			return 2.0;
		if (porcentaje>=81 && porcentaje <86)
			return 1.80;
		return 1.6;
	}

	public double factorColocacion()
	{
		return 1;
	}

}
