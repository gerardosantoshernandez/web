package com.crenx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.crenx.business.FileManagerFactory;
import com.crenx.business.FileManagerFactoryImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;	

@SpringBootApplication
@PropertySource(value="classpath:rutas.properties")														  
public class CrenxApplication 
//extends SpringBootServletInitializer
{
	@Autowired
	private Environment env;
	/*
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CrenxApplication.class);
	}
	*/
	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    MappingJackson2HttpMessageConverter converter = 
	        new MappingJackson2HttpMessageConverter(mapper);
	    return converter;
	}
	
	/**
	 * Crea el Bean del File Manager con las propiedades del sistema
	 * @return
	 */
	@Bean
	public FileManagerFactory getFileManagerSystem(){
		return FileManagerFactoryImpl.createFileManagerFactory(env);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(CrenxApplication.class, args);		
	}
}
