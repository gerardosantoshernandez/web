package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Visita;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.VisitaRepository;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;
import com.crenx.data.domain.vo.VisitaToVisitaReportVOModelMapper;
import com.crenx.data.domain.vo.VisitaToVisitaVOModelMapper;
import com.crenx.data.domain.vo.VisitaVO;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class VisitasServices {

	@Autowired
	private VisitaRepository vrepo;
	@Autowired
	UtilityServices utServices;	
	@Autowired
	private OrgRepository orgRepository;
	
	public List<VisitaVO> getVisitas(FiltrosOrgVO filtros){
		List<Visita> lvisitas;
		List<VisitaVO> coleccion = null;
		
		
		Date fechaInicial = utServices.convertStringToDate(filtros.getFechaInicial()+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date fechaFinal = utServices.convertStringToDate(filtros.getFechaFinal()+" 23:59:59", "yyyy-MM-dd HH:mm:ss");

		
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		
		
		if (rutas.isEmpty())
		{
			//si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if(gerencias.isEmpty()){
				//si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if(regiones.isEmpty()){
					//si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if(divisiones.isEmpty()){
						//no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem: nodos)
						{
							rutas.add(oneItem.getIdOrg());
						}
						
					}else{
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for(String idDivision : divisiones){
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for(Organizacion region : lregiones){
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for(Organizacion gerencia : lgerencias){
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for(Organizacion ruta : lrutas){
							rutas.add(ruta.getIdOrg());
						}
					}
				}else{
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for(String idRegion : regiones){
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for(Organizacion gerencia : lgerencias){
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for(Organizacion ruta : lrutas){
						rutas.add(ruta.getIdOrg());
					}
				}
			}else{
				//recupero las rutas de las gerencias
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for(Organizacion ruta : lrutas){
					rutas.add(ruta.getIdOrg());
				}
				
			}
			
		}

		
		//findByFechaVisitaBetweenAndCreditoRutaIdOrgIn
		//lvisitas = vrepo.findByFechaVisitaBetweenAndRutaIdOrgIn(fechaInicial, fechaFinal, rutas);
		lvisitas = vrepo.findByFechaVisitaBetweenAndCreditoClienteRutaIdOrg(fechaInicial, fechaFinal, rutas);
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new VisitaToVisitaReportVOModelMapper());
		Type listType = new  TypeToken<List<VisitaVO>>(){}.getType();
		try{
			coleccion = mapper.map(lvisitas, listType);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return coleccion;
	}
	private void joinOrg(ArrayList<String> idsOrg, Collection<NameValueVO> values)
	{
		for (NameValueVO value : values)
		{
			idsOrg.add(value.getId());
		}		
	}
}
