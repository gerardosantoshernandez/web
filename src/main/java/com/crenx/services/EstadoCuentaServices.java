package com.crenx.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.TablaAmortizacionRepository;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.EstadoCuentaVO;
import com.google.common.collect.Lists;

@Service
public class EstadoCuentaServices {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private CreditoRepository creditoRepository;
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private PagoRepository pagoRepository;
	
	@Autowired
	private TablaAmortizacionRepository tablaAmortizacionRepository;
	
	@Autowired
	private MovimientoCajaRepository movimientoCajaRepository;
	
	
	public EstadoCuentaVO getEstadoCuenta(String idCredito){
		
		EstadoCuentaVO estadoCuenta = new EstadoCuentaVO();
		// Lista Creditos
		List<Credito> creditoList = new ArrayList<Credito>();
		// Lista Cliente
		List<Cliente> clienteList = new ArrayList<Cliente>();
		// Lista Tabla amortizacion
		List<TablaAmortizacion> listTabAmortizacion = new ArrayList<TablaAmortizacion>();
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		
		creditoList= Lists.newArrayList(creditoRepository.findByIdCredito(idCredito));
		String aux = creditoList.get(0).getCliente().getIdCliente();
		clienteList = clienteRepository.findByIdCliente(aux);
		// Productos
		List<Producto> listProductos= new ArrayList<Producto>();
    	for (Credito credito : creditoList) {
    		listProductos.addAll(productoRepository.findByIdProducto(credito.getProducto().getIdProducto())); 		
		}
    	// Pagos
    	List<Pago> listPagos= new ArrayList<Pago>();
    	for (Credito credito : creditoList) {
    		listPagos.addAll(pagoRepository.findByCreditoIdCredito(credito.getIdCredito()));		
		}
    	
    	listTabAmortizacion = Lists.newArrayList(tablaAmortizacionRepository.findByCreditoIdCredito(idCredito));
    	// Suma de IVA 
    	float totIvaInteres = 0;
    	for (TablaAmortizacion taIva : listTabAmortizacion) {
			if(taIva.getEstatusPago().getClaveInterna().equals("PAGO_PAGADO")){
				totIvaInteres += taIva.getIvaInteres();
			}
		}
    	// Suma de total de comisiones
    	float totalComisiones = 0;
    	for (Pago tabPagoComisiones : listPagos) {
    		totalComisiones += tabPagoComisiones.getComision(); 
		}
    	
    	// Lista Movimiento caja
    	// APLICACION DE LOS PAGOS RECIVIDOS
    	Date fechaInicio = creditoList.get(0).getFechaEmision();
    	Date fechaFin = creditoList.get(0).getFechaUltimoPago();
    	List<MovimientoCaja> listMovCaja = new ArrayList<MovimientoCaja>();
    	listMovCaja = Lists.newArrayList(movimientoCajaRepository.findByReferenciaAndFechaEmisionBetween(idCredito, fechaInicio, fechaFin));
    	
    	//Dias del periodo de Fecha Emision y Fecha Ultimo Pago 
    	Date fechaInicial = creditoList.get(0).getFechaEmision();
        Date fechaFinal = creditoList.get(0).getFechaUltimoPago();
    	DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    	String fechaInicioString = df.format(fechaInicial);
    	try {
    		fechaInicial = df.parse(fechaInicioString);
    	}catch (ParseException ex){
    		
    	}
    	String fechaFinString = df.format(fechaFinal);
    	try {
    		fechaFinal = df.parse(fechaFinString);
    	}catch (ParseException ex){
    		
    	} 
    	long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        
        // Total de intereses
        float totIntereses = 0;
        for (TablaAmortizacion totInt : listTabAmortizacion) {
			if(totInt.getEstatusPago().getClaveInterna().equals("PAGO_PAGADO")){
				totIntereses += totInt.getInteres();
			}
		}
        
        // Intereses Moratorios Cargados
        // Cero por el momento, Queda pendiente!
        float intMorCargados = 0;
        
        // INFORMACIÓN DEL CREDITO
        // Calcular fecha de hoy
        Date fechaHoy = new Date();
      
        //--> saldo vencido
        float saldoVencido = 0;
        for (TablaAmortizacion totSaldoVencido : listTabAmortizacion) {
			if(totSaldoVencido.getFechaVencimiento().before(fechaHoy) || totSaldoVencido.getEstatusPago().getClaveInterna().equals("PAGO_PENDIENTE")){
				saldoVencido += totSaldoVencido.getCuota();
			}
		}
        
        // Cargar datos a EstadoCuentaVO
        estadoCuenta.setSaldoVencido(saldoVencido);
        estadoCuenta.setInteresesMoratoriosCargados(intMorCargados);
        estadoCuenta.setTotalIntereses(totIntereses);
    	estadoCuenta.setDiasPeriodo(dias);
    	estadoCuenta.setSumaComisionesCobradas(totalComisiones);
    	estadoCuenta.setSumaIVA(totIvaInteres);
    	estadoCuenta.setMovimientoCaja(listMovCaja);
    	estadoCuenta.setCliente(clienteList);
    	estadoCuenta.setCredito(creditoList);
    	estadoCuenta.setProductos(listProductos);
    	estadoCuenta.setPagos(listPagos);
    	estadoCuenta.setTablaAmortizacion(listTabAmortizacion);
		return estadoCuenta;
	}

}
