package com.crenx.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.crenx.data.domain.entity.IndicadorCobranzaPeriodo;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

@Component
public class ScheduledTasks {

	@Autowired
	CajaServices cajaServices;
	@Autowired
	UtilityServices utServices;
	@Autowired
	NominaServices nominaServices;
	@Autowired
	MigracionServices migServices;
	@Autowired
	CreditoServices credServices;
	
	
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	//Diario 2am
	@Scheduled(cron = "0 0 2 * * *")
	//@Scheduled(fixedDelay = 5000)
    public void corteDiario() {
        Date fechaProceso = new Date();
		Calendar ss = Calendar.getInstance();
		ss.setTime(fechaProceso);
		ss.add(Calendar.DAY_OF_MONTH, -1);
		System.out.println("The time is now " + dateFormat.format(ss.getTime()));
		try
		{
	        System.out.println("corteDiario Inicio " + dateFormat.format(new Date()));
			credServices.calculaCarteraIncobrable();
			cajaServices.calculoIndicadorCobranzaDia(ss.getTime());   
	        credServices.saveIndicadorDiarioCartera();
			cajaServices.corteCaja();
	        System.out.println("corteDiario Final " + dateFormat.format(new Date()));
		}catch(Exception ex)
		{
			System.out.println("Exception " + dateFormat.format(ss.getTime()) + " " + ex.getMessage());			
		}
    }

	//Domingo 22pm
	@Scheduled(cron = "0 0 22 * * SUN")
	//@Scheduled(fixedDelay = 12000)
    public void nomina() {
		try
		{
	        System.out.println("Indicador de cobranza semanal Inicio " + dateFormat.format(new Date()));
	        //Proceso Normal
	        Date fechaProceso = new Date();
			Calendar ss = Calendar.getInstance();
			ss.setTime(fechaProceso);
			ss.add(Calendar.DAY_OF_MONTH, -1);
			DateRangeVO semana = utServices.getWeekDates(ss.getTime());
			//Descomentar para probar
			//Date startDate = semana.getStartDate();
			//startDate.setMonth(4);
			//semana.setStartDate(startDate);
			cajaServices.calculaIndicadoresNegocio(semana);
			nominaServices.calculaNomina(semana);
	        System.out.println("Indicador de cobranza semanal Fin " + dateFormat.format(new Date()));
		}catch(Exception ex)
		{
			System.out.println("Exception " + dateFormat.format(new Date()) + " " + ex.getMessage());						
		}
    }
	
	//Primer día de cada mes
	//Estados de Cuenta
	@Scheduled(cron = "0 0 1 1 1/1 *")
    public void mensual() {
        System.out.println("The time is now Mensual" + dateFormat.format(new Date()));
    }
	
	//Primer día de cada semestre
	@Scheduled(cron = "0 0 1 1 1/6 *")
    public void semestral() {
        System.out.println("The time is now Semestral " + dateFormat.format(new Date()));
    }
	
	
	//Domingo 22pm
	//@Scheduled(cron = "0 0 22 ? * SUN")
	@Scheduled(fixedDelay = 120000)
    public void migracion() {
		try
		{
	        System.out.println("Migracion Inicio " + dateFormat.format(new Date()));
	        //Proceso Normal
	        migServices.migraUsuarios();
	        migServices.migraArchivos();
	        System.out.println("Migracion Fin " + dateFormat.format(new Date()));
		}catch(Exception ex)
		{
			System.out.println("Exception " + dateFormat.format(new Date()) + " " + ex.getMessage());						
		}
    }
	
	
	
}
