package com.crenx.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.activiti.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;

import com.crenx.br.CatalogoBRImpl;
import com.crenx.br.CreditoBRImpl;
import com.crenx.business.FileManager;
import com.crenx.business.FileManagerFactory;
import com.crenx.business.FileUtils;
import com.crenx.data.domain.entity.Caja;
import com.crenx.data.domain.entity.Calendario;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.Horario;
import com.crenx.data.domain.entity.IndicadorCarteraDiario;
import com.crenx.data.domain.entity.IndicadorOperacionesCliente;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.entity.PropiedadesDoc;
import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.entity.Visita;
import com.crenx.data.domain.repository.CajaRepository;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.IndicadorCarteraDiarioRepository;
import com.crenx.data.domain.repository.IndicadorCobranzaRepository;
import com.crenx.data.domain.repository.IndicadorOperacionesClienteRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.PropiedadesDocRepository;
import com.crenx.data.domain.repository.TablaAmortizacionRepository;
import com.crenx.data.domain.repository.VisitaRepository;
import com.crenx.data.domain.vo.AmortizacionVO;
import com.crenx.data.domain.vo.CreditoBRVO;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.CreditoRestVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.CreditosUnifVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.EstadoCarteraVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.FondeoVO;
import com.crenx.data.domain.vo.IndicadorCarteraDiarioVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.IndicadorOperacionesClienteVO;
import com.crenx.data.domain.vo.InidicadorRutaVO;
import com.crenx.data.domain.vo.MovCajaDivModelMap;
import com.crenx.data.domain.vo.MovCajaGerenciaModelMap;
import com.crenx.data.domain.vo.MovCajaRegionModelMap;
import com.crenx.data.domain.vo.MovimientoCajaModelMap;
import com.crenx.data.domain.vo.MovimientoCajaVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.PagoToVOModelMapper;
import com.crenx.data.domain.vo.PagoVO;
import com.crenx.data.domain.vo.ReglaGastoBRVO;
import com.crenx.data.domain.vo.TablaAmortizacionVO;
import com.crenx.data.domain.vo.TaskVO;
import com.crenx.data.domain.vo.VisitaVO;
import com.crenx.security.UserAuthentication;
import com.crenx.util.Constantes;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class CreditoServices {

	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private CatalogoRepository catRepository;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private CreditoRepository credRepository;
	@Autowired
	PagoRepository pagoRepository;
	@Autowired
	TablaAmortizacionRepository taRepository;
	@Autowired
	MovimientoCajaRepository mcRepository;
	@Autowired
	ProductoRepository prodRepository;
	@Autowired
	UtilityServices utServices;
	@Autowired
	private CreditoBRImpl brCredito;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private CalendarioServices calServices;
	@Autowired
	private CajaRepository cajaRepository;
	@Autowired
	private SeguridadServices segServices;
	@Autowired
	private EmpleadoServices empServices;
	@Autowired
	IndicadorCobranzaRepository icRepository;
	@Autowired
	private VisitaRepository visRespository;
	@Autowired
	private IndicadorOperacionesClienteRepository iocRepository;
	@Autowired
	private ActivitiServices bpmServices;
	@Autowired
	private CatalogoBRImpl regGasService;
	@Autowired
	private DocumentoRepository documentoRepository;
	@Autowired
	private FileManagerFactory fileManagerFactory;
	@Autowired
	private PropiedadesDocRepository propsDoctoRepository;
	@Autowired
	private IndicadorCarteraDiarioRepository icrRepository;

	@Autowired
	private MovimientoCajaRepository movRepository;
	// private static final SimpleDateFormat dateFormat = new
	// SimpleDateFormat("HH:mm:ss");

	@Transactional
	public void guardaVisita(VisitaVO visita) throws Exception {
		Credito credito = credRepository.findOne(visita.getCredito());

		Catalogo motivo = catRepository.findByNombreAndClaveInternaIsLike(visita.getIdMotivo(), "%CAT_MOTIVO_NOPAGO%");
		Organizacion ruta = orgRepository.findOne(visita.getIdRuta());
		Cliente cliente = clienteRepository.findByIdCliente(visita.getIdCliente()).get(0);
		Visita dbVisita = new Visita();
		dbVisita.setGeoPosicion(visita.getGeoPosicion());
		dbVisita.setIdCliente(cliente.getId());
		dbVisita.setIdCredito(Long.valueOf(visita.getIdCredito()));
		dbVisita.setIdDispositivo(visita.getIdDispositivo());
		dbVisita.setIdUsuario(visita.getIdUsuario());
		dbVisita.setNombreCliente(
				cliente.getNombre() + " " + (cliente.getApellidoPaterno() == null ? " " : cliente.getApellidoPaterno())
						+ " " + (cliente.getApellidoMaterno() == null ? " " : cliente.getApellidoMaterno()));
		dbVisita.setMotivo(motivo);
		dbVisita.setRuta(ruta);
		dbVisita.setIdOrg(orgRepository.findOne(visita.getIdOrg()));
		dbVisita.setCredito(credito);
		Date fechaOpe = null;
		long milliSecs;
		try {
			milliSecs = Long.parseLong(visita.getFechaVisita());
			fechaOpe = new Date(milliSecs);
		} catch (NumberFormatException ex) {
			fechaOpe = utServices.convertStringToDate(visita.getFechaVisita(), "yyyy-MM-dd hh:mm:ss");
		}
		fechaOpe = fechaOpe == null ? new Date() : fechaOpe;
		dbVisita.setFechaVisita(fechaOpe);
		visRespository.save(dbVisita);

		credito.setFechaUltimaVisita(fechaOpe);
		credRepository.save(credito);

	}

	@Transactional
	public void crearPago(Credito credito, PagoVO pagoVO, String idDispositivo, String usuario) throws Exception {
		crearPago(credito, pagoVO, idDispositivo, usuario, false);
	}

	@Transactional
	public void crearPago(Credito credito, PagoVO pagoVO, String idDispositivo, String usuario, boolean restructura)
			throws Exception {
		boolean rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();
		Catalogo abono = catServices.getCatalogoCveInterna("TIPO_PAGO_ABONO");
		Catalogo comision = catServices.getCatalogoCveInterna("TIPO_PAGO_COMISION");
		Catalogo pendiente = catServices.getCatalogoCveInterna("PAGO_PENDIENTE");
		Catalogo pagado = catServices.getCatalogoCveInterna("PAGO_PAGADO");
		Catalogo credPagado = catServices.getCatalogoCveInterna("CRED_PAGADO");

		Catalogo abonoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");
		Catalogo ingresoCobro = catServices.getCatalogoCveInterna("TIPO_OPE_ING_COBRO");
		Catalogo cobroComision = catServices.getCatalogoCveInterna("TIPO_OPE_ING_COMISION");
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		Pago pago = new Pago();
		pago.setAbono(pagoVO.getAbono());
		pago.setComision(pagoVO.getComision());
		pago.setSaldoAnterior(credito.getSaldo());
		long pagoAdicional = Math.floorMod((long) pago.getAbono(), (long) credito.getPago());
		pago.setPagoAdicional((double) pagoAdicional);
		Date fechaOpe = null;
		long milliSecs;
		try {
			milliSecs = Long.parseLong(pagoVO.getFechaPago());
			fechaOpe = new Date(milliSecs);
		} catch (NumberFormatException ex) {
			fechaOpe = utServices.convertStringToDate(pagoVO.getFechaPago(), "yyyy-MM-dd hh:mm:ss");
		}
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();
		fechaOpe = fechaOpe == null ? new Date() : fechaOpe;
		pago.setFechaPago(fechaOpe);
		pago.setCredito(credito);
		pago.setGeoPosicion(pagoVO.getGeoPosicion());

		double pagoTotal = pagoVO.getAbono() + pagoVO.getComision();
		// Visita
		Organizacion ruta = orgRepository.findOne(credito.getCliente().getRuta().getIdOrg());
		Organizacion rutaOrigen = null;
		if (ruta.getIdOrg() != null)
			rutaOrigen = orgRepository.findOne(ruta.getIdOrg());
		Catalogo motivo = null;
		try {
			if (pagoVO.getIdMotivo() != null)
				motivo = catServices.getCatalogoId(pagoVO.getIdMotivo());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();
		credito.setFechaUltimaVisita(fechaOpe);
		// guardaVisita(visita);
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();

		if (pagoTotal == 0) {
			return;
		}

		// Validacionea antes de continuar
		double difComision = pago.getComision() - pago.getCredito().getSaldoComision();

		if (difComision > 0.01) {
			System.out.println(String.format("Diferencia de comisión %f", difComision));
			System.out.println(String.format("pago.getComision() %f", pago.getComision()));
			System.out.println(
					String.format("pago.getCredito().getSaldoComision() %f", pago.getCredito().getSaldoComision()));
			if (!restructura)
				throw new Exception("El crédito no tiene saldo para la comisión o el pago es mayor a la comisión");

		} else if (difComision < 0) {
			if (!restructura)
				throw new Exception("El saldo de comisión es mayor al pago registrado");
		}
		// Ajustamos el pago de comisión con el abono
		pago.setComision(pago.getComision() - difComision);
		pago.setAbono(pago.getAbono() + difComision);
		// FIXME: Todo esto debe sucede en transacción
		pago.setId(utServices.generaId("Pago"));
		pago.setIdDispositivo(idDispositivo == null ? "Web" : idDispositivo);
		pago.setIdUsuario(usuario);
		pago.setNoRecibo(pagoVO.getNoRecibo());

		pagoRepository.save(pago);
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();

		// Cargar la tabla de amortización
		Collection<TablaAmortizacion> tabla = pago.getCredito().getPagos();

		// Si el pago incluye pago de comision
		TablaAmortizacion itemPagado = siguientePago(comision, tabla, pendiente);
		if (itemPagado != null) {
			itemPagado.setPago(pago);
			itemPagado.setEstatusPago(pagado);
			// taRepository.save(itemPagado);
			double saldoComision = pago.getCredito().getSaldoComision();
			saldoComision -= pago.getComision();
			pago.getCredito().setSaldoComision(saldoComision);
		}
		double importeAbonar = pago.getAbono() + credito.getPagoPendienteAplicar();
		credito.setPagoPendienteAplicar(0);
		int totPagos = pago.getCredito().getTotPagos();
		int totPagosAtiempo = pago.getCredito().getTotPagoATiempo();

		while (importeAbonar > 0) {
			itemPagado = siguientePago(abono, tabla, pendiente);
			if (itemPagado == null)
				break;
			if (importeAbonar > pago.getCredito().getSaldo() && importeAbonar > pago.getCredito().getPago())
				throw new Exception("El crédito no tiene saldo o el abono es mayor al saldo");

			// Puede que el importe restante sea menor que la Cuota pero todavía
			// hay un saldo
			if (importeAbonar >= itemPagado.getCuota() || (importeAbonar >= pago.getCredito().getSaldo()
					&& (totPagos + 1) == pago.getCredito().getPlazo())) {
				totPagos++;

				if ((pago.getFechaPago().compareTo(itemPagado.getFechaVencimiento())) <= 0)
					totPagosAtiempo++;
				itemPagado.setPago(pago);
				itemPagado.setEstatusPago(pagado);
				double abonoReal = pago.getCredito().getSaldo() < itemPagado.getCuota() ? pago.getCredito().getSaldo()
						: itemPagado.getCuota();
				itemPagado.setCuota(abonoReal);
				importeAbonar -= abonoReal;
				pago.getCredito().setSaldo(pago.getCredito().getSaldo() - abonoReal);
				pago.getCredito().setSaldoInsoluto(itemPagado.getSaldo());
			} else {
				// Ahora se pueden aplicar pagos con valores diferentes a los
				// múltiplos
				// if (!restructura)
				// throw new Exception("Solo se pueden aplicar pagos con
				// múltiplos del pago acordado");
				// FIXME: POr resolver que hacer con esta diferencia
				pago.getCredito().setPagoPendienteAplicar(importeAbonar);
				importeAbonar -= importeAbonar;

			}
		}
		if (importeAbonar > 0) {
			// Si por alguna razón ya no hay amortizaciones por cubrir y sobra
			// dinero se registra como pendiente
			pago.getCredito().setPagoPendienteAplicar(importeAbonar);
			importeAbonar -= importeAbonar;
		}

		pago.getCredito().setTotPagoATiempo(totPagosAtiempo);
		pago.getCredito().setTotPagos(totPagos);
		int calificacion = 1;
		if (totPagos > 0) {
			double calTemp = ((double) totPagosAtiempo / (double) totPagos);
			Double d = calTemp * 100;
			calificacion = d.intValue();
		}
		pago.getCredito().setCalificacion(calificacion);
		pago.getCredito().setFechaUltimoPago(pago.getFechaPago());
		if (pago.getCredito().getSaldo() <= 0)
			pago.getCredito().setEstatusCredito(credPagado);
		credRepository.save(pago.getCredito());
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();
		// Movimientos de Caja
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador("system");
		mc.setFechaEmision(pago.getFechaPago());
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo(idDispositivo);
		mc.setIdUsuario(usuario);
		mc.setAbono(pago.getAbono());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(pago.getIdPago());
		mc.setRuta(pago.getCredito().getRuta());
		mc.setTipoMovimiento(abonoCaja);
		mc.setTipoOperacion(ingresoCobro);
		mc.setReferenciaLarga(
				pago.getCredito().getCliente().getNombre() + " " + pago.getCredito().getCliente().getApellidoPaterno()
						+ pago.getCredito().getCliente().getApellidoMaterno());
		mc.setEstatusMovimiento(statusAprobado);
		registraMovCaja(mc);
		rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();

		// Movimientos de Caja
		if (pago.getComision() > 0) {
			mc = new MovimientoCaja();
			// FIXME: Nombre del autorizador
			mc.setAutorizador("system");
			mc.setFechaEmision(pago.getFechaPago());
			mc.setFechaSistema(new Date());
			// FIXME: Id del Dispositivo
			mc.setIdDispositivo(idDispositivo);
			mc.setIdUsuario(usuario);
			mc.setAbono(pago.getComision());
			// FIXME: Numero de recibo o Credito
			mc.setReferencia(pago.getIdPago());
			mc.setRuta(pago.getCredito().getRuta());
			mc.setTipoMovimiento(abonoCaja);
			mc.setTipoOperacion(cobroComision);
			mc.setReferenciaLarga(pago.getCredito().getCliente().getNombre() + " "
					+ pago.getCredito().getCliente().getApellidoPaterno()
					+ pago.getCredito().getCliente().getApellidoMaterno());
			mc.setEstatusMovimiento(statusAprobado);
			registraMovCaja(mc);
			rb = TransactionAspectSupport.currentTransactionStatus().isRollbackOnly();
		}

		System.out.println(rb);
	}

	@Transactional
	public void fondeoCaja(FondeoVO fondeo, UserAuthentication profile) throws Exception {
		Catalogo tipoMovCat = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");
		Catalogo tipoOperacionCat = catServices.getCatalogoCveInterna("TIPO_OPE_FONDEO");
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		Organizacion orgNode = orgRepository.findOne(fondeo.getIdOrg());
		// Movimientos de Caja
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador(fondeo.getUsuario());
		mc.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo(fondeo.getIdDispositivo());
		mc.setIdUsuario(fondeo.getUsuario());
		mc.setAbono(fondeo.getMonto());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(fondeo.getReferencia());
		mc.setRuta(orgNode);
		mc.setTipoMovimiento(tipoMovCat);
		mc.setTipoOperacion(tipoOperacionCat);
		mc.setReferenciaLarga(fondeo.getReferenciaLarga());
		mc.setEstatusMovimiento(statusAprobado);
		registraMovCaja(mc);
	}

	@Transactional
	public void registroGasto(FondeoVO fondeo, UserAuthentication profile) throws Exception {
		Catalogo tipoMovCat = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo tipoOperacionCat = catServices.getCatalogoCveInterna("TIPO_OPE_GASTO");
		Catalogo tipoGasto = catServices.getCatalogoId(fondeo.getIdTipoGasto());
		Organizacion orgNode = orgRepository.findOne(fondeo.getIdOrg());

		ReglaGastoBRVO result = regGasService.estatusMovCajaCredito(fondeo, profile);

		// Movimientos de Caja
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador(fondeo.getUsuario());
		mc.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo(fondeo.getIdDispositivo());
		mc.setIdUsuario(fondeo.getUsuario());
		if (result.isCorreccion())
			mc.setPendiente(fondeo.getMonto());
		else
			mc.setCargo(fondeo.getMonto());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(fondeo.getReferencia());
		mc.setRuta(orgNode);
		mc.setTipoMovimiento(tipoMovCat);
		mc.setTipoOperacion(tipoOperacionCat);
		mc.setTipoGasto(tipoGasto);
		mc.setReferenciaLarga(fondeo.getReferenciaLarga());
		mc.setEstatusMovimiento(result.getEstatusGasto());
		registraMovCaja(mc);

		fondeo.setIdMC(mc.getIdMovCaja());

		this.sendFileTransfer(fondeo);

		if (result.isCorreccion()) {
			HashMap<String, Object> variables = new HashMap<String, Object>();
			variables.put("IdMC", mc.getIdMovCaja());
			variables.put("Monto", fondeo.getMonto());
			variables.put("Usuario", profile.getName());
			ProcessInstance theInstance = bpmServices.createInstance("expenseApproval", variables);
		}
	}

	@Transactional
	public void aprobarGasto(UserAuthentication profile, FondeoVO gasto) {
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		MovimientoCaja movCaja = mcRepository.findOne(gasto.getIdMC());
		if (gasto.isAprobado()) {
			movCaja.setEstatusMovimiento(statusAprobado);
			movCaja.setCargo(movCaja.getPendiente());
			movCaja.setPendiente(0);
		} else {
			movCaja.setEstatusMovimiento(statusRechazado);
			movCaja.setCargo(0);
		}
		movCaja.setAutorizador(profile.getName());
		registraMovCaja(movCaja);
		if (gasto.getTaskId() != null)
			bpmServices.completeTask(gasto.getTaskId(), "");

	}

	@Transactional
	private void completeExpenseApproval(FondeoVO gasto, String userName) {
		if (gasto.getTaskId() != null) {
			bpmServices.completeTask(gasto.getTaskId(), "");
		} else {
			TaskVO taskVO = bpmServices.getTaskByVarValue("idGasto", gasto.getIdMC());
			bpmServices.completeTask(taskVO.getId(), "");
		}

	}

	@Transactional
	public void aprobarTransfer(UserAuthentication profile, FondeoVO gasto) {
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		MovimientoCaja movCajaOrigen = mcRepository.findOne(gasto.getIdMC());
		MovimientoCaja movCajaDestino = mcRepository.findOne(gasto.getIdMCDestino());
		if (gasto.isAprobado()) {
			movCajaOrigen.setEstatusMovimiento(statusAprobado);
			movCajaOrigen.setCargo(movCajaOrigen.getPendiente());
			movCajaOrigen.setPendiente(0);

			movCajaDestino.setEstatusMovimiento(statusAprobado);
			movCajaDestino.setAbono(movCajaDestino.getPendiente());
			movCajaDestino.setPendiente(0);

		} else {
			movCajaOrigen.setEstatusMovimiento(statusRechazado);
			movCajaOrigen.setCargo(0);
			movCajaDestino.setEstatusMovimiento(statusRechazado);
			movCajaDestino.setAbono(0);
		}
		movCajaOrigen.setAutorizador(profile.getName());
		movCajaDestino.setAutorizador(profile.getName());
		registraMovCaja(movCajaOrigen);
		registraMovCaja(movCajaDestino);

		if (gasto.getTaskId() != null)
			bpmServices.completeTask(gasto.getTaskId(), "");

	}

	public FondeoVO loadGastoAprobar(String idMovCaja) {
		MovimientoCaja movCaja = mcRepository.findOne(idMovCaja);
		FondeoVO retVal = new FondeoVO();
		retVal.setIdMC(movCaja.getIdMovCaja());
		retVal.setIdTipoGasto(movCaja.getTipoGasto().getIdCatalogo());
		retVal.setNombreGasto(movCaja.getTipoGasto().getNombre());
		retVal.setFechaEmision(utServices.convertDateToString(movCaja.getFechaEmision(), "yyyy-MM-dd"));
		retVal.setMonto(movCaja.getPendiente());
		retVal.setReferencia(movCaja.getReferencia());
		retVal.setDescripcion(movCaja.getReferenciaLarga());
		return retVal;
	}

	// FIXME: Mecanismo de aprobación/aceptación de transferencia de fondos
	@Transactional
	public void transferenciaCajas(FondeoVO fondeo, UserAuthentication profile) throws Exception {
		Organizacion orgNodeOrigen = orgRepository.findOne(fondeo.getIdOrg());
		Organizacion orgNodeDestino = orgRepository.findOne(fondeo.getIdOrgDestino());

		if (orgNodeDestino.getTitular() == null)
			throw new Exception(
					"El nodo " + orgNodeDestino.getNombre() + " no tiene titular para recibir la transferencia");

		// FIXME: Generar registro de autorización o aviso de alerta
		Catalogo tipoMovOrigen = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo tipoMovDestino = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");

		Catalogo tipoOperacionOrigen = catServices.getCatalogoCveInterna("TIPO_OPE_EGR_TRANS");
		Catalogo tipoOperacionDestino = catServices.getCatalogoCveInterna("TIPO_OPE_ING_TRANS");

		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");
		Catalogo statusPendiente = catServices.getCatalogoCveInterna("ESTATUS_GPENDIENTE");

		// Movimientos de Caja
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador(fondeo.getUsuario());
		mc.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo(fondeo.getIdDispositivo());
		mc.setIdUsuario(fondeo.getUsuario());
		// mc.setCargo(fondeo.getMonto());
		mc.setPendiente(fondeo.getMonto());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(fondeo.getReferencia());
		mc.setRuta(orgNodeOrigen);
		mc.setTipoMovimiento(tipoMovOrigen);
		mc.setTipoOperacion(tipoOperacionOrigen);
		mc.setReferenciaLarga(fondeo.getReferenciaLarga());
		mc.setEstatusMovimiento(statusPendiente);
		String idMovInsertado = registraMovCaja(mc).getIdMovCaja();

		MovimientoCaja mcDestino = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mcDestino.setAutorizador(fondeo.getUsuario());
		mcDestino.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		mcDestino.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mcDestino.setIdDispositivo(fondeo.getIdDispositivo());
		mcDestino.setIdUsuario(fondeo.getUsuario());
		// mcDestino.setAbono(fondeo.getMonto());
		mcDestino.setPendiente(fondeo.getMonto());

		// FIXME: Numero de recibo o Credito
		mcDestino.setReferencia(fondeo.getReferencia());
		mcDestino.setRuta(orgNodeDestino);
		mcDestino.setTipoMovimiento(tipoMovDestino);
		mcDestino.setTipoOperacion(tipoOperacionDestino);
		mcDestino.setReferenciaLarga(mc.getReferenciaLarga());
		mcDestino.setEstatusMovimiento(statusPendiente);
		mcDestino.setMovcruze(idMovInsertado);
		String idMovDestino = registraMovCaja(mcDestino).getIdMovCaja();
		// Actualizamos movimiento origen
		mc.setMovcruze(idMovDestino);
		movRepository.save(mc);

		fondeo.setIdMC(mc.getIdMovCaja());
		fondeo.setIdMCDestino(mcDestino.getIdMovCaja());
		// Se guarda el documento físico transferido
		this.sendFileTransfer(fondeo);

		HashMap<String, Object> variables = new HashMap<String, Object>();
		variables.put("origen", orgNodeOrigen.getNombre());
		variables.put("destino", orgNodeDestino.getNombre());
		variables.put("monto", fondeo.getMonto());
		variables.put("mcOrigen", mc.getIdMovCaja());
		variables.put("mcDestino", mcDestino.getIdMovCaja());
		variables.put("aprobador", orgNodeDestino.getTitular().getUsername());
		variables.put("fechaTransfer", fondeo.getFechaEmision());

		ProcessInstance theInstance = bpmServices.createInstance("transferApproval", variables);

	}

	private TablaAmortizacion siguientePago(Catalogo tipoPago, Collection<TablaAmortizacion> tabla, Catalogo estatus) {
		TablaAmortizacion retVal = null;
		for (TablaAmortizacion item : tabla) {
			if (!item.getTipoPago().getIdCatalogo().equals(tipoPago.getIdCatalogo()))
				continue;
			if (!item.getEstatusPago().getIdCatalogo().equals(estatus.getIdCatalogo()))
				continue;
			retVal = item;
			break;
		}
		return retVal;
	}

	@Transactional
	public MovimientoCaja registraMovCaja(MovimientoCaja mc) {

		MovimientoCaja mov = mcRepository.save(mc);
		// Registrar actualizacion de Caja
		// Localizamos el último corte
		Caja corte = cajaRepository.findTop1ByUnidadIdOrgOrderByFechaCorteDesc(mc.getRuta().getIdOrg());
		corte.setTotalAbonos(corte.getTotalAbonos() + mc.getAbono());
		corte.setTotalCargos(corte.getTotalCargos() + mc.getCargo());
		corte.setSaldoFinal(corte.getSaldoInicial() + corte.getTotalAbonos() - corte.getTotalCargos());
		cajaRepository.save(corte);
		// Cuando se trate de un Pago se debe desglosar el IVA, Intereses y
		// Capital
		// Registra asiento contable
		return mov;
	}

	@Transactional
	public void restructuraCredito(String principalName, CreditoRestVO input) throws Exception {

		Date fechaOperacion = utServices.convertStringToDate(input.getDestino().getFechaEmision(), "yyyy-MM-dd");
		Catalogo cancelado = catServices.getCatalogoCveInterna("CRED_CANCELADO");

		Catalogo abonoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");
		Catalogo reingresoCaja = catServices.getCatalogoCveInterna("TIPO_OPE_ING_CORR_CREDITO");

		Catalogo cargoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo desembolso = catServices.getCatalogoCveInterna("TIPO_OPE_EGR_CORR_CREDITO");
		Catalogo desembolsoCom = catServices.getCatalogoCveInterna("TIPO_OPE_EGR_CORR_CREDITO_COM");
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		// Cancela crédito Original
		Credito oldCred = credRepository.findOne(input.getOriginal().getIdCredito());
		oldCred.setEstatusCredito(cancelado);
		oldCred.setFechaCancelacion(fechaOperacion);
		credRepository.save(oldCred);
		// Asigna la misma fecha de Emisión del original
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador("system");
		mc.setFechaEmision(fechaOperacion);
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo("WebApp");
		mc.setIdUsuario(principalName);
		mc.setAbono(oldCred.getMonto());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(oldCred.getIdCredito());
		mc.setRuta(oldCred.getRuta());
		mc.setTipoMovimiento(abonoCaja);
		mc.setTipoOperacion(reingresoCaja);
		mc.setReferenciaLarga(oldCred.getCliente().getNombre() + " " + oldCred.getCliente().getApellidoPaterno()
				+ oldCred.getCliente().getApellidoMaterno());
		mc.setEstatusMovimiento(statusAprobado);
		registraMovCaja(mc);
		// Cancela Pagos
		Collection<Pago> pagos = Lists
				.newArrayList(pagoRepository.findByCreditoIdCredito(input.getOriginal().getIdCredito()));
		double totPago = 0;
		double totComision = 0;
		for (Pago onePago : pagos) {
			// Movimientos de Caja
			mc = new MovimientoCaja();
			// FIXME: Nombre del autorizador
			mc.setAutorizador("system");
			mc.setFechaEmision(fechaOperacion);
			mc.setFechaSistema(new Date());
			// FIXME: Id del Dispositivo
			mc.setIdDispositivo("WebApp");
			mc.setIdUsuario(principalName);
			totPago += onePago.getAbono();
			totComision += onePago.getComision();
			mc.setCargo(onePago.getAbono());
			// FIXME: Numero de recibo o Credito
			mc.setReferencia(onePago.getIdPago());
			mc.setRuta(oldCred.getRuta());
			mc.setTipoMovimiento(cargoCaja);
			mc.setTipoOperacion(desembolso);
			mc.setReferenciaLarga(oldCred.getCliente().getNombre() + " " + oldCred.getCliente().getApellidoPaterno()
					+ oldCred.getCliente().getApellidoMaterno());
			mc.setEstatusMovimiento(statusAprobado);
			registraMovCaja(mc);

			if (onePago.getComision() > 0) {
				mc = new MovimientoCaja();
				// FIXME: Nombre del autorizador
				mc.setAutorizador("system");
				mc.setFechaEmision(fechaOperacion);
				mc.setFechaSistema(new Date());
				// FIXME: Id del Dispositivo
				mc.setIdDispositivo("WebApp");
				mc.setIdUsuario(principalName);
				totPago += onePago.getAbono();
				totComision += onePago.getComision();
				mc.setCargo(onePago.getComision());
				// FIXME: Numero de recibo o Credito
				mc.setReferencia(onePago.getIdPago());
				mc.setRuta(oldCred.getRuta());
				mc.setTipoMovimiento(cargoCaja);
				mc.setTipoOperacion(desembolso);
				mc.setReferenciaLarga(oldCred.getCliente().getNombre() + " " + oldCred.getCliente().getApellidoPaterno()
						+ oldCred.getCliente().getApellidoMaterno());
				mc.setEstatusMovimiento(statusAprobado);
				registraMovCaja(mc);
			}

		}
		if (input.getPagadoReal() > 0) {
			totPago = input.getPagadoReal();
		}
		// Creamos el nuevo Crédito
		Credito newCredito = crearCredito(input.getDestino(), principalName, fechaOperacion);
		PagoVO pagoVO = new PagoVO();
		pagoVO.setAbono(totPago);
		pagoVO.setComision(totComision);
		pagoVO.setFechaPago(utServices.convertDateToString(fechaOperacion, "yyyy-MM-dd hh:mm:ss"));
		crearPago(newCredito, pagoVO, "WebApp", principalName, true);
	}

	@Transactional
	public Credito crearCredito(CreditoVO creditoVo, String principalName, Date fechaEmision) throws Exception {
		/*
		 * ModelMapper mapper = new ModelMapper(); mapper.addMappings(new
		 * CreditoVOToCreditoModelMap()); Credito credito =
		 * mapper.map(creditoVo, Credito.class);
		 */
		Credito credito = new Credito();
		credito.setCalificacion(creditoVo.getCalificacion());
		credito.setComision(creditoVo.getComision());
		credito.setIdCredito(creditoVo.getIdCredito());
		credito.setMonto(creditoVo.getMonto());
		credito.setPago(creditoVo.getPago());
		credito.setPlazo(creditoVo.getPlazo());
		credito.setTasa(creditoVo.getTasa());
		credito.setFechaEmision(fechaEmision);
		credito.setGeoPosicion(creditoVo.getGeoPosicion());
		credito.setId(creditoVo.getId());

		List<Credito> creditos = creditosPorEstatus(creditoVo.getIdCliente(), "CRED_PAGADO");
		String tipoCredito = creditos.size() == 0 ? "CRED_NUEVO" : "CRED_RECOMPRA";
		Catalogo tipoCred = catServices.getCatalogoCveInterna(tipoCredito);
		credito.setTipoCredito(tipoCred);

		if (creditoVo.getIdCoDedudor() != null) {
			Cliente coDeudor = clienteRepository.findOne(creditoVo.getIdCoDedudor());
			credito.setCoDeudor(coDeudor);
		}
		credito.setRuta(catServices.getNodoById(creditoVo.getIdRuta()));
		if (credito.getRuta() == null)
			throw new Exception("La ruta es un campo requerido");
		credito.setCliente(catServices.getClienteById(creditoVo.getIdCliente()));
		credito.setFrecuenciaCobro(catRepository.findOne(creditoVo.getIdFrecuenciaCobro()));
		credito.setUnidadPlazo(catRepository.findOne(creditoVo.getIdUnidadPlazo()));
		credito.setProducto(prodRepository.findOne(creditoVo.getIdProducto()));
		// FIXME: Recuperar los datos desde el Principal
		Usuario usuario = segServices.getPrincipal(principalName);
		usuario.setPerfil(empServices.loadPerfil(usuario));
		CreditoBRVO status = brCredito.estatusNuevoCredito(credito, usuario, creditoVo.getMonto(),
				creditoVo.getIdCoDedudor());
		if (!status.isCrearCredito()) {
			throw new Exception(status.getMotivoRechazo());
		}
		credito.setEstatusCredito(status.getEstatusCredito());
		crearCredito(credito, creditoVo.getIdDispositivo(), principalName, status);
		creditoVo.setId(credito.getId());
		creditoVo.setIdCredito(credito.getIdCredito());
		return credito;
	}

	@Transactional
	public void crearCredito(Credito credito, String idDispositivo, String usuario, CreditoBRVO estatus) {

		Catalogo abono = catServices.getCatalogoCveInterna("TIPO_PAGO_ABONO");
		Catalogo comision = catServices.getCatalogoCveInterna("TIPO_PAGO_COMISION");
		Catalogo pendiente = catServices.getCatalogoCveInterna("PAGO_PENDIENTE");

		Catalogo cargoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo desembolso = catServices.getCatalogoCveInterna("TIPO_OPE_CREDITO");

		Catalogo abonoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");
		Catalogo reingresoCaja = catServices.getCatalogoCveInterna("TIPO_OPE_ING_CORR_CREDITO");

		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo statusRechazado = catServices.getCatalogoCveInterna("ESTATUS_GRECHAZADO");

		if (estatus.isCorreccion()) {
			// Elimina tabla de amortización actual
			taRepository.deleteByCreditoIdCredito(credito.getIdCredito());
			// Obtiene el crédito anterior para generar un movimiento de reversa
			// en caja
			Credito oldCred = credRepository.findOne(credito.getIdCredito());
			// Asigna la misma fecha de Emisión del original ??? PAra que???
			// credito.setFechaEmision(oldCred.getFechaEmision());
			MovimientoCaja mc = new MovimientoCaja();
			// FIXME: Nombre del autorizador
			mc.setAutorizador("system");
			mc.setFechaEmision(new Date());
			mc.setFechaSistema(new Date());
			// FIXME: Id del Dispositivo
			mc.setIdDispositivo(idDispositivo);
			mc.setIdUsuario(usuario);
			mc.setAbono(oldCred.getMonto());
			// FIXME: Numero de recibo o Credito
			mc.setReferencia(oldCred.getIdCredito());
			mc.setRuta(oldCred.getRuta());
			mc.setTipoMovimiento(abonoCaja);
			mc.setTipoOperacion(reingresoCaja);
			mc.setEstatusMovimiento(statusAprobado);
			mc.setReferenciaLarga(oldCred.getCliente().getNombre() + " " + oldCred.getCliente().getApellidoPaterno()
					+ oldCred.getCliente().getApellidoMaterno());
			registraMovCaja(mc);
		}

		// Calcular la tabla de amortización
		double iva = catServices.getParametroByName("TASA_IVA").getValorNum() / 100;

		AmortizacionVO amortizacion = credito.getProducto().isInteresFijo()
				? brCredito.calculaTablaAmortizacion(iva, credito.getPago(), credito.getMonto(), credito.getPlazo(),
						credito.getTasa(), credito.getComision(), true)
				: brCredito.calculaTablaAmortizacion(iva, credito.getPago(), credito.getMonto(), credito.getPlazo(),
						credito.getTasa(), credito.getComision());
		credito.setPago(amortizacion.getPagoFijo());
		// Obtener la agenda de la ruta
		Calendar cc = Calendar.getInstance();
		if (credito.getFechaEmision() == null)
			credito.setFechaEmision(new Date());
		cc.setTime(credito.getFechaEmision());
		Calendario cal = credito.getRuta().getCalendario();
		if (cal == null) {
			cal = catServices.defaultCalendario();
		}
		int noPago = 0;
		double saldoTotal = 0;
		Horario hor = calServices.horarioCalendario(cal);
		TablaAmortizacion oneItem = new TablaAmortizacion();
		oneItem.setNoPago(noPago);
		oneItem.setCapital(amortizacion.getComision());
		oneItem.setIvaInteres(amortizacion.getIvaComision());
		oneItem.setTipoPago(comision);
		oneItem.setCredito(credito);
		oneItem.setCuota(amortizacion.getTotalComision());
		oneItem.setEstatusPago(pendiente);
		oneItem.setFechaVencimiento(cc.getTime());
		credito.setSaldoComision(amortizacion.getTotalComision());
		credito.getPagos().add(oneItem);

		for (TablaAmortizacionVO unPago : amortizacion.getTablaAmoritzacion()) {
			noPago++;
			oneItem = new TablaAmortizacion();
			cc = siguienteDiaActivo(cc, hor);
			oneItem.setNoPago(noPago);
			oneItem.setCapital(unPago.getAmortizacion());
			oneItem.setIvaInteres(unPago.getIvaInteres());
			oneItem.setInteres(unPago.getInteres());
			oneItem.setTipoPago(abono);
			oneItem.setCredito(credito);
			oneItem.setCuota(unPago.getPagoFijo());
			oneItem.setFechaVencimiento(cc.getTime());
			oneItem.setEstatusPago(pendiente);
			oneItem.setSaldo(unPago.getSaldo());
			saldoTotal += unPago.getPagoFijo();
			credito.getPagos().add(oneItem);
		}

		// Guardar el Crédito
		credito.setFechaVencimiento(cc.getTime());
		credito.setSaldo(saldoTotal);
		credito.setSaldoInsoluto(credito.getMonto());
		credito.setTotPagoATiempo(0);
		credito.setTotPagos(0);
		credito.setCalificacion(100);
		credito.setSaldoComision(amortizacion.getTotalComision());
		if (credito.getId() == 0)
			credito.setId(utServices.generaId("Credito"));
		credRepository.save(credito);
		// aqui voy
		if (credito.getEstatusCredito().getClaveInterna().equals("CRED_PENDIENTE")) {
			HashMap<String, Object> variables = new HashMap<String, Object>();
			variables.put("idCredito", credito.getIdCredito());
			variables.put("continueProcess", false);
			variables.put("msg", credito.getCliente().getNombreCompleto());
			variables.put("cantidad", credito.getMonto());
			ProcessInstance theInstance = bpmServices.createInstance("creditApproval", variables);
		} else {
			// Movimientos de Caja
			MovimientoCaja mc = new MovimientoCaja();
			// FIXME: Nombre del autorizador
			mc.setAutorizador("system");
			mc.setFechaEmision(credito.getFechaEmision());
			mc.setFechaSistema(new Date());
			// FIXME: Id del Dispositivo
			mc.setIdDispositivo(idDispositivo);
			mc.setIdUsuario(usuario);
			mc.setCargo(credito.getMonto());
			// FIXME: Numero de recibo o Credito
			mc.setReferencia(credito.getIdCredito());
			mc.setRuta(credito.getRuta());
			mc.setTipoMovimiento(cargoCaja);
			mc.setTipoOperacion(desembolso);
			mc.setEstatusMovimiento(statusAprobado);
			mc.setReferenciaLarga(credito.getCliente().getNombre() + " " + credito.getCliente().getApellidoPaterno()
					+ credito.getCliente().getApellidoMaterno());
			registraMovCaja(mc);

		}

	}

	public IndicadorCreditoVO carteraTotal(FiltrosOrgVO filtros) {
		IndicadorCreditoVO result = null;
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());

		if (rutas.size() > 0) {
			result = credRepository.findCarteraByRutas(rutas);
		} else if (gerencias.size() > 0) {
			result = credRepository.findCarteraByGerencias(gerencias);
		} else if (regiones.size() > 0) {
			result = credRepository.findCarteraByRegiones(regiones);
		} else if (divisiones.size() > 0) {
			result = credRepository.findCarteraByDivisiones(divisiones);
		} else {
			result = credRepository.findCarteraAll();
		}
		return result;
	}

	public IndicadorCreditoVO carteraVencida(FiltrosOrgVO filtros) {
		Date fechaProceso = utServices.justDate(new Date());
		IndicadorCreditoVO result = null;
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());

		if (rutas.size() > 0) {
			result = credRepository.findCarteraVencidaByRutas(rutas, fechaProceso);
		} else if (gerencias.size() > 0) {
			result = credRepository.findCarteraVencidaByGerencias(gerencias, fechaProceso);
		} else if (regiones.size() > 0) {
			result = credRepository.findCarteraVencidaByRegiones(regiones, fechaProceso);
		} else if (divisiones.size() > 0) {
			result = credRepository.findCarteraVencidaByDivisiones(divisiones, fechaProceso);
		} else {
			result = credRepository.findCarteraVencidaAll(fechaProceso);
		}
		return result;
	}

	public long creditosNuevos(DateRangeVO week, FiltrosOrgVO filtros) {

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
		List<Credito> nuevos = new ArrayList<Credito>();
		if (rutas.size() > 0) {
			nuevos = credRepository.findByFechaEmisionBetweenAndRutaIdOrgIn(week.getStartDate(), week.getEndDate(),
					rutas);
		} else if (gerencias.size() > 0) {
			nuevos = credRepository.findByFechaEmisionBetweenAndRutaParentIdOrgIn(week.getStartDate(),
					week.getEndDate(), gerencias);
		} else if (regiones.size() > 0) {
			nuevos = credRepository.findByFechaEmisionBetweenAndRutaParentParentIdOrgIn(week.getStartDate(),
					week.getEndDate(), regiones);
		} else if (divisiones.size() > 0) {
			nuevos = credRepository.findByFechaEmisionBetweenAndRutaParentParentParentIdOrgIn(week.getStartDate(),
					week.getEndDate(), divisiones);
		} else {
			nuevos = credRepository.findByFechaEmisionBetween(week.getStartDate(), week.getEndDate());
		}
		return nuevos.size();
	}

	public List<InidicadorRutaVO> indicadorCobranza(Date fechaProceso, FiltrosOrgVO filtros) {
		fechaProceso = utServices.justDate(fechaProceso);

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
		List<InidicadorRutaVO> inidcadores = new ArrayList<InidicadorRutaVO>();
		if (rutas.size() > 0) {
			inidcadores = icRepository.findIndicadorCobranzaRutas(fechaProceso, rutas);
		} else if (gerencias.size() > 0) {
			inidcadores = icRepository.findIndicadorCobranzaGerencias(fechaProceso, gerencias);
		} else if (regiones.size() > 0) {
			inidcadores = icRepository.findIndicadorCobranzaRegiones(fechaProceso, regiones);
		} else if (divisiones.size() > 0) {
			inidcadores = icRepository.findIndicadorCobranzaDivisiones(fechaProceso, divisiones);
		} else {
			inidcadores = icRepository.findIndicadorCobranzaAll(fechaProceso);
		}
		return inidcadores;
	}

	private Calendar siguienteDiaActivo(Calendar cc, Horario hor) {
		boolean diaActivo = false;
		while (!diaActivo) {
			cc.add(Calendar.DATE, 1);
			diaActivo = calServices.activo(cc.getTime(), hor.getDiasEfectivos());
		}
		cc.setTime(utServices.justDate(cc.getTime()));
		return cc;

	}

	public Collection<MovimientoCajaVO> consultarMovimientosCaja(FiltrosOrgVO filtros) {
		Collection<MovimientoCaja> mcs = null;
		Collection<MovimientoCaja> mcgerencias = null;
		Collection<MovimientoCaja> mcregiones = null;
		Collection<MovimientoCaja> mcdivisiones = null;

		List<MovimientoCajaVO> listaMCs = null;
		List<MovimientoCajaVO> listaMCGerencias = null;
		List<MovimientoCajaVO> listaMCRegiones = null;
		List<MovimientoCajaVO> listaMCDivisiones = null;

		// ArrayList<String> idsOrg = new ArrayList<String>();
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());

		if (rutas.isEmpty()) {
			// si no hay rutas busco gerencias
			if (gerencias.isEmpty()) {
				// si no hay gerencias busco regiones
				if (regiones.isEmpty()) {
					// si no hay regiones busco divisiones
					if (divisiones.isEmpty()) {
						// no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem : nodos) {
							rutas.add(oneItem.getIdOrg());
						}

					} else {
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for (String idDivision : divisiones) {
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for (Organizacion region : lregiones) {
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
							regiones.add(region.getIdOrg());
						}
						for (Organizacion gerencia : lgerencias) {
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
							gerencias.add(gerencia.getIdOrg());
						}
						for (Organizacion ruta : lrutas) {
							rutas.add(ruta.getIdOrg());
						}

					}
				} else {
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for (String idRegion : regiones) {
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));

					}
					for (Organizacion gerencia : lgerencias) {
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						gerencias.add(gerencia.getIdOrg());
					}
					for (Organizacion ruta : lrutas) {
						rutas.add(ruta.getIdOrg());
					}
					gerencias.addAll(gerencias);
					regiones.addAll(regiones);
				}
			} else {
				// recupero las rutas de las gerencias
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for (Organizacion ruta : lrutas) {
					rutas.add(ruta.getIdOrg());
				}
				// Agrego las gerencias
				gerencias.addAll(gerencias);
			}

		}

		/*
		 * joinOrg(idsOrg, filtros.getDivisiones()); joinOrg(idsOrg,
		 * filtros.getRegiones()); joinOrg(idsOrg, filtros.getGerencias());
		 * joinOrg(idsOrg, filtros.getRutas());
		 */

		System.out.println("************-->" + rutas);
		Date fechaInicial = utServices.convertStringToDate(filtros.getFechaInicial().substring(0, 10) + " 00:00:00",
				"yyyy-MM-dd HH:mm:ss");
		Date fechaFinal = utServices.convertStringToDate(filtros.getFechaFinal().substring(0, 10) + " 23:59:59",
				"yyyy-MM-dd HH:mm:ss");
		System.out.println("************-->" + fechaInicial + "----" + fechaFinal);
		if (filtros.getIdTipoMov() != null && filtros.getIdTipoOperacion() != null) {
			mcs = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogoAndTipoOperacionIdCatalogoOrderByRutaIdOrg(
							fechaInicial, fechaFinal, rutas, filtros.getIdTipoMov(), filtros.getIdTipoOperacion()));
			mcgerencias = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogoAndTipoOperacionIdCatalogoOrderByRutaIdOrg(
							fechaInicial, fechaFinal, gerencias, filtros.getIdTipoMov(), filtros.getIdTipoOperacion()));
			mcregiones = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogoAndTipoOperacionIdCatalogoOrderByRutaIdOrg(
							fechaInicial, fechaFinal, regiones, filtros.getIdTipoMov(), filtros.getIdTipoOperacion()));
			mcdivisiones = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogoAndTipoOperacionIdCatalogoOrderByRutaIdOrg(
							fechaInicial, fechaFinal, divisiones, filtros.getIdTipoMov(),
							filtros.getIdTipoOperacion()));
		} else if (filtros.getIdTipoMov() == null && filtros.getIdTipoOperacion() == null) {
			mcs = Lists.newArrayList(mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaInicial,
					fechaFinal, rutas));
			mcgerencias = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaInicial, fechaFinal, gerencias));
			mcregiones = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaInicial, fechaFinal, regiones));
			mcdivisiones = Lists.newArrayList(mcRepository
					.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaInicial, fechaFinal, divisiones));
		} else if (filtros.getIdTipoMov() != null) {
			mcs = Lists.newArrayList(mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogo(
					fechaInicial, fechaFinal, rutas, filtros.getIdTipoMov()));
			mcgerencias = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogo(fechaInicial,
							fechaFinal, gerencias, filtros.getIdTipoMov()));
			mcregiones = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogo(fechaInicial,
							fechaFinal, regiones, filtros.getIdTipoMov()));
			mcdivisiones = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogo(fechaInicial,
							fechaFinal, divisiones, filtros.getIdTipoMov()));
		} else if (filtros.getIdTipoOperacion() != null) {
			mcs = Lists.newArrayList(mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoOperacionIdCatalogo(
					fechaInicial, fechaFinal, rutas, filtros.getIdTipoOperacion()));
			mcgerencias = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoOperacionIdCatalogo(fechaInicial,
							fechaFinal, gerencias, filtros.getIdTipoOperacion()));
			mcregiones = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoOperacionIdCatalogo(fechaInicial,
							fechaFinal, regiones, filtros.getIdTipoOperacion()));
			mcdivisiones = Lists.newArrayList(
					mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInAndTipoOperacionIdCatalogo(fechaInicial,
							fechaFinal, divisiones, filtros.getIdTipoOperacion()));
		}

		listaMCs = new ArrayList<MovimientoCajaVO>();

		// Agrego movimientos de divisiones
		if (mcdivisiones != null) {
			ModelMapper mapper4 = new ModelMapper();
			mapper4.addMappings(new MovCajaDivModelMap());
			Type listType = new TypeToken<List<MovimientoCajaVO>>() {
			}.getType();
			listaMCs.addAll(mapper4.map(mcdivisiones, listType));
		}

		// Agrego movimientos de regiones
		if (mcregiones != null) {
			ModelMapper mapper3 = new ModelMapper();
			mapper3.addMappings(new MovCajaRegionModelMap());
			Type listType = new TypeToken<List<MovimientoCajaVO>>() {
			}.getType();
			listaMCs.addAll(mapper3.map(mcregiones, listType));

		}

		if (mcgerencias != null) {
			// Agrego movimientos de gerencias
			ModelMapper mapper2 = new ModelMapper();
			mapper2.addMappings(new MovCajaGerenciaModelMap());
			Type listType2 = new TypeToken<List<MovimientoCajaVO>>() {
			}.getType();
			listaMCs.addAll(mapper2.map(mcgerencias, listType2));
		}
		if (mcs != null) {
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new MovimientoCajaModelMap());
			Type listType = new TypeToken<List<MovimientoCajaVO>>() {
			}.getType();
			// listaMCs = mapper.map(mcs, listType);
			listaMCs.addAll(mapper.map(mcs, listType));
		}
		// Se complementa con Documentos
		setDocumentoToMovCaja(listaMCs);

		return listaMCs;
	}

	private void joinOrg(ArrayList<String> idsOrg, Collection<NameValueVO> values) {
		for (NameValueVO value : values) {
			idsOrg.add(value.getId());
		}
	}

	public List<Credito> creditosPorEstatus(String idCliente, String claveInternaEstatus) {
		String idEstausActivo = catServices.getCatalogoCveInterna(claveInternaEstatus).getIdCatalogo();
		return credRepository.findByClienteIdClienteAndEstatusCreditoIdCatalogoOrderByFechaEmisionDesc(idCliente,
				idEstausActivo);
	}

	public List<CreditoVO> carteraCreditos(FiltrosOrgVO filtros) {
		List<Credito> creditos = null;
		List<CreditoVO> listaCreditos = null;

		String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		String apellidoPaterno = StringUtils.isEmpty(filtros.getApellidoPaterno()) ? "%"
				: filtros.getApellidoPaterno() + "%";

		ArrayList<String> rutas = catServices.filtraRutas(filtros);
		if (rutas.size() > 0) {
			creditos = (this.credRepository
					.findByClienteRutaIdOrgInAndAndClienteNombreLikeAndClienteApellidoPaternoLike(rutas, nombre,
							apellidoPaterno));
		} else {
			creditos = (this.credRepository.findByClienteNombreLikeAndClienteApellidoPaternoLike(nombre,
					apellidoPaterno));
		}

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		Type listType = new TypeToken<List<CreditoVO>>() {
		}.getType();
		listaCreditos = mapper.map(creditos, listType);
		return listaCreditos;

	}

	public Credito getCreditoById(String idCredito) {
		return credRepository.findOne(idCredito);
	}

	public CreditoVO getCreditoVOById(String idCredito) {
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());

		CreditoVO retVal = mapper.map(getCreditoById(idCredito), CreditoVO.class);
		return retVal;
	}

	@Transactional
	public void operacionesMes(Date periodo) {
		DateRangeVO mes = utServices.getMonthDates(periodo);
		// Créditos
		ArrayList<IndicadorOperacionesCliente> operaciones = new ArrayList<IndicadorOperacionesCliente>();
		List<IndicadorOperacionesClienteVO> creditos = credRepository.findCreditosClienteMes(mes.getStartDate(),
				mes.getEndDate());
		for (IndicadorOperacionesClienteVO cliente : creditos) {
			Cliente theClient = clienteRepository.findOne(cliente.getIdCliente());
			ModelMapper mapper = new ModelMapper();
			IndicadorOperacionesCliente dbIndicadorOperacionesCliente = mapper.map(cliente,
					IndicadorOperacionesCliente.class);
			dbIndicadorOperacionesCliente.setPeriodo(mes.getStartDate());
			dbIndicadorOperacionesCliente.setTipoOperacion("Creditos");
			dbIndicadorOperacionesCliente.setNombreCliente(theClient.getApellidoPaterno() + " "
					+ theClient.getApellidoMaterno() + " " + theClient.getNombre());
			operaciones.add(dbIndicadorOperacionesCliente);
		}
		List<IndicadorOperacionesClienteVO> pagos = pagoRepository.findPagosClienteMes(mes.getStartDate(),
				mes.getEndDate());
		for (IndicadorOperacionesClienteVO cliente : pagos) {
			Cliente theClient = clienteRepository.findOne(cliente.getIdCliente());
			ModelMapper mapper = new ModelMapper();
			IndicadorOperacionesCliente dbIndicadorOperacionesCliente = mapper.map(cliente,
					IndicadorOperacionesCliente.class);
			dbIndicadorOperacionesCliente.setPeriodo(mes.getStartDate());
			dbIndicadorOperacionesCliente.setTipoOperacion("Pagos");
			dbIndicadorOperacionesCliente.setNombreCliente(theClient.getApellidoPaterno() + " "
					+ theClient.getApellidoMaterno() + " " + theClient.getNombre());
			operaciones.add(dbIndicadorOperacionesCliente);
		}

		iocRepository.save(operaciones);
	}

	public List<CreditoVO> detalleCartera(FiltrosOrgVO filtros) {

		List<Credito> creditos = new ArrayList<Credito>();
		List<CreditoVO> creditosVOsana = new ArrayList<CreditoVO>();
		List<CreditoVO> creditosVOpreventiva = new ArrayList<CreditoVO>();
		List<CreditoVO> creditosVOriesgosa = new ArrayList<CreditoVO>();
		List<CreditoVO> creditosVOvencida = new ArrayList<CreditoVO>();
		List<CreditoVO> creditosVOinco = new ArrayList<CreditoVO>();
		List<CreditoVO> allCreditosVO = new ArrayList<CreditoVO>();
		ArrayList<String> rutas = catServices.filtraRutas(filtros);
		if (rutas.isEmpty()) {
			// si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if (gerencias.isEmpty()) {
				// si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if (regiones.isEmpty()) {
					// si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if (divisiones.isEmpty()) {
						// no puso ningún filtro---> obtengo todas las rutas
						return null;
						/*
						 * List<Organizacion> nodos =
						 * orgRepository.findByTipoNodo("RR"); for (Organizacion
						 * oneItem: nodos) { rutas.add(oneItem.getIdOrg()); }
						 */
					} else {
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for (String idDivision : divisiones) {
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for (Organizacion region : lregiones) {
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for (Organizacion gerencia : lgerencias) {
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for (Organizacion ruta : lrutas) {
							rutas.add(ruta.getIdOrg());
						}
					}
				} else {
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for (String idRegion : regiones) {
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for (Organizacion gerencia : lgerencias) {
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for (Organizacion ruta : lrutas) {
						rutas.add(ruta.getIdOrg());
					}
				}
			} else {
				// recupero las rutas de las gerencias
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for (Organizacion ruta : lrutas) {
					rutas.add(ruta.getIdOrg());
				}

			}

		}

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		Type listType = new TypeToken<List<CreditoVO>>() {
		}.getType();
		Organizacion ruta;
		// Vamos por los creditos de Cartera Normal
		List<Credito> sana = credRepository.carteraDetalleByRutasAndRangos(rutas, 0, 1);
		creditosVOsana = mapper.map(sana, listType);
		for (CreditoVO credito : creditosVOsana) {
			credito.setTipoCartera("sana");
			ruta = orgRepository.findByNombre(credito.getRuta()).get(0);
			credito.setGerencia(ruta.getParent().getNombre());
			credito.setRegion(ruta.getParent().getParent().getNombre());
			credito.setDivision(ruta.getParent().getParent().getParent().getNombre());
		}

		// Vamos por lo creditos preventiva
		List<Credito> preventiva = credRepository.carteraDetalleByRutasAndRangos(rutas, 2, 3);
		creditosVOpreventiva = mapper.map(preventiva, listType);
		for (CreditoVO credito : creditosVOpreventiva) {
			credito.setTipoCartera("preventiva");
			ruta = orgRepository.findByNombre(credito.getRuta()).get(0);
			credito.setGerencia(ruta.getParent().getNombre());
			credito.setRegion(ruta.getParent().getParent().getNombre());
			credito.setDivision(ruta.getParent().getParent().getParent().getNombre());
		}

		// Vamos por la creditos riesgosa
		List<Credito> riesgosa = credRepository.carteraDetalleByRutasAndRangos(rutas, 4, 9999);
		creditosVOriesgosa = mapper.map(riesgosa, listType);
		for (CreditoVO credito : creditosVOriesgosa) {
			credito.setTipoCartera("riesgosa");
			ruta = orgRepository.findByNombre(credito.getRuta()).get(0);
			credito.setGerencia(ruta.getParent().getNombre());
			credito.setRegion(ruta.getParent().getParent().getNombre());
			credito.setDivision(ruta.getParent().getParent().getParent().getNombre());
		}

		// Vamos creditos de cartera vencida
		List<Credito> vencida = credRepository.carteraDetalleByRutasVencida(rutas);
		creditosVOvencida = mapper.map(vencida, listType);
		for (CreditoVO credito : creditosVOvencida) {
			credito.setTipoCartera("vencida");
			ruta = orgRepository.findByNombre(credito.getRuta()).get(0);
			credito.setGerencia(ruta.getParent().getNombre());
			credito.setRegion(ruta.getParent().getParent().getNombre());
			credito.setDivision(ruta.getParent().getParent().getParent().getNombre());
		}

		// Vamos creditos de cartera incobrable
		List<Credito> incobrable = credRepository.carteraDetalleByRutasIncobrable(rutas);
		creditosVOinco = mapper.map(incobrable, listType);
		for (CreditoVO credito : creditosVOinco) {
			credito.setTipoCartera("inco");
			ruta = orgRepository.findByNombre(credito.getRuta()).get(0);
			credito.setGerencia(ruta.getParent().getNombre());
			credito.setRegion(ruta.getParent().getParent().getNombre());
			credito.setDivision(ruta.getParent().getParent().getParent().getNombre());
		}

		allCreditosVO.addAll(creditosVOsana);
		allCreditosVO.addAll(creditosVOpreventiva);
		allCreditosVO.addAll(creditosVOriesgosa);
		allCreditosVO.addAll(creditosVOvencida);
		allCreditosVO.addAll(creditosVOinco);

		// Ordenamos
		Collections.sort(allCreditosVO, new Comparator<CreditoVO>() {
			@Override
			public int compare(CreditoVO cred1, CreditoVO cred2) {
				return cred1.getRuta().compareTo(cred2.getRuta());
			}
		});

		return allCreditosVO;
	}

	public List<EstadoCarteraVO> calculoCartera(FiltrosOrgVO filtros) {
		Date desde = utServices.justDate(new Date());
		Date hasta = desde;
		DateRangeVO week = null;
		if (filtros.getFechaInicial() != null) {
			desde = utServices.convertStringToDate(filtros.getFechaInicial(), "yyyy-MM-dd");
			desde = utServices.justDate(desde);
		}
		if (filtros.getFechaFinal() != null) {
			hasta = utServices.convertStringToDate(filtros.getFechaFinal(), "yyyy-MM-dd");
			hasta = utServices.justDate(hasta);
		}
		if (hasta == desde)
			week = utServices.getWeekDates(desde);
		else {
			week = new DateRangeVO();
			week.setStartDate(desde);
			week.setEndDate(hasta);
		}
		HashMap<String, EstadoCarteraVO> resumen = new HashMap<String, EstadoCarteraVO>();
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		if (rutas.isEmpty()) {
			// si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if (gerencias.isEmpty()) {
				// si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if (regiones.isEmpty()) {
					// si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if (divisiones.isEmpty()) {
						// no puso ningún filtro---> obtengo todas las rutas
						return null;
						/*
						 * List<Organizacion> nodos =
						 * orgRepository.findByTipoNodo("RR"); for (Organizacion
						 * oneItem: nodos) { rutas.add(oneItem.getIdOrg()); }
						 */

					} else {
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for (String idDivision : divisiones) {
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for (Organizacion region : lregiones) {
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for (Organizacion gerencia : lgerencias) {
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for (Organizacion ruta : lrutas) {
							rutas.add(ruta.getIdOrg());
						}
					}
				} else {
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for (String idRegion : regiones) {
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for (Organizacion gerencia : lgerencias) {
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for (Organizacion ruta : lrutas) {
						rutas.add(ruta.getIdOrg());
					}
				}
			} else {
				// recupero las rutas de las gerencias
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for (Organizacion ruta : lrutas) {
					rutas.add(ruta.getIdOrg());
				}

			}

		}

		// Vamos por los datos de Cartera Normal
		List<EstadoCarteraVO> sana = credRepository.carteraByRutasAndRangos(rutas, 0, 1);
		for (EstadoCarteraVO unaRuta : sana) {
			EstadoCarteraVO ruta = new EstadoCarteraVO();
			ruta.setRuta(unaRuta.getRuta());
			ruta.setGerencia(unaRuta.getGerencia());
			ruta.setNumCarteraSana(unaRuta.getContador());
			ruta.setCarteraSana(unaRuta.getMonto());
			ruta.setDivision(unaRuta.getDivision());
			ruta.setRegion(unaRuta.getRegion());
			resumen.put(unaRuta.getRuta(), ruta);
		}

		// Vamos por lo cartera preventiva
		List<EstadoCarteraVO> preventiva = credRepository.carteraByRutasAndRangos(rutas, 2, 3);
		for (EstadoCarteraVO unaRuta : preventiva) {
			EstadoCarteraVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem == null) {
				oneItem = new EstadoCarteraVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setNumCarteraPreventiva(unaRuta.getContador());
			oneItem.setCarteraPreventiva(unaRuta.getMonto());
		}
		// Vamos por la cartera riesgosa
		List<EstadoCarteraVO> riesgosa = credRepository.carteraByRutasAndRangos(rutas, 4, 9999);
		for (EstadoCarteraVO unaRuta : riesgosa) {
			EstadoCarteraVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem == null) {
				oneItem = new EstadoCarteraVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setNumCarteraRiesgo(unaRuta.getContador());
			oneItem.setCarteraRiesgo(unaRuta.getMonto());
		}

		// Vamos por la cartera vencida
		List<EstadoCarteraVO> vencida = credRepository.carteraByRutasVencida(rutas);
		for (EstadoCarteraVO unaRuta : vencida) {
			EstadoCarteraVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem == null) {
				oneItem = new EstadoCarteraVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setNumCarteraVencida(unaRuta.getContador());
			oneItem.setCarteraVencida(unaRuta.getMonto());
		}
		// Vamor por cartera incobrable
		List<EstadoCarteraVO> inco = credRepository.carteraByRutasIncobrable(rutas);
		for (EstadoCarteraVO unaRuta : inco) {
			EstadoCarteraVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem == null) {
				oneItem = new EstadoCarteraVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setNumCarteraIncobrable(unaRuta.getContador());
			oneItem.setCarteraIncobrable(unaRuta.getMonto());
		}
		return Lists.newArrayList(resumen.values());
	}

	@Transactional
	public void aplicaResolucion() {
		System.out.println("Storing resume ...");
	}

	@Transactional
	public void aplicaResolucionGasto() {
		System.out.println("Storing resume ...");
	}

	@Transactional
	public CreditoBRVO aplicarCreditoAprobado(CreditoVO credito, String userName, String idOrgCaja) {
		CreditoBRVO retVal = new CreditoBRVO();
		retVal.setMotivoRechazo("Se autorizó el crédito");
		Credito creditoDB = credRepository.findOne(credito.getIdCredito());
		if (!creditoDB.getEstatusCredito().getClaveInterna().equals("CRED_PENDIENTE_ENT")) {
			retVal.setMotivoRechazo("El crédito no está pendiente de entrega");
			retVal.setEstatusCredito(creditoDB.getEstatusCredito());
			return retVal;
		}
		Catalogo catStatus = catServices.getCatalogoCveInterna("CRED_ACTIVO");
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");

		int activos = (int) catServices.getParametroByName("CREDITOS_ACTIVOS").getValorNum();
		int creditosActivos = brCredito.creditosActivos(credito.getIdCliente(), credito.getIdCredito());
		if (creditosActivos >= activos) {
			catStatus = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			retVal.setEstatusCredito(catStatus);
			retVal.setMotivoRechazo((String.format("No puede tener mas de %d créditos activos", activos)));
		}
		creditoDB.setEstatusCredito(catStatus);
		credRepository.save(creditoDB);

		Catalogo cargoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo desembolso = catServices.getCatalogoCveInterna("TIPO_OPE_CREDITO");
		Organizacion cajaAfectada = orgRepository.findByIdOrg(idOrgCaja);
		// Movimientos de Caja
		MovimientoCaja mc = new MovimientoCaja();
		// FIXME: Nombre del autorizador
		mc.setAutorizador(userName);
		mc.setFechaEmision(creditoDB.getFechaEmision());
		mc.setFechaSistema(new Date());
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo(credito.getIdDispositivo());
		mc.setIdUsuario(creditoDB.getRuta().getTitular().getNombreUsuario());
		mc.setCargo(credito.getMonto());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(credito.getIdCredito());
		// mc.setRuta(creditoDB.getRuta());
		mc.setRuta(cajaAfectada);
		mc.setTipoMovimiento(cargoCaja);
		mc.setTipoOperacion(desembolso);
		mc.setReferenciaLarga(
				creditoDB.getCliente().getNombreCompleto() + " (" + creditoDB.getCliente().getRuta().getNombre() + ")");
		mc.setEstatusMovimiento(statusAprobado);
		registraMovCaja(mc);

		return retVal;
	}

	CreditoBRVO result = new CreditoBRVO();

	@Transactional
	public CreditoBRVO aprobarCredito(CreditoVO credito, String userName) {
		CreditoBRVO retVal = new CreditoBRVO();
		retVal.setMotivoRechazo("Se autorizó el crédito");
		Credito creditoDB = credRepository.findOne(credito.getIdCredito());
		if (!creditoDB.getEstatusCredito().getClaveInterna().equals("CRED_PENDIENTE")) {
			completeCreditApproval(credito, userName, false);
			retVal.setEstatusCredito(creditoDB.getEstatusCredito());
			return retVal;
		}
		boolean continueProcess = true;
		int activos = (int) catServices.getParametroByName("CREDITOS_ACTIVOS").getValorNum();
		int creditosActivos = brCredito.creditosActivos(credito.getIdCliente(), credito.getIdCredito());
		int creditosPendEnt = brCredito.creditosPorEstatus(credito.getIdCliente(), credito.getIdCredito(),
				"CRED_PENDIENTE_ENT");
		Catalogo catStatus = null;
		if (creditosActivos >= activos) {
			catStatus = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			retVal.setEstatusCredito(catStatus);
			retVal.setMotivoRechazo((String.format("No puede tener mas de %d créditos activos", activos)));
			continueProcess = false;
		}
		if (creditosPendEnt > 0) {
			catStatus = catServices.getCatalogoCveInterna("CRED_RECHAZADO");
			retVal.setEstatusCredito(catStatus);
			retVal.setMotivoRechazo(
					(String.format("Ya tiene %d créditos autorizados y pendientes de entrega", creditosPendEnt)));
			continueProcess = false;
		}
		String estatus = credito.isAprobado() ? "CRED_PENDIENTE_ENT" : "CRED_RECHAZADO";
		// Validamos si el monto para autorizar créditos es suficiente o
		// requiere otra autorización
		if (credito.isAprobado()) {
			double montoMaximo = brCredito.montoAutorizadoCredito(userName);
			estatus = montoMaximo >= credito.getMonto() ? "CRED_PENDIENTE_ENT" : "CRED_PENDIENTE";
			continueProcess = (credito.getMonto() > montoMaximo);
		} else {
			continueProcess = false;
		}

		catStatus = catServices.getCatalogoCveInterna(estatus);
		creditoDB.setEstatusCredito(catStatus);
		credRepository.save(creditoDB);
		completeCreditApproval(credito, userName, continueProcess);
		return retVal;
	}

	@Transactional
	private void completeCreditApproval(CreditoVO credito, String userName, boolean continueProcess) {
		String taskId = null;
		if (credito.getTaskId() != null) {
			taskId = credito.getTaskId();
		} else {
			TaskVO taskVO = bpmServices.getTaskByVarValue("idCredito", credito.getIdCredito());
			taskId = taskVO.getId();
		}
		bpmServices.setVariableValue(taskId, "continueProcess", continueProcess);
		bpmServices.completeTask(taskId, userName);
	}

	public List<String> findManagerForCredit(String idCredito) {
		Credito theCredit = this.getCreditoById(idCredito);
		theCredit.setRolAprobador("ROL_GERENTE");
		credRepository.save(theCredit);
		Organizacion ruta = theCredit.getCliente().getRuta();
		return findManagersFor(ruta);
	}

	public List<String> findRegionalForCredit(String idCredito) {
		Credito theCredit = this.getCreditoById(idCredito);
		theCredit.setRolAprobador("ROL_REGIONAL");
		credRepository.save(theCredit);
		Organizacion ruta = theCredit.getCliente().getRuta().getParent();
		return findManagersFor(ruta);
	}

	public List<String> findDivForCredit(String idCredito) {
		Credito theCredit = this.getCreditoById(idCredito);
		Organizacion ruta = theCredit.getCliente().getRuta().getParent().getParent();
		theCredit.setRolAprobador("ROL_DIVISIONAL");
		credRepository.save(theCredit);
		return findManagersFor(ruta);
	}

	public List<String> findCentralForCredit(String idCredito) {
		Credito theCredit = this.getCreditoById(idCredito);
		Organizacion ruta = theCredit.getCliente().getRuta().getParent().getParent();
		theCredit.setRolAprobador("ROL_SOPORTE_OP");
		credRepository.save(theCredit);
		return null;
	}

	public List<String> findManagerForExpense(String idMovCaja) {
		MovimientoCaja movCaja = mcRepository.findOne(idMovCaja);
		Organizacion ruta = movCaja.getRuta();
		return findManagersFor(ruta);
	}

	public List<String> findManagersFor(Organizacion source) {
		ArrayList<String> candidates = new ArrayList<String>();
		if (source.getParent().getTitular() != null)
			candidates.add(source.getParent().getTitular().getNombreUsuario());
		if (source.getParent().getSuplente() != null)
			candidates.add(source.getParent().getSuplente().getNombreUsuario());
		if (candidates.isEmpty())
			candidates.add("ROL_SOPORTE_OP");
		return candidates;
	}

	public List<String> findTransferApprover(String username) {
		ArrayList<String> candidates = new ArrayList<String>();
		candidates.add(username);
		return candidates;
	}

	public PagoVO loadPago(String idPago) {
		Pago pago = pagoRepository.findOne(idPago);
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new PagoToVOModelMapper());
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		PagoVO retVal = mapper.map(pago, PagoVO.class);
		return retVal;

	}

	@Transactional
	public void deletePago(Principal principal, String idPago) {

		Catalogo cargoCaja = catServices.getCatalogoCveInterna("MOV_CAJA_CARGO");
		Catalogo egresoCancelacion = catServices.getCatalogoCveInterna("TIPO_OPE_EGR_CANC_PAGO");
		Catalogo egresoCancelacionCom = catServices.getCatalogoCveInterna("TIPO_OPE_EGR_CANC_PAGO_COM");
		Catalogo statusAprobado = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");

		Pago pago = pagoRepository.findOne(idPago);
		List<TablaAmortizacion> pagados = taRepository.findByPagoIdPago(idPago);
		Catalogo pendiente = catServices.getCatalogoCveInterna("PAGO_PENDIENTE");
		double abonoCapital = 0;
		for (TablaAmortizacion item : pagados) {
			item.setEstatusPago(pendiente);
			item.setPago(null);
			abonoCapital += item.getCapital();
		}
		taRepository.save(pagados);
		Credito credito = pago.getCredito();
		credito.setSaldo(credito.getSaldo() + (pago.getAbono() - pago.getPagoAdicional()));
		credito.setSaldoInsoluto(credito.getSaldoInsoluto() + abonoCapital);
		credito.setSaldoComision(credito.getSaldoComision() + pago.getComision());
		credito.setPagoPendienteAplicar(credito.getPagoPendienteAplicar() - pago.getPagoAdicional());
		credito.setTotPagos(credito.getTotPagos() - 1);
		credRepository.save(credito);
		MovimientoCaja mc = new MovimientoCaja();
		Date fecOpe = new Date();
		// FIXME: Nombre del autorizador
		mc.setAutorizador("system");
		mc.setFechaEmision(fecOpe);
		mc.setFechaSistema(fecOpe);
		// FIXME: Id del Dispositivo
		mc.setIdDispositivo("WebApp");
		mc.setIdUsuario(principal.getName());
		mc.setCargo(pago.getAbono());
		// FIXME: Numero de recibo o Credito
		mc.setReferencia(pago.getIdPago());
		mc.setRuta(pago.getCredito().getRuta());
		mc.setTipoMovimiento(cargoCaja);
		mc.setTipoOperacion(egresoCancelacion);
		mc.setReferenciaLarga(pago.getCredito().getCliente().getNombreCompleto());
		mc.setEstatusMovimiento(statusAprobado);
		registraMovCaja(mc);
		if (pago.getComision() > 0) {
			mc = new MovimientoCaja();
			// FIXME: Nombre del autorizador
			mc.setAutorizador("system");
			mc.setFechaEmision(fecOpe);
			mc.setFechaSistema(fecOpe);
			// FIXME: Id del Dispositivo
			mc.setIdDispositivo("WebApp");
			mc.setIdUsuario(principal.getName());
			mc.setCargo(pago.getComision());
			// FIXME: Numero de recibo o Credito
			mc.setReferencia(pago.getIdPago());
			mc.setRuta(pago.getCredito().getRuta());
			mc.setTipoMovimiento(cargoCaja);
			mc.setTipoOperacion(egresoCancelacionCom);
			mc.setReferenciaLarga(pago.getCredito().getCliente().getNombreCompleto());
			mc.setEstatusMovimiento(statusAprobado);
			registraMovCaja(mc);
		}

	}

	/**
	 * Guarda el documento de transferecia y su referencia
	 * 
	 * @param fondeo
	 * @throws IOException
	 */
	private void sendFileTransfer(final FondeoVO fondeo) throws IOException {
		if (fondeo.getFile() != null) {
			final String fileName = fondeo.getFile().getOriginalFilename();
			final byte[] file = fondeo.getFile().getBytes();
			final String typeFile = fondeo.getFile().getContentType();
			// Se crea el documento de la transferencia
			Documento documento = new Documento();
			documento.setNombre(fileName);
			documento.setDescripcion(fileName);
			documento.setFileName(FileUtils.stampWithTime(fileName, null));
			documento.setContentType(typeFile);
			documento.setFechaCreacion(new Date());
			// Se agregan las referencias del documento
			this.addPropieties(fondeo, documento);
			// Se almacena el documento físico
			final FileManager manager = this.fileManagerFactory.createFileManager();
			manager.save(file, null, documento.getFileName(), typeFile);
			// Se guarda la referencia del documento
			documentoRepository.save(documento);
			fondeo.setIdDocto(documento.getIdDocumento());
		}
	}

	/**
	 * Agrega las propiedades al documento
	 * 
	 * @param fondeo
	 * @param docto
	 */
	private void addPropieties(final FondeoVO fondeo, final Documento docto) {
		final List<PropiedadesDoc> props = new ArrayList<PropiedadesDoc>();
		final PropiedadesDoc doctoType = new PropiedadesDoc();
		final PropiedadesDoc origin = new PropiedadesDoc();
		props.add(doctoType);
		props.add(origin);
		doctoType.setNombre(Constantes.TIPO_DOCUMENTO);
		doctoType.setDocumento(docto);
		origin.setDocumento(docto);
		if (fondeo.getTipoMov().equals("transfer")) {
			// Trasferencia
			doctoType.setValor("transferencia");
			origin.setNombre(Constantes.IDORG);
			origin.setValor(fondeo.getIdMC());
			final PropiedadesDoc destination = new PropiedadesDoc();
			destination.setDocumento(docto);
			destination.setNombre(Constantes.IDORG);
			destination.setValor(fondeo.getIdMCDestino());
			props.add(destination);
		} else {
			final Catalogo catDoc = catRepository.findByIdCatalogo(fondeo.getIdTipoGasto());
			// Gasto
			doctoType.setValor(catDoc.getNombre());
			origin.setNombre(Constantes.IDGASTO);
			origin.setValor(fondeo.getIdMC());
		}
		docto.setPropiedadesDocs(props);
	}

	/**
	 * Dado un listado de movimientos Vo se busca su respectivo documento
	 * relacionado
	 */
	private void setDocumentoToMovCaja(final List<MovimientoCajaVO> movimientos) {
		// Se obtienen los Id de los Documentos aspociados a los movimientos de
		// caja y trasferencia
		if (movimientos != null && movimientos.size() > 0) {
			List<String> idsTipoMov = catServices
					.getCatalogoIdsByClavesInternas(Arrays.asList("MOV_CAJA_CARGO", "MOV_CAJA_ABONO"));
			List<String> idsTipoOp = catServices.getCatalogoIdsByClavesInternas(
					Arrays.asList("TIPO_OPE_GASTO", "TIPO_OPE_EGR_TRANS", "TIPO_OPE_ING_TRANS"));
			// Se filtran los movimientos por tipo de movimiento igual a gasto o
			// trasferencia
			final Collection<MovimientoCajaVO> mcGastoTrans = Collections2.filter(movimientos,
					new Predicate<MovimientoCajaVO>() {
						@Override
						public boolean apply(MovimientoCajaVO mc) {
							return idsTipoOp.contains(mc.getIdTipoOperacion())
									&& idsTipoMov.contains(mc.getIdTipoMov());
						}
					});

			// Se obtienen los Id movimiento caja de los movimientos filtrados
			if (mcGastoTrans != null) {
				final Iterable<String> idsMovCaja = Iterables.transform(mcGastoTrans,
						new Function<MovimientoCajaVO, String>() {
							@Override
							public String apply(MovimientoCajaVO mc) {
								return (mc == null) ? null : mc.getIdMovCaja();
							}
						});
				// Se obtienen los id de los documentos
				if (idsMovCaja != null) {
					List<PropiedadesDoc> propiedades = propsDoctoRepository
							.findByValorIn(Lists.newArrayList(idsMovCaja));

					for (MovimientoCajaVO mc : mcGastoTrans) {
						PropiedadesDoc propiedad = Iterables.find(propiedades, new Predicate<PropiedadesDoc>() {
							@Override
							public boolean apply(PropiedadesDoc input) {
								return (input.getValor() != null && input.getValor().equals(mc.getIdMovCaja()));
							}
						}, null);
						if (propiedad != null) {
							mc.setIdDocto(propiedad.getDocumento().getIdDocumento());
						}
					}
				}
			}
		}
	}

	@Transactional
	public void calculaCarteraIncobrable() {
		Date today = new Date();
		Date fechaBase = utServices.getBaseVencimiento(utServices.justDate(today), 180, false);
		String estatus = "CRED_ACTIVO";
		Catalogo estatusInc = catServices.getCatalogoCveInterna("CRED_INCOBRABLE");
		List<Credito> incobrables = credRepository.findByFechaVencimientoBeforeAndEstatusCreditoClaveInterna(fechaBase,
				estatus);
		ArrayList<Credito> lista = new ArrayList<Credito>();
		for (Credito oneItem : incobrables) {
			oneItem.setEstatusCredito(estatusInc);
			oneItem.setFechaCancelacion(today);
			lista.add(oneItem);
		}
		credRepository.save(lista);
	}

	public List<IndicadorCarteraDiarioVO> getIndicadorDiarioCarteraRaw(FiltrosOrgVO filtros, String dia) {
		Date fechaProceso = utServices.justDate(new Date());
		List<IndicadorCarteraDiarioVO> result = null;
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());

		if (rutas.size() > 0) {
			result = icrRepository.findByCarteraRutaDiaRaw(dia, rutas);
		} /*
			 * else if (gerencias.size()>0) { result =
			 * credRepository.findCarteraVencidaByGerencias(gerencias,
			 * fechaProceso); }else if(regiones.size()>0) { result =
			 * credRepository.findCarteraVencidaByRegiones(regiones,
			 * fechaProceso); }else if(divisiones.size()>0) { result =
			 * credRepository.findCarteraVencidaByDivisiones(divisiones,
			 * fechaProceso); }else { result =
			 * credRepository.findCarteraVencidaAll(fechaProceso); }
			 */
		return result;
	}

	public List<IndicadorCarteraDiarioVO> getIndicadorDiarioCartera(FiltrosOrgVO filtros, DateRangeVO week) {
		Date fechaProceso = utServices.justDate(new Date());
		List<IndicadorCarteraDiarioVO> result = null;
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());

		switch (filtros.getPeriodo()) {
		case "dia":
			filtros.setNomPeriodo("dia_semana");
			break;
		case "semana":
			filtros.setNomPeriodo("semana");
			break;
		case "mes":
			filtros.setNomPeriodo("mes");
			break;
		case "trimestre":
			filtros.setNomPeriodo("trimestre");
			break;
		case "semestre":
			filtros.setNomPeriodo("semestre");
			break;
		case "anio":
			filtros.setNomPeriodo("anio");
			break;
		default:
			filtros.setNombre("dia_semana");
			filtros.setPeriodo("dia");
			break;
		}

		filtros.setPeriodo("'" + filtros.getPeriodo() + "'");

		if (rutas.size() > 0) {
			result = icrRepository.findByCarteraRutaDia(week.getStartDate(), week.getEndDate(), rutas);
		} /*
			 * else if (gerencias.size()>0) { result =
			 * credRepository.findCarteraVencidaByGerencias(gerencias,
			 * fechaProceso); }else if(regiones.size()>0) { result =
			 * credRepository.findCarteraVencidaByRegiones(regiones,
			 * fechaProceso); }else if(divisiones.size()>0) { result =
			 * credRepository.findCarteraVencidaByDivisiones(divisiones,
			 * fechaProceso); }else { result =
			 * credRepository.findCarteraVencidaAll(fechaProceso); }
			 */
		return result;
	}

	public List<IndicadorCarteraDiarioVO> getIndicadorDiarioCartera() {
		// return icrRepository.findByToday();
		return Lists.newArrayList(icrRepository.findByToday());
	}

	@Transactional
	public void saveIndicadorDiarioCartera() {
		List<IndicadorCarteraDiarioVO> indicador = getIndicadorDiarioCartera();

		for (IndicadorCarteraDiarioVO oneItem : indicador) {
			oneItem.setDia(utServices.diaSemana(oneItem.getFecha()));
			oneItem.setSemana(utServices.semanaAnio(oneItem.getFecha()));
			oneItem.setMes(utServices.mesAnio(oneItem.getFecha()));
			oneItem.setTrimestre(utServices.trimestre(oneItem.getFecha()));
			oneItem.setSemestre(utServices.semestre(oneItem.getFecha()));
			oneItem.setAnio(utServices.anio(oneItem.getFecha()));
		}

		ModelMapper mapper = new ModelMapper();
		Type listType = new TypeToken<List<IndicadorCarteraDiario>>() {
		}.getType();
		List<IndicadorCarteraDiario> listaIndicador = mapper.map(indicador, listType);
		icrRepository.save(listaIndicador);
	}

	@Transactional
	public void cancelarTransferencia(FondeoVO fondeo) throws Exception {
		// Movimiento caja a cancelar
		MovimientoCaja movCancelar = mcRepository.findByIdMovCaja(fondeo.getIdMC());
		MovimientoCaja movCruze = mcRepository.findByIdMovCaja(movCancelar.getMovcruze());
		MovimientoCaja movNEmisor = new MovimientoCaja();
		MovimientoCaja movNReceptor = new MovimientoCaja();

		Catalogo tipoOperacionEgreso = catRepository.findByClaveInterna("TIPO_OPE_EGR_CANC_TRANS");
		Catalogo tipoOperacionIngreso = catRepository.findByClaveInterna("TIPO_OPE_ING_CANC_TRANS");
		Catalogo tipoMovimientoAbono = catRepository.findByClaveInterna("MOV_CAJA_ABONO");
		Catalogo tipoMovimientoCargo = catRepository.findByClaveInterna("MOV_CAJA_CARGO");
		Catalogo estatus_can = catServices.getCatalogoCveInterna("ESTATUS_CANCELADO");

		if (movCancelar.getTipoOperacion().getNombre().equals("Ingreso por transferencia")) {
			movNEmisor.setRuta(movCruze.getRuta());
		} else {
			movNEmisor.setRuta(movCancelar.getRuta());
		}

		// Insertar movimiento en caja egreso (Ingreso)

		movNEmisor.setTipoMovimiento(tipoMovimientoAbono);
		movNEmisor.setTipoOperacion(tipoOperacionIngreso);// ingreso
		movNEmisor.setTipoGasto(movCancelar.getTipoGasto());
		movNEmisor.setEstatusMovimiento(movCancelar.getEstatusMovimiento());
		movNEmisor.setReferencia(movCancelar.getReferencia());
		movNEmisor.setAutorizador("system");
		movNEmisor.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		movNEmisor.setFechaSistema(new Date());
		movNEmisor.setCargo(0);
		movNEmisor.setAbono(movCancelar.getAbono());
		movNEmisor.setPendiente(0);
		movNEmisor.setIdUsuario(fondeo.getUsuario());
		movNEmisor.setIdDispositivo(fondeo.getIdDispositivo());
		movNEmisor.setReferenciaLarga(movCancelar.getReferenciaLarga());
		movNEmisor.setObservaciones(fondeo.getDescripcion());
		registraMovCaja(movNEmisor);

		// Insertar movimiento en caja Receptor (Egreso)
		movNReceptor.setRuta(movCancelar.getRuta());
		movNReceptor.setTipoMovimiento(tipoMovimientoCargo);
		movNReceptor.setTipoOperacion(tipoOperacionEgreso);// egreso por
															// cancelación de
															// transferencia
		movNReceptor.setTipoGasto(movCancelar.getTipoGasto());
		movNReceptor.setEstatusMovimiento(movCancelar.getEstatusMovimiento());
		movNReceptor.setReferencia(movCancelar.getReferencia());
		movNReceptor.setAutorizador("system");
		movNReceptor.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		movNReceptor.setFechaSistema(new Date());
		movNReceptor.setCargo(movCancelar.getAbono());
		movNReceptor.setAbono(0);
		movNReceptor.setPendiente(0);
		movNReceptor.setIdUsuario(fondeo.getUsuario());
		movNReceptor.setIdDispositivo(fondeo.getIdDispositivo());
		movNReceptor.setReferenciaLarga(movCancelar.getReferenciaLarga());
		movNReceptor.setObservaciones(fondeo.getDescripcion());
		registraMovCaja(movNReceptor);

		// Actualizar movimiento en caja Receptor
		movCancelar.setEstatusMovimiento(estatus_can);
		movCancelar.setAbono(0);
		movCancelar.setCargo(0);
		mcRepository.save(movCancelar);
		// Actualizar moviimiento en caja Emisor
		movCruze.setEstatusMovimiento(estatus_can);
		movCruze.setAbono(0);
		movCruze.setCargo(0);
		mcRepository.save(movCruze);

	}

	@Transactional
	public void cancelaCredito(CreditoVO creditoVO) throws Exception {
		Catalogo estatusCancelado = catServices.getCatalogoCveInterna("CRED_CANCELADO");
		Catalogo motivo = catServices.getCatalogoId(creditoVO.getMotivoCancelacion());
		// Obtener el credito a cancelar
		Credito credito = credRepository.findByIdCredito(creditoVO.getIdCredito()).get(0);
		credito.setEstatusCredito(estatusCancelado);
		credito.setFechaCancelacion(utServices.convertStringToDate(creditoVO.getFechaCancelacion(), "yyyy-MM-dd"));
		credito.setMotivoCancelacion(motivo);
		credito.setObservaciones(creditoVO.getObservaciones());
		credRepository.save(credito);

	}

	@Transactional
	public void cancelaGasto(FondeoVO fondeo) throws Exception {
		Catalogo tipoMovimiento = catServices.getCatalogoCveInterna("MOV_CAJA_ABONO");// abono
		Catalogo tipoOperacion = catServices.getCatalogoCveInterna("TIPO_OPE_ING_CANC_GASTO");
		Catalogo estatus = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		Catalogo estatus_can = catServices.getCatalogoCveInterna("ESTATUS_CANCELADO");

		// Movimiento caja a cancelar
		MovimientoCaja movOriginal = mcRepository.findByIdMovCaja(fondeo.getIdMC());

		// Moviiento caja a insertar
		MovimientoCaja mc = new MovimientoCaja();
		mc.setRuta(movOriginal.getRuta());
		mc.setTipoMovimiento(tipoMovimiento);
		mc.setTipoOperacion(tipoOperacion);
		mc.setTipoGasto(movOriginal.getTipoGasto());
		mc.setEstatusMovimiento(movOriginal.getEstatusMovimiento());
		mc.setReferencia(movOriginal.getReferencia());
		mc.setAutorizador("system");
		mc.setFechaEmision(utServices.convertStringToDate(fondeo.getFechaEmision(), "yyyy-MM-dd"));
		mc.setFechaSistema(new Date());
		mc.setCargo(0);
		mc.setAbono(movOriginal.getCargo());
		mc.setPendiente(0);
		mc.setIdUsuario(fondeo.getUsuario());
		mc.setIdDispositivo(fondeo.getIdDispositivo());
		mc.setReferenciaLarga(movOriginal.getReferenciaLarga());
		mc.setObservaciones(fondeo.getDescripcion());
		registraMovCaja(mc);
		// Cambiamos estatus movimiento original
		movOriginal.setEstatusMovimiento(estatus_can);
		mcRepository.save(movOriginal);

	}

	public Collection<CreditoVO> filtrarCreditosRecompra(FiltrosOrgVO filtros) {
		Collection<Credito> creditos = null;
		List<CreditoVO> listaCreditos = null;

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		if (rutas.isEmpty()) {
			// si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if (gerencias.isEmpty()) {
				// si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if (regiones.isEmpty()) {
					// si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if (divisiones.isEmpty()) {
						// no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem : nodos) {
							rutas.add(oneItem.getIdOrg());
						}

					} else {
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for (String idDivision : divisiones) {
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for (Organizacion region : lregiones) {
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for (Organizacion gerencia : lgerencias) {
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for (Organizacion ruta : lrutas) {
							rutas.add(ruta.getIdOrg());
						}
					}
				} else {
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for (String idRegion : regiones) {
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for (Organizacion gerencia : lgerencias) {
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for (Organizacion ruta : lrutas) {
						rutas.add(ruta.getIdOrg());
					}
				}
			} else {
				// recupero las rutas de las gerencias ok
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for (Organizacion ruta : lrutas) {
					rutas.add(ruta.getIdOrg());
				}

			}

		}

		Date fechaInicial = utServices.convertStringToDate(filtros.getFechaInicial().substring(0, 10) + " 00:00:00",
				"yyyy-MM-dd HH:mm:ss");
		Date fechaFinal = utServices.convertStringToDate(filtros.getFechaFinal().substring(0, 10) + " 23:59:59",
				"yyyy-MM-dd HH:mm:ss");

		if (rutas.size() > 0) {
			creditos = Lists.newArrayList(
					this.credRepository.findByClienteRutaIdOrgInAndFechaEmisionBetweenAndTipoCreditoClaveInternaEquals(
							rutas, fechaInicial, fechaFinal, "CRED_RECOMPRA"));

		} else {

		}

		// System.out.println("Prueba--->"+creditos.size());
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		Type listType = new TypeToken<List<CreditoVO>>() {
		}.getType();

		listaCreditos = mapper.map(creditos, listType);
		return listaCreditos;

	}

	public List<CreditoVO> carteraCreditosActivos(FiltrosOrgVO filtros) {
		List<Credito> creditos = null;
		List<CreditoVO> listaCreditos = null;

		String estatus = catRepository.findByClaveInterna("CRED_ACTIVO").getIdCatalogo();

		String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		String apellidoPaterno = StringUtils.isEmpty(filtros.getApellidoPaterno()) ? "%"
				: filtros.getApellidoPaterno() + "%";

		ArrayList<String> rutas = catServices.filtraRutas(filtros);
		if (rutas.size() > 0) {
			creditos = (this.credRepository
					.findByClienteRutaIdOrgInAndAndClienteNombreLikeAndClienteApellidoPaternoLikeAndEstatusCreditoIdCatalogo(
							rutas, nombre, apellidoPaterno, estatus));
		} else {
			creditos = (this.credRepository
					.findByClienteNombreLikeAndClienteApellidoPaternoLikeAndEstatusCreditoIdCatalogo(nombre,
							apellidoPaterno, estatus));
		}

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		Type listType = new TypeToken<List<CreditoVO>>() {
		}.getType();
		listaCreditos = mapper.map(creditos, listType);
		return listaCreditos;

	}

	@Transactional
	public void unificaCreditos(String principalName, CreditosUnifVO input) throws Exception {

		Date fechaOperacion = utServices.convertStringToDate(input.getDestino().getFechaEmision(), "yyyy-MM-dd");
		Catalogo cancelado = catServices.getCatalogoCveInterna("CRED_CANCELADO");
		Catalogo motivo = catServices.getCatalogoCveInterna("CAT_MOTIVO_CANCEL_CRED_UNIF");
		List<CreditoVO> creditos = input.getCreditos();
		// Creamos el nuevo Crédito
		Credito newCredito = crearCredito(input.getDestino(), principalName, fechaOperacion);

		// Cancelamos créditos que se unficaron

		for (CreditoVO creditoOld : creditos) {
			Credito credUnif = credRepository.findOne((creditoOld.getIdCredito()));
			credUnif.setEstatusCredito(cancelado);
			credUnif.setFechaCancelacion(fechaOperacion);
			credUnif.setMotivoCancelacion(motivo);
			credRepository.save(credUnif);

		}

	}

}
