package com.crenx.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.br.NominaBRImpl;
import com.crenx.data.domain.entity.CreditoEmpleado;
import com.crenx.data.domain.entity.DescuentosCreditoEmpleado;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.IncidenciaNomina;
import com.crenx.data.domain.entity.IndicadorCobranzaPeriodo;
import com.crenx.data.domain.entity.NominaPeriodo;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.CreditoEmpleadoRepository;
import com.crenx.data.domain.repository.DescuentosCreditoEmpleadoRepository;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.IncidenciaNominaRepository;
import com.crenx.data.domain.repository.IndicadorCobranzaPeriodoRepository;
import com.crenx.data.domain.repository.IndicadorNegocioRepository;
import com.crenx.data.domain.repository.NominaPeriodoRepository;
import com.crenx.data.domain.vo.CreditoEmpleadoVO;
import com.crenx.data.domain.vo.CreditosNominaVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.FiltrosCredEmplVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;

@Service
public class NominaServices {

	@Autowired
	private IndicadorNegocioRepository inRepository;
	@Autowired
	private CreditoEmpleadoServices credEmpServices;
	@Autowired
	private EmpleadoRepository empRepository;
	@Autowired
	private NominaBRImpl nomBR;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private IndicadorCobranzaPeriodoRepository icpRepository;
	@Autowired
	private CreditoEmpleadoRepository credEmpRepository;
	@Autowired
	private NominaPeriodoRepository nomPerRepository;
	@Autowired
	private IncidenciaNominaRepository incNomPerRepository;
	@Autowired
	private DescuentosCreditoEmpleadoRepository descCredEmplRepository;
	
	
	@Transactional
	public void calculaNomina(DateRangeVO semana)
	{
		
		Date fechaProceso = new Date();
		ArrayList<String> messages = new ArrayList<String>();
		List<Empleado> empleados = empRepository.findByFechaBajaIsNull();
		long semanaAnio = utServices.semanaAnio(semana.getEndDate());
		FiltrosCredEmplVO filtros =  filtrosBasicos();
		for (Empleado oneEmp : empleados)
		{
			filtros.setIdEmpleado(oneEmp.getIdEmpleado());
			String cargoEmpleado = oneEmp.getCargoEmpleado().getClaveInterna();
			boolean calcSalario = nomBR.calculaPorSalario(cargoEmpleado);
			if (calcSalario)
				calculaSueldoBase(oneEmp, semana, semanaAnio, messages, filtros, fechaProceso);
			else
				calculaComision(oneEmp, semana, semanaAnio, messages, filtros, fechaProceso);
		}
		for (String message : messages)
		{
			IncidenciaNomina incNom = new IncidenciaNomina();
			incNom.setFechaProceso(fechaProceso);
			incNom.setSemana(semanaAnio);
			incNom.setReporte(message);
			incNomPerRepository.save(incNom);
			System.out.println(message);
		}
	}
	
	private FiltrosCredEmplVO filtrosBasicos()
	{
		FiltrosCredEmplVO filtros = new FiltrosCredEmplVO();
		filtros.setTiposCredito(catServices.getCatalogoIds("CAT_TIPO_CRED_EMPL"));
		ArrayList<String> reValue = new ArrayList<String>();
		reValue.add(catServices.getCatalogoCveInterna("CRED_ACTIVO").getIdCatalogo());
		filtros.setEstatusCredito(reValue);
		filtros.setNombre("%");
		filtros.setApellidoPaterno("%");
		return filtros;

	}
	
	
	private void calculaComision(Empleado emp, DateRangeVO semana, long semanaAnio, ArrayList<String> messages, 
			FiltrosCredEmplVO filtros, Date fechaProceso)
	{
		double semArranque = catServices.getParametroByName("SEMANA_ARRANQUE").getValorNum();
		double baseNomina = catServices.getParametroByName("BASE_NOM_ARRANQUE").getValorNum();
		
		semArranque+=emp.getSemanasGracia();
		//Calcular las semanas desde que inició
		Calendar c1 = Calendar.getInstance();
		c1.setTime(emp.getFechaIngreso());
		Calendar c2 = Calendar.getInstance();
		c1.setTime(semana.getStartDate());
		long semanas = utServices.getFullWeeks(c1,c2);
		Organizacion ruta = null;
		try
		{
			ruta = utServices.getRutaTitular(emp);
		}catch(Exception ex)
		{
			messages.add("El empleado: " + emp.getNombreCompleto() + " no está asignado a una ruta y es Ejecutivo CyC");
			return;
			//FIXME: Que hacemos??
		}
		List<IndicadorCobranzaPeriodo> indCob = icpRepository.findByRutaIdOrgAndSemana(ruta.getIdOrg(),semanaAnio);
		if (indCob.size()==0)
		{
			messages.add("El empleado: " + emp.getNombreCompleto() + " no tuvo actividad en el periodo, puede ser que no fue asignado a una ruta ");
			return;
			
		}
			
		IndicadorCobranzaPeriodo onePeriod = indCob.get(0);

		double factorCobranza = nomBR.factorCobranza(onePeriod.getFactorCobranza());
		double factorNormalidad = nomBR.factorNormalidad(onePeriod.getFactorNormalidad());
		double fatorColocacion = nomBR.factorColocacion()/100;
		double importeCobranza = (factorCobranza/100) * onePeriod.getImporteCobradoTotal();
		double importeNormalidad = (factorNormalidad/100) * onePeriod.getImporteCobradoTotal();
		double importeColocacion = fatorColocacion * onePeriod.getCarteraNueva();
		double importeTotal = importeCobranza + importeNormalidad + importeColocacion;
		double importeNomina = importeTotal;
		if (semArranque>semanas)
		{
			if (importeTotal<baseNomina)
				importeNomina = baseNomina;
		}
		
		NominaPeriodo nomPer = new NominaPeriodo();
		nomPer.setFechaProceso(fechaProceso);
		nomPer.setFechaInicio(semana.getStartDate());
		nomPer.setFechaFin(semana.getEndDate());
		nomPer.setDivision(ruta.getParent().getParent().getParent().getNombre());
		nomPer.setRegion(ruta.getParent().getParent().getNombre());
		nomPer.setGerencia(ruta.getParent().getNombre());
		nomPer.setRuta(ruta.getNombre());
		nomPer.setSemana(semanaAnio);
		nomPer.setSemanas(semanas);
		nomPer.setCveEmpleado(emp.getCveEmpleado());
		nomPer.setNombreCompleto(emp.getNombreCompleto());
		nomPer.setCobranzaTotal(onePeriod.getImporteCobradoTotal());
		nomPer.setBaseCobranza(onePeriod.getFactorCobranza());
		nomPer.setBaseNormalidad(onePeriod.getFactorNormalidad());
		
		nomPer.setFactorCobranza(factorCobranza/100);
		nomPer.setFactorNormalidad(factorNormalidad/100);
		nomPer.setBaseClientesNuevos(importeColocacion);
		nomPer.setCobranza(importeCobranza);
		nomPer.setNormalidad(importeNormalidad);
		nomPer.setClientesNuevos(importeColocacion);
		nomPer.setTotalNomina(importeTotal);
		nomPer.setBaseNomina(emp.getSalarioBase());
		double difAsimilados = importeNomina - emp.getSalarioBase();
		double baseAsimilados = difAsimilados>0?difAsimilados:0;
		if (difAsimilados<0)
			messages.add("El empleado: " + emp.getNombreCompleto() + " tiene saldo negativo en asimilados: " + String.valueOf(difAsimilados));		
		nomPer.setBaseAsimilados(baseAsimilados);
		CreditosNominaVO creditos = getPrestamos(filtros, semanaAnio, fechaProceso);
		nomPer.setDescuentoMoto(creditos.getDescuentoMoto());
		nomPer.setDescuentoPrestamo(creditos.getDescuentoOtros());
		double neto = importeTotal-(creditos.getDescuentoMoto()+creditos.getDescuentoOtros());
		if (neto<0)
			messages.add("El empleado: " + emp.getNombreCompleto() + " tiene saldo negativo por crédtido: " + String.valueOf(neto));		
			
		nomPerRepository.save(nomPer);		
	}
	
	private void calculaSueldoBase(Empleado emp, DateRangeVO semana, long semanaAnio, ArrayList<String> messages, 
			FiltrosCredEmplVO filtros, Date fechaProceso)
	{
		Organizacion ruta = null;
		try
		{
			ruta = utServices.getRutaTitular(emp);
		}catch(Exception ex)
		{
			messages.add("El empleado: " + emp.getNombreCompleto() + " no está asignado a una ruta, cargo: " + emp.getCargoEmpleado().getNombre());
			//FIXME: Que hacemos??
		}

		NominaPeriodo nomPer = new NominaPeriodo();
		if (ruta!=null)
		{
			if (ruta.getTipoNodo().equals("RR"))
			{
				nomPer.setDivision(ruta.getParent().getParent().getParent().getNombre());
				nomPer.setRegion(ruta.getParent().getParent().getNombre());
				nomPer.setGerencia(ruta.getParent().getNombre());
				nomPer.setRuta(ruta.getNombre());
			}
			if (ruta.getTipoNodo().equals("G"))
			{
				nomPer.setDivision(ruta.getParent().getParent().getNombre());
				nomPer.setRegion(ruta.getParent().getNombre());
				nomPer.setGerencia(ruta.getNombre());
			}
			if (ruta.getTipoNodo().equals("R"))
			{
				nomPer.setDivision(ruta.getParent().getNombre());
				nomPer.setRegion(ruta.getNombre());
			}
			if (ruta.getTipoNodo().equals("D"))
			{
				nomPer.setDivision(ruta.getNombre());
			}
		}

		
		nomPer.setFechaProceso(fechaProceso);
		nomPer.setFechaInicio(semana.getStartDate());
		nomPer.setFechaFin(semana.getEndDate());
		nomPer.setSemana(semanaAnio);
		nomPer.setSemanas(0);
		nomPer.setCveEmpleado(emp.getCveEmpleado());
		nomPer.setNombreCompleto(emp.getNombreCompleto());
		nomPer.setCobranzaTotal(0);
		nomPer.setFactorCobranza(0);
		nomPer.setFactorNormalidad(0);
		nomPer.setBaseClientesNuevos(0);
		nomPer.setCobranza(0);
		nomPer.setNormalidad(0);
		nomPer.setClientesNuevos(0);
		nomPer.setTotalNomina(0);
		nomPer.setBaseNomina(emp.getSalarioBase());
		nomPer.setBaseAsimilados(emp.getSalarioAsimilado());
		CreditosNominaVO creditos = getPrestamos(filtros, semanaAnio, fechaProceso);
		nomPer.setDescuentoMoto(creditos.getDescuentoMoto());
		nomPer.setDescuentoPrestamo(creditos.getDescuentoOtros());
		double importeTotal = emp.getSalarioBase() + emp.getSalarioAsimilado();
		double neto = importeTotal-(creditos.getDescuentoMoto()+creditos.getDescuentoOtros());
		if (neto<0)
			messages.add("El empleado: " + emp.getNombreCompleto() + " tiene saldo negativo por crédtido: " + String.valueOf(neto));		
		
		nomPerRepository.save(nomPer);		
	}
	
	
	@Transactional
	private CreditosNominaVO getPrestamos(FiltrosCredEmplVO filtros, long semanaAnio, Date fechaProceso)
	{
		List<CreditoEmpleadoVO> creditos = credEmpServices.getCreditosEmpl(filtros);
		CreditosNominaVO retValue = new CreditosNominaVO();
		double descuentoMoto = 0;
		double descuentoOtros = 0;
		for (CreditoEmpleadoVO oneItem: creditos)
		{
			CreditoEmpleado oneCred = credEmpServices.getCreditoById(oneItem.getIdCreditoEmpl());
			double descuento = oneItem.getSaldo()>oneItem.getPago()?oneItem.getPago():oneItem.getSaldo();
			if (oneCred.getTipoCredito().getClaveInterna().equals("CRED_EMP_MOTO"))
				descuentoMoto+= descuento;
			else
				descuentoOtros+= descuento;
			oneCred.setAplicadoNomina(descuento);
			oneCred.setPeriodo(semanaAnio);
			oneCred.setFechaUltimoPago(fechaProceso);
			DescuentosCreditoEmpleado desc =  new DescuentosCreditoEmpleado();
			desc.setCreditoEmpleado(oneCred);
			desc.setFechaProceso(fechaProceso);
			desc.setSemana(semanaAnio);
			desc.setDescuento(oneItem.getPago());
			descCredEmplRepository.save(desc);
			credEmpRepository.save(oneCred);			
		}
		retValue.setDescuentoMoto(descuentoMoto);
		retValue.setDescuentoOtros(descuentoOtros);
		return retValue;
	}
	
	public List<NominaPeriodoVO> consultarNominaPeriodo(FiltrosOrgVO filtros){
		List<NominaPeriodoVO> coleccion = null;
		long noPeriodo= filtros.getNoPeriodo();
		int anio = filtros.getAnio();
		if(filtros.getPeriodo().equals("1")){//por semana
			coleccion = nomPerRepository.findByPeriodoSemana(noPeriodo, anio);
		}
		else if(filtros.getPeriodo().equals("2")){//por quincena
			coleccion = nomPerRepository.findByPeriodoQuincena(noPeriodo, anio);
		}
		return coleccion;
	}
}
