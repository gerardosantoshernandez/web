package com.crenx.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.specification.MovimientoCajaSpecifications;
import com.crenx.data.domain.vo.FiltroMovimientoCajaVO;
import com.crenx.data.domain.vo.TotalMovimientoCajaVO;

public class MovCajaHelperRepository extends HelperRepository {

	private MovimientoCajaRepository repository;

	public MovCajaHelperRepository(final MovimientoCajaRepository respository) {
		if (respository == null) {
			throw new IllegalArgumentException("Creating Handler with null value");
		}
		this.repository = respository;
	}

	/**
	 * Obtiene los totales de los movimietos de caja por filtro
	 * 
	 * @param filtro
	 * @return
	 */
	public List<TotalMovimientoCajaVO> getTotalesPorFiltro(final FiltroMovimientoCajaVO filtro) {
		List<TotalMovimientoCajaVO> totales = new ArrayList<TotalMovimientoCajaVO>();
		final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		if (filtro != null) {
			// Se valida si las fechas no vienen vacias
			if (StringUtils.isBlank(filtro.getFchInicio()) && StringUtils.isBlank(filtro.getFchFin())) {
				// se obtiene la fecha de sistema menor de todos los movimientos
				// de caja
				List<MovimientoCaja> movimientos = this.repository.findAll(MovimientoCajaSpecifications.oldest());
				if (CollectionUtils.isEmpty(movimientos)) {
					filtro.setFchInicio(formatter.print(DateTime.now()));
				} else {
					filtro.setFchInicio(formatter.print(new DateTime(movimientos.get(0).getFechaSistema())));
				}
			}
			if (StringUtils.isBlank(filtro.getFchInicio())) {
				filtro.setFchInicio(filtro.getFchFin());
			} else {
				if (StringUtils.isBlank(filtro.getFchFin())) {
					filtro.setFchFin(filtro.getFchInicio());
				}
			}
			// Se parsean las fechas y se modifican para obtener un rago de
			// inicio
			// de día a fin de día
			DateTime fchInicio = formatter.parseDateTime(filtro.getFchInicio()).withTimeAtStartOfDay();
			DateTime fchFin = formatter.parseDateTime(filtro.getFchFin()).withTime(23, 59, 59, 0);
			totales = this.repository.findTotalesMovCaja(getNullIfIsEmpty(filtro.getTiposMovimiento()),
					getWilcardIncludeAll(filtro.getTiposMovimiento()), getNullIfIsEmpty(filtro.getTiposOperacion()),
					getWilcardIncludeAll(filtro.getTiposOperacion()), getNullIfIsEmpty(filtro.getOrgs()),
					getWilcardIncludeAll(filtro.getOrgs()), fchInicio.toDate(), fchFin.toDate());
		}
		return totales;
	}
}
