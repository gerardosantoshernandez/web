package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.ClienteModelMap;
import com.crenx.data.domain.vo.ClienteRecompraVO;
import com.crenx.data.domain.vo.ClienteToClienteRecompraVOModelMap;
import com.crenx.data.domain.vo.ClienteTraspasoModelMap;
import com.crenx.data.domain.vo.ClienteTraspasoVO;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.ClienteVOToClienteModelMapper;
import com.crenx.data.domain.vo.CoDeudorVO;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.DocumentoVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.OrgToTreeModelMap;
import com.crenx.data.domain.vo.OrgVO;
import com.crenx.util.Constantes;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;

@Service

public class ClienteServices {
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	UtilityServices utServices;
	@Autowired
	OrgRepository orgRepository;
	@Autowired
	DomicilioRepository domRepository;
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private CreditoRepository credRepository;
	
	/**
	 * @param filtros.
	 *            Filtro para realizar la búsqueda de clientes
	 * @param codeudorAsClient.
	 *            Si es true los codeudores relacionados a los clientes,
	 *            resultantes de la búsqueda, serán regresados como clientes
	 * @return
	 */
	public Collection<ClienteVO> filtrarClientes(final FiltrosOrgVO filtros, final String figura) {
		Collection<Cliente> clientes = null;
		List<ClienteVO> listaClientes = null;
		final String idFigura = catServices.getCatalogoCveInterna(figura).getIdCatalogo();
		final String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		final String apellidoPaterno = StringUtils.isEmpty(filtros.getApellidoPaterno()) ? "%"
				: filtros.getApellidoPaterno() + "%";
		final List<String> rutas = this.getRutasIds(filtros);
		if (rutas.size() > 0) {
			clientes = Lists.newArrayList(this.clienteRepository
					.findByTipoFiguraIdCatalogoAndRutaIdOrgInAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(
							idFigura, rutas, nombre, apellidoPaterno));
		} else {
			clientes = Lists.newArrayList(this.clienteRepository
					.findByTipoFiguraIdCatalogoAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(idFigura, nombre,
							apellidoPaterno));
		}
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteModelMap());
		Type listType = new TypeToken<List<ClienteVO>>() {
		}.getType();
		listaClientes = mapper.map(clientes, listType);			
		// Se agregan los documentos de los clientes
		this.setDocumentos(listaClientes);
		//Se agregan los documentos de los codeudores
		
		return listaClientes;
	}
	
	public Collection<ClienteTraspasoVO> filtrarClientesTraspaso(final FiltrosOrgVO filtros, final String figura) {
		Collection<Cliente> clientes = null;
		List<ClienteTraspasoVO> listaClientes = null;
		final String idFigura = catServices.getCatalogoCveInterna(figura).getIdCatalogo();
		final String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		final String apellidoPaterno = StringUtils.isEmpty(filtros.getApellidoPaterno()) ? "%"
				: filtros.getApellidoPaterno() + "%";
		final List<String> rutas = this.getRutasIds(filtros);
		if (rutas.size() > 0) {
			clientes = Lists.newArrayList(this.clienteRepository
					.findByTipoFiguraIdCatalogoAndRutaIdOrgInAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(
							idFigura, rutas, nombre, apellidoPaterno));
		} else {
			clientes = Lists.newArrayList(this.clienteRepository
					.findByTipoFiguraIdCatalogoAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(idFigura, nombre,
							apellidoPaterno));
		}
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteTraspasoModelMap());
		Type listType = new TypeToken<List<ClienteTraspasoVO>>() {
		}.getType();
		listaClientes = mapper.map(clientes, listType);			
		// Se agregan los documentos de los clientes
		//this.setDocumentos(listaClientes);
		//Se agregan los documentos de los codeudores
		
		return listaClientes;
		//return null;
	}
	
	
	public long clientesNuevos(DateRangeVO  week, FiltrosOrgVO filtros)
	{

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
		List<Cliente> nuevos = new ArrayList<Cliente>();
		if	(rutas.size()>0)
		{
			nuevos = clienteRepository.findByCreacionBetweenAndRutaIdOrgIn(week.getStartDate(), week.getEndDate(), rutas);
		}
		else if (gerencias.size()>0) 
		{
			nuevos = clienteRepository.findByCreacionBetweenAndRutaParentIdOrgIn(week.getStartDate(), week.getEndDate(), gerencias);
		}else if(regiones.size()>0)
		{
			nuevos = clienteRepository.findByCreacionBetweenAndRutaParentParentIdOrgIn(week.getStartDate(), week.getEndDate(), regiones);
		}else if(divisiones.size()>0)
		{
			nuevos = clienteRepository.findByCreacionBetweenAndRutaParentParentParentIdOrgIn(week.getStartDate(), week.getEndDate(), divisiones);
		}else
		{
			nuevos = clienteRepository.findByCreacionBetween(week.getStartDate(), week.getEndDate());
		}
		return nuevos.size();		
	}

	public long clientesTotales(DateRangeVO  week, FiltrosOrgVO filtros)
	{

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
		List<Cliente> nuevos = new ArrayList<Cliente>();
		if	(rutas.size()>0)
		{
			nuevos = clienteRepository.findByRutaIdOrgIn(rutas);
		}
		else if (gerencias.size()>0) 
		{
			nuevos = clienteRepository.findByRutaParentIdOrgIn(gerencias);
		}else if(regiones.size()>0)
		{
			nuevos = clienteRepository.findByRutaParentParentIdOrgIn(regiones);
		}else if(divisiones.size()>0)
		{
			nuevos = clienteRepository.findByRutaParentParentParentIdOrgIn(divisiones);
		}else
		{
			nuevos = Lists.newArrayList( clienteRepository.findAll());
		}
		return nuevos.size();		
	}
	
	public long clientesActivos(DateRangeVO  week, FiltrosOrgVO filtros)
	{

		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
		List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
		List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
		IndicadorCreditoVO nuevos = new IndicadorCreditoVO();
		if	(rutas.size()>0)
		{
			nuevos = clienteRepository.findActivosRuta(rutas);
		}
		else if (gerencias.size()>0) 
		{
			nuevos = clienteRepository.findActivosGerencia(gerencias);
		}else if(regiones.size()>0)
		{
			nuevos = clienteRepository.findActivosRegion(regiones);
		}else if(divisiones.size()>0)
		{
			nuevos = clienteRepository.findActivosDivision(divisiones);
		}else
		{
			nuevos = clienteRepository.findActivos();
		}
		return nuevos.getCountCreditos();		
	}
	
	public Cliente saveCliente(ClienteVO input)
	{
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteVOToClienteModelMapper());

		Cliente clienteDB = mapper.map(input, Cliente.class);
		clienteDB.setEntidadNacimiento(input.getIdEntidadNacimiento() != null
				? catServices.getCatalogoId(input.getIdEntidadNacimiento()) : null);
		clienteDB.setGenero(catServices.getCatalogoId(input.getIdGenero()));
		clienteDB.setOcupacion(
				input.getIdOcupacion() != null ? catServices.getCatalogoId(input.getIdOcupacion()) : null);
		clienteDB.setPaisNacimiento(
				input.getIdPaisNacimiento() != null ? catServices.getCatalogoId(input.getIdPaisNacimiento()) : null);
		clienteDB.setRuta(input.getIdRuta() != null ? orgRepository.findOne(input.getIdRuta()) : null);
		clienteDB.setTipoDocId(input.getIdTipoDocumentoIdentidad()!=null?catServices.getCatalogoId(input.getIdTipoDocumentoIdentidad()):null);
		clienteDB.setTipoFigura(catServices.getCatalogoId(input.getIdTipoFigura()));
		clienteDB.setGeoPosicion(input.getGeoPosicion());
		Date fechaRegistro = utServices.justDate(new Date());
		clienteDB.setCreacion(fechaRegistro);
		if (input.getParentId() != null) {
			Cliente parent = new Cliente();
			parent.setIdCliente(input.getParentId());
			clienteDB.setParent(parent);
		}
		if (clienteDB.getDomicilioLaboral()!=null)
			domRepository.save(clienteDB.getDomicilioLaboral());
		if (clienteDB.getDomicilioResidencial()!=null)
			domRepository.save(clienteDB.getDomicilioResidencial());
		if (input.getId()==0)
			clienteDB.setId(utServices.generaId("Cliente"));
		
		clienteDB.setUsuario(input.getUsuario());
		
		clienteRepository.save(clienteDB);
		return clienteDB;		
	}
	
	public Collection<ClienteVO> obtenerCliente(FiltrosOrgVO filtros)
	{
		Collection<Cliente> clientes = null;
		List<ClienteVO> listaClientes = null;
		
		String idFigura = catServices.getCatalogoCveInterna("FIG_CLIENTE").getIdCatalogo();
		List<NameValueVO> rutasNV = Lists.newArrayList(filtros.getRutas());
		ArrayList<String> rutas = new ArrayList<String>();
		for (NameValueVO ruta : rutasNV)
		{
			rutas.add(ruta.getId());
		}
		
		if	(rutas.size()>0)
		{
			clientes = Lists.newArrayList(this.clienteRepository.findBytipoFiguraIdCatalogoAndRutaIdOrg(idFigura,rutas));
		}
		else
		{
			clientes = Lists.newArrayList(this.clienteRepository.findByTipoFiguraIdCatalogo(idFigura));
		}
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteModelMap());
		Type listType = new TypeToken<List<ClienteVO>>() {}.getType();
		listaClientes = mapper.map(clientes, listType);
		return listaClientes;
	}
	
	/**
	 * Obtiene los ids de las rutas contenidas en el filtro
	 * @param filtros
	 * @return
	 */
	private List<String> getRutasIds(final FiltrosOrgVO filtros) {
		final List<String> rutas = new ArrayList<String>();
		final List<NameValueVO> rutasNV = Lists.newArrayList(filtros.getRutas());
		if (rutasNV.size() == 1) {
			NameValueVO ruta = rutasNV.get(0);
			// Este caso solo sucede cuando la móvil carga los datos de
			// cliente y envía una gerencia
			if (ruta.getName().equals("Ruta")) {
				Organizacion tree = catServices.getNodoById(ruta.getId());
				if (tree.getTipoNodo().equals("RR")) {
					rutas.add(ruta.getId());
				} else {
					OrgVO orgVo = new OrgVO();
					ModelMapper mapper = new ModelMapper();
					mapper.addMappings(new OrgToTreeModelMap());
					orgVo = mapper.map(tree, OrgVO.class);
					FiltrosOrgVO filtrosLoc = new FiltrosOrgVO();
					catServices.getChildren(tree, orgVo, filtrosLoc, mapper, true);
					for (NameValueVO oneItem : filtrosLoc.getRutas()) {
						rutas.add(oneItem.getId());
					}
				}
			} else {
				rutas.add(ruta.getId());
			}
		} else {
			rutas.addAll(catServices.filtraRutas(filtros));
		}
		return rutas;
	}
	
	/**
	 * Complementa los clientes con sus respectivos documentos
	 * 
	 * @param clientes
	 */
	private void setDocumentos(final List<ClienteVO> clientes) {
		// Se obtienen los detalles de documento de los clientes
		if (clientes != null && clientes.size() > 0) {
			final Set<String> idsSet = new HashSet<String>();
			// Se agregan los ids de los clientes y codeudores
			for (ClienteVO cliente : clientes) {
				idsSet.add(cliente.getIdCliente());
				if (CollectionUtils.isNotEmpty(cliente.getCodeudores())) {
					for (CoDeudorVO codeudor : cliente.getCodeudores()) {
						idsSet.add(codeudor.getIdCodeudor());
					}
				}
			}
			final List<String> ids = Lists.newArrayList(idsSet);
			// Se particionan el conjunto de ids para realizar las búsquedas
			final List<List<String>> particiones = Lists.partition(ids, 300);
			// Se buscan los documentos
			final List<DocumentoVO> doctos = new ArrayList<DocumentoVO>();
			int index = 0;
			while (index < particiones.size()) {
				doctos.addAll(this.documentoRepository.findByNamesAndValuesProperties(
						Lists.newArrayList(Constantes.IDCLIENTE), particiones.get(index)));
				index++;
			}
			// Se asignan los documentos correspondientes a cada cliente y
			// codeudor
			if (CollectionUtils.isNotEmpty(doctos)) {
				for (ClienteVO cliente : clientes) {
					for (DocumentoVO docto : doctos) {
						cliente.agregaDocumento(docto, true);
						cliente.agregaDocumentoCodeudores(docto, true);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @return retorna los clientes que pueden ser titulares
	 */
	public Collection<ClienteVO> getClientesCandTitular(FiltrosOrgVO filtros, final String figura){
		Collection<Cliente> clientes = null;
		List<ClienteVO> listaClientes = null;
		
		final String idFigura = catServices.getCatalogoCveInterna(figura).getIdCatalogo();
		final String nombre = StringUtils.isEmpty(filtros.getNombre()) ? "%" : filtros.getNombre() + "%";
		final String apellidoPaterno = StringUtils.isEmpty(filtros.getApellidoPaterno()) ? "%"
				: filtros.getApellidoPaterno() + "%";
		
		ArrayList<String> rutas = catServices.filtraRutas(filtros);
		
		if(rutas.size() > 0){
			clientes = Lists.newArrayList(this.clienteRepository.findClientesSinCreditoByRuta(nombre, apellidoPaterno, rutas));
			
		}else{
			clientes = Lists.newArrayList(this.clienteRepository.findClientesSinCredito(nombre, apellidoPaterno));
		}
		
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteModelMap());
		Type listType = new TypeToken<List<ClienteVO>>() {
		}.getType();
		listaClientes = mapper.map(clientes, listType);			
		
		return listaClientes;
	}
	
}
