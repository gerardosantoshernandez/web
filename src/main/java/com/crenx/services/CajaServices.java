package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.data.domain.entity.Caja;
import com.crenx.data.domain.entity.Calendario;
import com.crenx.data.domain.entity.CorteCaja;
import com.crenx.data.domain.entity.IndicadorCobranza;
import com.crenx.data.domain.entity.IndicadorCobranzaPeriodo;
import com.crenx.data.domain.entity.IndicadorNegocio;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.CajaRepository;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CorteCajaRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.IndicadorCobranzaPeriodoRepository;
import com.crenx.data.domain.repository.IndicadorCobranzaRepository;
import com.crenx.data.domain.repository.IndicadorNegocioRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.ParametrosRepository;
import com.crenx.data.domain.repository.TablaAmortizacionRepository;
import com.crenx.data.domain.repository.VisitaRepository;
import com.crenx.data.domain.vo.InidicadorRutaVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import com.crenx.data.domain.vo.CajaToVOModelMapper;
import com.crenx.data.domain.vo.CajaVO;
import com.crenx.data.domain.vo.ClienteModelMap;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.IndicadorNegocioVO;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class CajaServices {

	@Autowired
	private CatalogoRepository catRepository;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private ParametrosRepository paramRepository;
	@Autowired
	PagoRepository pagoRepository;
	@Autowired
	TablaAmortizacionRepository taRepository;
	@Autowired
	MovimientoCajaRepository mcRepository;
	@Autowired
	private CajaRepository cajaRepository;
	@Autowired
	private CorteCajaRepository ccRepository;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private IndicadorCobranzaRepository icRepository;
	@Autowired
	private IndicadorCobranzaPeriodoRepository icpRespository;
	@Autowired
	private PagoRepository pagoRespository;
	@Autowired
	private CreditoRepository credRespository;
	@Autowired
	private VisitaRepository visRespository;
	@Autowired
	private IndicadorNegocioRepository inRepository;
	@Autowired
	private ClienteRepository cliRepository;
	
	
	@Transactional
	public void corteCaja() throws Exception
	{
		try
		{
			Date nuevoCorte = new Date();
			byte aplicado = 1;
			//Traemos todas las cajas
			List<Caja> cajas = Lists.newArrayList(cajaRepository.findAll());
			List<CorteCaja> cortes = new ArrayList<CorteCaja>();
			List<MovimientoCaja> movimientosUpd = new ArrayList<MovimientoCaja>();
			for (Caja oneCaja : cajas)
			{
				//Estas debe ser cifras de control y cuadrar con el resultado final de las operaciones
				double saldoInicialBase = oneCaja.getSaldoInicial();
				double saldoFinalBase = oneCaja.getSaldoFinal();
				Date fechaCorte = oneCaja.getFechaCorte();
				List<String> rutas = new ArrayList<String>();
				rutas.add(oneCaja.getUnidad().getIdOrg());
				double saldoFinal = saldoInicialBase;
				double totCargos = 0;
				double totAbonos = 0;
				//Traemos las operaciones ejecutadas pendientes de aplicar
				List<MovimientoCaja> movimientos = Lists.newArrayList(mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaCorte, nuevoCorte, rutas));
				for (MovimientoCaja oneMov: movimientos)
				{
					String tipoMov = oneMov.getTipoMovimiento().getClaveInterna();
					if (tipoMov.equals("MOV_CAJA_CARGO"))
					{
						saldoFinal-= oneMov.getCargo();
						totCargos+=oneMov.getCargo();
					}
					else if (tipoMov.equals("MOV_CAJA_ABONO"))
					{
						saldoFinal+= oneMov.getAbono();
						totAbonos+=oneMov.getAbono();
					}
					oneMov.setEstatusCorte(aplicado);
					movimientosUpd.add(oneMov);
				}
				CorteCaja oneCorte = new CorteCaja();
				oneCorte.setCaja(oneCaja);
				oneCorte.setFechaCorteAnterior(fechaCorte);
				oneCorte.setFechaCorte(nuevoCorte);
				oneCorte.setTotalCargos(totCargos);
				oneCorte.setTotalAbonos(totAbonos);
				oneCorte.setSaldoInicial(saldoInicialBase);
				oneCorte.setSaldoFinal(saldoFinal);
				oneCorte.setDifAbono(oneCaja.getTotalAbonos()-totAbonos);
				oneCorte.setDifCargos(oneCaja.getTotalCargos()-totCargos);
				oneCorte.setDifTotal(oneCaja.getSaldoFinal()-saldoFinal);
				cortes.add(oneCorte);
				oneCaja.setFechaCorte(nuevoCorte);
				oneCaja.setSaldoInicial(saldoFinal);
				oneCaja.setSaldoFinal(0.0);
				oneCaja.setTotalAbonos(0.0);
				oneCaja.setTotalCargos(0.0);			
			}
			cajaRepository.save(cajas);
			mcRepository.save(movimientosUpd);
			ccRepository.save(cortes);
		}catch (Exception ex)
		{
			throw new Exception(ex);
		}		
	}

	private IndicadorCobranza newItem(HashMap<String, IndicadorCobranza> indicador, String orgId, Date fechaProceso)
	{
		IndicadorCobranza oneInd = indicador.get(orgId);
		oneInd=oneInd==null?new IndicadorCobranza():oneInd;
		if (oneInd.getRuta()==null)
		{
			Organizacion org = orgRepository.findOne(orgId);
			oneInd.setRuta(org);	
			Organizacion gerencia = org.getParent();
			oneInd.setGerencia(gerencia);
			Organizacion region = gerencia.getParent();
			oneInd.setRegion(region);
			Organizacion division = region.getParent();
			oneInd.setDivision(division);
			oneInd.setFecha(fechaProceso);
			indicador.put(orgId, oneInd);			
		}
		return oneInd;		
	}

	private IndicadorCobranzaPeriodo newItemPeriodo(HashMap<String, IndicadorCobranzaPeriodo> indicador, String orgId, DateRangeVO semana)
	{
		IndicadorCobranzaPeriodo oneInd = indicador.get(orgId);
		oneInd=oneInd==null?new IndicadorCobranzaPeriodo():oneInd;
		Calendar cal = Calendar.getInstance();
		cal.setTime(semana.getEndDate());
		
		if (oneInd.getRuta()==null)
		{
			Organizacion org = orgRepository.findOne(orgId);
			oneInd.setRuta(org);	
			Organizacion gerencia = org.getParent();
			oneInd.setGerencia(gerencia);
			Organizacion region = gerencia.getParent();
			oneInd.setRegion(region);
			Organizacion division = region.getParent();
			oneInd.setDivision(division);
			oneInd.setFechaInicial(semana.getStartDate());
			oneInd.setFechaFinal(semana.getEndDate());
			oneInd.setSemana(cal.get(Calendar.WEEK_OF_YEAR));
			indicador.put(orgId, oneInd);			
		}
		return oneInd;		
	}

	private IndicadorCobranzaPeriodo newItemPeriodoNueva(HashMap<String, IndicadorCobranzaPeriodo> indicador, String ruta, DateRangeVO semana)
	{
		Organizacion org = orgRepository.findByNombre(ruta).get(0);
		IndicadorCobranzaPeriodo oneInd = indicador.get(org.getIdOrg());
		oneInd=oneInd==null?new IndicadorCobranzaPeriodo():oneInd;
		Calendar cal = Calendar.getInstance();
		cal.setTime(semana.getEndDate());
		
		if (oneInd.getRuta()==null)
		{
			oneInd.setRuta(org);	
			Organizacion gerencia = org.getParent();
			oneInd.setGerencia(gerencia);
			Organizacion region = gerencia.getParent();
			oneInd.setRegion(region);
			Organizacion division = region.getParent();
			oneInd.setDivision(division);
			oneInd.setFechaInicial(semana.getStartDate());
			oneInd.setFechaFinal(semana.getEndDate());
			oneInd.setSemana(cal.get(Calendar.WEEK_OF_YEAR));
			indicador.put(org.getIdOrg(), oneInd);			
		}
		return oneInd;		
	}
	
	
	
	@Transactional
	public List<IndicadorCobranza> calculoIndicadorCobranzaDia(Date fechaProceso)
	{
		List<InidicadorRutaVO> result = null;
		fechaProceso = utServices.justDate(fechaProceso);
		//Traemos los indicadores de todas las rutas
		List<InidicadorRutaVO> cobranzaEjecutar = Lists.newArrayList(taRepository.findByCobranzaEjecutable(fechaProceso));
		List<InidicadorRutaVO> cobranzaEjecutada = Lists.newArrayList(taRepository.findByCobranzaNormalidad(fechaProceso));
		List<InidicadorRutaVO> cobranzaDiaria = Lists.newArrayList(taRepository.findByCobranzaDiaria(fechaProceso));
		List<InidicadorRutaVO> cobranzaVencida = Lists.newArrayList(taRepository.findByCobranzaDiariaVencida(fechaProceso));

		Date fecha = utServices.justDate(fechaProceso);
		HashMap<String, IndicadorCobranza> indicador = new HashMap<String, IndicadorCobranza>();
		//Cobranza a ejecutar
		for (InidicadorRutaVO ruta: cobranzaEjecutar)
		{
			IndicadorCobranza oneInd = newItem(indicador, ruta.getIdOrg(), fecha);
			oneInd.setCobrables(ruta.getNormalidad());
			oneInd.setImporteCobrables(ruta.getCobranza());
		}
		for (InidicadorRutaVO ruta: cobranzaEjecutada)
		{
			IndicadorCobranza oneInd = newItem(indicador, ruta.getIdOrg(), fecha);
			oneInd.setCobradosDia(ruta.getNormalidad());
			oneInd.setImporteCobradoDia(ruta.getCobranza());			
		}
		for (InidicadorRutaVO ruta: cobranzaDiaria)
		{
			IndicadorCobranza oneInd = newItem(indicador, ruta.getIdOrg(), fecha);
			oneInd.setCobradosTotal(ruta.getNormalidad());
			oneInd.setImporteCobradoTotal(ruta.getCobranza());			
		}

		for (InidicadorRutaVO ruta: cobranzaVencida)
		{
			IndicadorCobranza oneInd = newItem(indicador, ruta.getIdOrg(), fecha);
			oneInd.setCobradosVencidos(ruta.getNormalidad());
			oneInd.setImporteCobradoVencido(ruta.getCobranza());			
		}
		
		for (IndicadorCobranza oneInd : indicador.values())
		{
			double cobrados = oneInd.getCobrables();
			double factorNormalidad = cobrados!=0?oneInd.getCobradosDia()/cobrados:0;
			oneInd.setFactorNormalidad(factorNormalidad*100.);
			double importeCobrables = oneInd.getImporteCobrables();
			double factorCobranza = importeCobrables!=0?oneInd.getImporteCobradoTotal()/importeCobrables:0;
			oneInd.setFactorCobranza(factorCobranza*100.);
			
		}
		icRepository.save(indicador.values());
		return new ArrayList<IndicadorCobranza>( indicador.values());
	}
	
	@Transactional
	public List<IndicadorCobranzaPeriodo> calculoIndicadorCobranzaSemana(DateRangeVO semana)
	{
		List<InidicadorRutaVO> result = null;
		//Traemos los indicadores de todas las rutas
		//Cobranza ejecutable en el periodo
		List<InidicadorRutaVO> cobranzaEjecutar = Lists.newArrayList(taRepository.findByCobranzaEjecutable(semana.getStartDate(), semana.getEndDate()));
		HashMap<String, IndicadorCobranzaPeriodo> indicador = new HashMap<String, IndicadorCobranzaPeriodo>();
		//La cobranza ejecutada en el periodo y que corresponde al periodo
		List<InidicadorRutaVO> cobranzaEjecutada = Lists.newArrayList(taRepository.findByCobranzaNormalidad(semana.getStartDate(), semana.getEndDate()));
		//La cobranza ejecutada en el periodo sin importar su estado (atraso o anticipo)
		List<InidicadorRutaVO> cobranzaDiaria = Lists.newArrayList(taRepository.findByCobranzaPeriodo(semana.getStartDate(), semana.getEndDate()));
		//Cobranza con retraso o vencida ejecutada en el periodo
		List<InidicadorRutaVO> cobranzaRetraso = Lists.newArrayList(taRepository.findByCobranzaPeriodoRetraso(semana.getStartDate(), semana.getEndDate()));
		//Total de cartera vencida a la fecha inicial del periodo
		List<InidicadorRutaVO> carteraVencida = Lists.newArrayList(taRepository.findByCarteraPeriodoVencida(semana.getStartDate()));

        List<String> rutas =  soloRutas();
        List<ResumenEmpresaVO> carteraNueva = cliRepository.findCarteraTotal(semana.getStartDate(), semana.getEndDate(), rutas);
		
		//Cobranza a ejecutar
		for (InidicadorRutaVO ruta: cobranzaEjecutar)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCobrables(ruta.getNormalidad());
			oneInd.setImporteCobrables(ruta.getCobranza());
		}
		//Cobranza ejecutada coreespondiente al periodo (normal)
		for (InidicadorRutaVO ruta: cobranzaEjecutada)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCobradosDia(ruta.getNormalidad());
			oneInd.setImporteCobradoDia(ruta.getCobranza());			
		}
		//Total de cobranza del día, sin importar su estado
		for (InidicadorRutaVO ruta: cobranzaDiaria)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCobradosTotal(ruta.getNormalidad());
			oneInd.setImporteCobradoTotal(ruta.getCobranza());			
		}
		
		for (InidicadorRutaVO ruta: cobranzaDiaria)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCobradosTotal(ruta.getNormalidad());
			oneInd.setImporteCobradoTotal(ruta.getCobranza());			
		}
		
		for (InidicadorRutaVO ruta: cobranzaRetraso)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCobradosVencidos(ruta.getNormalidad());
			oneInd.setImporteCobradoVencido(ruta.getCobranza());			
		}

		for (InidicadorRutaVO ruta: carteraVencida)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodo(indicador, ruta.getIdOrg(), semana);
			oneInd.setCarteraVencida(ruta.getCobranza());
		}
		
		for (ResumenEmpresaVO oneItem: carteraNueva)
		{
			IndicadorCobranzaPeriodo oneInd = newItemPeriodoNueva(indicador, oneItem.getRuta(), semana);
			oneInd.setCarteraNueva(oneItem.getCarteraNueva());
			oneInd.setClientesNuevos(oneItem.getNuevosClientes());
		}
		
		
		
		for (IndicadorCobranzaPeriodo oneInd : indicador.values())
		{
			double cobrables = oneInd.getCobrables();
			double factorNormalidad = cobrables!=0?oneInd.getCobradosDia()/cobrables:0;
			oneInd.setFactorNormalidad(factorNormalidad*100.);
			double importeCobrables = oneInd.getImporteCobrables();
			double factorCobranza = importeCobrables!=0?oneInd.getImporteCobradoTotal()/importeCobrables:0;
			oneInd.setFactorCobranza(factorCobranza*100.);
			
		}
		icpRespository.save(indicador.values());
		
		return new ArrayList<IndicadorCobranzaPeriodo>( indicador.values());
	}
	
	public List<String> soloRutas()
	{
		List<String> rutas = new ArrayList<String>();
		List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
		for (Organizacion oneItem: nodos)
		{
			rutas.add(oneItem.getIdOrg());
		}
		return rutas;
	}
	
	public List<ResumenEmpresaVO> calculoResumenEmpresa(FiltrosOrgVO filtros)
	{
		Date desde = utServices.justDate(new Date());
		Date hasta = desde;
		DateRangeVO week = null;
		if (filtros.getFechaInicial()!=null)
		{
			desde = utServices.convertStringToDate(filtros.getFechaInicial()+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
			desde = utServices.justDate(desde);				
		}
		if (filtros.getFechaFinal()!=null)
		{
			hasta = utServices.convertStringToDate(filtros.getFechaFinal()+" 23:59:59", "yyyy-MM-dd HH:mm:ss");
			hasta = utServices.justDate(hasta);				
		}
		if (hasta==desde)
			week = utServices.getWeekDates(desde);
		else
		{
			week = new DateRangeVO();
			week.setStartDate(desde);
			week.setEndDate(hasta);
		}
		HashMap<String, ResumenEmpresaVO> resumen = new HashMap<String,ResumenEmpresaVO>();
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		if (rutas.isEmpty())
		{
			//si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if(gerencias.isEmpty()){
				//si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if(regiones.isEmpty()){
					//si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if(divisiones.isEmpty()){
						//no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem: nodos)
						{
							rutas.add(oneItem.getIdOrg());
						}
						
					}else{
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for(String idDivision : divisiones){
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for(Organizacion region : lregiones){
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for(Organizacion gerencia : lgerencias){
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for(Organizacion ruta : lrutas){
							rutas.add(ruta.getIdOrg());
						}
					}
				}else{
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for(String idRegion : regiones){
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for(Organizacion gerencia : lgerencias){
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for(Organizacion ruta : lrutas){
						rutas.add(ruta.getIdOrg());
					}
				}
			}else{
				//recupero las rutas de las gerencias ok
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for(Organizacion ruta : lrutas){
					rutas.add(ruta.getIdOrg());
				}
				
			}
			
		}

		/*
		if (rutas.isEmpty())
		{
			rutas = soloRutas();
		}
*/
		//Vamos por los datos de Abonos y Comisiones
		if(rutas.size()<=0)
			return Lists.newArrayList();
		List<ResumenEmpresaVO> abonos = pagoRepository.findByRutasAndFechas(week.getStartDate(),week.getEndDate(), rutas);
		for (ResumenEmpresaVO unaRuta : abonos)
		{
			resumen.put(unaRuta.getRuta(), unaRuta);
		}
		//Vamos por los datos de Desembolsos
		String tipoOperacion = "TIPO_OPE_CREDITO";
		abonos = mcRepository.findByRutasAndFechas(week.getStartDate(),week.getEndDate(), rutas, tipoOperacion);
		for (ResumenEmpresaVO unaRuta : abonos)
		{
			ResumenEmpresaVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem==null)
			{
				oneItem = new ResumenEmpresaVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setDesembolso(unaRuta.getMonto());	
			oneItem.setNuevosCreditos(unaRuta.getNuevosCreditos());
		}
		//Vamos por la cartera total
		abonos = credRespository.findByRutasAndFechas(rutas);
		for (ResumenEmpresaVO unaRuta : abonos)
		{
			ResumenEmpresaVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem==null)
			{
				oneItem = new ResumenEmpresaVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setCartera(unaRuta.getCartera());				
			oneItem.setCarteraCapital(unaRuta.getCarteraCapital());
		}
		//FIXME: Esto es de primaria
		//Vamos por Saldo de Caja (que chafa....)
		double filtroSaldo = 99999999.00;
		if (filtros.isCajaRojo())
			filtroSaldo=0;
			
		abonos = cajaRepository.findByRutasAndFechas(rutas, filtroSaldo);
		for (ResumenEmpresaVO unaRuta : abonos)
		{
			ResumenEmpresaVO oneItem = resumen.get(unaRuta.getRuta());
			if (oneItem==null)
			{
				oneItem = new ResumenEmpresaVO();
				oneItem.setRuta(unaRuta.getRuta());
				oneItem.setGerencia(unaRuta.getGerencia());
				oneItem.setRegion(unaRuta.getRegion());
				oneItem.setDivision(unaRuta.getDivision());
				resumen.put(oneItem.getRuta(), oneItem);
			}
			oneItem.setCaja(unaRuta.getMonto());				
		}
		return Lists.newArrayList(resumen.values());
	}

	private IndicadorNegocioVO newIndicadorNegocioVO(String ruta, String gerencia, String region, String division, Date fechaProceso)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(fechaProceso);
		IndicadorNegocioVO oneItem = new IndicadorNegocioVO();
		oneItem.setDivision(division);
		oneItem.setRegion(region);
		oneItem.setGerencia(gerencia);
		oneItem.setRuta(ruta);
		oneItem.setFechaProceso(fechaProceso);
		oneItem.setSemana(cal.get(Calendar.WEEK_OF_YEAR));
		oneItem.setMes(cal.get(Calendar.MONTH) + 1);
		switch (oneItem.getMes())
		{
			case 1:
			case 2:
			case 3:
				oneItem.setTrimestre(1);
				break;
			case 4:
			case 5:
			case 6:
				oneItem.setTrimestre(2);
				break;
			case 7:
			case 8:
			case 9:
				oneItem.setTrimestre(3);
				break;
			case 10:
			case 11:
			case 12:
				oneItem.setTrimestre(4);
				break;
		}
		oneItem.setSemestre(oneItem.getMes()<=6?1:2);
		oneItem.setAnio(cal.get(Calendar.YEAR));
		return oneItem;		
	}
	private IndicadorNegocioVO newIndicadorNegocioVOItem(HashMap<String, IndicadorNegocioVO> map, IndicadorCobranzaPeriodo item, Date fechaProceso)
	{
		IndicadorNegocioVO oneItem = map.get(item.getRuta().getNombre());
		if (oneItem==null)
		{
			oneItem = newIndicadorNegocioVO(item.getRuta().getNombre(),item.getGerencia().getNombre(), item.getRegion().getNombre(), item.getDivision().getNombre(), fechaProceso);
			map.put(item.getRuta().getNombre(), oneItem);
		}
		oneItem.setCarteraVencida(item.getCarteraVencida());
		oneItem.setCobranza(item.getImporteCobradoTotal());
		return oneItem;
	}
	private IndicadorNegocioVO newIndicadorNegocioVOItem(HashMap<String, IndicadorNegocioVO> map, ResumenEmpresaVO item, Date fechaProceso)
	{
		IndicadorNegocioVO oneItem = map.get(item.getRuta());
		if (oneItem==null)
		{
			oneItem = newIndicadorNegocioVO(item.getRuta(), item.getGerencia(), item.getRegion(),item.getDivision(), fechaProceso);
			map.put(item.getRuta(), oneItem);
		}
		oneItem.setCarteraCapital(item.getCarteraCapital());
		oneItem.setCarteraTotal(item.getCartera());
		oneItem.setColocacion(item.getDesembolso());
		oneItem.setCreditosNuevos(item.getNuevosCreditos());
		return oneItem;
	}

	private IndicadorNegocioVO clientesTotalesIndicadorNegocioVOItem(HashMap<String, IndicadorNegocioVO> map, ResumenEmpresaVO item, Date fechaProceso)
	{
		IndicadorNegocioVO oneItem = map.get(item.getRuta());
		if (oneItem==null)
		{
			oneItem = newIndicadorNegocioVO(item.getRuta(), item.getGerencia(), item.getRegion(),item.getDivision(), fechaProceso);
			map.put(item.getRuta(), oneItem);
		}
		oneItem.setTotalClientes(item.getTotalClientes());
		oneItem.setClientesNuevos(item.getNuevosClientes());
		oneItem.setCarteraNueva(item.getCarteraNueva());
		return oneItem;
	}
	
	
	@Transactional
	public List<IndicadorNegocioVO> calculaIndicadoresNegocio(DateRangeVO semana )
	{
        List<IndicadorCobranzaPeriodo> indicadorCobranza = calculoIndicadorCobranzaSemana(semana);
        FiltrosOrgVO filtros =  new FiltrosOrgVO();
        filtros.setFechaInicial(utServices.convertDateToString(semana.getStartDate(), "yyyy-MM-dd"));
        filtros.setFechaFinal(utServices.convertDateToString(semana.getEndDate(), "yyyy-MM-dd"));
        List<ResumenEmpresaVO> resumen = calculoResumenEmpresa(filtros);
        List<String> rutas =  soloRutas();
        List<ResumenEmpresaVO> cartera = cliRepository.findCarteraTotal(semana.getStartDate(), semana.getEndDate(), rutas);

        HashMap<String, IndicadorNegocioVO>  map = new HashMap<String, IndicadorNegocioVO>();
        for (IndicadorCobranzaPeriodo oneInd : indicadorCobranza)
        {
        	IndicadorNegocioVO indNegocio = newIndicadorNegocioVOItem(map, oneInd, semana.getEndDate());
        }
        for (ResumenEmpresaVO oneInd : resumen)
        {
        	IndicadorNegocioVO indNegocio = newIndicadorNegocioVOItem(map, oneInd, semana.getEndDate());
        }
        for (ResumenEmpresaVO oneInd : cartera)
        {
        	IndicadorNegocioVO indNegocio = clientesTotalesIndicadorNegocioVOItem(map, oneInd, semana.getEndDate());
        }

        
        
        List<IndicadorNegocioVO> result =  new ArrayList<IndicadorNegocioVO>(map.values());
        
		ModelMapper mapper = new ModelMapper();
		Type listType = new TypeToken<List<IndicadorNegocio>>() {}.getType();
		List<IndicadorNegocio> ins = mapper.map(result, listType);
		inRepository.save(ins);        
        return result;
	}
	
	public CajaVO corteCaja(String idRuta)
	{
		Caja corteCajs = cajaRepository.findByUnidadIdOrg(idRuta);
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CajaToVOModelMapper());
		return mapper.map(corteCajs, CajaVO.class);		
	}
	
	
}
