package com.crenx.services;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.record.formula.functions.T;

public abstract class HelperRepository {

	public static String LIKE_WILDCARD_ZERO_MORE ="%";
	public static String LIKE_WILDCARD_ONE_CHARACTER= "_";
	
	/**
	 * Si la colección es vacia regresa NULL
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("hiding")
	protected <T> List<T>getNullIfIsEmpty(List<T> collection){
		return (CollectionUtils.isEmpty(collection))?null:collection;
	}
	
	/**
	 * Regresa % si y sólo si la colección contiene al menos un caracter
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("hiding")
	protected <T> String getWilcardIncludeAll(List<T> collection){
		return (CollectionUtils.isEmpty(collection))?LIKE_WILDCARD_ZERO_MORE:null;
	}
}
