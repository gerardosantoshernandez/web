package com.crenx.services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Calendario;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Horario;
import com.crenx.data.domain.repository.CalendarioRepository;
import com.crenx.data.domain.repository.HorarioRepository;
import com.google.common.collect.Lists;

@Service
public class CalendarioServices {
    @Autowired
    private CalendarioRepository calendarioRepository;
    @Autowired
    private HorarioRepository horarioRepository;
    
    
	public boolean activo(Date fecha, int diasActivos)
	{
		Calendar ss = Calendar.getInstance();
		ss.setTime(fecha);				
		int currentDay=0;
		int dayOfWeek = ss.get(Calendar.DAY_OF_WEEK);
		switch (dayOfWeek)
		{
		case Calendar.SUNDAY:
			currentDay = 64;
			break;
		case Calendar.MONDAY:
			currentDay = 1;
			break;
		case Calendar.TUESDAY:
			currentDay = 2;
			break;
		case Calendar.WEDNESDAY:
			currentDay = 4;
			break;
		case Calendar.THURSDAY:
			currentDay = 8;
			break;
		case Calendar.FRIDAY:
			currentDay = 16;
			break;
		case Calendar.SATURDAY:
			currentDay = 32;
			break;
		}
		int bitMask = (currentDay&diasActivos);
		return (bitMask>0);
	}
	
	public Horario horarioCalendario(Calendario cal)
	{		
		Collection<Horario> cats = Lists.newArrayList(horarioRepository.findByCalendarioIdCalendario(cal.getIdCalendario()));
		Horario cat = cats.iterator().next();
		return cat;
	}

}
