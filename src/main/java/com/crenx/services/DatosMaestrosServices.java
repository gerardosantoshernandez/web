package com.crenx.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crenx.data.domain.entity.Caja;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.entity.Parametros;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.Traspaso;
import com.crenx.data.domain.entity.Visita;
import com.crenx.data.domain.repository.CajaRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.ParametrosRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.TraspasoRepository;
import com.crenx.data.domain.repository.VisitaRepository;
import com.crenx.data.domain.vo.CajaToVOModelMapper;
import com.crenx.data.domain.vo.CajaVO;
import com.crenx.data.domain.vo.CatalogoVO;
import com.crenx.data.domain.vo.ClienteModelMap;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.DatosMaestrosVO;
import com.crenx.data.domain.vo.DetalleCreditoPagosVO;
import com.crenx.data.domain.vo.FiltroMovimientoCajaVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.TotalMovimientoCajaVO;
import com.crenx.data.domain.vo.TraspasoToVO;
import com.crenx.data.domain.vo.TraspasoVO;
import com.crenx.data.domain.vo.VisitaToVisitaVOModelMapper;
import com.crenx.data.domain.vo.VisitaVO;
import com.crenx.security.UserAuthentication;
import com.crenx.util.Constantes;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;

@Service
public class DatosMaestrosServices {
	@Autowired
	private ParametrosRepository parametrosRepository;
	@Autowired
	private ProductoRepository productoRepository;
	@Autowired
	private CreditoRepository creditoRepository;
	@Autowired
	private VisitaRepository visitasRepository;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private OrgRepository orgRepository;	
	@Autowired
	private PagoRepository pagoRepository;
	@Autowired
	private MovimientoCajaRepository mvRepository;
	@Autowired
	private CajaRepository cajaRepository;
	@Autowired
	private TraspasoRepository trasRepository;

	public DatosMaestrosVO obtenerDatosMaestros(UserAuthentication perfil) {

		DatosMaestrosVO datosMaestros = new DatosMaestrosVO();
		List<Parametros> parametros = Lists.newArrayList(this.parametrosRepository.findAll());
		datosMaestros.setParametros(parametros);
		String[] catalogosACargar = { Constantes.CAT_FIG, Constantes.CAT_TIP_DOC_ID, Constantes.CAT_GEN,
				Constantes.CAT_PAIS, Constantes.CAT_ENTIDAD_FED, Constantes.CAT_OCUP, Constantes.CAT_TIP_DOC,
				Constantes.CAT_FAM_PROD, Constantes.CAT_UNI_PLAZO, Constantes.CAT_FREC_COBRO, Constantes.CAT_TIPO_GASTO,
				Constantes.CAT_MOTIVO_NOPAGO, Constantes.PERIODO_DIARIO };
		List<CatalogoVO> catalogos = new ArrayList<CatalogoVO>();
		for (String cat : catalogosACargar) {
			catalogos.add(obtenerCatalogo(cat, perfil));
		}
		datosMaestros.setCatalogos(catalogos);
		datosMaestros.setProductos(obtenerProductos(perfil));
		return datosMaestros;
	}

	private CatalogoVO obtenerCatalogo(String claveInterna, UserAuthentication perfil) {
		CatalogoVO catVo = catServices.catalogItems(perfil, claveInterna);
		return catVo;
	}

	private List<Producto> obtenerProductos(UserAuthentication perfil) {
		CatalogoVO catProductos = obtenerCatalogo(Constantes.CAT_FAM_PROD, perfil);
		List<String> familias = new ArrayList<String>();
		for (Catalogo cat : catProductos.getItems()) {
			familias.add(cat.getIdCatalogo());
		}
		List<Producto> productos = productoRepository.findByFamiliaProducto_IdCatalogoIn(familias);
		return productos;
	}

	/**
	 * Obtiene los créditos pertenecientes a una determinada ruta
	 * 
	 * @param idRuta
	 * @return
	 */
	public List<DetalleCreditoPagosVO> obtenerCreditosPorRuta(String idRuta) {
		FiltrosOrgVO filtros = new FiltrosOrgVO();
		Organizacion tree = catServices.getNodoById(idRuta);
		filtros.addOrganizacion(tree.getIdOrg(), tree.getNombre(), tree.getTipoNodo());
		return obtenerCreditosPorRuta(filtros);
	}

	/**
	 * Obtiene los créditos pertenecientes a las rutas determinadas por el
	 * filtro
	 * 
	 * @param filtros
	 * @param idOrg
	 * @return
	 */
	public List<DetalleCreditoPagosVO> obtenerCreditosPorRuta(FiltrosOrgVO filtros) {
		final OrganizacionHelperRepository helperRepo = new OrganizacionHelperRepository(this.orgRepository);
		final List<String> idRutas = helperRepo.getIds(helperRepo.getRutas(filtros));
		return this.getDetalleCreditos(
				// anterior : findAllByRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByRutaIdOrg
				//findAllByClienteRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByClienteRutaIdOrg
				creditoRepository.findAllByClienteRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByClienteRutaIdOrg(idRutas,
						Lists.newArrayList("CRED_ACTIVO", "CRED_PENDIENTE", "CRED_PENDIENTE_ENT")));
	}
	
	

	public Collection<VisitaVO> obtenerVisitas(String idOrg) {
		Collection<Visita> visitas = null;
		ArrayList<VisitaVO> visitasVO = null;
		Organizacion o = orgRepository.findOne(idOrg);
		visitas = Lists.newArrayList(visitasRepository.findByIdOrg(o));

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new VisitaToVisitaVOModelMapper());
		@SuppressWarnings("serial")
		Type listType = new TypeToken<List<VisitaVO>>() {
		}.getType();
		try {
			visitasVO = mapper.map(visitas, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return visitasVO;
	}

	/**
	 * Busca los créditos de los clientes que pertenecen a una determinada ruta
	 * 
	 * @param filtro
	 * @return
	 */
	public List<DetalleCreditoPagosVO> obtenerCreditosDesdeClientesPorRuta(final String idOrg) {
		FiltrosOrgVO filtro = new FiltrosOrgVO();
		Organizacion org = catServices.getNodoById(idOrg);
		filtro.addOrganizacion(org.getIdOrg(), org.getNombre(), org.getTipoNodo());
		return this.obtenerCreditosDesdeClientesPorRuta(filtro);
	}
	
	/**
	 * Busca los créditos de los clientes que pertenecen a una ruta detreminada por un filtro
	 * @param filtro
	 * @return
	 */
	public List<DetalleCreditoPagosVO> obtenerCreditosDesdeClientesPorRuta(final FiltrosOrgVO filtro) {
		final OrganizacionHelperRepository helperRepo = new OrganizacionHelperRepository(this.orgRepository);
		final List<Credito> creditos = new ArrayList<Credito>();
		final List<Organizacion> rutas = helperRepo.getRutas(filtro);
		if (rutas.size() > 0) {
			creditos.addAll(this.creditoRepository.findCreditosPorEstatusDesdeClientesPorRuta(
					Lists.newArrayList("CRED_ACTIVO", "CRED_PENDIENTE", "CRED_PENDIENTE_ENT"),
					helperRepo.getIds(rutas)));
		}
		return this.getDetalleCreditos(creditos);
	}
	
	/**
	 * Regresa los detalles de cada uno de los créditos
	 * 
	 * @param creditos
	 * @return
	 */
	private List<DetalleCreditoPagosVO> getDetalleCreditos(final List<Credito> creditos) {
		final List<DetalleCreditoPagosVO> detalles = new ArrayList<DetalleCreditoPagosVO>();
		if (creditos != null && creditos.size() > 0) {
			final ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new CreditoModelMap());
			final ModelMapper clienteMapper = new ModelMapper();
			clienteMapper.addMappings(new ClienteModelMap());			
			final Date hoy = utServices.justDate(new Date());
			final Map<String,IndicadorCreditoVO> mapIndicadoresRetraso = new HashMap<String,IndicadorCreditoVO>();
			final Map<String,Pago> mapUltimoPagos = new HashMap<String,Pago>();
			//Se obtienen todos los Ids de los créditos
			final List<String> idCreditos = Lists.newArrayList(Iterables.transform(creditos, new Function<Credito,String>(){
				@Override
				public String apply(Credito credito) {
					return (credito == null)?null:credito.getIdCredito();
				}
			}));			
			// Se obtienen los indicadores de cada uno de los créditos donde su
			// fecha de último pago coincide con la fecha actual
			final List<IndicadorCreditoVO> indicadoresRetraso = creditoRepository.findPagosRetrasoCredito(idCreditos,
					hoy);
			mapIndicadoresRetraso
					.putAll(Maps.uniqueIndex(indicadoresRetraso, new Function<IndicadorCreditoVO, String>() {
						@Override
						public String apply(IndicadorCreditoVO indicador) {
							return (indicador == null) ? null : indicador.getIdCredito();
						}
					}));
			
			// Se obtienen las tablas de amortización de cada credito donde su
			// fecha de último pago coincide con la fecha del pago de la tabla
			final List<Pago> ultimosPago = pagoRepository
					.findUltimosPagos(idCreditos);
			mapUltimoPagos
					.putAll(Maps.uniqueIndex(ultimosPago, new Function<Pago, String>() {
						@Override
						public String apply(Pago pago) {
							return (pago == null) ? null : pago.getCredito().getIdCredito();
						}
					}));
			for (Credito credito : creditos) {
				DetalleCreditoPagosVO detalleCreditoPago = new DetalleCreditoPagosVO();
				detalleCreditoPago.setNumeroTotalPagos(credito.getPlazo());
				detalleCreditoPago.setNumeroPagados(credito.getTotPagos());
				detalleCreditoPago.setCredito(mapper.map(credito, CreditoVO.class));
				detalleCreditoPago.setCliente(clienteMapper.map(credito.getCliente(), ClienteVO.class));
				detalleCreditoPago.setPagoPactado(credito.getPago());
				final IndicadorCreditoVO retraso = mapIndicadoresRetraso.get(credito.getIdCredito());
				final Pago ultimoPago = mapUltimoPagos.get(credito.getIdCredito());
				if(retraso != null){
					detalleCreditoPago.setNumeroVencidos(retraso.getCountCreditos());
					detalleCreditoPago.setTotalMontoVencido(retraso.getTotalCartera());
				}
				if (ultimoPago != null) {
					detalleCreditoPago.getCredito().setSaldoAnterior(ultimoPago.getSaldoAnterior());
					detalleCreditoPago.getCredito().setPagoAdicional(ultimoPago.getPagoAdicional());
					detalleCreditoPago.getCredito().setPagoComision(ultimoPago.getComision());
					detalleCreditoPago.getCredito().setPagoAbonado(ultimoPago.getAbono());
				}
				detalleCreditoPago.setRuta(credito.getRuta().getNombre());
				detalleCreditoPago.setGerencia(credito.getRuta().getParent().getNombre());
				detalleCreditoPago.setRegion(credito.getRuta().getParent().getParent().getNombre());
				detalleCreditoPago.setDivision(credito.getRuta().getParent().getParent().getParent().getNombre());
				detalles.add(detalleCreditoPago);
			}
		}
		return detalles;
	}
	
	/**
	 * Obtiene los totales de cargo y abono de los movimientos deacuerdo al
	 * filtro dado
	 * 
	 * @param filtro
	 * @return
	 */
	public List<TotalMovimientoCajaVO> getTotalesMovimientoCaja(final FiltroMovimientoCajaVO filtro) {
		final MovCajaHelperRepository helper = new MovCajaHelperRepository(mvRepository);		
		return (filtro == null) ? new ArrayList<TotalMovimientoCajaVO>() : helper.getTotalesPorFiltro(filtro);
	}
	
/**
 * 
 * @param idOrg
 * @return
 */
	public Collection<CajaVO> getConcentradoCaja(FiltrosOrgVO filtros) {
		Collection<Caja> cajas = null;
		ArrayList<CajaVO> cajasVO = null;
		filtros = utServices.filtrosById(filtros);
		
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		if (rutas.isEmpty())
		{
			//si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if(gerencias.isEmpty()){
				//si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if(regiones.isEmpty()){
					//si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if(divisiones.isEmpty()){
						//no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem: nodos)
						{
							rutas.add(oneItem.getIdOrg());
						}
						
					}else{
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for(String idDivision : divisiones){
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for(Organizacion region : lregiones){
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for(Organizacion gerencia : lgerencias){
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for(Organizacion ruta : lrutas){
							rutas.add(ruta.getIdOrg());
						}
					}
				}else{
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for(String idRegion : regiones){
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for(Organizacion gerencia : lgerencias){
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for(Organizacion ruta : lrutas){
						rutas.add(ruta.getIdOrg());
					}
				}
			}else{
				//recupero las rutas de las gerencias ok
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for(Organizacion ruta : lrutas){
					rutas.add(ruta.getIdOrg());
				}
				
			}
			
		}
		Date fechaInicial = utServices.convertStringToDate(filtros.getFechaInicial()+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date fechaFinal = utServices.convertStringToDate(filtros.getFechaFinal()+" 23:59:59", "yyyy-MM-dd HH:mm:ss");

		cajas = Lists.newArrayList(cajaRepository.findByUnidadIdOrgInAndFechaCorteBetweenOrderByUnidad(rutas, fechaInicial, fechaFinal));

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CajaToVOModelMapper());
		@SuppressWarnings("serial")
		Type listType = new TypeToken<List<CajaVO>>() {
		}.getType();
		try {
			cajasVO = mapper.map(cajas, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cajasVO;
	}
	
	public Collection<TraspasoVO> getTraspasoClientes(FiltrosOrgVO filtros){
		ArrayList<TraspasoVO> traspasosVO = null;
		Collection<Traspaso> traspasos = null;
		
		Date fechaIni= utServices.convertStringToDate(filtros.getFechaInicial()+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date fechaFin= utServices.convertStringToDate(filtros.getFechaFinal()+" 23:59:59", "yyyy-MM-dd HH:mm:ss");
		
		
		filtros = utServices.filtrosById(filtros);
		List<String> rutas = Lists.newArrayList(filtros.getRutasIds());
		if (rutas.isEmpty())
		{
			//si no hay rutas busco gerencias
			List<String> gerencias = Lists.newArrayList(filtros.getGerenciasIds());
			if(gerencias.isEmpty()){
				//si no hay gerencias busco regiones
				List<String> regiones = Lists.newArrayList(filtros.getRegionesIds());
				if(regiones.isEmpty()){
					//si no hay regiones busco divisiones
					List<String> divisiones = Lists.newArrayList(filtros.getDivisionesIds());
					if(divisiones.isEmpty()){
						//no puso ningún filtro---> obtengo todas las rutas
						List<Organizacion> nodos = orgRepository.findByTipoNodo("RR");
						for (Organizacion oneItem: nodos)
						{
							rutas.add(oneItem.getIdOrg());
						}
						
					}else{
						List<Organizacion> lregiones = new ArrayList<Organizacion>();
						List<Organizacion> lgerencias = new ArrayList<Organizacion>();
						List<Organizacion> lrutas = new ArrayList<Organizacion>();
						for(String idDivision : divisiones){
							lregiones.addAll(orgRepository.findByParentIdOrg(idDivision));
						}
						for(Organizacion region : lregiones){
							lgerencias.addAll(orgRepository.findByParentIdOrg(region.getIdOrg()));
						}
						for(Organizacion gerencia : lgerencias){
							lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
						}
						for(Organizacion ruta : lrutas){
							rutas.add(ruta.getIdOrg());
						}
					}
				}else{
					List<Organizacion> lgerencias = new ArrayList<Organizacion>();
					List<Organizacion> lrutas = new ArrayList<Organizacion>();
					for(String idRegion : regiones){
						lgerencias.addAll(orgRepository.findByParentIdOrg(idRegion));
					}
					for(Organizacion gerencia : lgerencias){
						lrutas.addAll(orgRepository.findByParentIdOrg(gerencia.getIdOrg()));
					}
					for(Organizacion ruta : lrutas){
						rutas.add(ruta.getIdOrg());
					}
				}
			}else{
				//recupero las rutas de las gerencias ok
				List<Organizacion> lrutas = new ArrayList<Organizacion>();
				for (String idGerencia : gerencias) {
					lrutas.addAll(orgRepository.findByParentIdOrg(idGerencia));
				}
				for(Organizacion ruta : lrutas){
					rutas.add(ruta.getIdOrg());
				}
				
			}
			
		}
		traspasos = Lists.newArrayList(trasRepository.findByRutaOrigenIdOrgInAndFechaOperacionBetween(rutas, fechaIni, fechaFin));
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new TraspasoToVO());
		@SuppressWarnings("serial")
		Type listType = new TypeToken<List<TraspasoVO>>() {
		}.getType();
		try {
			traspasosVO = mapper.map(traspasos, listType);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("--->"+traspasosVO.size());
		return traspasosVO;
	}
}
