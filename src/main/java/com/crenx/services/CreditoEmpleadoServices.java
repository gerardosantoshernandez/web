package com.crenx.services;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.CreditoEmpleado;
import com.crenx.data.domain.repository.CreditoEmpleadoRepository;
import com.crenx.data.domain.vo.CreditoEmpleadoToVOModelMapper;
import com.crenx.data.domain.vo.CreditoEmpleadoVO;
import com.crenx.data.domain.vo.CreditoEmpleadoVOToCreditoEmpleadoMapper;
import com.crenx.data.domain.vo.FiltrosCredEmplVO;
import com.crenx.data.domain.vo.MovimientoCajaModelMap;
import com.crenx.data.domain.vo.NameValueVO;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@Service
public class CreditoEmpleadoServices {
	@Autowired
	private CreditoEmpleadoRepository credEmpRepository;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	UtilityServices utServices;	


	public List<CreditoEmpleadoVO> getCreditosEmpl(FiltrosCredEmplVO filtros) {
		
		if (filtros.getEstatusCredito()==null)
		{
			filtros.setEstatusCredito(catServices.getCatalogoIds("CAT_ESTATUS_CREDITO"));
		}
		
		if (filtros.getTiposCredito()==null)
		{
			filtros.setTiposCredito(catServices.getCatalogoIds("CAT_TIPO_CRED_EMPL"));
		}
			
		filtros.setNombre(filtros.getNombre()==null?"%":filtros.getNombre()+"%");
		filtros.setApellidoPaterno(filtros.getApellidoPaterno()==null?"%":filtros.getApellidoPaterno()+"%");
		filtros.setIdEmpleado(filtros.getIdEmpleado()==null?"%":filtros.getIdEmpleado());
		
		List<CreditoEmpleado> creditos = credEmpRepository.findByEstatusCreditoIdCatalogoInAndTipoCreditoIdCatalogoInAndEmpleadoNombreLikeAndEmpleadoApellidoPaternoLikeAndEmpleadoIdEmpleadoLike(
				Lists.newArrayList(filtros.getEstatusCredito()), 
				Lists.newArrayList(filtros.getTiposCredito()), 
				filtros.getNombre(), 
				filtros.getApellidoPaterno(),
				filtros.getIdEmpleado());

		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoEmpleadoToVOModelMapper());
		Type listType = new TypeToken<List<CreditoEmpleadoVO>>() {}.getType();
		List<CreditoEmpleadoVO> listaCreditos = mapper.map(creditos, listType);
		return listaCreditos;
	}
	
	public CreditoEmpleado getCreditoById(String idCredito)
	{
		return credEmpRepository.findOne(idCredito);
	}
	
	@Transactional
	public void saveCreditoEmpleado(CreditoEmpleadoVO  cred)
	{
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoEmpleadoVOToCreditoEmpleadoMapper());
		CreditoEmpleado credDB = mapper.map(cred, CreditoEmpleado.class);
		if (cred.getIdTipoCredito()!=null)
		{
			Catalogo tipoCred = catServices.getCatalogoId(cred.getIdTipoCredito());
			credDB.setTipoCredito(tipoCred);

		}
		if (credDB.getId()==0)
		{
			credDB.setId(utServices.generaId("CreditoEmpleado"));
			credDB.setSaldo(credDB.getMonto());
		}
		if (cred.getIdEstatusCredito()==null)
		{
			Catalogo estatus = catServices.getCatalogoCveInterna("CRED_ACTIVO");
			credDB.setEstatusCredito(estatus);
		}else
		{
			Catalogo estatus = catServices.getCatalogoId(cred.getIdEstatusCredito());
			credDB.setEstatusCredito(estatus);
			
		}
		credEmpRepository.save(credDB);
	}
	
	
}
