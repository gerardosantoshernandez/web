package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.GeneradorId;

public interface GeneradorIdRepository extends CrudRepository<GeneradorId,String>{
	GeneradorId findByEntidad(String entidad);
}
