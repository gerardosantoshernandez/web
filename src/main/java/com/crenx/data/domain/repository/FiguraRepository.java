package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Figura;

public interface FiguraRepository extends CrudRepository<Figura,String> {

}
