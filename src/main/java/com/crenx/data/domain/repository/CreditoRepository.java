package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.vo.EstadoCarteraVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.IndicadorOperacionesClienteVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;

public interface CreditoRepository extends CrudRepository<Credito,String> {

	List<Credito> findByClienteIdClienteOrderByFechaEmisionDesc(String idCliente);
	List<Credito> findByClienteIdClienteAndEstatusCreditoIdCatalogoOrderByFechaEmisionDesc(String idCliente, String idEstatus);
	//mcRepository.findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(fechaInicial, fechaFinal, idsOrg));
	List<Credito> findAllByRutaIdOrgAndEstatusCreditoClaveInterna(String idRuta, String cleveInterna);
	List<Credito> findAllByRutaIdOrgInAndEstatusCreditoClaveInternaOrderByClienteNombre(List<String> rutas, String cleveInterna);
	List<Credito> findAllByRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByClienteNombre(List<String> rutas, List<String> clevesInternas);
	List<Credito> findAllByRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByRutaIdOrg(List<String> rutas, List<String> clevesInternas);
	List<Credito> findAllByClienteRutaIdOrgInAndEstatusCreditoClaveInternaInOrderByClienteRutaIdOrg(List<String> rutas, List<String> clevesInternas);
	List<Credito> findByIdCredito(String idCredito);
	List<Credito> findByClienteRutaIdOrgInAndFechaEmisionBetweenAndTipoCreditoClaveInternaEquals(List<String> rutas, Date fechaInicial, Date fechaFinal, String tipoCred);
	
	
	@Query("select c "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta o "
			+ "LEFT JOIN c.visitas v "
			+ "JOIN v.idOrg aux "
			+ "WHERE "
			+ "o.idOrg in (:rutas) "
			+ "AND sc.claveInterna in (:clavesInternas) "
			+ "AND aux.idOrg = :idOrg "
			)
	List<Credito> findAllCreditosVisitas(@Param("rutas")List<String> rutas,@Param("clavesInternas") List<String> clavesInternas, @Param ("idOrg") String idOrg);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(c.saldo),0) as totalCartera, COALESCE(sum(c.saldoInsoluto),0) as carteraCapital  ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta o "
			+ "WHERE o.idOrg in (:filtros )  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO'")
	IndicadorCreditoVO findCarteraByRutas (@Param("filtros") List<String> rutas);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(c.saldo),0) as totalCartera, COALESCE(sum(c.saldoInsoluto),0) as carteraCapital  ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "WHERE g.idOrg in (:filtros )  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO'")
	IndicadorCreditoVO findCarteraByGerencias (@Param("filtros") List<String> rutas);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(c.saldo),0) as totalCartera, COALESCE(sum(c.saldoInsoluto),0) as carteraCapital  ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "WHERE rr.idOrg in (:filtros )  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO'")
	IndicadorCreditoVO findCarteraByRegiones (@Param("filtros") List<String> rutas);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(c.saldo),0) as totalCartera, COALESCE(sum(c.saldoInsoluto),0) as carteraCapital  ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE d.idOrg in (:filtros )  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO'")
	IndicadorCreditoVO findCarteraByDivisiones (@Param("filtros") List<String> rutas);

	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(c.saldo),0) as totalCartera, COALESCE(sum(c.saldoInsoluto),0) as carteraCapital ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.ruta o "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'")
	IndicadorCreditoVO findCarteraAll();

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN t.tipoPago tp "			
			+ "WHERE date(t.fechaVencimiento) <=:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' " 
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "AND c.idCredito = :idCredito " )
	IndicadorCreditoVO findPagosRetrasoCredito(@Param("idCredito")String idCredito, @Param("fechaProceso") Date fechaProceso);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (c.idCredito, count(*) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN t.tipoPago tp "			
			+ "WHERE date(t.fechaVencimiento) <=:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' " 
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "AND c.idCredito in(:idsCredito) "
			+ "group by c.idCredito " )
	List<IndicadorCreditoVO> findPagosRetrasoCredito(@Param("idsCredito")List<String> idsCredito, @Param("fechaProceso") Date fechaProceso);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(distinct c.idCredito) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' " )
	IndicadorCreditoVO findCarteraVencidaAll(@Param("fechaProceso") Date fechaProceso);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(distinct c.idCredito) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN c.ruta o "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' "
			+ "AND o.idOrg in (:filtros )  " )
	IndicadorCreditoVO findCarteraVencidaByRutas(@Param("filtros") List<String> rutas, @Param("fechaProceso") Date fechaProceso);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(distinct c.idCredito) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' "
			+ "AND g.idOrg in (:filtros )  " )
	IndicadorCreditoVO findCarteraVencidaByGerencias(@Param("filtros") List<String> rutas, @Param("fechaProceso") Date fechaProceso);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(distinct c.idCredito) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' "
			+ "AND rr.idOrg in (:filtros )  " )
	IndicadorCreditoVO findCarteraVencidaByRegiones(@Param("filtros") List<String> rutas, @Param("fechaProceso") Date fechaProceso);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(distinct c.idCredito) as countCreditos, COALESCE(sum(t.cuota),0) as totalCartera, coalesce(sum(0.0),0.0) ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN t.estatusPago sp "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND sp.claveInterna = 'PAGO_PENDIENTE' "
			+ "AND d.idOrg in (:filtros )  " )
	IndicadorCreditoVO findCarteraVencidaByDivisiones(@Param("filtros") List<String> rutas, @Param("fechaProceso") Date fechaProceso);

	//Créditos acumulados en un mes
	@Query("select new com.crenx.data.domain.vo.IndicadorOperacionesClienteVO (d.nombre as division, rr.nombre as region, g.nombre as gerencia, r.nombre as ruta, cl.idCliente as idCliente, COALESCE(sum(c.monto),0) as monto ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND date(c.fechaEmision) between :fechaInicial AND :fechaFinal "
			+ "GROUP BY d.nombre, rr.nombre, g.nombre, r.nombre, cl.idCliente ")
	List<IndicadorOperacionesClienteVO> findCreditosClienteMes(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal);
	
	
	
	//Nuevos créditos en el periodo x Ruta
	List<Credito> findByFechaEmisionBetween(Date fechaInicial, Date fechaFinal);

	List<Credito> findByFechaEmisionBetweenAndRutaIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> rutas);
	
	List<Credito> findByFechaEmisionBetweenAndRutaParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> gerencias);

	List<Credito> findByFechaEmisionBetweenAndRutaParentParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> regiones);

	List<Credito> findByFechaEmisionBetweenAndRutaParentParentParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> divisiones);

	//Para Resumen Empresarial, todos la cartera
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO (r.nombre as ruta, g.nombre as gerencia, rr.nombre as region, d.nombre as division, COALESCE(sum(c.saldo),0.0) as carteraTotal, COALESCE(sum(c.saldoInsoluto),0.0) as carteraCapital ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.cliente cl "
			+ "JOIN cl.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND r.idOrg in (:filtros) "
			//+ "GROUP BY r.nombre , g.nombre, rr.nombre, d.nombre "
			+ "GROUP BY  d.nombre, rr.nombre, g.nombre, r.nombre "
			+ "ORDER BY d.nombre, rr.nombre, g.nombre, r.nombre")
	List<ResumenEmpresaVO> findByRutasAndFechas(@Param("filtros") List<String> rutas);
	
	
	//Para Reporte de estado de cartera no vencida
	@Query("select new com.crenx.data.domain.vo.EstadoCarteraVO (div.nombre as division, region.nombre as region, g.nombre as gerencia, r.nombre as ruta,   "
			+ "count(*) as contador, COALESCE(sum(c.saldo),0.0) as monto ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.cliente cl "
			+ "JOIN cl.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent region "
			+ "JOIN region.parent div "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND DATEDIFF(CURDATE(), COALESCE(c.fechaUltimoPago, c.fechaEmision) ) between :rangoInicial AND :rangoFinal "
			+ "AND c.fechaVencimiento >= CURDATE() "
			+ "AND r.idOrg in (:filtros) "
			+ "AND c.saldo >0 "
			+ "GROUP BY div.nombre, region.nombre, g.nombre, r.nombre "
			+ "ORDER BY div.nombre, region.nombre, g.nombre, r.nombre ")
	List<EstadoCarteraVO> carteraByRutasAndRangos(@Param("filtros") List<String> rutas, @Param("rangoInicial") int rangoInicial, @Param("rangoFinal") int rangoFinal);
	
	//Para Reporte de estado de cartera no vencida
	@Query("select new com.crenx.data.domain.vo.EstadoCarteraVO (div.nombre, region.nombre, g.nombre, r.nombre, "
			+ "count(*) as contador, COALESCE(sum(c.saldo),0.0) as monto ) "
			+ "FROM Credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.cliente cl "
			+ "JOIN cl.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent region "
			+ "JOIN region.parent div "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND c.fechaVencimiento < CURDATE() "
			+ "AND r.idOrg in (:filtros) "
			+ "AND c.saldo >0 "
			+ "GROUP BY div.nombre, region.nombre, g.nombre, r.nombre "
			+ "ORDER BY div.nombre, region.nombre, g.nombre, r.nombre ")
	List<EstadoCarteraVO> carteraByRutasVencida(@Param("filtros") List<String> rutas );

	List<Credito> findByRutaIdOrgInAndClienteNombreLikeAndClienteApellidoPaternoLike(List<String> rutas, String nombre, String apellidoPaterno);
	
	List<Credito> findByClienteNombreLikeAndClienteApellidoPaternoLike(String nombre, String apellidoPaterno);

	List<Credito> findByClienteNombreLikeAndClienteApellidoPaternoLikeAndEstatusCreditoIdCatalogo(String nombre, String apellidoPaterno,String estatus);
	List<Credito> findByFechaVencimientoBeforeAndEstatusCreditoClaveInterna(Date fechaVencimiento, String cleveInterna );
	List<Credito> findByClienteRutaIdOrgInAndAndClienteNombreLikeAndClienteApellidoPaternoLike(List<String> rutas, String nombre, String apellidoPaterno);
	
	List<Credito> findByClienteRutaIdOrgInAndAndClienteNombreLikeAndClienteApellidoPaternoLikeAndEstatusCreditoIdCatalogo(List<String> rutas, String nombre, String apellidoPaterno,String idEstatus);
	//Para Reporte de estado de cartera no vencida
		@Query("select new com.crenx.data.domain.vo.EstadoCarteraVO (div.nombre, region.nombre, g.nombre, r.nombre, "
				+ "count(*) as contador, COALESCE(sum(c.saldo),0.0) as monto ) "
				+ "FROM Credito c  "
				+ "JOIN c.estatusCredito sc "
				+ "JOIN c.cliente cl "
				+ "JOIN cl.ruta r "
				+ "JOIN r.parent g "
				+ "JOIN g.parent region "
				+ "JOIN region.parent div "
				+ "WHERE sc.claveInterna = 'CRED_INCOBRABLE' "
				+ "AND r.idOrg in (:filtros) "
				+ "GROUP BY div.nombre, region.nombre, g.nombre, r.nombre "
				+ "ORDER BY div.nombre, region.nombre, g.nombre, r.nombre ")
		List<EstadoCarteraVO> carteraByRutasIncobrable(@Param("filtros") List<String> rutas );

		//Para detalle Reporte de estado de cartera incobrable
		@Query("select c "
				+ "FROM Credito c  "
				+ "JOIN c.estatusCredito sc  "
				+ "JOIN c.cliente cl "
				+ "JOIN cl.ruta r "
				+ "JOIN r.parent g "
				+ "JOIN g.parent region "
				+ "JOIN region.parent div "
				+ "WHERE sc.claveInterna = 'CRED_INCOBRABLE' "
				+ "AND r.idOrg in (:filtros) "
				)
		List<Credito> carteraDetalleByRutasIncobrable(@Param("filtros") List<String> rutas );

	//Para detalle de estado de cartera no vencida
		@Query("select c "
				+ "FROM Credito c  "
				+ "JOIN c.estatusCredito sc  "
				+ "JOIN c.ruta r "
				+ "JOIN r.parent g "
				+ "JOIN g.parent region "
				+ "JOIN region.parent div "
				+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
				+ "AND DATEDIFF(CURDATE(), COALESCE(c.fechaUltimoPago, c.fechaEmision)) between :rangoInicial AND :rangoFinal "
				+ "AND c.fechaVencimiento >= CURDATE() "
				+ "AND r.idOrg in (:filtros) "
				+ "AND c.saldo >0 "
				)
		List<Credito> carteraSanaDetalleByRutasAndRangos(@Param("filtros") List<String> rutas, @Param("rangoInicial") int rangoInicial, @Param("rangoFinal") int rangoFinal);

		//Para detalle -preventiva
		@Query("select c "
				+ "FROM Credito c  "
				+ "JOIN c.estatusCredito sc  "
				+ "JOIN c.cliente cl "
				+ "JOIN cl.ruta r "
				+ "JOIN r.parent g "
				+ "JOIN g.parent region "
				+ "JOIN region.parent div "
				+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
				+ "AND DATEDIFF(CURDATE(), COALESCE(c.fechaUltimoPago, c.fechaEmision) ) between :rangoInicial AND :rangoFinal "
				+ "AND c.fechaVencimiento >= CURDATE() "
				+ "AND r.idOrg in (:filtros) "
				+ "AND c.saldo >0 "
				)
		List<Credito> carteraDetalleByRutasAndRangos(@Param("filtros") List<String> rutas, @Param("rangoInicial") int rangoInicial, @Param("rangoFinal") int rangoFinal);
		
		//Para detalle Reporte de estado de cartera no vencida
		@Query("select c "
				+ "FROM Credito c  "
				+ "JOIN c.estatusCredito sc  "
				+ "JOIN c.cliente cl "
				+ "JOIN cl.ruta r "
				+ "JOIN r.parent g "
				+ "JOIN g.parent region "
				+ "JOIN region.parent div "
				+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
				+ "AND c.fechaVencimiento < CURDATE() "
				+ "AND r.idOrg in (:filtros) "
				+ "AND c.saldo >0 "
				)
		List<Credito> carteraDetalleByRutasVencida(@Param("filtros") List<String> rutas );
		
		/**
		 * Obtiene un conjunto de créditos pertencecientes a los clientes de
		 * determinaas rutas
		 * 
		 * @param clavesEstatus. Las claves de los estatus del crédito
		 * @param rutas Los ids de las rutas
		 * @return
		 */
		@Query("select credito from Credito as credito " 
				+ "inner join credito.estatusCredito as estatus "
				+ "inner join fetch credito.cliente as cliente "			
				+ "inner join fetch cliente.ruta as ruta "
				+ "where ruta.idOrg in (:rutas) " 
				+ "and estatus.claveInterna in (:clavesEstatus) "
				+ "order by credito.fechaEmision")
		List<Credito> findCreditosPorEstatusDesdeClientesPorRuta(@Param("clavesEstatus") List<String> clavesEstatus,
				@Param("rutas") List<String> rutas);
}