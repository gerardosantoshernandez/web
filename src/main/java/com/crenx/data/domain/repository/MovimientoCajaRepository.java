package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import com.crenx.data.domain.vo.TotalMovimientoCajaVO;

public interface MovimientoCajaRepository
		extends CrudRepository<MovimientoCaja, String>, JpaSpecificationExecutor<MovimientoCaja> {

	List<MovimientoCaja> findByRutaIdOrgAndFechaEmisionAfterAndFechaEmisionBefore(List<String> rutas, Date fechaInicial,
			Date fechaFinal);

	// List<MovimientoCaja> findByFechaEmisionBetweenAndRutaIdOrgIn(Date
	// fechaInicial, Date fechaFinal, List<String> rutas);
	List<MovimientoCaja> findByFechaEmisionBetweenAndRutaIdOrgInOrderByRutaIdOrg(Date fechaInicial, Date fechaFinal,
			List<String> rutas);

	List<MovimientoCaja> findByFechaEmisionBetweenAndRutaIdOrgInAndTipoOperacionIdCatalogo(Date fechaInicial,
			Date fechaFinal, List<String> rutas, String idTipoOperacion);

	List<MovimientoCaja> findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogo(Date fechaInicial,
			Date fechaFinal, List<String> rutas, String idTipoMovimiento);

	List<MovimientoCaja> findByFechaEmisionBetweenAndRutaIdOrgInAndTipoMovimientoIdCatalogoAndTipoOperacionIdCatalogoOrderByRutaIdOrg(
			Date fechaInicial, Date fechaFinal, List<String> rutas, String tMov, String tOp);

	// Para Resumen Empresarial, todos los desembolsos
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO (r.nombre as ruta, g.nombre as gerencia, rr.nombre as region, d.nombre as division, COALESCE(sum(m.cargo),0.0) as monto, sum(0.0) as abono, sum(0.0) as comision, count(*) as nuevosCreditos ) "
			+ "FROM MovimientoCaja m  " + "JOIN m.tipoOperacion tp " + "JOIN m.ruta r " + "JOIN r.parent g "
			+ "JOIN g.parent rr " + "JOIN rr.parent d "
			+ "WHERE date(m.fechaEmision) between :fechaInicial AND :fechaFinal "
			+ "AND tp.claveInterna = :tipoOperacion " + "AND r.idOrg in (:filtros) "
			// + "GROUP BY r.nombre, g.nombre, rr.nombre, d.nombre "
			+ "GROUP BY  d.nombre, rr.nombre, g.nombre, r.nombre " + "ORDER BY d.nombre, rr.nombre, g.nombre, r.nombre")
	List<ResumenEmpresaVO> findByRutasAndFechas(@Param("fechaInicial") Date fechaInicial,
			@Param("fechaFinal") Date fechaFinal, @Param("filtros") List<String> rutas,
			@Param("tipoOperacion") String tipoOperacion);

	List<MovimientoCaja> findByReferenciaAndFechaEmisionBetween(String referencia, Date fechaInicio, Date fechaFin);

	MovimientoCaja findByIdMovCaja(String movcaja);

	/**
	 * Regresa un con junto de totales de los movimientos de caja.
	 * 
	 * Los parámetros de all<X> son auxiliares que permiten simular una búsqueda
	 * con la combinación de cualquier organización tipo de movimiento, tipo de
	 * oepración y ruta.
	 * 
	 * Para que este método funcione adecuadamente:
	 * 
	 * 1) Si la búsqueda no incluye tipos de movimientos, tipos de operación o
	 * rutas específicas entonces la lista correspondiente debe ser NULL y el
	 * valor auxiliar correspondiente debe ser el Wildcard %
	 * (@HelperRepository.LIKE_WILDCARD_ZERO_MORE)
	 * 
	 * 2) Si se desea buscar un tipos de movimiento, tipos de operación o rutas
	 * específicos, la lista corrspondiente debe contener al menos un valor y el
	 * valor auxiliar respectivo debe ser NULL
	 * 
	 * @param tiposMov
	 * @param allTmvs
	 * @param tiposOp
	 * @param allTops
	 * @param rutas
	 * @param allRutas
	 * @return
	 */
	@Query("select  new com.crenx.data.domain.vo.TotalMovimientoCajaVO(rt.idOrg, rt.nombre, tm.idCatalogo,tm.nombre, "
			+ "to.idCatalogo, to.nombre, sum(mv.cargo), sum(mv.abono))  " 
			+ "from MovimientoCaja mv "
			+ "inner join mv.tipoMovimiento tm " 
			+ "inner join mv.tipoOperacion to " 
			+ "inner join mv.ruta rt "
			+ "where mv.fechaSistema between :fchInicio and :fchFin "
			+ "and (tm.idCatalogo in (:tiposMov) or tm.idCatalogo like :allTmvs) "
			+ "and (to.idCatalogo in (:tiposOp) or to.idCatalogo like :allTops) "
			+ "and (rt.idOrg in (:rutas) or rt.idOrg like :allRutas) "
			+ "group by  rt.idOrg, tm.idCatalogo, to.idCatalogo " + "order by rt.nombre")
	List<TotalMovimientoCajaVO> findTotalesMovCaja(@Param("tiposMov") List<String> tiposMov,
			@Param("allTmvs") String allTmvs, @Param("tiposOp") List<String> tiposOp, @Param("allTops") String allTops,
			@Param("rutas") List<String> rutas, @Param("allRutas") String allRutas,
			@Param("fchInicio") Date fchInicio, @Param("fchFin") Date fchFin);

}
