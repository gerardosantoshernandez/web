package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.vo.IndicadorOperacionesClienteVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;

public interface PagoRepository extends CrudRepository<Pago,String> {

	List<Pago> findByCreditoIdCredito(String idCredito);
	
	
	//Créditos acumulados en un mes
	@Query("select new com.crenx.data.domain.vo.IndicadorOperacionesClienteVO (d.nombre as division, rr.nombre as region, g.nombre as gerencia, r.nombre as ruta, cl.idCliente as idCliente, COALESCE(sum(p.abono),0) as monto ) "
			+ "FROM Pago p  "
			+ "JOIN p.credito c  "
			+ "JOIN c.estatusCredito sc  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO' "
			+ "AND date(p.fechaPago) between :fechaInicial AND :fechaFinal "
			+ "GROUP BY d.nombre, rr.nombre, g.nombre, r.nombre, cl.idCliente ")
	List<IndicadorOperacionesClienteVO> findPagosClienteMes(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal);

	//Para Resumen Empresarial
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO (r.nombre as ruta, g.nombre as gerencia, rr.nombre as region, d.nombre as division, sum(0.0) as monto, COALESCE(sum(m.abono),0.0) as abono, COALESCE(sum(m.comision),0.0) as comision ) "
			+ "FROM Pago m  "
			+ "JOIN m.credito c "
			+ "JOIN c.cliente cl "
			+ "JOIN cl.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE date(m.fechaPago) between :fechaInicial AND :fechaFinal "
			+ "AND r.idOrg in (:filtros) "
		//	+ "GROUP BY r.nombre, g.nombre, rr.nombre, d.nombre ")
			+ "GROUP BY  d.nombre, rr.nombre, g.nombre, r.nombre "
			+ "ORDER BY d.nombre, rr.nombre, g.nombre, r.nombre ")
	List<ResumenEmpresaVO> findByRutasAndFechas(@Param("fechaInicial")  Date fechaInicial, @Param("fechaFinal") Date fechaFinal, @Param("filtros") List<String> rutas);
	
	/**
	 * Buscan los pagos de pago de determinados créditos que coincidan la fecha actual
	 * 
	 * @param idsCredito
	 * @param fechaPago
	 * @return
	 */
	@Query("select pago from TablaAmortizacion as tabla " 
			+ "inner join tabla.credito as credito "
			+ "inner join tabla.pago as pago "
			+ "where credito.idCredito in (:idsCredito) "
			+ "and date(credito.fechaUltimoPago) = date(current_date) "
			+ "and pago.fechaPago = credito.fechaUltimoPago "
			+ "and tabla.noPago = ("
			+ "select max(tabla1.noPago) from TablaAmortizacion tabla1 "
			+ "inner join tabla1.estatusPago as catalogo with catalogo.claveInterna =  'PAGO_PAGADO' "
			+ "inner join tabla1.credito as credito1 "
			+ "where credito1.idCredito = credito.idCredito) "
			+ "group by pago.idPago")
	List<Pago> findPagosDeHoy(@Param("idsCredito") List<String> idsCredito);
	
	/**
	 * Buscan los últimos pagos de determinados créditos
	 * 
	 * @param idsCredito
	 * @return
	 */
	@Query("select pago from TablaAmortizacion as tabla " 
			+ "inner join tabla.credito as credito "
			+ "inner join tabla.pago as pago "
			+ "where credito.idCredito in (:idsCredito) "			
			+ "and tabla.noPago = ("
			+ "select max(tabla1.noPago) from TablaAmortizacion tabla1 "
			+ "inner join tabla1.pago as pago1 "
			+ "inner join tabla1.estatusPago as catalogo with catalogo.claveInterna =  'PAGO_PAGADO' "
			+ "inner join tabla1.credito as credito1 "
			+ "where credito1.idCredito = credito.idCredito "
			+ "and pago1.fechaPago = credito.fechaUltimoPago "
			+ ") "
			+ "group by pago.idPago")
	List<Pago> findUltimosPagos(@Param("idsCredito") List<String> idsCredito);
	
}
