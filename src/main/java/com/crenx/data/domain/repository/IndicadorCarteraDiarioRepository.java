package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.IndicadorCarteraDiario;
import com.crenx.data.domain.vo.IndicadorCarteraDiarioVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.InidicadorRutaVO;

public interface IndicadorCarteraDiarioRepository extends CrudRepository<IndicadorCarteraDiario,String> {
	
	@Query(nativeQuery = true)
	public List<IndicadorCarteraDiarioVO> findByToday();
		
	@Query( nativeQuery = true)
	List<IndicadorCarteraDiarioVO> findByCarteraRutaDia(@Param("fechaInicial") Date fechaProceso, @Param("fechaFinal") Date fechaFinal, @Param("filtros") List<String> rutas);

	@Query( nativeQuery = true)
	List<IndicadorCarteraDiarioVO> findByCarteraRutaDiaRaw(@Param("dia") String dia, @Param("filtros") List<String> rutas);

	
}
