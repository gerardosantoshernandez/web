package com.crenx.data.domain.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.IncidenciaNomina;

public interface IncidenciaNominaRepository extends CrudRepository<IncidenciaNomina, String> {

	List<IncidenciaNomina> findBySemana(long semana);
}

