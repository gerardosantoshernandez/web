package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.IndicadorCobranza;
import com.crenx.data.domain.entity.IndicadorCobranzaPeriodo;

public interface IndicadorCobranzaPeriodoRepository extends CrudRepository<IndicadorCobranzaPeriodo,String> {

	List<IndicadorCobranzaPeriodo> findByRutaIdOrgAndSemana(String idRuta, long semana);
}
