package com.crenx.data.domain.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.CreditoEmpleado;

public interface CreditoEmpleadoRepository extends CrudRepository<CreditoEmpleado,String>{

	List<CreditoEmpleado> findByEstatusCreditoIdCatalogoInAndTipoCreditoIdCatalogoInAndEmpleadoNombreLikeAndEmpleadoApellidoPaternoLikeAndEmpleadoIdEmpleadoLike(
				List<String> estatus,
				List<String> tipoCredito,
				String nombre,
				String apePat,
				String idEmpleado);
}
