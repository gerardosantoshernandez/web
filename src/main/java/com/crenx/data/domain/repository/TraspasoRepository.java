package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Traspaso;



public interface TraspasoRepository extends CrudRepository<Traspaso,String>  {
	List<Traspaso> findByRutaOrigenIdOrgInAndFechaOperacionBetween(List<String> orgs, Date fechaIni, Date fechaFin);

}
