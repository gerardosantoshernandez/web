package com.crenx.data.domain.repository;



import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Visita;
import com.crenx.data.domain.entity.Organizacion;

import java.util.Date;
import java.util.List;

public interface VisitaRepository extends CrudRepository<Visita,String>{
	List<Visita> findByIdOrg(Organizacion idorg);
	List<Visita> findByFechaVisitaBetweenAndRutaIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> rutas);
	List<Visita> findByFechaVisitaBetweenAndCreditoClienteRutaIdOrg(Date fechaInicial, Date fechaFinal, List<String> rutas);
}
