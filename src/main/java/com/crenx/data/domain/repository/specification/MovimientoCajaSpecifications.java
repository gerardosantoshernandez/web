package com.crenx.data.domain.repository.specification;

import java.sql.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import com.crenx.data.domain.entity.MovimientoCaja;

public class MovimientoCajaSpecifications {

	/**
	 * Especificación para obtener el movimiento de caja con la fecha de sistema
	 * más antigüa
	 * 
	 * @return
	 */
	public static Specification<MovimientoCaja> oldest() {
		return new Specification<MovimientoCaja>() {
			@Override
			public Predicate toPredicate(Root<MovimientoCaja> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				Subquery<Date> subquery = query.subquery(Date.class);
				Root<MovimientoCaja> mcs = subquery.from(MovimientoCaja.class);
				subquery.select(builder.least(mcs.<Date>get("fechaSistema")));
				return builder.equal(root.get("fechaSistema"), builder.all(subquery));				
			}
		};
	}
}
