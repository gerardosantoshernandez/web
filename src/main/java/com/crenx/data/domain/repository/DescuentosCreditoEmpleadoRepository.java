package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.DescuentosCreditoEmpleado;

public interface DescuentosCreditoEmpleadoRepository extends CrudRepository<DescuentosCreditoEmpleado, String> {

	List<DescuentosCreditoEmpleado> findBySemana(long semana);
	
}
