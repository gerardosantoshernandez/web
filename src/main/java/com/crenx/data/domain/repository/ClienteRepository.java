package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import java.lang.String;

public interface ClienteRepository extends CrudRepository<Cliente,String> {
	List<Cliente> findByIdCliente(String idcliente);
	List<Cliente> findByTipoFiguraIdCatalogo(String idFigura);
	List<Cliente> findByRutaIdOrgAndNombre(List<NameValueVO> rutas, String nombre);
	List<Cliente> findByNombreIsLikeAndApellidoPaternoIsLike(String nombre, String apellidoPaterno);

	List<Cliente> findByTipoFiguraIdCatalogoAndRutaIdOrgInAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(String idFigura, List<String> rutas, String nombre, String apellidoPaterno );
	List<Cliente> findByTipoFiguraIdCatalogoAndNombreIsLikeAndApellidoPaternoIsLikeOrderByNombre(String idFigura, String nombre, String apellidoPaterno );
	
	List<Cliente> findBytipoFiguraIdCatalogoAndRutaIdOrg(String idFigura, List<String> rutas);

	List<Cliente> findByCreacionBetweenAndRutaIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> rutas);
	List<Cliente> findByCreacionBetweenAndRutaParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> gerencias);
	List<Cliente> findByCreacionBetweenAndRutaParentParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> regiones);
	List<Cliente> findByCreacionBetweenAndRutaParentParentParentIdOrgIn(Date fechaInicial, Date fechaFinal, List<String> divisiones);
	List<Cliente> findByCreacionBetween(Date fechaInicial, Date fechaFinal);

	List<Cliente> findByRutaIdOrgIn(List<String> rutas);
	List<Cliente> findByRutaParentIdOrgIn(List<String> gerencias);
	List<Cliente> findByRutaParentParentIdOrgIn(List<String> regiones);
	List<Cliente> findByRutaParentParentParentIdOrgIn(List<String> divisiones);
	
	//
	List<Cliente> findByTipoFiguraIdCatalogoAndRutaIdOrgInAndNombreIsLikeAndApellidoPaternoIsLikeAndCreditosTipoCreditoClaveInternaEqualsOrderByNombre(String idFigura, List<String> rutas, String nombre, String apellidoPaterno, String tipoCred);
	List<Cliente> findByTipoFiguraIdCatalogoAndNombreIsLikeAndApellidoPaternoIsLikeAndCreditosTipoCreditoClaveInternaEqualsOrderByNombre(String idFigura, String nombre, String apellidoPaterno, String tipoCred );

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as clientesActivos, coalesce(sum(0.0),0.0), coalesce(sum(0.0),0.0) ) "
			+ "FROM Credito c  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "JOIN c.estatusCredito sc  "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'  "
			+ "AND d.idOrg in (:filtros )  " )	
	IndicadorCreditoVO findActivosDivision (@Param("filtros") List<String> divisiones);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as clientesActivos, coalesce(sum(0.0),0.0), coalesce(sum(0.0),0.0) ) "
			+ "FROM Credito c  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN c.estatusCredito sc  "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'  "
			+ "AND rr.idOrg in (:filtros )  " )	
	IndicadorCreditoVO findActivosRegion (@Param("filtros") List<String> regiones);

	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as clientesActivos, coalesce(sum(0.0),0.0), coalesce(sum(0.0),0.0) ) "
			+ "FROM Credito c  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN c.estatusCredito sc  "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'  "
			+ "AND g.idOrg in (:filtros )  " )	
	IndicadorCreditoVO findActivosGerencia (@Param("filtros") List<String> gerencias);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as clientesActivos, coalesce(sum(0.0),0.0), coalesce(sum(0.0),0.0) ) "
			+ "FROM Credito c  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.ruta r "
			+ "JOIN c.estatusCredito sc  "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'  "
			+ "AND r.idOrg in (:filtros )  " )	
	IndicadorCreditoVO findActivosRuta (@Param("filtros") List<String> rutas);
	
	@Query("select new com.crenx.data.domain.vo.IndicadorCreditoVO (count(*) as clientesActivos, coalesce(sum(0.0),0.0), coalesce(sum(0.0),0.0) ) "
			+ "FROM Credito c  "
			+ "JOIN c.cliente cl  "
			+ "JOIN c.estatusCredito sc  "
			+ "WHERE sc.claveInterna = 'CRED_ACTIVO'  " )
	IndicadorCreditoVO findActivos ();
	
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO ("
			+ "r.nombre as ruta, "
			+ "g.nombre as gerencia, "
			+ "rr.nombre as region, "
			+ "d.nombre as division, "
			+ " sum( case when (cr.fechaEmision >= :fechaInicial AND cr.fechaEmision <= :fechaFinal) then 1 else 0 end ) as nuevosClientes, "
			+ " count(*) as totalClientes, "
			+ " sum( case when (cr.fechaEmision >= :fechaInicial AND cr.fechaEmision <= :fechaFinal) then cr.monto else 0 end) as carteraNueva ) "
			+ "FROM Credito cr  "
			+ "JOIN cr.cliente c "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE r.idOrg in (:filtros)  "
			+ "AND c.creacion between :fechaInicial AND :fechaFinal "
			+ "GROUP BY r.nombre , g.nombre, rr.nombre, d.nombre")
	List<ResumenEmpresaVO> findCarteraTotal(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal, @Param("filtros") List<String> rutas);
	
	/**
	 * @param rutasCliente
	 * @param nombreCliente
	 * @param apellidoCliente
	 * @return
	 */
	@Query("select cl.codeudores " + "from Cliente  as cl "
			+ "inner join cl.tipoFigura as figura with figura.claveInterna = 'FIG_CLIENTE' "
			+ "where cl.ruta.idOrg in (:rutasCliente) " + "and cl.nombre like :nombreCliente "
			+ "and cl.apellidoPaterno like :apellidoCliente ")
	List<Cliente> findCodeudoresFromClientes(@Param("rutasCliente") List<String> rutasCliente,
			@Param("nombreCliente") String nombreCliente, @Param("apellidoCliente") String apellidoCliente);

	/**
	 * @param nombreCliente
	 * @param apellidoCliente
	 * @return
	 */
	@Query("select cl.codeudores " + "from Cliente  as cl "
			+ "inner join cl.tipoFigura as figura with figura.claveInterna = 'FIG_CLIENTE' "
			+ "where  cl.nombre like :nombreCliente " + "and cl.apellidoPaterno like :apellidoCliente ")
	List<Cliente> findCodeudoresFromClientes(@Param("nombreCliente") String nombreCliente,
			@Param("apellidoCliente") String apellidoCliente);
	
	
	/*
	@Query("select new com.crenx.data.domain.vo.ResumenEmpresaVO (r.nombre as ruta, g.nombre as gerencia, rr.nombre as region, d.nombre as division, COALESCE(0.0,0.0) as monto, sum(0.0) as abono, sum(0.0) as comision, count(*) ) "
			+ "FROM Cliente c  "
			+ "JOIN c.ruta r "
			+ "JOIN r.parent g "
			+ "JOIN g.parent rr "
			+ "JOIN rr.parent d "
			+ "WHERE r.idOrg in (:filtros)  "
			+ "GROUP BY r.nombre , g.nombre, rr.nombre, d.nombre")
	List<ResumenEmpresaVO> findCarteraTotal(@Param("filtros") List<String> rutas);
	*/
	

	/**
	 * @param nombreCliente
	 * @param apellidoCliente
	 * @return
	 */
	@Query("select cl "
			+ " FROM Cliente  cl "
			+ " LEFT JOIN cl.creditos c "
			+ " JOIN cl.ruta r "
			+ " where "
			+ " r.idOrg in (:filtros)"
			+ " and c.cliente is null "
			+ " and cl.nombre like :nombreCliente " + "and cl.apellidoPaterno like :apellidoCliente"
			+ " or c.cliente not in (select cr2.cliente from Credito cr2 inner join cr2.estatusCredito cat "
			+ " where cat.claveInterna = 'CRED_ACTIVO' )"
			+ " and cl.nombre like :nombreCliente " + "and cl.apellidoPaterno like :apellidoCliente"
			+ " and r.idOrg in (:filtros) "
			
			)
	List<Cliente> findClientesSinCreditoByRuta(@Param("nombreCliente") String nombreCliente,@Param("apellidoCliente") String apellidoCliente, @Param("filtros")List<String> rutas);
	
	/**
	 * @param nombreCliente
	 * @param apellidoCliente
	 * @return
	 */
	
	@Query("select cl "
			+ " FROM Cliente as cl "
			+ " left join cl.creditos c "
			+ " where c.cliente is null "
			+ " and cl.nombre like :nombreCliente " + "and cl.apellidoPaterno like :apellidoCliente"
			+ " or c.cliente not in (select cr2.cliente from Credito cr2 inner join cr2.estatusCredito cat "
			+ " where cat.claveInterna = 'CRED_ACTIVO' )"
			+ " and cl.nombre like :nombreCliente " + "and cl.apellidoPaterno like :apellidoCliente ")
	List<Cliente> findClientesSinCredito(@Param("nombreCliente") String nombreCliente,@Param("apellidoCliente") String apellidoCliente);
	
	
}
