package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.IndicadorOperacionesCliente;

public interface IndicadorOperacionesClienteRepository extends CrudRepository<IndicadorOperacionesCliente,String>{
	
}
