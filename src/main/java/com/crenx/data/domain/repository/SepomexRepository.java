package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.Sepomex;

public interface SepomexRepository extends CrudRepository<Sepomex,String> {
	List<Sepomex> findByDCodigo(String codigo);
}
