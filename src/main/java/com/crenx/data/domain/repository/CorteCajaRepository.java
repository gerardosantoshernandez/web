package com.crenx.data.domain.repository;

import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.CorteCaja;

public interface CorteCajaRepository extends CrudRepository<CorteCaja,String>{

}