package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.ReglaGastoRol;

public interface ReglaGastoRolRepository extends CrudRepository<ReglaGastoRol,String> {

	List<ReglaGastoRol> findByRolLlaveNegocioIn(List<String> llaves);
}
