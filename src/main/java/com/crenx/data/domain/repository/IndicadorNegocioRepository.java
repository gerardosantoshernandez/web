package com.crenx.data.domain.repository;
import org.springframework.data.repository.CrudRepository;
import com.crenx.data.domain.entity.IndicadorNegocio;

public interface IndicadorNegocioRepository extends CrudRepository<IndicadorNegocio,String> {

}

