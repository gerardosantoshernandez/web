package com.crenx.data.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.crenx.data.domain.entity.NominaPeriodo;
import com.crenx.data.domain.vo.NominaPeriodoVO;

import org.springframework.data.repository.query.Param;

public interface NominaPeriodoRepository extends CrudRepository<NominaPeriodo, String> {
	@Query("select new com.crenx.data.domain.vo.NominaPeriodoVO(division as division,region as region, gerencia as gerencia, ruta as ruta, semana as semana, "
			+ "cveEmpleado, semanas as semanas,"
			+ "nombreCompleto as nombreCompleto, cobranzaTotal as cobranzaTotal,"
			+ "factorNormalidad, factorCobranza, baseCobranza, baseNormalidad, baseClientesNuevos as baseClientesNuevos,"
			+ "cobranza, normalidad, clientesNuevos,"
			+ "totalNomina as totalNomina,baseNomina as baseNomina,baseAsimilados as baseAsimilados, descuentoPrestamo as descuentoPrestamo,"
			+ "descuentoMoto as descuentoMoto, quincena as quincena)" + " FROM NominaPeriodo " 
			+ " WHERE semana= :noPeriodo AND (EXTRACT(YEAR FROM fecha_inicio)= :anio) OR EXTRACT(YEAR FROM fecha_fin)= :anio)"
			+ " order by division,region,gerencia,ruta,cveEmpleado")
	List<NominaPeriodoVO> findByPeriodoSemana(@Param("noPeriodo") long noPeriodo, @Param("anio") int anio);
//and (EXTRACT(YEAR FROM fecha_inicio) = 2016 OR extract(YEAR FROM fecha_fin) = 2016)
	@Query("select new com.crenx.data.domain.vo.NominaPeriodoVO(division as division,region as region, gerencia as gerencia, ruta as ruta, semana as semana, "
			+ "cveEmpleado, semanas as semanas,"
			+ "nombreCompleto as nombreCompleto, cobranzaTotal as cobranzaTotal,"
			+ "factorNormalidad, factorCobranza, baseCobranza, baseNormalidad, baseClientesNuevos as baseClientesNuevos,"
			+ "cobranza, normalidad, clientesNuevos,"
			+ "totalNomina as totalNomina,baseNomina as baseNomina,baseAsimilados as baseAsimilados, descuentoPrestamo as descuentoPrestamo,"
			+ "descuentoMoto as descuentoMoto, quincena as quincena)" + " FROM NominaPeriodo " 
			+ " WHERE quincena= :noPeriodo AND (EXTRACT(YEAR FROM fecha_inicio)= :anio) OR EXTRACT(YEAR FROM fecha_fin)= :anio)"
			+ " order by division,region,gerencia,ruta,cveEmpleado")
	List<NominaPeriodoVO> findByPeriodoQuincena(@Param("noPeriodo") long noPeriodo, @Param("anio") int anio);

}
