package com.crenx.data.domain.repository;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.IndicadorCobranza;
import com.crenx.data.domain.vo.InidicadorRutaVO;

public interface IndicadorCobranzaRepository extends CrudRepository<IndicadorCobranza,String> {


	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (i.fecha, COALESCE(AVG(i.factorNormalidad),0) as normalidad, COALESCE(AVG(i.factorCobranza),0) as cobranza ) "
			+ "FROM IndicadorCobranza i  "
			+ "JOIN i.ruta r "
			+ "WHERE date(i.fecha) >=:fechaProceso  "
			+ "AND r.idOrg in (:filtros)  "
			+ "GROUP BY i.fecha  ")
	List<InidicadorRutaVO> findIndicadorCobranzaRutas(@Param("fechaProceso") Date fechaProceso, @Param("filtros") List<String> rutas);

	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (i.fecha, COALESCE(AVG(i.factorNormalidad),0) as normalidad, COALESCE(AVG(i.factorCobranza),0) as cobranza ) "
			+ "FROM IndicadorCobranza i  "
			+ "JOIN i.gerencia g "
			+ "WHERE date(i.fecha) >=:fechaProceso  "
			+ "AND g.idOrg in (:filtros)  "
			+ "GROUP BY i.fecha  ")
	List<InidicadorRutaVO> findIndicadorCobranzaGerencias(@Param("fechaProceso") Date fechaProceso, @Param("filtros") List<String> gerencias);
	
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (i.fecha, COALESCE(AVG(i.factorNormalidad),0) as normalidad, COALESCE(AVG(i.factorCobranza),0) as cobranza ) "
			+ "FROM IndicadorCobranza i  "
			+ "JOIN i.region rr "
			+ "WHERE date(i.fecha) >=:fechaProceso  "
			+ "AND rr.idOrg in (:filtros)  "
			+ "GROUP BY i.fecha  ")
	List<InidicadorRutaVO> findIndicadorCobranzaRegiones(@Param("fechaProceso") Date fechaProceso, @Param("filtros") List<String> regiones);

	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (i.fecha, COALESCE(AVG(i.factorNormalidad),0) as normalidad, COALESCE(AVG(i.factorCobranza),0) as cobranza ) "
			+ "FROM IndicadorCobranza i  "
			+ "JOIN i.division d "
			+ "WHERE date(i.fecha) >=:fechaProceso  "
			+ "AND d.idOrg in (:filtros)  "
			+ "GROUP BY i.fecha  ")
	List<InidicadorRutaVO> findIndicadorCobranzaDivisiones(@Param("fechaProceso") Date fechaProceso, @Param("filtros") List<String> divisiones);

	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (i.fecha, COALESCE(AVG(i.factorNormalidad),0) as normalidad, COALESCE(AVG(i.factorCobranza),0) as cobranza ) "
			+ "FROM IndicadorCobranza i  "
			+ "WHERE date(i.fecha) >=:fechaProceso  "
			+ "GROUP BY i.fecha  ")
	List<InidicadorRutaVO> findIndicadorCobranzaAll(@Param("fechaProceso") Date fechaProceso);
	
}
