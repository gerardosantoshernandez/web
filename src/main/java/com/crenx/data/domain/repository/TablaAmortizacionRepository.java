package com.crenx.data.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.vo.InidicadorRutaVO;

public interface TablaAmortizacionRepository extends CrudRepository<TablaAmortizacion,String> {
	List<TablaAmortizacion> findByCreditoIdCredito(String idCredito);
	List<TablaAmortizacion> findByCreditoIdCreditoAndNoPago(String idCredito, int noPago);
	List<TablaAmortizacion> findByCreditoIdCreditoAndPagoFechaPago(String idCredito, Date fechaPago);

	List<TablaAmortizacion> findByPagoIdPago(String idPago);
	
	//NoPago(String idCredito, int noPago);

	@Query("SELECT COUNT(*) FROM TablaAmortizacion t WHERE t.pago.idPago!=null AND t.credito.idCredito=:idcredito ")
	int obtenerPagosRealizados(@Param("idcredito") String idCredito);

	@Query("select count(*) from TablaAmortizacion t where t.pago.idPago =null and DATE_FORMAT(t.fechaVencimiento, '%Y-%m-%d') <= curdate() and t.credito.idCredito=:idCredito")
	int obtenerPagosVencidos(@Param("idCredito") String idCredito);

	@Query("select sum(t.cuota) from TablaAmortizacion t where t.pago.idPago =null and DATE_FORMAT(t.fechaVencimiento, '%Y-%m-%d') <= curdate() and t.credito.idCredito=:idCredito")
	Double obtenerMontoVencido(@Param("idCredito") String idCredito);	
	
	
	
	Long deleteByCreditoIdCredito(String idCredito);

	//La cobranza que se debió ejecutar en el día
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "WHERE date(t.fechaVencimiento) =:fechaProceso  "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO'"
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO> findByCobranzaEjecutable(@Param("fechaProceso") Date fechaProceso);

	//La cobranza que se debió ejecutar en la semana (o Periodo)
	//Items cobrables, no necessariamente cobrados
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "WHERE date(t.fechaVencimiento) between :fechaInicial and :fechaFinal  "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO'"
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO> findByCobranzaEjecutable(@Param("fechaInicial") Date fechaProceso, @Param("fechaFinal") Date fechaFinal);
	
	
	//La cobranza ejecutada en el día solo vencimientos del día
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(t.fechaVencimiento) =:fechaProceso  "
			+ "AND date(p.fechaPago)= :fechaProceso "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO>  findByCobranzaNormalidad(@Param("fechaProceso") Date fechaProceso);

	//La cobranza ejecutada en el periodo de vencimiento
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(t.fechaVencimiento) between :fechaInicial and :fechaFinal  "
			+ "AND date(p.fechaPago) between :fechaInicial and :fechaFinal "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO>  findByCobranzaNormalidad(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal);
	
	
	//La cobranza total del día, incluyendo anticipos y vencidos
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(p.fechaPago)= :fechaProceso "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")			
	List<InidicadorRutaVO>   findByCobranzaDiaria(@Param("fechaProceso") Date fechaProceso);

	//La cobranza total del periodo, incluyendo anticipos y vencidos
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(p.fechaPago) between :fechaInicial and :fechaFinal "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")			
	List<InidicadorRutaVO>   findByCobranzaPeriodo(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal);

	//La cobranza total del día solo vencidos
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(t.fechaVencimiento) <:fechaProceso  "
			+ "AND date(p.fechaPago)= :fechaProceso "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO>   findByCobranzaDiariaVencida(@Param("fechaProceso") Date fechaProceso);

	//La cobranza total del periodo solo retrasada
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.tipoPago tp "
			+ "JOIN t.pago p "
			+ "WHERE date(t.fechaVencimiento) <:fechaInicial  "
			+ "AND date(p.fechaPago) between  :fechaInicial and :fechaFinal "
			+ "AND tp.claveInterna = 'TIPO_PAGO_ABONO' "
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO>   findByCobranzaPeriodoRetraso(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal);

	//La cobranza total del periodo solo vencida
	@Query("select new com.crenx.data.domain.vo.InidicadorRutaVO (c.ruta.idOrg, o.nombre, count(*) as normalidad, sum(t.cuota) as cobranza ) "
			+ "FROM TablaAmortizacion t  "
			+ "JOIN t.credito c  "
			+ "JOIN c.ruta o "
			+ "JOIN t.estatusPago ep "
			+ "JOIN c.estatusCredito ec "
			+ "WHERE date(c.fechaVencimiento) <:fechaInicial  "
			+ "AND ep.claveInterna = 'PAGO_PENDIENTE' "
			+ "AND ec.claveInterna = 'CRED_ACTIVO' "
			+ "Group By c.ruta.idOrg, o.nombre")
	List<InidicadorRutaVO>   findByCarteraPeriodoVencida(@Param("fechaInicial") Date fechaInicial);
	
	
}
