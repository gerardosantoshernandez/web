package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class IndicadorCobranza {
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_indcob", unique = true)
	private String idIndicadorCobranza;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_division", nullable=false, referencedColumnName="id_org")
	private Organizacion division;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_region", nullable=false, referencedColumnName="id_org")
	private Organizacion region;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_gerencia", nullable=false, referencedColumnName="id_org")
	private Organizacion gerencia;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_org", nullable=false, referencedColumnName="id_org")
	private Organizacion ruta;

	/*
	 * La fecha del día de cobranza
	 */
	@Column(name = "fecha")
	private Date fecha;

	
	/*
	 * Todos los vencimientos del día
	 */
	@Column(name = "cobrables")
	private double cobrables;

	/*
	 * El importe de la cobranza del día
	 */
	@Column(name = "importe_cobrables")
	private double importeCobrables;

	/*
	 * Los items cobrados del día
	 */
	@Column(name = "cobrados")
	private double cobradosDia;

	
	/*
	 * El importe de los items cobrados del día
	 */
	@Column(name = "importe_cobrado_dia")
	private double importeCobradoDia;
	

	/*
	 * Los items cobrados del día
	 */
	@Column(name = "cobrados_total")
	private double cobradosTotal;

	/*
	 * La cobranza del día de vencimientos de otros días
	 */
	@Column(name = "importe_cobrado_total")
	private double importeCobradoTotal;

	/*
	 * Los items cobrados vencidos
	 */
	@Column(name = "cobrados_vencidos")
	private double cobradosVencidos;

	
	/*
	 * El importe de los items cobrados vencidos
	 */
	@Column(name = "importe_cobrado_vencido")
	private double importeCobradoVencido;


	@Column(name = "factor_normalidad")
	private double factorNormalidad;
	
	@Column(name = "factor_cobranza")
	private double factorCobranza;
	
	public String getIdIndicadorCobranza() {
		return idIndicadorCobranza;
	}


	public void setIdIndicadorCobranza(String idIndicadorCobranza) {
		this.idIndicadorCobranza = idIndicadorCobranza;
	}



	public Organizacion getRuta() {
		return ruta;
	}


	public void setRuta(Organizacion ruta) {
		this.ruta = ruta;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public double getCobrables() {
		return cobrables;
	}


	public void setCobrables(double cobrables) {
		this.cobrables = cobrables;
	}


	public double getImporteCobrables() {
		return importeCobrables;
	}


	public void setImporteCobrables(double importeCobrables) {
		this.importeCobrables = importeCobrables;
	}


	public double getCobradosDia() {
		return cobradosDia;
	}


	public void setCobradosDia(double cobradosDia) {
		this.cobradosDia = cobradosDia;
	}


	public double getImporteCobradoDia() {
		return importeCobradoDia;
	}


	public void setImporteCobradoDia(double importeCobradoDia) {
		this.importeCobradoDia = importeCobradoDia;
	}


	public double getCobradosTotal() {
		return cobradosTotal;
	}


	public void setCobradosTotal(double cobradosTotal) {
		this.cobradosTotal = cobradosTotal;
	}


	public double getImporteCobradoTotal() {
		return importeCobradoTotal;
	}


	public void setImporteCobradoTotal(double importeCobradoTotal) {
		this.importeCobradoTotal = importeCobradoTotal;
	}


	public double getCobradosVencidos() {
		return cobradosVencidos;
	}


	public void setCobradosVencidos(double cobradosVencidos) {
		this.cobradosVencidos = cobradosVencidos;
	}


	public double getImporteCobradoVencido() {
		return importeCobradoVencido;
	}


	public void setImporteCobradoVencido(double importeCobradoVencido) {
		this.importeCobradoVencido = importeCobradoVencido;
	}


	public double getFactorNormalidad() {
		return factorNormalidad;
	}


	public void setFactorNormalidad(double factorNormalidad) {
		this.factorNormalidad = factorNormalidad;
	}


	public double getFactorCobranza() {
		return factorCobranza;
	}


	public void setFactorCobranza(double factorCobranza) {
		this.factorCobranza = factorCobranza;
	}


	public Organizacion getDivision() {
		return division;
	}


	public void setDivision(Organizacion division) {
		this.division = division;
	}


	public Organizacion getRegion() {
		return region;
	}


	public void setRegion(Organizacion region) {
		this.region = region;
	}


	public Organizacion getGerencia() {
		return gerencia;
	}


	public void setGerencia(Organizacion gerencia) {
		this.gerencia = gerencia;
	}
	
	

}
