package com.crenx.data.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Activo {
	

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_activo", unique = true)
	private String idActivo;
	
	@Column(name = "id_tipo_activo",length = 50)
	private String idTipoActivo;
	
	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "fecha_asignacion",length = 10)
	private String fechaAsignacion;
	
	@Column(name = "clave",length = 10)
	private String clave;
	
	@Column(name = "valor_reposicion")
	private double valorReposicion;
	
	@Column(name = "id_usuario",length = 40)
	private String idUsuario;

	public String getIdActivo() {
		return idActivo;
	}

	public void setIdActivo(String idActivo) {
		this.idActivo = idActivo;
	}

	public String getIdTipoActivo() {
		return idTipoActivo;
	}

	public void setIdTipoActivo(String idTipoActivo) {
		this.idTipoActivo = idTipoActivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public double getValorReposicion() {
		return valorReposicion;
	}

	public void setValorReposicion(double valorReposicion) {
		this.valorReposicion = valorReposicion;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	
	
}
