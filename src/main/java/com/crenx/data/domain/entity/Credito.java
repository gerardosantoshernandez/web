package com.crenx.data.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Credito implements Serializable{

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_credito", unique = true)
	private String idCredito;

	@Column(name = "id", nullable=false)
	private long id;
	
	@Column(name = "monto")
	private double monto;
	
	@Column(name = "comision")
	private double comision;
	
	@Column(name = "plazo")
	private short plazo=0;
	
	@Column(name = "tasa")
	private double tasa=0;
	
	@Column(name = "pago")
	private double pago=0;

	@Column(name = "fecha_emision")
	private Date fechaEmision;

	@Column(name = "fecha_vencimiento")
	private Date fechaVencimiento;

	@Column(name = "fecha_ultimo_pago")
	private Date fechaUltimoPago;

	@Column(name = "fecha_ultima_visita")
	private Date fechaUltimaVisita;
	
	@Column(name = "fecha_cancelacion")
	private Date fechaCancelacion;

	@Column(name = "saldo")
	private double  saldo=0;
	
	@Column(name = "saldo_insoluto")
	private double  saldoInsoluto=0;

	@Column(name = "tot_pagos")
	private int totPagos=0;

	@Column(name = "pago_atiempo")
	private int totPagoATiempo=0;
	
	@Column(name = "calificacion")
	private int calificacion=0;
	
	@Column(name = "saldo_comision")
	private double saldoComision=0;
	
	@Column(name = "pago_pendiente_aplicar")
	private double pagoPendienteAplicar=0;

		
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_unidad_plazo", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo unidadPlazo;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_frecuencia_cobro", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo frecuenciaCobro;


	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_producto", nullable=false, referencedColumnName="id_producto")
	private Producto producto;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_cliente", nullable=false, referencedColumnName="id_cliente")
	private Cliente cliente;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_codeudor", nullable=true, referencedColumnName="id_cliente")
	private Cliente coDeudor;

	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_estatus_credito", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusCredito;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_ruta", nullable=false, referencedColumnName="id_org")
	private Organizacion ruta;

	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="credito")
	@Cascade( {CascadeType.ALL} )
	@JsonIgnore
	private List<TablaAmortizacion> pagos = new ArrayList<TablaAmortizacion>();

	@OneToMany(fetch=FetchType.LAZY, mappedBy="credito")
	@Cascade( {CascadeType.ALL} )
	private List<Visita> visitas = new ArrayList<Visita>();	
	
	@Column(name = "geo_posicion", length = 40)
	private String geoPosicion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_credito", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoCredito;
	
	private String RolAprobador;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_cat_motcanc", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo motivoCancelacion;
	
	@Column(name="observaciones")
	private String observaciones;
	
	public Organizacion getRuta() {
		return ruta;
	}

	public void setRuta(Organizacion ruta) {
		this.ruta = ruta;
	}

	public String getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public short getPlazo() {
		return plazo;
	}

	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}

	public double getTasa() {
		return tasa;
	}

	public void setTasa(double tasa) {
		this.tasa = tasa;
	}

	public double getPago() {
		return pago;
	}

	public void setPago(double pago) {
		this.pago = pago;
	}

	public Catalogo getUnidadPlazo() {
		return unidadPlazo;
	}

	public void setUnidadPlazo(Catalogo unidadPlazo) {
		this.unidadPlazo = unidadPlazo;
	}


	public Catalogo getFrecuenciaCobro() {
		return frecuenciaCobro;
	}

	public void setFrecuenciaCobro(Catalogo frecuenciaCobro) {
		this.frecuenciaCobro = frecuenciaCobro;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Catalogo getEstatusCredito() {
		return estatusCredito;
	}

	public void setEstatusCredito(Catalogo estatusCredito) {
		this.estatusCredito = estatusCredito;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	
	public List<TablaAmortizacion> getPagos() {
		return pagos;
	}

	public void setPagos(ArrayList<TablaAmortizacion> pagos) {
		this.pagos = pagos;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaUltimoPago() {
		return fechaUltimoPago;
	}

	public void setFechaUltimoPago(Date fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getTotPagos() {
		return totPagos;
	}

	public void setTotPagos(int totPagos) {
		this.totPagos = totPagos;
	}

	public int getTotPagoATiempo() {
		return totPagoATiempo;
	}

	public void setTotPagoATiempo(int totPagoATiempo) {
		this.totPagoATiempo = totPagoATiempo;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public void setPagos(List<TablaAmortizacion> pagos) {
		this.pagos = pagos;
	}

	public double getSaldoComision() {
		return saldoComision;
	}

	public void setSaldoComision(double saldoComision) {
		this.saldoComision = saldoComision;
	}

	public double getSaldoInsoluto() {
		return saldoInsoluto;
	}

	public void setSaldoInsoluto(double saldoInsoluto) {
		this.saldoInsoluto = saldoInsoluto;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public double getPagoPendienteAplicar() {
		return pagoPendienteAplicar;
	}

	public void setPagoPendienteAplicar(double pagoPendienteAplicar) {
		this.pagoPendienteAplicar = pagoPendienteAplicar;
	}

	public String getGeoPosicion() {
		return geoPosicion;
	}

	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}

	public Cliente getCoDeudor() {
		return coDeudor;
	}

	public void setCoDeudor(Cliente coDeudor) {
		this.coDeudor = coDeudor;
	}

	public String getTitular()
	{
		String retVal = "";
		if (ruta!=null)
			if (ruta.getTitular()!=null)
			{
				retVal = ruta.getTitular().getNombre() + " " + ruta.getTitular().getApellidoPaterno() + " " + ruta.getTitular().getApellidoMaterno();
			}
		return retVal;
	}
	public String getNombreCliente()
	{
		String retVal = "";
		if (cliente!=null)
				retVal = cliente.getNombre() + " " 
						+ (cliente.getApellidoPaterno()==null?"":cliente.getApellidoPaterno())
						+ " " + (cliente.getApellidoMaterno()==null?"":cliente.getApellidoMaterno());
		return retVal;
	}

	public String getNombreCodeudor()
	{
		String retVal = "";
		if (coDeudor!=null)
				retVal = coDeudor.getNombre() + " " 
						+ (coDeudor.getApellidoPaterno()==null?"":coDeudor.getApellidoPaterno()) 
						+ " " + (coDeudor.getApellidoMaterno()==null?"":coDeudor.getApellidoMaterno());
		return retVal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaUltimaVisita() {
		return fechaUltimaVisita;
	}

	public void setFechaUltimaVisita(Date fechaUltimaVisita) {
		this.fechaUltimaVisita = fechaUltimaVisita;
	}

	public Catalogo getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(Catalogo tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public String getRolAprobador() {
		return RolAprobador;
	}

	public void setRolAprobador(String rolAprobador) {
		RolAprobador = rolAprobador;
	}

	public Catalogo getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(Catalogo motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
		
}
