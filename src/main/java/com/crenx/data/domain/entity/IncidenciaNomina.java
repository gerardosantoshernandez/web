package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class IncidenciaNomina {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_incidente", unique = true)
	private String idIncidente;

	@Column(name = "fecha_proceso")
	Date fechaProceso;
	@Column(name = "semana")
	long semana;

	@Column(name = "reporte")
	String reporte;


	public String getIdIncidente() {
		return idIncidente;
	}

	public void setIdIncidente(String idIncidente) {
		this.idIncidente = idIncidente;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public long getSemana() {
		return semana;
	}

	public void setSemana(long semana) {
		this.semana = semana;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	
}
