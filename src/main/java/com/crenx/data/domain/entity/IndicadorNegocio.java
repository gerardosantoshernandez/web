package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class IndicadorNegocio {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_indicador_negocio", unique = true)
	private String idIndicadorNegocio;

	@Column(name = "division")
	String division;
	@Column(name = "region")
	String region;
	@Column(name = "gerencia")
	String gerencia;
	@Column(name = "ruta")
	String ruta;
	@Column(name = "fecha_proceso")
	Date fechaProceso;
	@Column(name = "semana")
	int semana;
	@Column(name = "mes")
	int mes;
	@Column(name = "trimestre")
	int trimestre;
	@Column(name = "semestre")
	int semestre;
	@Column(name = "anio")
	int anio;
	@Column(name = "cobranza")
	double cobranza;
	@Column(name = "colocacion")
	double colocacion;
	@Column(name = "creditos_nuevos")
	long creditosNuevos;
	@Column(name = "cartera_total")
	double carteraTotal;
	@Column(name = "cartera_capital")
	double carteraCapital;
	@Column(name = "cartera_vencida")
	double carteraVencida;
	@Column(name = "cartera_retraso")
	double carteraRetraso;
	@Column(name = "total_clientes")
	long totalClientes;
	@Column(name = "clientes_nuevos")
	long clientesNuevos;
	@Column(name = "cartera_nueva")
	double carteraNueva;
	
	
	public String getIdIndicadorNegocio() {
		return idIndicadorNegocio;
	}
	public void setIdIndicadorNegocio(String idIndicadorNegocio) {
		this.idIndicadorNegocio = idIndicadorNegocio;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public int getSemana() {
		return semana;
	}
	public void setSemana(int semana) {
		this.semana = semana;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getTrimestre() {
		return trimestre;
	}
	public void setTrimestre(int trimestre) {
		this.trimestre = trimestre;
	}
	public int getSemestre() {
		return semestre;
	}
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public double getCobranza() {
		return cobranza;
	}
	public void setCobranza(double cobranza) {
		this.cobranza = cobranza;
	}
	public double getColocacion() {
		return colocacion;
	}
	public void setColocacion(double colocacion) {
		this.colocacion = colocacion;
	}
	public long getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(long creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public double getCarteraTotal() {
		return carteraTotal;
	}
	public void setCarteraTotal(double carteraTotal) {
		this.carteraTotal = carteraTotal;
	}
	public double getCarteraCapital() {
		return carteraCapital;
	}
	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}
	public double getCarteraVencida() {
		return carteraVencida;
	}
	public void setCarteraVencida(double carteraVencida) {
		this.carteraVencida = carteraVencida;
	}
	public double getCarteraRetraso() {
		return carteraRetraso;
	}
	public void setCarteraRetraso(double carteraRetraso) {
		this.carteraRetraso = carteraRetraso;
	}
	public long getTotalClientes() {
		return totalClientes;
	}
	public void setTotalClientes(long totalClientes) {
		this.totalClientes = totalClientes;
	}
	public long getClientesNuevos() {
		return clientesNuevos;
	}
	public void setClientesNuevos(long clientesNuevos) {
		this.clientesNuevos = clientesNuevos;
	}
	public double getCarteraNueva() {
		return carteraNueva;
	}
	public void setCarteraNueva(double carteraNueva) {
		this.carteraNueva = carteraNueva;
	}	
	
	
	
}
