package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Traspaso {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	
	@Column(name = "id_traspaso", unique = true)
	private String idTraspaso;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_rutaOrigen", nullable=false, referencedColumnName="id_org")
	private Organizacion rutaOrigen;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_rutaDestino", nullable=false, referencedColumnName="id_org")
	private Organizacion rutaDestino;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_cliente", nullable=false, referencedColumnName="id_cliente")
	private Cliente cliente;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_motivo", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo motivo;
	
	@Column(name = "fecha_operacion", nullable=false)
	private Date fechaOperacion;

	@Column(name = "usuario", length = 40)
	private String Usuario;
	
	@Column(name = "monto")
	private Double monto;

	
	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getIdTraspaso() {
		return idTraspaso;
	}

	public void setIdTraspaso(String idTraspaso) {
		this.idTraspaso = idTraspaso;
	}

	public Organizacion getRutaOrigen() {
		return rutaOrigen;
	}

	public void setRutaOrigen(Organizacion rutaOrigen) {
		this.rutaOrigen = rutaOrigen;
	}

	public Organizacion getRutaDestino() {
		return rutaDestino;
	}

	public void setRutaDestino(Organizacion rutaDestino) {
		this.rutaDestino = rutaDestino;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}



	public Catalogo getMotivo() {
		return motivo;
	}

	public void setMotivo(Catalogo motivo) {
		this.motivo = motivo;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String usuario) {
		Usuario = usuario;
	}


	

}
