package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Estatus {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_estatus", unique = true)
	private String idEstatus;
	
	@Column(name = "nombre",length = 50)
    private String nombre;
	
	@Column(name = "llave",length = 50)
	private String llave;
	
	@Column(name = "creacion",length = 50)
	private Date creacion;
	
}
