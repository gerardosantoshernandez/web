package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class TablaAmortizacion {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_amortizacion", unique = true)
	private String idAmortizacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_credito", nullable=false, referencedColumnName="id_credito")
	private Credito credito;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_pago", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoPago;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_estatus_pago", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusPago;

	@Column(name = "no_pago")
	private int noPago;
	
	@Column(name = "cuota")
	private double cuota;
	
	@Column(name = "fecha_vencimiento")
	private Date fechaVencimiento;
	
	@Column(name = "saldo")
	private double saldo;

	@Column(name = "capital")
	private double capital;
	
	@Column(name = "interes")
	private double interes;
	
	@Column(name = "iva_interes")
	private double ivaInteres;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_pago", nullable=true, referencedColumnName="id_pago")
	private Pago pago;

	
	public String getIdAmortizacion() {
		return idAmortizacion;
	}

	public void setIdAmortizacion(String idAmortizacion) {
		this.idAmortizacion = idAmortizacion;
	}

	public int getNoPago() {
		return noPago;
	}

	public void setNoPago(int noPago) {
		this.noPago = noPago;
	}

	public double getCuota() {
		return cuota;
	}

	public void setCuota(double cuota) {
		this.cuota = cuota;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}

	public double getIvaInteres() {
		return ivaInteres;
	}

	public void setIvaInteres(double ivaInteres) {
		this.ivaInteres = ivaInteres;
	}

	public Credito getCredito() {
		return credito;
	}

	public void setCredito(Credito credito) {
		this.credito = credito;
	}

	public Catalogo getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Catalogo tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Catalogo getEstatusPago() {
		return estatusPago;
	}

	public void setEstatusPago(Catalogo estatusPago) {
		this.estatusPago = estatusPago;
	}


	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}


}
