package com.crenx.data.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Visita {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_visita", unique = true)
	private String idVisita;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_ruta", nullable=false, referencedColumnName="id_org")
	private Organizacion ruta;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_motivo", referencedColumnName="id_catalogo")
	private Catalogo motivo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_creditoa", referencedColumnName="id_credito", nullable = true)
	private Credito credito;
	
	
	@Column(name = "id_credito", nullable=false)
	private long idCredito;
	
	@Column(name = "fecha_visita")
	private Date fechaVisita;

	@Column(name = "id_usuario", length = 40)
	private String idUsuario;
	
	@Column(name = "id_dispositivo", length = 40)
	private String idDispositivo;

	@Column(name = "id_cliente", nullable=false)
	private long idCliente;

	@Column(name = "nombre_cliente", length = 200)
	private String nombreCliente;
	
	
	@Column(name = "geo_posicion", length = 40)
	private String geoPosicion;

	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_org", nullable=true, referencedColumnName="id_org")
	private Organizacion idOrg;
	
	
	
	public Organizacion getIdOrg() {
		return idOrg;
	}

	public void setIdOrg(Organizacion idOrg) {
		this.idOrg = idOrg;
	}

	public String getIdVisita() {
		return idVisita;
	}

	public void setIdVisita(String idVisita) {
		this.idVisita = idVisita;
	}

	public Organizacion getRuta() {
		return ruta;
	}

	public void setRuta(Organizacion ruta) {
		this.ruta = ruta;
	}

	public Date getFechaVisita() {
		return fechaVisita;
	}

	public void setFechaVisita(Date fechaVisita) {
		this.fechaVisita = fechaVisita;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getGeoPosicion() {
		return geoPosicion;
	}

	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}

	public Catalogo getMotivo() {
		return motivo;
	}

	public void setMotivo(Catalogo motivo) {
		this.motivo = motivo;
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}
	public long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(long idCredito) {
		this.idCredito = idCredito;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public Credito getCredito() {
		return credito;
	}

	public void setCredito(Credito credito) {
		this.credito = credito;
	}

	@Override
	public String toString() {
		return "Visita [idVisita=" + idVisita + ", ruta=" + ruta + ", motivo=" + motivo + ", credito=" + credito
				+ ", idCredito=" + idCredito + ", fechaVisita=" + fechaVisita + ", idUsuario=" + idUsuario
				+ ", idDispositivo=" + idDispositivo + ", idCliente=" + idCliente + ", nombreCliente=" + nombreCliente
				+ ", geoPosicion=" + geoPosicion + ", idOrg=" + idOrg + "]";
	}

	
}
