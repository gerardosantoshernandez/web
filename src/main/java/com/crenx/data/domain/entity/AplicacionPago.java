package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

public class AplicacionPago {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_pago_aplicacion", unique = true)
	private String idPagoAplicacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_amortizacion", nullable=false, referencedColumnName="id_amortizacion")
	private TablaAmortizacion amoirtizacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_pago", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoPago;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_pago", nullable=false, referencedColumnName="id_pago")
	private Pago pago;

	@Column(name = "fecha_pago")
	private Date fechaPago;
	
	@Column(name = "importe")
	private double importe;
	
	@Column(name = "capital")
	private double capital;
	
	@Column(name = "intereses")
	private double intereses;
	
	@Column(name = "iva")
	private double iva;

	public String getIdPagoAplicacion() {
		return idPagoAplicacion;
	}

	public void setIdPagoAplicacion(String idPagoAplicacion) {
		this.idPagoAplicacion = idPagoAplicacion;
	}

	public TablaAmortizacion getAmoirtizacion() {
		return amoirtizacion;
	}

	public void setAmoirtizacion(TablaAmortizacion amoirtizacion) {
		this.amoirtizacion = amoirtizacion;
	}

	public Catalogo getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Catalogo tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getIntereses() {
		return intereses;
	}

	public void setIntereses(double intereses) {
		this.intereses = intereses;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

}
