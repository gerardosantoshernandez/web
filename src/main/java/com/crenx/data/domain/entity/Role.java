package com.crenx.data.domain.entity;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {
	
	private String name = "ROLE_USER";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return name;
	}
}
