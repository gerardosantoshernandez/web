package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Parametros {

	@Column(name = "nombre",length = 50)
	String nombre;
	@Column(name = "valor",length = 50)
	String valor;

	@Column(name = "valorNum")
	double valorNum;
	
	@Column(name = "valorFecha")
	Date valorFecha;
	
	@Column(name = "tipo_param",length = 50)
	String tipoParam;
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_parametro", unique = true)
	private String idParametro;

	public Parametros()
	{
		
	}
	public Parametros(String nombre, String valor)
	{
		this.nombre = nombre;
		this.valor = valor;
		this.tipoParam = "String";
	}
	
	public Parametros(String nombre, double valorNum)
	{
		this.nombre = nombre;
		this.valorNum = valorNum;
		this.tipoParam = "Num";
	}
	
	public Parametros(String nombre, Date valor)
	{
		this.nombre = nombre;
		this.valorFecha = valor;
		this.tipoParam = "Date";
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public double getValorNum() {
		return valorNum;
	}

	public void setValorNum(double valorNum) {
		this.valorNum = valorNum;
	}

	public Date getValorFecha() {
		return valorFecha;
	}

	public void setValorFecha(Date valorFecha) {
		this.valorFecha = valorFecha;
	}

	public String getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(String idParametro) {
		this.idParametro = idParametro;
	}

	public String getTipoParam() {
		return tipoParam;
	}

	public void setTipoParam(String tipoParam) {
		this.tipoParam = tipoParam;
	}
}
