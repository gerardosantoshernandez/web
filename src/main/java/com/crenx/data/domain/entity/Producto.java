package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Producto {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_producto", unique = true)
	private String idProducto;
	
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="familia_prodcto_id", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo familiaProducto;
	
	/*
	@Column(name = "id_familia_producto",length = 40)
	private String idFamiliaProducto;
	*/
	
	
	
	@Column(name = "nombre",length = 100)
	private String nombre;
	
	@Column(name = "descripcion",length = 500)
	private String descripcion;
	
	@Column(name = "minimo")
	private double minimo;
	
	@Column(name = "maximo")
	private double maximo;
	
	@Column(name = "plazo")
	private short plazo;
	
	@Column(name = "tasa")
	private double tasa;
	
	@Column(name = "pago_x_mil")
	private double pagoXmil;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_unidad_plazo", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo unidadPlazo;
	/*
	@Column(name = "id_unidad_plazo",length = 40)
	private String idUnidadPlazo;
	*/
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_frecuencia_cobro", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo frecuenciaCobro;
	/*
	@Column(name = "id_frecuencia_cobro",length = 40)
	private String idFrecuenciaCobro;
	*/
	@Column(name = "comision")
	private double comision;
	
	@Column(name = "fecha_fin_operaciones")
	private Date fechaFinOperaciones;
	
	@Column(name = "fecha_inicio_operaciones")
	private Date fechaInicioOperaciones;
	
	@Column(name="permite_editar")
	private boolean permiteEditar=false;
	
	@Column(name="interes_fijo")
	private boolean interesFijo=false;

	@Column(name="cat")
	private double cat=0.0;
	
	
	@Column(name = "estatus")
	private byte estatus;

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Catalogo getFamiliaProducto() {
		return familiaProducto;
	}

	public void setFamiliaProducto(Catalogo familiaProducto) {
		this.familiaProducto = familiaProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getMinimo() {
		return minimo;
	}

	public void setMinimo(double minimo) {
		this.minimo = minimo;
	}

	public double getMaximo() {
		return maximo;
	}

	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}

	public short getPlazo() {
		return plazo;
	}

	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}

	public double getTasa() {
		return tasa;
	}

	public void setTasa(double tasa) {
		this.tasa = tasa;
	}

	public double getPagoXmil() {
		return pagoXmil;
	}

	public void setPagoXmil(double pagoXmil) {
		this.pagoXmil = pagoXmil;
	}


	public Catalogo getUnidadPlazo() {
		return unidadPlazo;
	}

	public void setUnidadPlazo(Catalogo unidadPlazo) {
		this.unidadPlazo = unidadPlazo;
	}

	public Catalogo getFrecuenciaCobro() {
		return frecuenciaCobro;
	}

	public void setFrecuenciaCobro(Catalogo frecuenciaCobro) {
		this.frecuenciaCobro = frecuenciaCobro;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public Date getFechaFinOperaciones() {
		return fechaFinOperaciones;
	}

	public void setFechaFinOperaciones(Date fechaFinOperaciones) {
		this.fechaFinOperaciones = fechaFinOperaciones;
	}

	public Date getFechaInicioOperaciones() {
		return fechaInicioOperaciones;
	}

	public void setFechaInicioOperaciones(Date fechaInicioOperaciones) {
		this.fechaInicioOperaciones = fechaInicioOperaciones;
	}

	public byte getEstatus() {
		return estatus;
	}

	public void setEstatus(byte estatus) {
		this.estatus = estatus;
	}

	public boolean isPermiteEditar() {
		return permiteEditar;
	}

	public void setPermiteEditar(boolean permiteEditar) {
		this.permiteEditar = permiteEditar;
	}

	public double getCat() {
		return cat;
	}

	public void setCat(double cat) {
		this.cat = cat;
	}

	public boolean isInteresFijo() {
		return interesFijo;
	}

	public void setInteresFijo(boolean interesFijo) {
		this.interesFijo = interesFijo;
	}
	
	
	
}
