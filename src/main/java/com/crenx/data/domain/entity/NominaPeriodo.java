package com.crenx.data.domain.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class NominaPeriodo {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_nomin", unique = true)
	private String idNomina;

	@Column(name = "division")
	String division;
	@Column(name = "region")
	String region;
	@Column(name = "gerencia")
	String gerencia;
	@Column(name = "ruta")
	String ruta;
	@Column(name = "fecha_proceso")
	Date fechaProceso;

	@Column(name = "fecha_inicio")
	Date fechaInicio;

	@Column(name = "fecha_fin")
	Date fechaFin;
	
	
	@Column(name = "semana")
	long semana;

	@Column(name = "quincena")
	long quincena;

	@Column(name = "cveEmpleado")
	String cveEmpleado;
	
	@Column(name = "semanas")
	long semanas;

	@Column(name = "nombreCompleto")
	String nombreCompleto;
	
	@Column(name = "cobranza_total")
	double cobranzaTotal;
	
	@Column(name = "factor_cobranza")
	double factorCobranza;

	@Column(name = "factor_normalidad")
	double factorNormalidad;
	
	@Column(name = "base_cobranza")
	double baseCobranza;

	@Column(name = "base_normalidad")
	double baseNormalidad;
	
	@Column(name = "base_clientes_nuevos")
	double baseClientesNuevos;
	
	@Column(name = "cobranza")
	double cobranza;

	@Column(name = "normalidad")
	double normalidad;
	
	@Column(name = "clientes_nuevos")
	double clientesNuevos;

	@Column(name = "total_nomina")
	double totalNomina;

	@Column(name = "base_nomina")
	double baseNomina;

	@Column(name = "base_asimilados")
	double baseAsimilados;

	@Column(name = "descuento_prestamo")
	double descuentoPrestamo;

	@Column(name = "descuento_moto")
	double descuentoMoto;

	public String getIdNomina() {
		return idNomina;
	}

	public void setIdNomina(String idNomina) {
		this.idNomina = idNomina;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public long getSemana() {
		return semana;
	}

	public void setSemana(long semana) {
		this.semana = semana;
	}

	public String getCveEmpleado() {
		return cveEmpleado;
	}

	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}

	public long getSemanas() {
		return semanas;
	}

	public void setSemanas(long semanas) {
		this.semanas = semanas;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public double getCobranzaTotal() {
		return cobranzaTotal;
	}

	public void setCobranzaTotal(double cobranzaTotal) {
		this.cobranzaTotal = cobranzaTotal;
	}

	public double getFactorCobranza() {
		return factorCobranza;
	}

	public void setFactorCobranza(double factorCobranza) {
		this.factorCobranza = factorCobranza;
	}

	public double getFactorNormalidad() {
		return factorNormalidad;
	}

	public void setFactorNormalidad(double factorNormalidad) {
		this.factorNormalidad = factorNormalidad;
	}

	public double getBaseCobranza() {
		return baseCobranza;
	}

	public void setBaseCobranza(double baseCobranza) {
		this.baseCobranza = baseCobranza;
	}

	public double getBaseNormalidad() {
		return baseNormalidad;
	}

	public void setBaseNormalidad(double baseNormalidad) {
		this.baseNormalidad = baseNormalidad;
	}

	public double getBaseClientesNuevos() {
		return baseClientesNuevos;
	}

	public void setBaseClientesNuevos(double baseClientesNuevos) {
		this.baseClientesNuevos = baseClientesNuevos;
	}

	public double getCobranza() {
		return cobranza;
	}

	public void setCobranza(double cobranza) {
		this.cobranza = cobranza;
	}

	public double getNormalidad() {
		return normalidad;
	}

	public void setNormalidad(double normalidad) {
		this.normalidad = normalidad;
	}

	public double getClientesNuevos() {
		return clientesNuevos;
	}

	public void setClientesNuevos(double clientesNuevos) {
		this.clientesNuevos = clientesNuevos;
	}

	public double getTotalNomina() {
		return totalNomina;
	}

	public void setTotalNomina(double total_nomina) {
		this.totalNomina = total_nomina;
	}

	public double getBaseNomina() {
		return baseNomina;
	}

	public void setBaseNomina(double baseNomina) {
		this.baseNomina = baseNomina;
	}

	public double getBaseAsimilados() {
		return baseAsimilados;
	}

	public void setBaseAsimilados(double baseAsimilados) {
		this.baseAsimilados = baseAsimilados;
	}

	public double getDescuentoPrestamo() {
		return descuentoPrestamo;
	}

	public void setDescuentoPrestamo(double descuento_prestamo) {
		this.descuentoPrestamo = descuento_prestamo;
	}

	public double getDescuentoMoto() {
		return descuentoMoto;
	}

	public void setDescuentoMoto(double descuento_moto) {
		this.descuentoMoto = descuento_moto;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public long getQuincena() {
		return quincena;
	}

	public void setQuincena(long quincena) {
		this.quincena = quincena;
	}
	
	

}
