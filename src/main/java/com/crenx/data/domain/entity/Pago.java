package com.crenx.data.domain.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Pago {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_pago", unique = true)
	private String idPago;

	@Column(name = "id", nullable=false)
	private long id;
	
	@Column(name = "fecha_pago")
	private Date fechaPago;
	
	@Column(name = "abono")
	private double abono;
	
	@Column(name = "comision")
	private double comision;
		
	@Column(name = "saldo_anterior")
	private double saldoAnterior=0;

	@Column(name = "pago_adicional")
	private double pagoAdicional=0;

	
	@Column(name = "id_usuario", length = 40)
	private String idUsuario;
	
	@Column(name = "id_dispositivo", length = 20)
	private String idDispositivo;

	@Column(name = "no_recibo", length = 20)
	private String noRecibo;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_credito", nullable=false, referencedColumnName="id_credito")
	private Credito credito;

	@Column(name = "geo_posicion", length = 40)
	private String geoPosicion;

	public String getIdPago() {
		return idPago;
	}

	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public double getAbono() {
		return abono;
	}

	public void setAbono(double abono) {
		this.abono = abono;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public Credito getCredito() {
		return credito;
	}

	public void setCredito(Credito credito) {
		this.credito = credito;
	}

	public String getNoRecibo() {
		return noRecibo;
	}

	public void setNoRecibo(String noRecibo) {
		this.noRecibo = noRecibo;
	}

	public String getGeoPosicion() {
		return geoPosicion;
	}

	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(double saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public double getPagoAdicional() {
		return pagoAdicional;
	}

	public void setPagoAdicional(double pagoAdicional) {
		this.pagoAdicional = pagoAdicional;
	}
	

}
