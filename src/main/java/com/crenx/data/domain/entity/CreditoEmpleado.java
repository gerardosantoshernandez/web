package com.crenx.data.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class CreditoEmpleado implements Serializable{

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_credito_empl", unique = true)
	private String idCreditoEmpl;

	@Column(name = "id", nullable=false)
	private long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_empleado", nullable=false, referencedColumnName="id_empleado")
	private Empleado empleado;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_credito", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoCredito;

	@Column(name = "fecha_emision")
	private Date fechaEmision;

	@Column(name = "fecha_ultimo_pago")
	private Date fechaUltimoPago;
	
	@Column(name = "monto")
	private double monto;
	
	@Column(name = "comision")
	private double comision;
	
	@Column(name = "plazo")
	private short plazo=0;
	
	@Column(name = "pago")
	private double pago=0;

	@Column(name = "saldo")
	private double  saldo=0;
	
	@Column(name = "aplicado_nomina")
	private double  aplicadoNomina=0;
	
	@Column(name = "periodo")
	private long  periodo=0;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_estatus_credito", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusCredito;
	

	public String getNombreEmpleado()
	{
		String retVal = "";
		if (empleado!=null)
				retVal = empleado.getNombre() + " " + empleado.getApellidoPaterno() + " " + empleado.getApellidoMaterno();
		return retVal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIdCreditoEmpl() {
		return idCreditoEmpl;
	}

	public void setIdCreditoEmpl(String idCreditoEmpl) {
		this.idCreditoEmpl = idCreditoEmpl;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Catalogo getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(Catalogo tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaUltimoPago() {
		return fechaUltimoPago;
	}

	public void setFechaUltimoPago(Date fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public short getPlazo() {
		return plazo;
	}

	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}

	public double getPago() {
		return pago;
	}

	public void setPago(double pago) {
		this.pago = pago;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Catalogo getEstatusCredito() {
		return estatusCredito;
	}

	public void setEstatusCredito(Catalogo estatusCredito) {
		this.estatusCredito = estatusCredito;
	}

	public double getAplicadoNomina() {
		return aplicadoNomina;
	}

	public void setAplicadoNomina(double aplicadoNomina) {
		this.aplicadoNomina = aplicadoNomina;
	}

	public long getPeriodo() {
		return periodo;
	}

	public void setPeriodo(long periodo) {
		this.periodo = periodo;
	}
	
}
