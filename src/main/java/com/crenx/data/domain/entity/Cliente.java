package com.crenx.data.domain.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.crenx.data.domain.vo.NameValueVO;

@Entity
public class Cliente {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_cliente", unique = true)
	private String idCliente;

	@Column(name = "id", nullable=false)
	private long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_figura", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoFigura;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_documento_identidad", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo tipoDocId;

	@Column(name = "doc_identificacion",length = 50)
	private String docIdentificacion;

	@Column(name = "cve_fiscal",length = 100)
	private String cveFiscal;
	
	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "apellido_paterno",length = 50)
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno",length = 50)
	private String apellidoMaterno;

	@Column(name = "correo_electronico",length = 100)
	private String correoElectronico;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_genero", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo genero;
	
	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;
	
	@Column(name = "curp",length = 20)
	private String curp;

	@Column(name = "fecha_creacion")
	private Date creacion;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_domicilio_res", nullable=true, referencedColumnName="id_domicilio")
	private Domicilio domicilioResidencial;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_domicilio_lab", nullable=true, referencedColumnName="id_domicilio")
	private Domicilio domicilioLaboral;

	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_ocupacion", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo ocupacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_entidad_nacimiento", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo entidadNacimiento;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_pais_nacimiento", nullable=true, referencedColumnName="id_catalogo")
	private Catalogo paisNacimiento;
		
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_ruta", nullable=true, referencedColumnName="id_org")
	private Organizacion ruta;
	
   @ManyToOne
   @JoinColumn(name="parent_id")
   private Cliente parent;

   @OneToMany(mappedBy="parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
   private Collection<Cliente> codeudores = new ArrayList<Cliente>();
   	
	@Column(name = "tel_residencia",length = 20)
	private String telResidencia;
	
	@Column(name = "tel_celular",length = 20)
	private String telCelular;
	
	@Column(name = "tel_lab",length = 20)
	private String telLab;
	
	@Column(name = "nombre_ref_personal",length = 100)
	private String nombreRefPersonal;
	
	@Column(name = "nombre_ref_familiar",length = 100)
	private String nombreRefFamiliar;
	
	@Column(name = "nombre_empresa",length = 100)
	private String nombreEmpresa;
	
	@Column(name = "tel_ref_personal",length = 20)
	private String telRefPersonal;
	
	@Column(name = "tel_ref_familiar",length = 20)
	private String telRefFamiliar;

	@Column(name = "geo_posicion", length = 40)
	private String geoPosicion;
	
	@OneToMany(mappedBy="cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Collection<Credito> creditos = new ArrayList<Credito>();
	
	@Column(name= "usuario")
	private String usuario;

	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Collection<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(Collection<Credito> creditos) {
		this.creditos = creditos;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public Catalogo getTipoFigura() {
		return tipoFigura;
	}

	public void setTipoFigura(Catalogo tipoFigura) {
		this.tipoFigura = tipoFigura;
	}

	public Catalogo getTipoDocId() {
		return tipoDocId;
	}

	public void setTipoDocId(Catalogo tipoDocId) {
		this.tipoDocId = tipoDocId;
	}

	public String getDocIdentificacion() {
		return docIdentificacion;
	}

	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}

	public String getCveFiscal() {
		return cveFiscal;
	}

	public void setCveFiscal(String cveFiscal) {
		this.cveFiscal = cveFiscal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public Domicilio getDomicilioResidencial() {
		return domicilioResidencial;
	}

	public void setDomicilioResidencial(Domicilio domicilioResidencial) {
		this.domicilioResidencial = domicilioResidencial;
	}

	public Domicilio getDomicilioLaboral() {
		return domicilioLaboral;
	}

	public void setDomicilioLaboral(Domicilio domicilioLaboral) {
		this.domicilioLaboral = domicilioLaboral;
	}

	public Catalogo getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(Catalogo ocupacion) {
		this.ocupacion = ocupacion;
	}

	public Catalogo getEntidadNacimiento() {
		return entidadNacimiento;
	}

	public void setEntidadNacimiento(Catalogo entidadNacimiento) {
		this.entidadNacimiento = entidadNacimiento;
	}

	public Catalogo getPaisNacimiento() {
		return paisNacimiento;
	}

	public void setPaisNacimiento(Catalogo paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}

	public Organizacion getRuta() {
		return ruta;
	}

	public void setRuta(Organizacion ruta) {
		this.ruta = ruta;
	}

	public String getTelResidencia() {
		return telResidencia;
	}

	public void setTelResidencia(String telResidencia) {
		this.telResidencia = telResidencia;
	}

	public String getTelCelular() {
		return telCelular;
	}

	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}

	public String getTelLab() {
		return telLab;
	}

	public void setTelLab(String telLab) {
		this.telLab = telLab;
	}

	public String getNombreRefPersonal() {
		return nombreRefPersonal;
	}

	public void setNombreRefPersonal(String nombreRefPersonal) {
		this.nombreRefPersonal = nombreRefPersonal;
	}

	public String getNombreRefFamiliar() {
		return nombreRefFamiliar;
	}

	public void setNombreRefFamiliar(String nombreRefFamiliar) {
		this.nombreRefFamiliar = nombreRefFamiliar;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getTelRefPersonal() {
		return telRefPersonal;
	}

	public void setTelRefPersonal(String telRefPersonal) {
		this.telRefPersonal = telRefPersonal;
	}

	public String getTelRefFamiliar() {
		return telRefFamiliar;
	}

	public void setTelRefFamiliar(String telRefFamiliar) {
		this.telRefFamiliar = telRefFamiliar;
	}

	public Cliente getParent() {
		return parent;
	}

	public void setParent(Cliente parent) {
		this.parent = parent;
	}

	public Collection<Cliente> getCodeudores() {
		return codeudores;
	}

	public void setCodeudores(Collection<Cliente> codeudores) {
		this.codeudores = codeudores;
	}
	
	
	public List<NameValueVO> codeudorToValueList()
	{
		ArrayList<NameValueVO> nameValue =  new ArrayList<NameValueVO>();
		for (Cliente oneItem : this.codeudores) {
			NameValueVO oneCo = new NameValueVO(oneItem.getIdCliente(), oneItem.getNombre()+" " + oneItem.getApellidoPaterno()+ " " + oneItem.getApellidoMaterno());
			nameValue.add(oneCo);
		}	
		return nameValue;
	}

	public String getGeoPosicion() {
		return geoPosicion;
	}

	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getNombreCompleto()
	{
		return this.nombre + " "+ this.getApellidoPaterno() + " " + this.getApellidoMaterno();
	}
	
}
