package com.crenx.data.domain.entity;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

public class Perfil implements GrantedAuthority {
	
	public Perfil(){
		super();
	}
	
	private String nombre = "ROLE_USER";
	private String idEmpleado;
	private String idCargo;
	private String idTitOrg;
	private String nombreTitOrg;
	private String tipoTitOrg;
	private String idSupOrg;
	private String nombreSupOrg;
	private String tipoSupOrg;
	private List<String> permisos;
	private List<String> roles;
	private String idParentTit;
	private String nombreParentTit;
	
		
	public String getName() {
		return nombre;
	}

	public void setName(String name) {
		this.nombre = name;
	}


	@Override
	public String getAuthority() {
		// FIXME: Este valor se debe calcular, tomando en cuenta la ruta y el nivel del usuario (Ruta, GErencia, Region, etc)
		//En UserDetails
		return nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(String idCargo) {
		this.idCargo = idCargo;
	}

	public String getIdTitOrg() {
		return idTitOrg;
	}

	public void setIdTitOrg(String idTitOrg) {
		this.idTitOrg = idTitOrg;
	}

	public String getNombreTitOrg() {
		return nombreTitOrg;
	}

	public void setNombreTitOrg(String nombreTitOrg) {
		this.nombreTitOrg = nombreTitOrg;
	}

	public String getTipoTitOrg() {
		return tipoTitOrg;
	}

	public void setTipoTitOrg(String tipoTitOrg) {
		this.tipoTitOrg = tipoTitOrg;
	}

	public String getIdSupOrg() {
		return idSupOrg;
	}

	public void setIdSupOrg(String idSupOrg) {
		this.idSupOrg = idSupOrg;
	}

	public String getNombreSupOrg() {
		return nombreSupOrg;
	}

	public void setNombreSupOrg(String nombreSupOrg) {
		this.nombreSupOrg = nombreSupOrg;
	}

	public String getTipoSupOrg() {
		return tipoSupOrg;
	}

	public void setTipoSupOrg(String tipoSupOrg) {
		this.tipoSupOrg = tipoSupOrg;
	}

	public List<String> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<String> permisos) {
		this.permisos = permisos;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getIdParentTit() {
		return idParentTit;
	}

	public void setIdParentTit(String idParentTit) {
		this.idParentTit = idParentTit;
	}

	public String getNombreParentTit() {
		return nombreParentTit;
	}

	public void setNombreParentTit(String nombreParentTit) {
		this.nombreParentTit = nombreParentTit;
	}


}
