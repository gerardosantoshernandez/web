package com.crenx.data.domain.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

@Entity
public class ObjetoSeguridad implements GrantedAuthority {
	
	
	public ObjetoSeguridad(){
		super();
	}
	
	public ObjetoSeguridad(String tipoObje, String nombre, String descripcion, String llaveNegocio, String paginaInicio,
			Date creacion) {
		super();
		this.tipoObje = tipoObje;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.llaveNegocio = llaveNegocio;
		this.paginaInicio = paginaInicio;
		this.creacion = creacion;
	}

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_objetoSeguridad", unique = true)
	private String idObjetoSeguridad;

	@Column(name = "tipoObje",length = 50)
	private String tipoObje;
	
	@Column(name = "nombre",length = 150)
	private String nombre;
	
	@Column(name = "descripcion",length = 150)
	private String descripcion;
	
	@Column(name = "llave_negocio",length = 50)
	private String llaveNegocio;
	
	@Column(name = "paginaInicio",length = 50)
	private String paginaInicio;
	
	@Column(name = "creacion",length = 50)
	private Date creacion;

	@OneToOne
	@JoinColumn(name="id_estatus", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusActual;
	
	/*
	@OneToMany
	@JoinColumn(name="id_objetoSeguridad", nullable=false, referencedColumnName="id_parent")
	private Collection<ObjetoSeguridadRelaciones> objetoSeguridadHijos;
	*/
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	private Collection<Usuario> usuarios;

	public String getIdObjetoSeguridad() {
		return idObjetoSeguridad;
	}

	public void setIdObjetoSeguridad(String idObjetoSeguridad) {
		this.idObjetoSeguridad = idObjetoSeguridad;
	}

	public String getTipoObje() {
		return tipoObje;
	}

	public void setTipoObje(String tipoObje) {
		this.tipoObje = tipoObje;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLlaveNegocio() {
		return llaveNegocio;
	}

	public void setLlaveNegocio(String llaveNegocio) {
		this.llaveNegocio = llaveNegocio;
	}

	public String getPaginaInicio() {
		return paginaInicio;
	}

	public void setPaginaInicio(String paginaInicio) {
		this.paginaInicio = paginaInicio;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}


	public Catalogo getEstatusActual() {
		return estatusActual;
	}

	public void setEstatusActual(Catalogo estatusActual) {
		this.estatusActual = estatusActual;
	}

	/*
	public Collection<ObjetoSeguridadRelaciones> getObjetoSeguridadHijos() {
		return objetoSeguridadHijos;
	}

	public void setObjetoSeguridadHijos(Collection<ObjetoSeguridadRelaciones> objetoSeguridadHijos) {
		this.objetoSeguridadHijos = objetoSeguridadHijos;
	}
*/
	public Collection<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Collection<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return this.nombre;
	}
	
	
	
	
}
