package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;




@Entity
@NamedNativeQueries({
	@NamedNativeQuery(name = "IndicadorCarteraDiario.findByCarteraRutaDiaRaw",
			query=
			"Select 'dia' as tipoperiodo, :dia as periodo, "
			+ " sum( case WHEN (tc.clave_interna = 'CRED_NUEVO' AND sc.clave_interna = 'CRED_ACTIVO') then 1 else 0 END ) as creditos_nuevos, "
			+ " sum( case WHEN (tc.clave_interna = 'CRED_NUEVO' AND sc.clave_interna = 'CRED_ACTIVO') then c.saldo else 0.0 END) as creditos_nuevos_imp, "
			+ " sum( case WHEN (tc.clave_interna = 'CRED_RECOMPRA' AND sc.clave_interna = 'CRED_ACTIVO') then 1 else 0 END ) as creditos_recompra, "
			+ " sum( case WHEN (tc.clave_interna = 'CRED_RECOMPRA' AND sc.clave_interna = 'CRED_ACTIVO') then c.saldo else 0.0 END) as creditos_recompra_imp, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND c.fecha_vencimiento <=curdate()) then 1 else 0 END ) as creditos_vencidos, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND c.fecha_vencimiento <=curdate()) then  c.saldo else 0.0 END) as creditos_vencidos_imp, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes=0) then 1 else 0 end) as creditos_normal, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes=0) then c.saldo else 0 end) as creditos_normal_imp, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>0 and ta.pendientes <=3) then 1 else 0 end) as creditos_correctivo, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>0 and ta.pendientes <=3) then c.saldo else 0 end) as creditos_correctivo_imp, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>3) then 1 else 0 end) as creditos_riesgo, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>3) then c.saldo else 0 end) as creditos_riesgo_imp, "
			+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN 1 else 0 end) as creditos_total, "
			+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN c.saldo else 0 end) as creditos_total_imp, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_INCOBRABLE' ) then 1 else 0 end) as creditos_incobrable, "
			+ " sum( case WHEN (sc.clave_interna = 'CRED_INCOBRABLE' ) then c.saldo else 0 end) as creditos_incobrable_imp, " 
			+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN c.saldo_insoluto else 0 end) as cartera_capital "
			+ " FROM credito c "
			+ " INNER JOIN catalogo tc on c.id_tipo_credito = tc.id_catalogo "
			+ " INNER JOIN catalogo sc on c.id_estatus_credito = sc.id_catalogo "
			+ " INNER JOIN organizacion r on c.id_ruta = r.id_org "
			+ " INNER JOIN organizacion g on r.id_parent=g.id_org "
			+ " INNER JOIN organizacion region on g.id_parent=region.id_org "
			+ " INNER JOIN organizacion di on region.id_parent = di.id_org "
			+ " INNER JOIN producto pr on c.id_producto = pr.id_producto "
			+ " LEFT OUTER JOIN "
			+ " (Select id_credito, count(*) as pendientes, sum(cuota) as pagos "
			+ " From tabla_amortizacion ta "
			+ " INNER JOIN catalogo ca on ta.id_estatus_pago=ca.id_catalogo "
			+ " Where ta.fecha_vencimiento <= curdate() "
			+ " AND ca.clave_interna in ('PAGO_PENDIENTE') "
			+ " Group by id_credito) ta on c.id_credito = ta.id_credito "
			+ " WHERE sc.clave_interna in ('CRED_ACTIVO','CRED_INCOBRABLE') "
			+ " AND r.id_org in (:filtros) "
			+ " group by :dia ",
			resultSetMapping="IndicadorCarteraPeriodo"
			),
	
	
	
	
@NamedNativeQuery(name = "IndicadorCarteraDiario.findByToday",
query=
"Select di.id_org as idDivision, di.nombre as division, region.id_org as idRegion, region.nombre as region, g.id_org as idGerencia, g.nombre as gerencia, r.id_org as idRuta, r.nombre as ruta, "
+ " pr.nombre as producto, pr.plazo, curdate() as fecha, "
+ " sum( case WHEN (tc.clave_interna = 'CRED_NUEVO' AND sc.clave_interna = 'CRED_ACTIVO') then 1 else 0 END ) as creditos_nuevos, "
+ " sum( case WHEN (tc.clave_interna = 'CRED_NUEVO' AND sc.clave_interna = 'CRED_ACTIVO') then c.saldo else 0.0 END) as creditos_nuevos_imp, "
+ " sum( case WHEN (tc.clave_interna = 'CRED_RECOMPRA' AND sc.clave_interna = 'CRED_ACTIVO') then 1 else 0 END ) as creditos_recompra, "
+ " sum( case WHEN (tc.clave_interna = 'CRED_RECOMPRA' AND sc.clave_interna = 'CRED_ACTIVO') then c.saldo else 0.0 END) as creditos_recompra_imp, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND c.fecha_vencimiento <=curdate()) then 1 else 0 END ) as creditos_vencidos, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND c.fecha_vencimiento <=curdate()) then  c.saldo else 0.0 END) as creditos_vencidos_imp, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes=0) then 1 else 0 end) as creditos_normal, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes=0) then c.saldo else 0 end) as creditos_normal_imp, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>0 and ta.pendientes <=3) then 1 else 0 end) as creditos_correctivo, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>0 and ta.pendientes <=3) then c.saldo else 0 end) as creditos_correctivo_imp, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>3) then 1 else 0 end) as creditos_riesgo, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_ACTIVO' AND ta.pendientes>3) then c.saldo else 0 end) as creditos_riesgo_imp, "
+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN 1 else 0 end) as creditos_total, "
+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN c.saldo else 0 end) as creditos_total_imp, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_INCOBRABLE' ) then 1 else 0 end) as creditos_incobrable, "
+ " sum( case WHEN (sc.clave_interna = 'CRED_INCOBRABLE' ) then c.saldo else 0 end) as creditos_incobrable_imp, " 
+ " sum( case WHEN sc.clave_interna = 'CRED_ACTIVO' THEN c.saldo_insoluto else 0 end) as cartera_capital "
+ " FROM credito c "
+ " INNER JOIN catalogo tc on c.id_tipo_credito = tc.id_catalogo "
+ " INNER JOIN catalogo sc on c.id_estatus_credito = sc.id_catalogo "
+ " INNER JOIN organizacion r on c.id_ruta = r.id_org "
+ " INNER JOIN organizacion g on r.id_parent=g.id_org "
+ " INNER JOIN organizacion region on g.id_parent=region.id_org "
+ " INNER JOIN organizacion di on region.id_parent = di.id_org "
+ " INNER JOIN producto pr on c.id_producto = pr.id_producto "
+ " LEFT OUTER JOIN "
+ " (Select id_credito, count(*) as pendientes, sum(cuota) as pagos "
+ " From tabla_amortizacion ta "
+ " INNER JOIN catalogo ca on ta.id_estatus_pago=ca.id_catalogo "
+ " Where ta.fecha_vencimiento <= curdate() "
+ " AND ca.clave_interna in ('PAGO_PENDIENTE') "
+ " Group by id_credito) ta on c.id_credito = ta.id_credito "
+ " WHERE sc.clave_interna in ('CRED_ACTIVO','CRED_INCOBRABLE') "
+ " group by di.id_org, di.nombre, region.id_org, region.nombre, g.id_org, g.nombre, r.id_org, r.nombre, "
+ " pr.nombre, pr.plazo, fecha ",
resultSetMapping="IndicadorCarteraDiarioMappings"
),

@NamedNativeQuery(name = "IndicadorCarteraDiario.findByCarteraRutaDia",
query=
	"Select  "
	+ " 'dia' as tipoperiodo, dia_semana as periodo, "
	+ " sum(creditos_nuevos) as creditos_nuevos, "
	+ " sum(creditos_nuevos_imp) as creditos_nuevos_imp, "
	+ " sum(creditos_recompra) as creditos_recompra, "
	+ " sum(creditos_recompra_imp) as creditos_recompra_imp, "
	+ " sum(creditos_vencidos) as creditos_vencidos, "
	+ " sum(creditos_vencidos_imp) as creditos_vencidos_imp, "
	+ " sum(creditos_normal) as creditos_normal, "
	+ " sum(creditos_normal_imp) as creditos_normal_imp, "
	+ " sum(creditos_correctivo) as creditos_correctivo, "
	+ " sum(creditos_correctivo_imp) as creditos_correctivo_imp, "
	+ " sum(creditos_riesgo) as creditos_riesgo, "
	+ " sum(creditos_riesgo_imp) as creditos_riesgo_imp, "
	+ " sum(creditos_total) as creditos_total, "
	+ " sum(creditos_total_imp) as creditos_total_imp, "
	+ " sum(creditos_incobrable) as creditos_incobrable, "
	+ " sum(creditos_incobrable_imp) as creditos_incobrable_imp, " 
	+ " sum(cartera_capital) as cartera_capital "
	+ " FROM indicador_cartera_diario "
	+ " WHERE id_ruta in (:filtros )  "
	+ " AND fecha between :fechaInicial and :fechaFinal  "
	+ "Group By dia_semana "
,
resultSetMapping="IndicadorCarteraPeriodo"
),

@NamedNativeQuery(name = "IndicadorCarteraDiario.findByCarteraRutaMes",
query=
	"Select  "
	+ " 'mes' as tipoperiodo, mes as periodo, "
	+ " sum(creditos_nuevos) as creditos_nuevos, "
	+ " sum(creditos_nuevos_imp) as creditos_nuevos_imp, "
	+ " sum(creditos_recompra) as creditos_recompra, "
	+ " sum(creditos_recompra_imp) as creditos_recompra_imp, "
	+ " sum(creditos_vencidos) as creditos_vencidos, "
	+ " sum(creditos_vencidos_imp) as creditos_vencidos_imp, "
	+ " sum(creditos_normal) as creditos_normal, "
	+ " sum(creditos_normal_imp) as creditos_normal_imp, "
	+ " sum(creditos_correctivo) as creditos_correctivo, "
	+ " sum(creditos_correctivo_imp) as creditos_correctivo_imp, "
	+ " sum(creditos_riesgo) as creditos_riesgo, "
	+ " sum(creditos_riesgo_imp) as creditos_riesgo_imp, "
	+ " sum(creditos_total) as creditos_total, "
	+ " sum(creditos_total_imp) as creditos_total_imp, "
	+ " sum(creditos_incobrable) as creditos_incobrable, "
	+ " sum(creditos_incobrable_imp) as creditos_incobrable_imp, " 
	+ " sum(cartera_capital) as cartera_capital "
	+ " FROM indicador_cartera_diario "
	+ " WHERE id_ruta in (:filtros )  "
	+ " AND fecha between :fechaInicial and :fechaFinal  "
	+ "Group By mes "
,
resultSetMapping="IndicadorCarteraPeriodo"
),

@NamedNativeQuery(name = "IndicadorCarteraDiario.findByCarteraRutaYear",
query=
	"Select  "
	+ " 'anio' as tipoperiodo, anio as periodo, "
	+ " sum(creditos_nuevos) as creditos_nuevos, "
	+ " sum(creditos_nuevos_imp) as creditos_nuevos_imp, "
	+ " sum(creditos_recompra) as creditos_recompra, "
	+ " sum(creditos_recompra_imp) as creditos_recompra_imp, "
	+ " sum(creditos_vencidos) as creditos_vencidos, "
	+ " sum(creditos_vencidos_imp) as creditos_vencidos_imp, "
	+ " sum(creditos_normal) as creditos_normal, "
	+ " sum(creditos_normal_imp) as creditos_normal_imp, "
	+ " sum(creditos_correctivo) as creditos_correctivo, "
	+ " sum(creditos_correctivo_imp) as creditos_correctivo_imp, "
	+ " sum(creditos_riesgo) as creditos_riesgo, "
	+ " sum(creditos_riesgo_imp) as creditos_riesgo_imp, "
	+ " sum(creditos_total) as creditos_total, "
	+ " sum(creditos_total_imp) as creditos_total_imp, "
	+ " sum(creditos_incobrable) as creditos_incobrable, "
	+ " sum(creditos_incobrable_imp) as creditos_incobrable_imp, " 
	+ " sum(cartera_capital) as cartera_capital "
	+ " FROM indicador_cartera_diario "
	+ " WHERE id_ruta in (:filtros )  "
	+ " AND fecha between :fechaInicial and :fechaFinal  "
	+ "Group By anio "
,
resultSetMapping="IndicadorCarteraPeriodo"
)

})

@SqlResultSetMappings(
		{

@SqlResultSetMapping(
		name="IndicadorCarteraDiarioMappings", 
		classes = @ConstructorResult(
				targetClass = com.crenx.data.domain.vo.IndicadorCarteraDiarioVO.class,
				columns = {
					@ColumnResult (name="idDivision" ),
					@ColumnResult (name="division" ),
					@ColumnResult (name="idRegion" ),
					@ColumnResult (name="region" ),
					@ColumnResult (name="idGerencia" ),
					@ColumnResult (name="gerencia" ),
					@ColumnResult (name="idRuta" ),					
					@ColumnResult (name="ruta" ),					
					@ColumnResult (name="producto" ),
					@ColumnResult (name="plazo" , type=Long.class),
					@ColumnResult (name="fecha" , type=Date.class),					
					@ColumnResult (name="creditos_nuevos" , type=Long.class),
					@ColumnResult (name="creditos_nuevos_imp" , type=Double.class),
					@ColumnResult (name="creditos_recompra" , type=Long.class),
					@ColumnResult (name="creditos_recompra_imp" , type=Double.class),
					@ColumnResult (name="creditos_vencidos" , type=Long.class),
					@ColumnResult (name="creditos_vencidos_imp" , type=Double.class),
					@ColumnResult (name="creditos_normal" , type=Long.class),
					@ColumnResult (name="creditos_normal_imp" , type=Double.class),
					@ColumnResult (name="creditos_correctivo" , type=Long.class),
					@ColumnResult (name="creditos_correctivo_imp" , type=Double.class),
					@ColumnResult (name="creditos_riesgo" , type=Long.class),
					@ColumnResult (name="creditos_riesgo_imp" , type=Double.class),
					@ColumnResult (name="creditos_total" , type=Long.class),
					@ColumnResult (name="creditos_total_imp" , type=Double.class),
					@ColumnResult (name="creditos_incobrable" , type=Long.class),
					@ColumnResult (name="creditos_incobrable_imp" , type=Double.class),
					@ColumnResult (name="cartera_capital" , type=Double.class),
				})),


@SqlResultSetMapping(
		name="IndicadorCarteraPeriodo", 
		classes = @ConstructorResult(
				targetClass = com.crenx.data.domain.vo.IndicadorCarteraDiarioVO.class,
				columns = {
					@ColumnResult (name="tipoperiodo" ),
					@ColumnResult (name="periodo" ),
					@ColumnResult (name="creditos_nuevos" , type=Long.class),
					@ColumnResult (name="creditos_nuevos_imp" , type=Double.class),
					@ColumnResult (name="creditos_recompra" , type=Long.class),
					@ColumnResult (name="creditos_recompra_imp" , type=Double.class),
					@ColumnResult (name="creditos_vencidos" , type=Long.class),
					@ColumnResult (name="creditos_vencidos_imp" , type=Double.class),
					@ColumnResult (name="creditos_normal" , type=Long.class),
					@ColumnResult (name="creditos_normal_imp" , type=Double.class),
					@ColumnResult (name="creditos_correctivo" , type=Long.class),
					@ColumnResult (name="creditos_correctivo_imp" , type=Double.class),
					@ColumnResult (name="creditos_riesgo" , type=Long.class),
					@ColumnResult (name="creditos_riesgo_imp" , type=Double.class),
					@ColumnResult (name="creditos_total" , type=Long.class),
					@ColumnResult (name="creditos_total_imp" , type=Double.class),
					@ColumnResult (name="creditos_incobrable" , type=Long.class),
					@ColumnResult (name="creditos_incobrable_imp" , type=Double.class),
					@ColumnResult (name="cartera_capital" , type=Double.class),
				}))
		}
		)


public class IndicadorCarteraDiario {
	
	
	
	
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "id_indicador_cartera", unique = true)
	private String idIndicadorCartera;

	
	@Column(name = "division", length = 250)
	private String division;

	@Column(name = "region", length = 250)
	private String region;

	@Column(name = "gerencia", length = 250)
	private String gerencia;
	
	@Column(name = "ruta", length = 250)
	private String ruta;
	
	@Column(name = "id_division", length = 250)
	private String idDivision;

	@Column(name = "id_region", length = 250)
	private String idRegion;

	@Column(name = "id_gerencia", length = 250)
	private String idGerencia;
	
	@Column(name = "id_ruta", length = 250)
	private String idRuta;

	
	@Column(name = "producto", length = 250)
	private String producto;

	@Column(name = "plazo")
	private long plazo=0;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha")
	private Date fecha;
	
	@Column(name = "creditos_nuevos")
	private long creditosNuevos=0;
	
	@Column(name = "creditos_nuevos_imp")
	private double creditosNuevosImp=0;

	@Column(name = "creditos_recompra")
	private long creditosRecompra=0;
	
	@Column(name = "creditos_recompra_imp")
	private double creditosRecompraImp=0;
	
	@Column(name = "creditos_vencidos")
	private long creditosVencidos=0;
	
	@Column(name = "creditos_vencidos_imp")
	private double creditosVencidosImp=0;

	@Column(name = "creditos_normal")
	private long creditoNormal=0;
	
	@Column(name = "creditos_normal_imp")
	private double creditoNormalImp=0;

	@Column(name = "creditos_correctivo")
	private long creditoCorrectivo=0;
	
	@Column(name = "creditos_correctivo_imp")
	private double creditoCorrectivoImp=0;
	
	@Column(name = "creditos_riesgo")
	private long creditoRiesgo=0;
	
	@Column(name = "creditos_riesgo_imp")
	private double creditoRiesgoImp=0;

	@Column(name = "creditos_total")
	private long creditoTotal=0;
	
	@Column(name = "creditos_total_imp")
	private double creditoTotalImp=0;

	
	@Column(name = "creditos_incobrable")
	private long creditoIncobrable=0;
	
	@Column(name = "creditos_incobrable_imp")
	private double creditoIncobrableImp=0;

	@Column(name = "cartera_capital")
	private double carteraCapital=0;
	
	@Column(name = "dia_semana", length = 20)
	private String dia;
	
	@Column(name = "semana", length = 20)
	private long semana;

	@Column(name = "mes", length = 20)
	private String mes;
	
	@Column(name = "trimestre", length = 20)
	private String trimestre;
	
	@Column(name = "semestre", length = 20)
	private String semestre;

	@Column(name = "anio", length = 20)
	private long anio;
	
	public String getIdIndicadorCartera() {
		return idIndicadorCartera;
	}
	public void setIdIndicadorCartera(String idIndicadorCartera) {
		this.idIndicadorCartera = idIndicadorCartera;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public long getPlazo() {
		return plazo;
	}
	public void setPlazo(long plazo) {
		this.plazo = plazo;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public long getCreditosNuevos() {
		return creditosNuevos;
	}
	public void setCreditosNuevos(long creditosNuevos) {
		this.creditosNuevos = creditosNuevos;
	}
	public double getCreditosNuevosImp() {
		return creditosNuevosImp;
	}
	public void setCreditosNuevosImp(double creditosNuevosImp) {
		this.creditosNuevosImp = creditosNuevosImp;
	}
	public long getCreditosRecompra() {
		return creditosRecompra;
	}
	public void setCreditosRecompra(long creditosRecompra) {
		this.creditosRecompra = creditosRecompra;
	}
	public double getCreditosRecompraImp() {
		return creditosRecompraImp;
	}
	public void setCreditosRecompraImp(double creditosRecompraImp) {
		this.creditosRecompraImp = creditosRecompraImp;
	}
	public long getCreditosVencidos() {
		return creditosVencidos;
	}
	public void setCreditosVencidos(long creditosVencidos) {
		this.creditosVencidos = creditosVencidos;
	}
	public double getCreditosVencidosImp() {
		return creditosVencidosImp;
	}
	public void setCreditosVencidosImp(double creditosVencidosImp) {
		this.creditosVencidosImp = creditosVencidosImp;
	}
	public long getCreditoNormal() {
		return creditoNormal;
	}
	public void setCreditoNormal(long creditoNormal) {
		this.creditoNormal = creditoNormal;
	}
	public double getCreditoNormalImp() {
		return creditoNormalImp;
	}
	public void setCreditoNormalImp(double creditoNormalImp) {
		this.creditoNormalImp = creditoNormalImp;
	}
	public long getCreditoCorrectivo() {
		return creditoCorrectivo;
	}
	public void setCreditoCorrectivo(long creditoCorrectivo) {
		this.creditoCorrectivo = creditoCorrectivo;
	}
	public double getCreditoCorrectivoImp() {
		return creditoCorrectivoImp;
	}
	public void setCreditoCorrectivoImp(double creditoCorrectivoImp) {
		this.creditoCorrectivoImp = creditoCorrectivoImp;
	}
	public long getCreditoRiesgo() {
		return creditoRiesgo;
	}
	public void setCreditoRiesgo(long creditoRiesgo) {
		this.creditoRiesgo = creditoRiesgo;
	}
	public double getCreditoRiesgoImp() {
		return creditoRiesgoImp;
	}
	public void setCreditoRiesgoImp(double creditoRiesgoImp) {
		this.creditoRiesgoImp = creditoRiesgoImp;
	}
	public long getCreditoTotal() {
		return creditoTotal;
	}
	public void setCreditoTotal(long creditoTotal) {
		this.creditoTotal = creditoTotal;
	}
	public double getCreditoTotalImp() {
		return creditoTotalImp;
	}
	public void setCreditoTotalImp(double creditoTotalImp) {
		this.creditoTotalImp = creditoTotalImp;
	}
	public long getCreditoIncobrable() {
		return creditoIncobrable;
	}
	public void setCreditoIncobrable(long creditoIncobrable) {
		this.creditoIncobrable = creditoIncobrable;
	}
	public double getCreditoIncobrableImp() {
		return creditoIncobrableImp;
	}
	public void setCreditoIncobrableImp(double creditoIncobrableImp) {
		this.creditoIncobrableImp = creditoIncobrableImp;
	}
	public double getCarteraCapital() {
		return carteraCapital;
	}
	public void setCarteraCapital(double carteraCapital) {
		this.carteraCapital = carteraCapital;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public long getSemana() {
		return semana;
	}
	public void setSemana(long semana) {
		this.semana = semana;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getTrimestre() {
		return trimestre;
	}
	public void setTrimestre(String trimestre) {
		this.trimestre = trimestre;
	}
	public String getSemestre() {
		return semestre;
	}
	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	public long getAnio() {
		return anio;
	}
	public void setAnio(long anio) {
		this.anio = anio;
	}
	public String getIdDivision() {
		return idDivision;
	}
	public void setIdDivision(String idDivision) {
		this.idDivision = idDivision;
	}
	public String getIdRegion() {
		return idRegion;
	}
	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}
	public String getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}
	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}

	
}
