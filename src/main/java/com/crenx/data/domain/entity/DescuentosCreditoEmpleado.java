package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class DescuentosCreditoEmpleado {

	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name = "idDescuento", unique = true)
	private String idDescuento;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_credito_empl", nullable=false, referencedColumnName="id_credito_empl")
	private CreditoEmpleado creditoEmpleado;
	
	@Column(name = "fecha_proceso")
	Date fechaProceso;
	@Column(name = "semana")
	long semana;

	@Column(name = "descuento")
	private double descuento;

	public String getIdDescuento() {
		return idDescuento;
	}

	public void setIdDescuento(String idDescuento) {
		this.idDescuento = idDescuento;
	}


	public CreditoEmpleado getCreditoEmpleado() {
		return creditoEmpleado;
	}

	public void setCreditoEmpleado(CreditoEmpleado creditoEmpleado) {
		this.creditoEmpleado = creditoEmpleado;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public long getSemana() {
		return semana;
	}

	public void setSemana(long semana) {
		this.semana = semana;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	
}
