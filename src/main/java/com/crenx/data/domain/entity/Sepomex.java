package com.crenx.data.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sepomex {
	 @Id @GeneratedValue long id; // still set automatically

	@Column(name = "d_codigo")
	private String dCodigo;
	@Column(name = "d_asenta")
	private String dasenta; 
	@Column(name = "d_tipo_asenta")
	private String dTipoAsenta; 
	@Column(name = "D_mnpio")
	private String dMnpio; 
	@Column(name = "d_estado")
	private String dEstado; 
	@Column(name = "d_ciudad")
	private String dCiudad; 
	@Column(name = "d_CP")
	private String dCP; 
	@Column(name = "c_estado")
	private String cEstado; 
	@Column(name = "c_oficina")
	private String cOficina; 
	@Column(name = "c_CP")
	private String cCP; 
	@Column(name = "c_tipo_asenta")
	private String cTipoAsenta; 
	@Column(name = "c_mnpio")
	private String cMnpio; 
	@Column(name = "id_asenta_cpcons")
	private String idAsentaCpcons; 
	@Column(name = "d_zona")
	private String dZona; 
	@Column(name = "c_cve_ciudad")
	private String cCveCiudad;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getdCodigo() {
		return dCodigo;
	}
	public void setdCodigo(String dCodigo) {
		this.dCodigo = dCodigo;
	}
	public String getDasenta() {
		return dasenta;
	}
	public void setDasenta(String dasenta) {
		this.dasenta = dasenta;
	}
	public String getdTipoAsenta() {
		return dTipoAsenta;
	}
	public void setdTipoAsenta(String dTipoAsenta) {
		this.dTipoAsenta = dTipoAsenta;
	}
	public String getdMnpio() {
		return dMnpio;
	}
	public void setdMnpio(String dMnpio) {
		this.dMnpio = dMnpio;
	}
	public String getdEstado() {
		return dEstado;
	}
	public void setdEstado(String dEstado) {
		this.dEstado = dEstado;
	}
	public String getdCiudad() {
		return dCiudad;
	}
	public void setdCiudad(String dCiudad) {
		this.dCiudad = dCiudad;
	}
	public String getdCP() {
		return dCP;
	}
	public void setdCP(String dCP) {
		this.dCP = dCP;
	}
	public String getcEstado() {
		return cEstado;
	}
	public void setcEstado(String cEstado) {
		this.cEstado = cEstado;
	}
	public String getcOficina() {
		return cOficina;
	}
	public void setcOficina(String cOficina) {
		this.cOficina = cOficina;
	}
	public String getcCP() {
		return cCP;
	}
	public void setcCP(String cCP) {
		this.cCP = cCP;
	}
	public String getcTipoAsenta() {
		return cTipoAsenta;
	}
	public void setcTipoAsenta(String cTipoAsenta) {
		this.cTipoAsenta = cTipoAsenta;
	}
	public String getcMnpio() {
		return cMnpio;
	}
	public void setcMnpio(String cMnpio) {
		this.cMnpio = cMnpio;
	}
	public String getIdAsentaCpcons() {
		return idAsentaCpcons;
	}
	public void setIdAsentaCpcons(String idAsentaCpcons) {
		this.idAsentaCpcons = idAsentaCpcons;
	}
	public String getdZona() {
		return dZona;
	}
	public void setdZona(String dZona) {
		this.dZona = dZona;
	}
	public String getcCveCiudad() {
		return cCveCiudad;
	}
	public void setcCveCiudad(String cCveCiudad) {
		this.cCveCiudad = cCveCiudad;
	}

}
