package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Figura {

	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	@Column(name="id_figura",  unique = true)
	private String idFigura;
	 
	@PrimaryKeyJoinColumn
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private Empleado empleado;	
	
	@PrimaryKeyJoinColumn
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private Cliente cliente;	
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_figura", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoFigura;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_documento_identidad", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoDocId;
	
	@Column(name = "doc_identificacion",length = 50)
	private String docIdentificacion;
	
	@Column(name = "nombre",length = 50)
	private String nombre;
	
	@Column(name = "apellido_paterno",length = 50)
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno",length = 50)
	private String apellidoMaterno;

	@Column(name = "correo_electronico",length = 100)
	private String correoElectronico;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_genero", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo genero;
	
	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;
	
	@Column(name = "curp",length = 20)
	private String curp;

	@Column(name = "fecha_creacion")
	private Date creacion;
	



	public String getIdFigura() {
		return idFigura;
	}

	public void setIdFigura(String idFigura) {
		this.idFigura = idFigura;
	}

	public Catalogo getTipoFigura() {
		return tipoFigura;
	}

	public void setTipoFigura(Catalogo tipoFigura) {
		this.tipoFigura = tipoFigura;
	}

	public Catalogo getTipoDocId() {
		return tipoDocId;
	}

	public void setTipoDocId(Catalogo tipoDocId) {
		this.tipoDocId = tipoDocId;
	}

	public String getDocIdentificacion() {
		return docIdentificacion;
	}

	public void setDocIdentificacion(String docIdentificacion) {
		this.docIdentificacion = docIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
}
