package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class MovimientoCaja {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_mov_caja", unique = true)
	private String idMovCaja;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_ruta", nullable=false, referencedColumnName="id_org")
	private Organizacion ruta;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_mov", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoMovimiento;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_operacion", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo tipoOperacion;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_gasto", referencedColumnName="id_catalogo")
	private Catalogo tipoGasto;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_estatus_mov", nullable=false, referencedColumnName="id_catalogo")
	private Catalogo estatusMovimiento;

	
	@Column(name = "referencia", length = 100, nullable=false)
	private String referencia;

	@Column(name = "autorizador", length = 100, nullable=false)
	private String autorizador;
	
	@Column(name = "fecha_emision", nullable=false)
	private Date fechaEmision;

	@Column(name = "fecha_sistema", nullable=false)
	private Date fechaSistema;

	
	@Column(name = "cargo", nullable=false)
	private double cargo;

	@Column(name = "abono", nullable=false)
	private double abono;

	@Column(name = "pendiente", nullable=false)
	private double pendiente;

	@Column(name = "id_usuario", length = 40, nullable=false)
	private String idUsuario;
	
	@Column(name = "id_dispositivo", length = 20)
	private String idDispositivo;

	@Column(name = "referenciaLarga", length = 300, nullable=false)
	private String referenciaLarga ;

	@Column(name = "estatus_corte", nullable=false)
	private byte estatusCorte = 0;
	
	@Column(name= "observaciones")
	private String observaciones;
	
	@Column(name= "movcruze")
	private String movcruze;
	
	public String getIdMovCaja() {
		return idMovCaja;
	}

	public void setIdMovCaja(String idMovCaja) {
		this.idMovCaja = idMovCaja;
	}

	public Organizacion getRuta() {
		return ruta;
	}

	public void setRuta(Organizacion ruta) {
		this.ruta = ruta;
	}

	public Catalogo getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(Catalogo tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public Catalogo getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Catalogo tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getAutorizador() {
		return autorizador;
	}

	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getReferenciaLarga() {
		return referenciaLarga;
	}

	public void setReferenciaLarga(String referenciaLarga) {
		this.referenciaLarga = referenciaLarga;
	}

	public Date getFechaSistema() {
		return fechaSistema;
	}

	public void setFechaSistema(Date fechaSistema) {
		this.fechaSistema = fechaSistema;
	}

	public Catalogo getTipoGasto() {
		return tipoGasto;
	}

	public void setTipoGasto(Catalogo tipoGasto) {
		this.tipoGasto = tipoGasto;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	public double getAbono() {
		return abono;
	}

	public void setAbono(double abono) {
		this.abono = abono;
	}

	public byte getEstatusCorte() {
		return estatusCorte;
	}

	public void setEstatusCorte(byte estatusCorte) {
		this.estatusCorte = estatusCorte;
	}

	public Catalogo getEstatusMovimiento() {
		return estatusMovimiento;
	}

	public void setEstatusMovimiento(Catalogo estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}

	public double getPendiente() {
		return pendiente;
	}

	public void setPendiente(double pendiente) {
		this.pendiente = pendiente;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getMovcruze() {
		return movcruze;
	}

	public void setMovcruze(String movcruze) {
		this.movcruze = movcruze;
	}

}
