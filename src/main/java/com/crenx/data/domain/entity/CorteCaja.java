package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class CorteCaja {
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_corte_caja", unique = true)
	private String idCorteCaja;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_caja", nullable=false, referencedColumnName="id_caja")
	private Caja caja;

	@Column(name = "fecha_corte")
	private Date fechaCorte;

	@Column(name = "fecha_corte_anterior")
	private Date fechaCorteAnterior;

	@Column(name = "saldo_incial")
	private double saldoInicial;
	
	@Column(name = "saldo_final")
	private double saldoFinal;

	@Column(name = "total_cargos")
	private double totalCargos;

	@Column(name = "total_abonos")
	private double totalAbonos;
	
	@Column(name = "diferencia_cargo")
	private double difCargos;
	
	@Column(name = "diferencia_abono")
	private double difAbono;

	@Column(name = "diferencia_total")
	private double difTotal;

	public String getIdCorteCaja() {
		return idCorteCaja;
	}

	public void setIdCorteCaja(String idCorteCaja) {
		this.idCorteCaja = idCorteCaja;
	}

	public Caja getCaja() {
		return caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public Date getFechaCorteAnterior() {
		return fechaCorteAnterior;
	}

	public void setFechaCorteAnterior(Date fechaCorteAnterior) {
		this.fechaCorteAnterior = fechaCorteAnterior;
	}

	public double getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}

	public double getSaldoFinal() {
		return saldoFinal;
	}

	public void setSaldoFinal(double saldoFinal) {
		this.saldoFinal = saldoFinal;
	}

	public double getTotalCargos() {
		return totalCargos;
	}

	public void setTotalCargos(double totalCargos) {
		this.totalCargos = totalCargos;
	}

	public double getTotalAbonos() {
		return totalAbonos;
	}

	public void setTotalAbonos(double totalAbonos) {
		this.totalAbonos = totalAbonos;
	}

	public double getDifCargos() {
		return difCargos;
	}

	public void setDifCargos(double difCargos) {
		this.difCargos = difCargos;
	}

	public double getDifAbono() {
		return difAbono;
	}

	public void setDifAbono(double difAbono) {
		this.difAbono = difAbono;
	}

	public double getDifTotal() {
		return difTotal;
	}

	public void setDifTotal(double difTotal) {
		this.difTotal = difTotal;
	}
	
	

}
