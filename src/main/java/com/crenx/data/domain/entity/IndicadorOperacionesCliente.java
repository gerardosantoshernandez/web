package com.crenx.data.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class IndicadorOperacionesCliente {
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")

	@Column(name = "id_indope", unique = true)
	private String idIndicadorOperaciones;

	@Column(name = "tipo_operacion")
	private String tipoOperacion;
	@Column(name = "ruta")
	private String ruta;
	@Column(name = "gerencia")
	private String gerencia;	
	@Column(name = "region")
	private String region;	
	@Column(name = "division")
	private String division;	
	@Column(name = "id_cliente")
	private String idCliente;
	@Column(name = "nombre_cliente")
	private String nombreCliente;
	@Column(name = "monto")
	private double monto;
	@Column(name = "periodo")
	private Date periodo;
	
	public String getIdIndicadorOperaciones() {
		return idIndicadorOperaciones;
	}
	public void setIdIndicadorOperaciones(String idIndicadorOperaciones) {
		this.idIndicadorOperaciones = idIndicadorOperaciones;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Date getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}
	

}
