package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.TablaAmortizacion;
public class CreditoModelMap extends PropertyMap<Credito, CreditoVO>{
	   @Override
	    protected void configure() {
	        map().setIdUnidadPlazo(source.getUnidadPlazo().getIdCatalogo());
	        map().setIdFrecuenciaCobro(source.getFrecuenciaCobro().getIdCatalogo());
	        map().setIdEstatusCredito(source.getEstatusCredito().getIdCatalogo());	  
	        map().setUnidadPlazo((source.getUnidadPlazo().getNombre()));
	        map().setFrecuenciaCobro((source.getFrecuenciaCobro().getNombre()));
	        map().setProducto(source.getProducto().getNombre());
	        map().setEstatusCredito(source.getEstatusCredito().getNombre());
	        map().setRuta(source.getRuta().getNombre());
	        map().setNombreCliente(source.getNombreCliente());
	        map().setTitularRuta(source.getTitular());
	        map().setIdCoDedudor(source.getCoDeudor().getIdCliente());
	        map().setNombreCoDeudor(source.getNombreCodeudor());
	        map().setPagoAdicional(source.getPagoPendienteAplicar());
	        map().setPagoPendienteAplicar(source.getPagoPendienteAplicar());
	        map().setRolAprobador(source.getRolAprobador());
	        //map().setCalificacion(source.getTotPagos()==0?100:((int)(source.getTotPagoATiempo()/source.getTotPagos())));
	        using(dateToString).map(source.getFechaEmision()).setFechaEmision(null);
	        using(dateToString).map(source.getFechaVencimiento()).setFechaVencimiento(null);
	        using(dateToString).map(source.getFechaUltimoPago()).setFechaUltimoPago(null);
	        using(dateToString).map(source.getFechaUltimaVisita()).setFechaUltimaVisita(null);
	        using(dateToString).map(source.getFechaCancelacion()).setFechaCancelacion(null);
	        map().setMotivoCancelacion(source.getMotivoCancelacion().getNombre());
	        map().setRutaCliente(source.getCliente().getRuta().getNombre());
	        using(getFechaPagoNoPagado).map(source.getPagos()).setFechaPagoNoPagado(null);
	        using(getDiasVencidos).map(source.getPagos()).setDiasAtraso(0);
	        map().setGerencia(source.getRuta().getParent().getNombre());
	        map().setRegion(source.getRuta().getParent().getParent().getNombre());
	        map().setDivision(source.getRuta().getParent().getParent().getParent().getNombre());
	        map().setCveFiscal(source.getCliente().getCveFiscal());
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 
	Converter<List<TablaAmortizacion>, String> getFechaPagoNoPagado = new AbstractConverter<List<TablaAmortizacion>, String>(){
		protected String convert(List<TablaAmortizacion> pagos){
			Date fecha = new Date();
			for (TablaAmortizacion tA : pagos) {
				if(tA.getEstatusPago().getClaveInterna().equals("PAGO_PENDIENTE")){
					if(tA.getFechaVencimiento().before(fecha))
						fecha = tA.getFechaVencimiento();
				}
			}
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(fecha);
		}
	};	 
	
	Converter<List<TablaAmortizacion>, Integer> getDiasVencidos= new AbstractConverter<List<TablaAmortizacion>, Integer>(){
		protected Integer convert(List<TablaAmortizacion> pagos){
			int dias = 0;
			Date fechaActual = new Date();
			Date fecha = new Date();
			for (TablaAmortizacion tA : pagos) {
				if(tA.getEstatusPago().getClaveInterna().equals("PAGO_PENDIENTE")){
					if(tA.getFechaVencimiento().before(fecha))
						fecha = tA.getFechaVencimiento();
				}
			}
			
			if(fechaActual.compareTo(fecha)!=0){//fechas no son iguales
				dias = (int) Math.floor((fechaActual.getTime() - fecha.getTime())/(1000*24*60*60));
			}
			
			return dias;
		}
	};	
		 		 
}