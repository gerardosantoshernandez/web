package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.TablaAmortizacion;
public class TablaAmortizacionModelMap extends PropertyMap<TablaAmortizacion, TablaAmortizacionVO>{
	   @Override
	    protected void configure() {
			map().setAmortizacion(source.getCapital());
			map().setInteres(source.getInteres());
			map().setIvaInteres(source.getIvaInteres());
			map().setNumPago(source.getNoPago());
			map().setEstatusPago(source.getEstatusPago().getNombre());
			map().setPagoFijo(source.getCuota());
			map().setIdPago(source.getPago().getIdPago());
			using(dateToString).map(source.getFechaVencimiento()).setFechaVencimiento(null);
			using(dateToString).map(source.getPago().getFechaPago()).setFechaPago(null);
			
			map().setTipoPago(source.getTipoPago().getNombre());
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 		 
}