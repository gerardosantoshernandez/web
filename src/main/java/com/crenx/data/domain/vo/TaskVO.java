package com.crenx.data.domain.vo;

import java.util.Date;

import org.activiti.engine.impl.persistence.entity.SuspensionState;

public class TaskVO {

	  protected String id;
	  protected int revision;

	  protected String owner;
	  protected String assignee;
	  protected String initialAssignee;	  
	  protected String parentTaskId;
	  
	  protected String name;
	  protected String description;
	  protected Date createTime; // The time when the task has been created
	  protected Date dueDate;
	  protected String category;
	  protected String executionId;
	  protected String processInstanceId;	  
	  protected String processDefinitionId;
	  protected String taskDefinitionKey;
	  protected String formKey;	  
	  protected boolean isDeleted;
	  
	  public TaskVO(){}
	  public TaskVO(String id, String name)
	  {
		  this.id = id;
		  this.name = name;
	  }
	  
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getRevision() {
		return revision;
	}
	public void setRevision(int revision) {
		this.revision = revision;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getInitialAssignee() {
		return initialAssignee;
	}
	public void setInitialAssignee(String initialAssignee) {
		this.initialAssignee = initialAssignee;
	}
	public String getParentTaskId() {
		return parentTaskId;
	}
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getExecutionId() {
		return executionId;
	}
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}
	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}
	public String getFormKey() {
		return formKey;
	}
	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	  
	  
}
