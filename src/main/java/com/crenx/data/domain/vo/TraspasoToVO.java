package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Traspaso;

public class TraspasoToVO extends PropertyMap<Traspaso, TraspasoVO>{

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		map().setCliente(source.getCliente().getNombreCompleto());
		using(dateToString).map(source.getFechaOperacion()).setFechaOperacion(null);
		map().setMotivo(source.getMotivo().getNombre());
		map().setRutaDestino(source.getRutaDestino().getNombre());
		map().setRutaOrigen(source.getRutaOrigen().getNombre());
		map().setRutaActual(source.getCliente().getRuta().getNombre());
		map().setUsuario(source.getUsuario());
		using(dateToString).map(source.getCliente().getCreacion()).setFechaCreacion(null);
		map().setMonto(source.getMonto());
		map().setUsuarioCreador(source.getCliente().getUsuario());
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		protected String convert(Date source) {
			if (source == null)
				return "";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(source);
		}
	};
}
