package com.crenx.data.domain.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Visita;

public class VisitaVOToVisitaModelMapper extends PropertyMap<VisitaVO,Visita>{
	   @Override
	    protected void configure() {
			using(stringToDate).map(source.getFechaVisita()).setFechaVisita(null);
			
	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };

}
