package com.crenx.data.domain.vo;

import java.util.Date;

public class ListaRolVO {

	private String idObjetoSeguridad;
	private String nombre;
	private String descripcion;
	private String llaveNegocio;
	private Date creacion;
	
	
	public String getIdObjetoSeguridad() {
		return idObjetoSeguridad;
	}
	public void setIdObjetoSeguridad(String idObjetoSeguridad) {
		this.idObjetoSeguridad = idObjetoSeguridad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getLlaveNegocio() {
		return llaveNegocio;
	}
	public void setLlaveNegocio(String llaveNegocio) {
		this.llaveNegocio = llaveNegocio;
	}
	
	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}
	
	
}
