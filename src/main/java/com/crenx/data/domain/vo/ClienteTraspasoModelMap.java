package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;

public class ClienteTraspasoModelMap extends PropertyMap<Cliente, ClienteTraspasoVO> {
	@Override
	protected void configure() {
		map().setIdRuta(source.getRuta().getIdOrg());
		map().setRuta(source.getRuta().getNombre());
		using(creditoConverter).map(source.getCreditos()).setCreditos(null);
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		protected String convert(Date source) {
			if (source == null)
				return "";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(source);
		}
	};

	Converter<List<Credito>, List<CreditoTraspasoVO>> creditoConverter = new AbstractConverter<List<Credito>, List<CreditoTraspasoVO>>() {
		protected List<CreditoTraspasoVO> convert(List<Credito> source) {
			final ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new CreditoModelMap());
			List<CreditoTraspasoVO> result = new ArrayList<CreditoTraspasoVO>();
			
			// Ordenamos
			Collections.sort(source, new Comparator<Credito>() {
				@Override
				public int compare(Credito cred1, Credito cred2) {
					return cred1.getFechaEmision().compareTo(cred2.getFechaEmision());
				}
			});
			
			if(source.size()>0){
				Credito selec =  source.get(0);
				CreditoTraspasoVO coVo = mapper.map(selec, CreditoTraspasoVO.class);
				coVo.setEstatusCredito(selec. getEstatusCredito().getNombre());
				coVo.setMonto(selec.getMonto());
				coVo.setRuta(selec.getRuta().getNombre());
				result.add(coVo);
				
			}
			
			return result;
		}
	};

}
