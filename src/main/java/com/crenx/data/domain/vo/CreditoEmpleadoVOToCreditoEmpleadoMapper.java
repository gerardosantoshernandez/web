package com.crenx.data.domain.vo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import com.crenx.data.domain.entity.CreditoEmpleado;

public class CreditoEmpleadoVOToCreditoEmpleadoMapper extends PropertyMap<CreditoEmpleadoVO, CreditoEmpleado>{
	   @Override
	    protected void configure() {
			using(stringToDate).map(source.getFechaEmision()).setFechaEmision(null);
			using(stringToDate).map(source.getFechaUltimoPago()).setFechaUltimoPago(null);

	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };

	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };

}
