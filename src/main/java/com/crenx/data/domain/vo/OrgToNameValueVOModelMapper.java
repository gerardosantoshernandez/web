package com.crenx.data.domain.vo;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Organizacion;

public class OrgToNameValueVOModelMapper extends PropertyMap<Organizacion, NameValueVO>{
	   @Override
	    protected void configure() {
			map().setId(source.getIdOrg());
			map().setName(source.getNombre());
	    }

}
