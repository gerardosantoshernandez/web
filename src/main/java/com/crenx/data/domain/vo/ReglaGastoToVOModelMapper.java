package com.crenx.data.domain.vo;

import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.ReglaGasto;

public class ReglaGastoToVOModelMapper extends PropertyMap<ReglaGasto, ReglaGastoVO>{
	   @Override
	    protected void configure() {
		   map().setIdRol(source.getRol().getIdObjetoSeguridad());
		   map().setNombreRol(source.getRol().getNombre());
		   map().setIdPeriodo(source.getPeriodo().getIdCatalogo());
		   map().setNombrePeriodo(source.getPeriodo().getNombre());
		   map().setIdGasto(source.getGasto().getIdCatalogo());
		   map().setNombreGasto(source.getGasto().getNombre());
		   map().setRequiereCFDI(source.isRequiereCFDI());
	    }
}
