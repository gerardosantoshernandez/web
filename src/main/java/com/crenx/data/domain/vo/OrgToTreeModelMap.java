package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Organizacion;

public class OrgToTreeModelMap extends PropertyMap<Organizacion, OrgVO>{
	   @Override
	    protected void configure() {
			map().setId(source.getIdOrg());
			map().setKey(source.getClave());
			map().setText(source.getNombre());
			map().setIdCalendario(source.getCalendario().getIdCalendario());
			map().setIdTitular(source.getTitular().getIdUsuario());
			map().setIdSuplente(source.getSuplente().getIdUsuario());
			map().setIdStatus(source.getEstatus().getClave());
			map().setIdParent(source.getParent().getIdOrg());
			map().setTitular(source.getTitFullName());
			map().setSuplente(source.getSupFullName());
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 		 
}