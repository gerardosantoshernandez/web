package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;

public class ClienteModelMap extends PropertyMap<Cliente, ClienteVO> {
	@Override
	protected void configure() {
		map().setIdEntidadNacimiento(source.getEntidadNacimiento().getIdCatalogo());
		map().setIdGenero(source.getGenero().getIdCatalogo());
		map().setIdOcupacion(source.getOcupacion().getIdCatalogo());
		map().setIdPaisNacimiento((source.getPaisNacimiento().getIdCatalogo()));
		map().setIdTipoFigura(source.getTipoFigura().getIdCatalogo());
		map().setIdRuta(source.getRuta().getIdOrg());
		map().setRuta(source.getRuta().getNombre());
		map().setIdTipoDocumentoIdentidad(source.getTipoDocId().getIdCatalogo());
		map().setParentId(source.getParent().getIdCliente());
		using(codeudorConverter).map(source.getCodeudores()).setCodeudores(null);
		using(dateToString).map(source.getFechaNacimiento()).setFechaNacimiento(null);
	//	using(creditoConverter).map(source.getCreditos()).setCreditos(null);
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		protected String convert(Date source) {
			if (source == null)
				return "";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(source);
		}
	};

	Converter<List<Cliente>, List<CoDeudorVO>> codeudorConverter = new AbstractConverter<List<Cliente>, List<CoDeudorVO>>() {
		protected List<CoDeudorVO> convert(List<Cliente> source) {
			final ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new CodeudorModelMap());
			List<CoDeudorVO> result = new ArrayList<CoDeudorVO>();
			for (Cliente codeudor : source) {
				CoDeudorVO coVo = mapper.map(codeudor, CoDeudorVO.class);
				coVo.setIdCliente(codeudor.getParent().getIdCliente());
				coVo.setParentId(codeudor.getParent().getIdCliente());
				coVo.setIdCodeudor(codeudor.getIdCliente());
				coVo.setNombreCompleto(codeudor.getNombreCompleto());
				result.add(coVo);
			}
			return result;
		}
	};
	
	/*Converter<List<Credito>, List<CreditoVO>> creditoConverter = new AbstractConverter<List<Credito>, List<CreditoVO>>() {
		protected List<CreditoVO> convert(List<Credito> source) {
			final ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new CreditoModelMap());
			List<CreditoVO> result = new ArrayList<CreditoVO>();
			
			// Ordenamos
			Collections.sort(source, new Comparator<Credito>() {
				@Override
				public int compare(Credito cred1, Credito cred2) {
					return cred1.getFechaEmision().compareTo(cred2.getFechaEmision());
				}
			});
			
			if(source.size()>0){
				Credito selec =  source.get(0);
				CreditoVO coVo = mapper.map(selec, CreditoVO.class);
				coVo.setEstatusCredito(selec. getEstatusCredito().getNombre());
				coVo.setMonto(selec.getMonto());
				
				result.add(coVo);
				
			}
			
			
			
			return result;
		}
	}; */

}
