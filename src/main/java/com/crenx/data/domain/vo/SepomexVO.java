package com.crenx.data.domain.vo;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;

public class SepomexVO {

	private Collection<String> colonias;
	private String municipio;
	private String estado;
	private String pais;
	public Collection<String> getColonias() {
		if (colonias==null)
			colonias = new ArrayList<String>();
		return colonias;
	}
	public void setColonias(Collection<String> colonias) {
		this.colonias = colonias;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
	
}
