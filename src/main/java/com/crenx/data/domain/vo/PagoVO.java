package com.crenx.data.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PagoVO {

	private long id;	
	private String idPago;
	private String noRecibo;
	private String fechaPago;
	private double abono;
	private double comision;
	private String idUsuario;
	private String idDispositivo;
	private String idCredito;
	private String geoPosicion;
	private String idMotivo;
	private String nombreCliente;
	private String nombreProducto;
	private String ruta;
	private String ejecutivo;
	private String estatusCredito;
	private double pagoFijo;
	private double pagoAdicional;
	private CreditoVO credito;
	

	public String getIdPago() {
		return idPago;
	}
	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public double getAbono() {
		return abono;
	}
	public void setAbono(double abono) {
		this.abono = abono;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public String getIdCredito() {
		return idCredito;
	}
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
	public String getNoRecibo() {
		return noRecibo;
	}
	public void setNoRecibo(String noRecibo) {
		this.noRecibo = noRecibo;
	}
	public String getGeoPosicion() {
		return geoPosicion;
	}
	public void setGeoPosicion(String geoPosicion) {
		this.geoPosicion = geoPosicion;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdMotivo() {
		return idMotivo;
	}
	public void setIdMotivo(String idMotivo) {
		this.idMotivo = idMotivo;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public String getEstatusCredito() {
		return estatusCredito;
	}
	public void setEstatusCredito(String estatusCredito) {
		this.estatusCredito = estatusCredito;
	}
	public double getPagoFijo() {
		return pagoFijo;
	}
	public void setPagoFijo(double pagoFijo) {
		this.pagoFijo = pagoFijo;
	}
	/**
	 * @return the credito
	 */
	public CreditoVO getCredito() {
		return credito;
	}
	/**
	 * @param credito the credito to set
	 */
	public void setCredito(CreditoVO credito) {
		this.credito = credito;
	}
	public double getPagoAdicional() {
		return pagoAdicional;
	}
	public void setPagoAdicional(double pagoAdicional) {
		this.pagoAdicional = pagoAdicional;
	}	
	
	
}
