package com.crenx.data.domain.vo;

import java.util.List;

public class CreditosUnifVO {
	private List<CreditoVO> creditos;
	private String titular;
	CreditoVO destino;
	
	public List<CreditoVO> getCreditos() {
		return creditos;
	}
	public void setCreditos(List<CreditoVO> creditos) {
		this.creditos = creditos;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public CreditoVO getDestino() {
		return destino;
	}
	public void setDestino(CreditoVO destino) {
		this.destino = destino;
	}
	
	
	

}
