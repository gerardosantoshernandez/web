package com.crenx.data.domain.vo;

public class NominaPeriodoVO {
	private String division;
	private String region;
	private String gerencia;
	private String ruta;
	private long semana;
	private long quincena;
	private String cveEmpleado;
	private long semanas;
	private String nombreCompleto;
	private double cobranzaTotal;
	private double factorNormalidad;
	private double factorCobranza;
	private double baseCobranza;
	private double baseNormalidad;
	private double baseClientesNuevos;
	private double cobranza;
	private double normalidad;
	private double clientesNuevos;
	private double totalNomina;
	private double baseNomina;
	private double baseAsimilados;
	private double descuentoPrestamo;
	private double descuentoMoto;
	
	public NominaPeriodoVO() {
		super();
	}
	
	
	public NominaPeriodoVO(String division, String region, String gerencia, String ruta, long semana,
			String cveEmpleado, long semanas, String nombreCompleto, double cobranzaTotal, double factorNormalidad,
			double factorCobranza, double baseCobranza, double baseNormalidad, double baseClientesNuevos,
			double cobranza, double normalidad, double clientesNuevos, double totalNomina, double baseNomina,
			double baseAsimilados, double descuentoPrestamo, double descuentoMoto, long quincena) {
		super();
		this.division = division;
		this.region = region;
		this.gerencia = gerencia;
		this.ruta = ruta;
		this.semana = semana;
		this.cveEmpleado = cveEmpleado;
		this.semanas = semanas;
		this.nombreCompleto = nombreCompleto;
		this.cobranzaTotal = cobranzaTotal;
		this.factorNormalidad = factorNormalidad;
		this.factorCobranza = factorCobranza;
		this.baseCobranza = baseCobranza;
		this.baseNormalidad = baseNormalidad;
		this.baseClientesNuevos = baseClientesNuevos;
		this.cobranza = cobranza;
		this.normalidad = normalidad;
		this.clientesNuevos = clientesNuevos;
		this.totalNomina = totalNomina;
		this.baseNomina = baseNomina;
		this.baseAsimilados = baseAsimilados;
		this.descuentoPrestamo = descuentoPrestamo;
		this.descuentoMoto = descuentoMoto;
		this.quincena = quincena;
	}


	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public long getSemana() {
		return semana;
	}
	public void setSemana(long semana) {
		this.semana = semana;
	}
	public String getCveEmpleado() {
		return cveEmpleado;
	}
	public void setCveEmpleado(String cveEmpleado) {
		this.cveEmpleado = cveEmpleado;
	}
	public long getSemanas() {
		return semanas;
	}
	public void setSemanas(long semanas) {
		this.semanas = semanas;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public double getCobranzaTotal() {
		return cobranzaTotal;
	}
	public void setCobranzaTotal(double cobranzaTotal) {
		this.cobranzaTotal = cobranzaTotal;
	}
	public double getFactorNormalidad() {
		return factorNormalidad;
	}
	public void setFactorNormalidad(double factorNormalidad) {
		this.factorNormalidad = factorNormalidad;
	}
	public double getBaseCobranza() {
		return baseCobranza;
	}
	public void setBaseCobranza(double baseCobranza) {
		this.baseCobranza = baseCobranza;
	}
	public double getBaseNormalidad() {
		return baseNormalidad;
	}
	public void setBaseNormalidad(double baseNormalidad) {
		this.baseNormalidad = baseNormalidad;
	}
	public double getBaseClientesNuevos() {
		return baseClientesNuevos;
	}
	public void setBaseClientesNuevos(double baseClientesNuevos) {
		this.baseClientesNuevos = baseClientesNuevos;
	}
	public double getCobranza() {
		return cobranza;
	}
	public void setCobranza(double cobranza) {
		this.cobranza = cobranza;
	}
	public double getNormalidad() {
		return normalidad;
	}
	public void setNormalidad(double normalidad) {
		this.normalidad = normalidad;
	}
	public double getClientesNuevos() {
		return clientesNuevos;
	}
	public void setClientesNuevos(double clientesNuevos) {
		this.clientesNuevos = clientesNuevos;
	}
	public double getTotalNomina() {
		return totalNomina;
	}
	public void setTotalNomina(double totalNomina) {
		this.totalNomina = totalNomina;
	}
	public double getBaseNomina() {
		return baseNomina;
	}
	public void setBaseNomina(double baseNomina) {
		this.baseNomina = baseNomina;
	}
	public double getBaseAsimilados() {
		return baseAsimilados;
	}
	public void setBaseAsimilados(double baseAsimilados) {
		this.baseAsimilados = baseAsimilados;
	}
	public double getDescuentoPrestamo() {
		return descuentoPrestamo;
	}
	public void setDescuentoPrestamo(double descuentoPrestamo) {
		this.descuentoPrestamo = descuentoPrestamo;
	}
	public double getDescuentoMoto() {
		return descuentoMoto;
	}
	public void setDescuentoMoto(double descuentoMoto) {
		this.descuentoMoto = descuentoMoto;
	}
	public double getFactorCobranza() {
		return factorCobranza;
	}
	public void setFactorCobranza(double factorCobranza) {
		this.factorCobranza = factorCobranza;
	}
	public long getQuincena() {
		return quincena;
	}


	public void setQuincena(long quincena) {
		this.quincena = quincena;
	}

}
