package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.MovimientoCaja;

public class MovCajaGerenciaModelMap extends PropertyMap<MovimientoCaja, MovimientoCajaVO> {
	   @Override
	    protected void configure() {
			using(dateToString).map(source.getFechaEmision()).setFechaEmision(null);
			using(dateToString).map(source.getFechaSistema()).setFechaSistema(null);
			map().setIdOrg(null);
			map().setNombreOrg(source.getRuta().getNombre());
			map().setIdTipoMov(source.getTipoMovimiento().getIdCatalogo());
			map().setTipoMov(source.getTipoMovimiento().getNombre());
			map().setIdTipoOperacion(source.getTipoOperacion().getIdCatalogo());
			map().setTipoOperacion(source.getTipoOperacion().getNombre());
			map().setIdTipoGasto(source.getTipoGasto().getIdCatalogo());
			map().setTipoGasto(source.getTipoGasto().getNombre());
			map().setRuta(null);
			map().setGerencia(source.getRuta().getNombre());
			map().setRegion(source.getRuta().getParent().getNombre()); //
			map().setDivision(source.getRuta().getParent().getParent().getNombre());//
			map().setObservaciones(source.getObservaciones());
			map().setEstatusMovimiento(source.getEstatusMovimiento().getNombre());
			
	    }
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				return df.format(source);
		   }
		 };
}
