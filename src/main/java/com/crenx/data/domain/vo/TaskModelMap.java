package com.crenx.data.domain.vo;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

public class TaskModelMap extends PropertyMap<TaskEntity, TaskVO>{
	   @Override
	    protected void configure() {
		   map().setTaskDefinitionKey(source.getTaskDefinitionKey());
		   map().setExecutionId(source.getExecutionId());
		   map().setProcessInstanceId(source.getProcessInstanceId());
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };
		 		 
}