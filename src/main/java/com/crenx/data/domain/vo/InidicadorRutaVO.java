package com.crenx.data.domain.vo;

import java.util.Date;

public class InidicadorRutaVO {
	String idOrg;
	String nombre;
	double normalidad;
	double cobranza;
	Date fecha;
	long dateMilliseconds;
	
	public InidicadorRutaVO(){}
	
	public InidicadorRutaVO(String idOrg, String nombre, long normalidad, double cobranza)
	{
		this.idOrg = idOrg;
		this.nombre=nombre;
		this.normalidad=normalidad;
		this.cobranza = cobranza;				
	}
	public InidicadorRutaVO(Date fecha, double normalidad, double cobranza)
	{
		this.fecha = fecha;
		this.normalidad=normalidad;
		this.cobranza = cobranza;	
		this.dateMilliseconds = fecha.getTime();
	}

	public String getIdOrg() {
		return idOrg;
	}
	public void setIdOrg(String idOrg) {
		this.idOrg = idOrg;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getNormalidad() {
		return normalidad;
	}

	public void setNormalidad(double normalidad) {
		this.normalidad = normalidad;
	}

	public double getCobranza() {
		return cobranza;
	}
	public void setCobranza(double cobranza) {
		this.cobranza = cobranza;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public long getDateMilliseconds() {
		return dateMilliseconds;
	}

	public void setDateMilliseconds(long dateMilliseconds) {
		this.dateMilliseconds = dateMilliseconds;
	}
	
	
}
