package com.crenx.data.domain.vo;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import com.crenx.data.domain.entity.Empleado;

public class EmpleadoToVOModelMapper extends PropertyMap<Empleado, EmpleadoVO>{
	   @Override
	    protected void configure() {
		   map().setNombreCompleto(source.getNombreCompleto());
		   using(dateToString).map(source.getFechaIngreso()).setFechaIngreso(null);
		   using(dateToString).map(source.getFechaBaja()).setFechaBaja(null);

	    }
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };

}
