package com.crenx.data.domain.vo;

import java.util.List;

public class ClienteTraspasoVO {

	private long id;	
	private String idCliente;	
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String idRuta;	
	private String ruta;
	private boolean traspasar = false;
	private List<CreditoTraspasoVO> creditos;
	private String usuario;
	
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(String idRuta) {
		this.idRuta = idRuta;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	public boolean isTraspasar() {
		return traspasar;
	}
	public void setTraspasar(boolean traspasar) {
		this.traspasar = traspasar;
	}
	public List<CreditoTraspasoVO> getCreditos() {
		return creditos;
	}
	public void setCreditos(List<CreditoTraspasoVO> creditos) {
		this.creditos = creditos;
	}
	
	
}
