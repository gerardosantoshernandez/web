package com.crenx.data.domain.vo;

import java.util.Date;


public class ProductoVO {
	private String idProducto;
	private String idFamiliaProducto;
	private String familiaProducto;
	private String nombre;
	private String descripcion;
	private double minimo;
	private double maximo;
	private short plazo;
	private double tasa;
	private double pagoXmil;
	private String idUnidadPlazo;
	private String unidadPlazo;
	private String idFrecuenciaCobro;
	private String frecuenciaCobro;
	private double comision;
	private String fechaFinOperaciones;
	private String fechaInicioOperaciones;
	private boolean permiteEditar=false;
	private boolean interesFijo=false;
	private double cat=0.0;
	private byte estatus;
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getIdFamiliaProducto() {
		return idFamiliaProducto;
	}
	public void setIdFamiliaProducto(String idFamiliaProducto) {
		this.idFamiliaProducto = idFamiliaProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getMinimo() {
		return minimo;
	}
	public void setMinimo(double minimo) {
		this.minimo = minimo;
	}
	public double getMaximo() {
		return maximo;
	}
	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}
	public short getPlazo() {
		return plazo;
	}
	public void setPlazo(short plazo) {
		this.plazo = plazo;
	}
	public double getTasa() {
		return tasa;
	}
	public void setTasa(double tasa) {
		this.tasa = tasa;
	}
	public double getPagoXmil() {
		return pagoXmil;
	}
	public void setPagoXmil(double pagoXmil) {
		this.pagoXmil = pagoXmil;
	}
	public String getIdUnidadPlazo() {
		return idUnidadPlazo;
	}
	public void setIdUnidadPlazo(String idUnidadPlazo) {
		this.idUnidadPlazo = idUnidadPlazo;
	}
	public String getIdFrecuenciaCobro() {
		return idFrecuenciaCobro;
	}
	public void setIdFrecuenciaCobro(String idFrecuenciaCobro) {
		this.idFrecuenciaCobro = idFrecuenciaCobro;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getFechaFinOperaciones() {
		return fechaFinOperaciones;
	}
	public void setFechaFinOperaciones(String fechaFinOperaciones) {
		this.fechaFinOperaciones = fechaFinOperaciones;
	}
	public String getFechaInicioOperaciones() {
		return fechaInicioOperaciones;
	}
	public void setFechaInicioOperaciones(String fechaInicioOperaciones) {
		this.fechaInicioOperaciones = fechaInicioOperaciones;
	}
	public boolean isPermiteEditar() {
		return permiteEditar;
	}
	public void setPermiteEditar(boolean permiteEditar) {
		this.permiteEditar = permiteEditar;
	}
	public double getCat() {
		return cat;
	}
	public void setCat(double cat) {
		this.cat = cat;
	}
	public byte getEstatus() {
		return estatus;
	}
	public void setEstatus(byte estatus) {
		this.estatus = estatus;
	}
	public String getFamiliaProducto() {
		return familiaProducto;
	}
	public void setFamiliaProducto(String familiaProducto) {
		this.familiaProducto = familiaProducto;
	}
	public String getUnidadPlazo() {
		return unidadPlazo;
	}
	public void setUnidadPlazo(String unidadPlazo) {
		this.unidadPlazo = unidadPlazo;
	}
	public String getFrecuenciaCobro() {
		return frecuenciaCobro;
	}
	public void setFrecuenciaCobro(String frecuenciaCobro) {
		this.frecuenciaCobro = frecuenciaCobro;
	}
	public boolean isInteresFijo() {
		return interesFijo;
	}
	public void setInteresFijo(boolean interesFijo) {
		this.interesFijo = interesFijo;
	}

	
}
