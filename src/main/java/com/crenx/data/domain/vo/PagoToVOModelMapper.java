package com.crenx.data.domain.vo;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Pago;

public class PagoToVOModelMapper extends PropertyMap<Pago, PagoVO> {
	   @Override
	    protected void configure() {
			map().setNombreCliente(source.getCredito().getNombreCliente());
			map().setRuta(source.getCredito().getCliente().getRuta().getNombre());
			map().setEjecutivo(source.getCredito().getCliente().getRuta().getTitFullName());
			map().setEstatusCredito(source.getCredito().getEstatusCredito().getNombre());
			map().setPagoAdicional(source.getPagoAdicional());
			map().setPagoFijo(source.getCredito().getPago());
			map().setNombreProducto(source.getCredito().getProducto().getNombre());
			map().getCredito().setNombreCliente(source.getCredito().getNombreCliente());
			map().getCredito().setIdFrecuenciaCobro(source.getCredito().getFrecuenciaCobro().getIdCatalogo());
			map().getCredito().setIdUnidadPlazo(source.getCredito().getUnidadPlazo().getIdCatalogo());
			map().getCredito().setIdEstatusCredito(source.getCredito().getEstatusCredito().getIdCatalogo());
			using(dateToString).map(source.getFechaPago()).setFechaPago(null);
	    }
	   
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };

}