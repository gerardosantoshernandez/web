package com.crenx.data.domain.vo;

import java.util.List;

public class TraspasoClienteVO {

	String idRutaDestino;
	List<ClienteTraspasoVO> clientes;
	String motivo;
	String fecha;
	
	public String getIdRutaDestino() {
		return idRutaDestino;
	}
	public void setIdRutaDestino(String idRutaDestino) {
		this.idRutaDestino = idRutaDestino;
	}

	public List<ClienteTraspasoVO> getClientes() {
		return clientes;
	}
	public void setClientes(List<ClienteTraspasoVO> clientes) {
		this.clientes = clientes;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
}
