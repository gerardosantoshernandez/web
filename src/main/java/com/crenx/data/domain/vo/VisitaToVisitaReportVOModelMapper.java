package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Visita;

public class VisitaToVisitaReportVOModelMapper  extends PropertyMap<Visita,VisitaVO>{

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		map().setIdVisita(source.getIdVisita());
		using(dateToString).map(source.getFechaVisita()).setFechaVisita(null);
		map().setGeoPosicion(source.getGeoPosicion());
		map().setIdCliente(String.valueOf(source.getIdCliente()));		
		map().setIdCredito(String.valueOf(source.getCredito().getId()));
		map().setCredito(source.getCredito().getIdCredito());
		map().setIdDispositivo(source.getIdDispositivo());
		map().setIdUsuario(source.getIdUsuario());
		map().setNombreCliente(source.getNombreCliente());
		map().setIdMotivo(source.getMotivo().getNombre());
		map().setIdRuta(source.getRuta().getNombre());
		map().setIdOrg(source.getIdOrg().getNombre());
		map().setRutaCliente(source.getCredito().getCliente().getRuta().getNombre());
		
		
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				return df.format(source);
		   }
	};
}
