package com.crenx.data.domain.vo;

import java.util.List;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.TablaAmortizacion;
public class EstadoCuentaVO {
	

	private List<Cliente> cliente;
	private List<Credito> credito;
	private List<Producto> productos;
	private List<Pago> pagos;
	private List<MovimientoCaja> movimientoCaja;
	private List<TablaAmortizacion> tablaAmortizacion;
	
	private float SumaIVA;
	private float SumaComisionesCobradas;
	private double diasPeriodo; 
	private float totalIntereses;
	private float interesesMoratoriosCargados;
	private float saldoVencido;
	
	
	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}
	
	public List<Producto> getProductos() {
		return productos;
	}
	
	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public List<Credito> getCredito() {
		return credito;
	}

	public void setCredito(List<Credito> credito) {
		this.credito = credito;
	}

	public List<TablaAmortizacion> getTablaAmortizacion() {
		return tablaAmortizacion;
	}

	public void setTablaAmortizacion(List<TablaAmortizacion> tablaAmortizacion) {
		this.tablaAmortizacion = tablaAmortizacion;
	}

	public List<MovimientoCaja> getMovimientoCaja() {
		return movimientoCaja;
	}

	public void setMovimientoCaja(List<MovimientoCaja> movimientoCaja) {
		this.movimientoCaja = movimientoCaja;
	}

	public float getSumaIVA() {
		return SumaIVA;
	}

	public void setSumaIVA(float sumaIVA) {
		SumaIVA = sumaIVA;
	}

	public float getSumaComisionesCobradas() {
		return SumaComisionesCobradas;
	}

	public void setSumaComisionesCobradas(float sumaComisionesCobradas) {
		SumaComisionesCobradas = sumaComisionesCobradas;
	}

	public float getTotalIntereses() {
		return totalIntereses;
	}

	public void setTotalIntereses(float totalIntereses) {
		this.totalIntereses = totalIntereses;
	}

	public double getDiasPeriodo() {
		return diasPeriodo;
	}

	public void setDiasPeriodo(double diasPeriodo) {
		this.diasPeriodo = diasPeriodo;
	}

	public float getInteresesMoratoriosCargados() {
		return interesesMoratoriosCargados;
	}

	public void setInteresesMoratoriosCargados(float interesesMoratoriosCargados) {
		this.interesesMoratoriosCargados = interesesMoratoriosCargados;
	}

	public float getSaldoVencido() {
		return saldoVencido;
	}

	public void setSaldoVencido(float saldoVencido) {
		this.saldoVencido = saldoVencido;
	}
	
}
