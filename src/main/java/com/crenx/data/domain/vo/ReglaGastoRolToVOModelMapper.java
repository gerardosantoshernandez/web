package com.crenx.data.domain.vo;
import org.modelmapper.PropertyMap;
import com.crenx.data.domain.entity.ReglaGastoRol;

public class ReglaGastoRolToVOModelMapper extends PropertyMap<ReglaGastoRol, ReglaGastoRolVO>{
	   @Override
	    protected void configure() {
		   map().setIdGasto(source.getGasto().getIdCatalogo());
		   map().setNombreGasto(source.getGasto().getNombre());
		   map().setIdRol(source.getRol().getIdObjetoSeguridad());
		   map().setNombreRol(source.getRol().getNombre());
	    }
}