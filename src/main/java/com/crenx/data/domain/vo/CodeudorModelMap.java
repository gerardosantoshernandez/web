package com.crenx.data.domain.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Cliente;

public class CodeudorModelMap extends PropertyMap<Cliente, CoDeudorVO> {
	@Override
	protected void configure() {
		map().setIdEntidadNacimiento(source.getEntidadNacimiento().getIdCatalogo());
		map().setIdGenero(source.getGenero().getIdCatalogo());
		map().setIdOcupacion(source.getOcupacion().getIdCatalogo());
		map().setIdPaisNacimiento((source.getPaisNacimiento().getIdCatalogo()));
		map().setIdTipoFigura(source.getTipoFigura().getIdCatalogo());
		map().setIdRuta(source.getRuta().getIdOrg());
		map().setRuta(source.getRuta().getNombre());
		map().setIdTipoDocumentoIdentidad(source.getTipoDocId().getIdCatalogo());
		map().setParentId(source.getParent().getIdCliente());		
		using(dateToString).map(source.getFechaNacimiento()).setFechaNacimiento(null);
	}

	Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		protected String convert(Date source) {
			if (source == null)
				return "";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(source);
		}
	};
}
