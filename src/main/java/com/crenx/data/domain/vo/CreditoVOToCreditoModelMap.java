package com.crenx.data.domain.vo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Credito;

public class CreditoVOToCreditoModelMap extends PropertyMap<CreditoVO, Credito> {
	   @Override
	    protected void configure() {
		   map().setId(source.getId());
			using(stringToDate).map(source.getFechaEmision()).setFechaEmision(null);
			using(stringToDate).map(source.getFechaVencimiento()).setFechaVencimiento(null);
			using(stringToDate).map(source.getFechaCancelacion()).setFechaCancelacion(null);
			using(stringToDate).map(source.getFechaUltimoPago()).setFechaUltimoPago(null);
	    }
	   Converter<String, Date> stringToDate = new AbstractConverter<String, Date>() {
		   protected Date convert(String source) {
			   if (source==null)
				   return null;
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					return df.parse(source);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
					return null;
				}
		   }
		 };
		 
		 
}
