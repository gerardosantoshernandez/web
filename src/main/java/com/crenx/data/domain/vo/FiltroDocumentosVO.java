package com.crenx.data.domain.vo;

import com.crenx.business.common.FilterElement;
import com.crenx.util.Constantes;

/**
 * 
 * Contiene datos que son filtros necesarios para la obtenciaón de un determinado conjunto de documentos
 * @author JCHR
 *
 */
public class FiltroDocumentosVO {

	@FilterElement(name = Constantes.IDCREDITIO)
	private String idCredito;
	@FilterElement(name = Constantes.IDCLIENTE)
	private String idCliente;
	@FilterElement(name = Constantes.IDGASTO)
	private String idMovCajaGasto;
	@FilterElement(name = Constantes.IDORG)
	private String idMovCajaTrasferencia;
	@FilterElement(name = Constantes.TIPO_DOCUMENTO)
	private String idTipoDocumento;
	@FilterElement(name = Constantes.IDEMPLEADO)
	private String idEmpleado;
	/**
	 * @return the idCredito
	 */
	public String getIdCredito() {
		return idCredito;
	}
	/**
	 * @param idCredito the idCredito to set
	 */
	public void setIdCredito(String idCredito) {
		this.idCredito = idCredito;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return the idMovCajaGasto
	 */
	public String getIdMovCajaGasto() {
		return idMovCajaGasto;
	}
	/**
	 * @param idMovCajaGasto the idMovCajaGasto to set
	 */
	public void setIdMovCajaGasto(String idMovCajaGasto) {
		this.idMovCajaGasto = idMovCajaGasto;
	}
	/**
	 * @return the idMovCajaTrasferencia
	 */
	public String getIdMovCajaTrasferencia() {
		return idMovCajaTrasferencia;
	}
	/**
	 * @param idMovCajaTrasferencia the idMovCajaTrasferencia to set
	 */
	public void setIdMovCajaTrasferencia(String idMovCajaTrasferencia) {
		this.idMovCajaTrasferencia = idMovCajaTrasferencia;
	}
	/**
	 * @return the idTipoDocumento
	 */
	public String getIdTipoDocumento() {
		return idTipoDocumento;
	}
	/**
	 * @param idTipoDocumento the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(String idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}
	/**
	 * @return the idEmpleado
	 */
	public String getIdEmpleado() {
		return idEmpleado;
	}
	/**
	 * @param idEmpleado the idEmpleado to set
	 */
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	
	
	
}
