package com.crenx.data.domain.vo;
import java.util.List;

public class FiltrosProductoVO {
	private String nombre;
	private List<String> familias;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getFamilias() {
		return familias;
	}
	public void setFamilias(List<String> familias) {
		this.familias = familias;
	}

	
}
