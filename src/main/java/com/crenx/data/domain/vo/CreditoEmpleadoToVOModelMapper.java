package com.crenx.data.domain.vo;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import com.crenx.data.domain.entity.CreditoEmpleado;

public class CreditoEmpleadoToVOModelMapper extends PropertyMap<CreditoEmpleado, CreditoEmpleadoVO>{
	   @Override
	    protected void configure() {
		   map().setIdEmpleado(source.getEmpleado().getIdEmpleado());
		   map().setNombreEmpleado(source.getNombreEmpleado());
		   map().setIdEstatusCredito(source.getEstatusCredito().getIdCatalogo());
		   map().setNombreEstatusCredito(source.getEstatusCredito().getNombre());
		   map().setIdTipoCredito(source.getTipoCredito().getIdCatalogo());
		   map().setNombreTipoCredito(source.getTipoCredito().getNombre());
			using(dateToString).map(source.getFechaEmision()).setFechaEmision(null);
			using(dateToString).map(source.getFechaUltimoPago()).setFechaUltimoPago(null);

	    }
	   Converter<Date, String> dateToString = new AbstractConverter<Date, String>() {
		   protected String convert(Date source) {
			   if (source==null)
				   return "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				return df.format(source);
		   }
		 };

}
