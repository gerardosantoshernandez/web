package com.crenx.data.domain.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;

public class ClienteToClienteRecompraVOModelMap extends PropertyMap<Credito,ClienteRecompraVO>{
	   @Override
	    protected void configure() {
		   map().setNombreCompleto(source.getCliente().getNombreCompleto());
	//	   using(creditoConverter).map(source.getCreditos().setCreditos(null);
		   map().setRuta(source.getRuta().getNombre());
	    }
	   Converter<List<Credito>, List<CreditoVO>> creditoConverter = new AbstractConverter<List<Credito>, List<CreditoVO>>() {
			protected List<CreditoVO> convert(List<Credito> source) {
				final ModelMapper mapper = new ModelMapper();
				mapper.addMappings(new CreditoModelMap());
				List<CreditoVO> result = new ArrayList<CreditoVO>();
				
				
				if(source.size()>0){
					Credito selec =  source.get(0);
					CreditoVO coVo = mapper.map(selec, CreditoVO.class);
					coVo.setEstatusCredito(selec. getEstatusCredito().getNombre());
					coVo.setMonto(selec.getMonto());
					coVo.setRuta(selec.getRuta().getNombre());
					result.add(coVo);
					
				}
				
				return result;
			}
		};

	}
