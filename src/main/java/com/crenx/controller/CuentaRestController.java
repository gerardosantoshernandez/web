	package com.crenx.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.Empleado;
import com.crenx.data.domain.entity.ObjetoSeguridad;
import com.crenx.data.domain.entity.ObjetoSeguridadRelaciones;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.EmpleadoRepository;
import com.crenx.data.domain.repository.FiguraRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRelacionesRepository;
import com.crenx.data.domain.repository.ObjetoSeguridadRepository;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.data.domain.vo.CatalogoListaVO;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CrearObjSecVO;
import com.crenx.data.domain.vo.CrearUserVO;
import com.crenx.data.domain.vo.CrearUsuarioVOToEmpleadoMapper;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.EmpleadoToCrearUsrVOMapper;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltroUsuarioVO;
import com.crenx.data.domain.vo.ListaRolVO;
import com.crenx.data.domain.vo.ListaUsuarioVO;
import com.crenx.security.UserAuthentication;
import com.crenx.services.CatalogoServices;
import com.crenx.services.SeguridadServices;
import com.crenx.services.UtilityServices;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@RestController
@RequestMapping("/api/cuenta")
@Transactional
public class CuentaRestController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private CatalogoRepository catRepository;
	@Autowired
	private FiguraRepository figRepository;
	@Autowired
	EmpleadoRepository emplRepository;
	@Autowired
	ObjetoSeguridadRepository objSegRepository;
	@Autowired
	DomicilioRepository domRepository;
	@Autowired
	CatalogoServices catServices;
	@Autowired
	private UtilityServices utilidades;
	
	@Autowired
	private ObjetoSeguridadRepository objetoSeguridadRepository;
	@Autowired
	private SeguridadServices secServices;
	@Autowired 
	ObjetoSeguridadRelacionesRepository objSecRelRepository;
	

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getUsuarios(Principal principal,@RequestBody FiltroUsuarioVO filtros) {
		return new ResponseEntity<>(secServices.filtraUsuarios(filtros), HttpStatus.CREATED);
	}

	@RequestMapping(value="empleados", method = RequestMethod.POST)
	ResponseEntity<?> getEmpleados(Principal principal,@RequestBody FiltroUsuarioVO filtros) {
		return new ResponseEntity<>(secServices.getEmpleados(filtros), HttpStatus.CREATED);
	}

	@RequestMapping(value="resetPassword", method = RequestMethod.POST)
	ResponseEntity<?> resetPassword(Principal principal, @RequestBody CrearUserVO usuario) {
		secServices.resetPassword(usuario.getIdUsuario(), usuario.getContrasena());
		return new ResponseEntity<>(new CrearUserVO(), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="tipodocumento", method = RequestMethod.GET)
	Collection<CatalogoListaVO> createUsuario() throws Exception {
		Catalogo resultado = catRepository.findByClaveInterna("CAT_TIP_DOC_ID");
		if(resultado!=null){
			List<Catalogo> hijosCatalogo = catRepository.findByParentId(resultado.getIdCatalogo());
			ModelMapper mapper = new ModelMapper();
			Type listType = new TypeToken<List<CatalogoListaVO>>() {}.getType();
			List<CatalogoListaVO> resultadoFinal = mapper.map(hijosCatalogo, listType);
			return resultadoFinal;
		}
		return null;
	}
	
	//TODO Implementar transacción
	@RequestMapping(value="create", method = RequestMethod.POST)
	ResponseEntity<?> createUsuario(@RequestBody CrearUserVO usuario) throws Exception {		
		try
		{
			usuario = secServices.saveUser(usuario);
			return new ResponseEntity<>(usuario, HttpStatus.CREATED);			
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}
	}
	
	//Generar RFC
	@RequestMapping(value="getRFC", method = RequestMethod.POST)
	ResponseEntity<?> getRFC(@RequestBody CrearUserVO usuario){
		String apaterno = usuario.getApellidoPaterno();
		String amaterno = usuario.getApellidoMaterno();
		String nombre = usuario.getNombre();
		String fechaNacimiento = usuario.getFechaNacimiento();
		String rfc = utilidades.validateRFC(apaterno, amaterno, nombre, fechaNacimiento);
		Object objRFC = new Object(){
			public String valor = rfc;
		};
		return  new ResponseEntity<>(objRFC,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="getCURP", method = RequestMethod.POST)
	ResponseEntity<?> getCURP(@RequestBody CrearUserVO usuario){
		String apaterno = usuario.getApellidoPaterno();
		String amaterno = usuario.getApellidoMaterno();
		String nombre = usuario.getNombre();
		String fechaNacimiento = usuario.getFechaNacimiento();
		String genero = usuario.getIdGenero();
		String entidad = usuario.getIdEntidadNacimiento();
		
		String curp = utilidades.validateCURP(apaterno, amaterno, nombre, fechaNacimiento, genero, entidad);
		Object objCURP = new Object(){
			public String valor = curp;
		};
		return  new ResponseEntity<>(objCURP,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="objetosseguridad", method = RequestMethod.POST)
	Collection<ListaRolVO> getObjSeguridad(@RequestBody String tipoObjeto) {
		ModelMapper mapper = new ModelMapper();
		Collection<ObjetoSeguridad> usuarios = Lists.newArrayList(this.objetoSeguridadRepository.findByTipoObje(tipoObjeto));
		Type listType = new TypeToken<List<ListaRolVO>>() {}.getType();
		List<ListaRolVO> listaRoles = mapper.map(usuarios, listType);
		return listaRoles;
	}
	
	
	@RequestMapping(value = "detailobjsec", method = RequestMethod.POST)
	CrearObjSecVO getSO(@RequestBody String objSecIdId) {
		ModelMapper mapper = new ModelMapper();
		ObjetoSeguridad rolDB = this.objetoSeguridadRepository.findOne(objSecIdId);
		CrearObjSecVO rol = mapper.map(rolDB, CrearObjSecVO.class);
		List<CrearObjSecVO> permisos = Lists.newArrayList(secServices.getChildren(objSecIdId));
		rol.setPermissions(permisos);
		return rol;
	}

	@RequestMapping(value = "detailuser/{userId}", method = RequestMethod.GET)
	ResponseEntity<?> getUserData(@PathVariable String userId) throws Exception {
		try
		{
			Usuario usuarioDB = usuarioRepository.findOne(userId);
			Empleado empDB =emplRepository.findByNombreUsuario(usuarioDB.getNombreUsuario());
			if (empDB==null)
				throw new Exception("No existe el usuario");
			ModelMapper mapperUsr = new ModelMapper();	
			CrearUserVO usurioVO = mapperUsr.map(usuarioDB, CrearUserVO.class);
			mapperUsr.addMappings(new EmpleadoToCrearUsrVOMapper());
			mapperUsr.map(empDB, usurioVO);
			
			usurioVO.setActivo(usuarioDB.getEstatusActual().getClaveInterna().equals("ESTATUS_ACTIVO"));			
			return new ResponseEntity<>(usurioVO, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}
	
	
/*
	@RequestMapping(value="detailrol", method = RequestMethod.POST)
	CrearRolVO createRol(@RequestBody CrearRolVO rol) {
		ModelMapper mapper = new ModelMapper();
		ObjetoSeguridad rolDB = mapper.map(rol, ObjetoSeguridad.class);
		rolDB.setTipoObje("SEC_ROLE");
		rolDB.setCreacion(new Date());
		objetoSeguridadRepository.save(rolDB);
		return rol;
	}
*/
	@RequestMapping(value="saveobjsec", method = RequestMethod.POST)
	CrearObjSecVO createObjSec(@RequestBody CrearObjSecVO objSec) {
		Catalogo estatus = catServices.getCatalogoCveInterna("ESTATUS_ACTIVO");
		ModelMapper mapper = new ModelMapper();
		ObjetoSeguridad objSecDB = mapper.map(objSec, ObjetoSeguridad.class);
		objSecDB.setCreacion(new Date());
		objSecDB.setEstatusActual(estatus);
		objetoSeguridadRepository.save(objSecDB);
		objSec.setIdObjetoSeguridad(objSecDB.getIdObjetoSeguridad());
		return objSec;
	}

	@RequestMapping(value="addpermissionrol", method = RequestMethod.POST)
	ResponseEntity<?>  addObjSecRole(@RequestBody CrearObjSecVO objSec) {
		return new ResponseEntity<>(secServices.agregaPermisoRol(objSec), HttpStatus.CREATED);
	}
	

	@RequestMapping(value="removerelation", method = RequestMethod.POST)
	ResponseEntity<?> removeRelation(@RequestBody String relationId) {
		secServices.removeRelation(relationId);
		return new ResponseEntity<>(new CrearObjSecVO(), HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value="permisos", method = RequestMethod.POST)
	ResponseEntity<?> getPermisos(@RequestBody String objSecId) {		
		Collection <CrearObjSecVO> children = secServices.getChildren(objSecId);
		return new ResponseEntity<>(children, HttpStatus.CREATED);
	}
	@RequestMapping(value="permisos", method = RequestMethod.GET)
	ResponseEntity<?> getPermisos() {		
		Collection <CrearObjSecVO> children = secServices.getChildren(null);
		return new ResponseEntity<>(children, HttpStatus.CREATED);
	}

	@RequestMapping(value="roleusers", method = RequestMethod.POST)
	ResponseEntity<?> getRoleUsers(@RequestBody String objSecId) {	
		FiltroUsuarioVO filtros = new FiltroUsuarioVO();
		filtros.setRoles(new ArrayList<String>());
		filtros.getRoles().add(objSecId);
		return new ResponseEntity<>(secServices.filtraUsuarios(filtros), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="adduserrol", method = RequestMethod.POST)
	ResponseEntity<?> addUserRole(@RequestBody CrearObjSecVO objSec) {
		return new ResponseEntity<>(secServices.agregaUsuarioRol(objSec), HttpStatus.CREATED);
	}
	@RequestMapping(value="removeuser", method = RequestMethod.POST)
	ResponseEntity<?> removeUserFromRole(@RequestBody CrearObjSecVO objSec) {
		secServices.removeUserRoleRelation(objSec);
		return new ResponseEntity<>(new CrearObjSecVO(), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="inactivaUsuario", method = RequestMethod.POST)
	ResponseEntity<?> inactivaUsuario(@RequestBody String idUsuario) {
		secServices.inactivaUsuario(idUsuario);
		return new ResponseEntity<>(new CrearObjSecVO(), HttpStatus.CREATED);
	}
	

	@RequestMapping(value="getRolesUsuario", method = RequestMethod.POST)
	ResponseEntity<?> getRolesUsuario(@RequestBody String idUsuario) {
		//secServices.getUserRoles(idUsuario);
		return new ResponseEntity<>(secServices.getUserRoles(idUsuario), HttpStatus.CREATED);
	}
}
