package com.crenx.controller;

import java.io.ByteArrayOutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.ExcelReportUtil;
import org.springframework.http.HttpStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.entity.Producto;
import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.PagoRepository;
import com.crenx.data.domain.repository.ProductoRepository;
import com.crenx.data.domain.repository.TablaAmortizacionRepository;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.EstadoCuentaVO;
import com.crenx.pentahoo.GeneradorReportes;
import com.crenx.services.EstadoCuentaServices;
import com.google.common.collect.Lists;


@RestController
@RequestMapping("/api/estadoCuenta")
public class EstadoCuentaRestController {

	@Autowired
	EstadoCuentaServices estCuentaServ;
	
	@RequestMapping(value = "/getEstadoCuenta", method = RequestMethod.POST)
	public byte[] edoCuenta(Principal principal, @RequestBody String idCredito, HttpServletResponse response){
		ByteArrayOutputStream baos=null;
		baos = new ByteArrayOutputStream();
		EstadoCuentaVO estadoCuenta = estCuentaServ.getEstadoCuenta(idCredito);
		
		
		try {
			response.setHeader("Pragma", "public");
			response.setHeader("Content-Disposition", "attachment; filename=EstadoCuenta.pdf");
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setContentType("UTF-8");
			response.setCharacterEncoding("UTF-8");

			GeneradorReportes generador =  new GeneradorReportes();
			MasterReport report = generador.getEstadoCuenta(estadoCuenta);
			response.setContentType("application/pdf;charset=UTF-8");
			
			PdfReportUtil.createPDF(report, baos);
			baos.flush();
			baos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return baos.toByteArray();
    	
	}
	
}