package com.crenx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;
import com.crenx.services.NominaServices;

@RestController
@RequestMapping("/api/nomina")
public class NominaRestController {

	@Autowired
	private NominaServices nominaServices;

	@RequestMapping(value="datos", method = RequestMethod.POST)
	ResponseEntity<?> getDatosNomina(@RequestBody FiltrosOrgVO filtros) {
		try {
			List<NominaPeriodoVO> result = nominaServices.consultarNominaPeriodo(filtros);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (Exception e) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}

	}

}
