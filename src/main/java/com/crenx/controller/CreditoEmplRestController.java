package com.crenx.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.vo.CreditoEmpleadoVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltroUsuarioVO;
import com.crenx.data.domain.vo.FiltrosCredEmplVO;
import com.crenx.services.CreditoEmpleadoServices;
import com.crenx.services.SeguridadServices;

@RestController
@RequestMapping("/api/creditoEmp")
public class CreditoEmplRestController {

	@Autowired
	private CreditoEmpleadoServices credEmplServices;
	@Autowired
	private SeguridadServices segServices;
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> getCreditosEmpl(@RequestBody FiltrosCredEmplVO filtros) {
		try
		{
			return new ResponseEntity<>(credEmplServices.getCreditosEmpl(filtros),HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al consultar datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value="datosNomina", method = RequestMethod.POST)
	ResponseEntity<?> getDatosNominaEmpl(@RequestBody FiltroUsuarioVO filtros) {
		try
		{
			
			return new ResponseEntity<>(segServices.getEmpleados(filtros),HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al consultar datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value="saveCreditoEmpl", method = RequestMethod.POST)
	ResponseEntity<?> saveCreditoEmpl(@RequestBody CreditoEmpleadoVO credVO) {		
		try
		{
			credEmplServices.saveCreditoEmpleado(credVO);
			return new ResponseEntity<>(credVO, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}
	}	

}
