package com.crenx.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.vo.CarteraTotalVO;
import com.crenx.data.domain.vo.ClienteModelMap;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.EstadoCarteraVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.IndicadorCarteraDiarioVO;
import com.crenx.data.domain.vo.IndicadorCreditoVO;
import com.crenx.data.domain.vo.InidicadorRutaVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.services.CajaServices;
import com.crenx.services.ClienteServices;
import com.crenx.services.CreditoServices;
import com.crenx.services.UtilityServices;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@RestController
@RequestMapping("/api/indicadores")
public class IndicadoresRestController {
	@Autowired
	private CreditoServices credServices;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private ClienteServices clienteServices;
	@Autowired
	private CajaServices cajaServices;

	@RequestMapping(value="carteratotal", method = RequestMethod.POST)
	ResponseEntity<?> carteraTotal(Principal principal ,@RequestBody FiltrosOrgVO filtros) {
		try
		{
			Date desde = utServices.justDate(new Date());
			Date hasta = desde;
			DateRangeVO week = null;
			if (filtros.getFechaInicial()!=null)
			{
				desde = utServices.convertStringToDate(filtros.getFechaInicial(), "yyyy-MM-dd");
				desde = utServices.justDate(desde);				
			}
			if (filtros.getFechaFinal()!=null)
			{
				hasta = utServices.convertStringToDate(filtros.getFechaFinal(), "yyyy-MM-dd");
				hasta = utServices.justDate(hasta);				
			}
			if (hasta==desde)
				week = utServices.getWeekDates(desde);
			else
			{
				week = new DateRangeVO();
				week.setStartDate(desde);
				week.setEndDate(hasta);
			}
			List<IndicadorCarteraDiarioVO> cartera = credServices.getIndicadorDiarioCartera(filtros,week);
			List<IndicadorCarteraDiarioVO> carteraHoy = credServices.getIndicadorDiarioCarteraRaw(filtros, utServices.diaSemana(new Date()));
			cartera = cartera==null?new ArrayList<IndicadorCarteraDiarioVO>():cartera; 
			if (carteraHoy!=null)
				cartera.addAll(carteraHoy);
			
			CarteraTotalVO result = new CarteraTotalVO();	
			result.setCarteraDia(cartera);
			//Cartera total y Cartera Capital
			IndicadorCreditoVO totalCartera = credServices.carteraTotal(filtros);
			result.setCartera(totalCartera);
			IndicadorCreditoVO totalCarteraVencida = credServices.carteraVencida(filtros);
			result.setCarteraVencida(totalCarteraVencida);
			IndicadorCreditoVO nuevosCreditos = new IndicadorCreditoVO();
			nuevosCreditos.setCountCreditos(credServices.creditosNuevos(week, filtros)); 
			result.setCreditosNuevos(nuevosCreditos);
			IndicadorCreditoVO nuevosClientes = new IndicadorCreditoVO();
			nuevosClientes.setCountCreditos(clienteServices.clientesNuevos(week, filtros));
			result.setClientesNuevos(nuevosClientes);

			IndicadorCreditoVO clientesTotales = new IndicadorCreditoVO();
			clientesTotales.setCountCreditos(clienteServices.clientesTotales(week, filtros));
			result.setClientesTotales(clientesTotales);

			IndicadorCreditoVO clientesActivos = new IndicadorCreditoVO();
			clientesActivos.setCountCreditos(clienteServices.clientesActivos(week, filtros));
			result.setClientesActivos(clientesActivos);

			
			List<InidicadorRutaVO> indicadoresCobranza = credServices.indicadorCobranza(week.getStartDate(), filtros);
			result.setIndicadoresCobranza(indicadoresCobranza);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}		
	}
	
	@RequestMapping(value="resumen", method = RequestMethod.POST)
	ResponseEntity<?> resumen(Principal principal ,@RequestBody FiltrosOrgVO filtros) {
		try
		{
			List<ResumenEmpresaVO> result =   cajaServices.calculoResumenEmpresa(filtros);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar los registros");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}		
	}

	@RequestMapping(value="fechasSemana", method = RequestMethod.POST)
	ResponseEntity<?> fechasSemana(Principal principal ,@RequestBody FiltrosOrgVO filtros) {
		try
		{
			
			Date desde = utServices.justDate(new Date());
			Date hasta = desde;
			DateRangeVO week = null;
			if (filtros!=null)
			{
				if (filtros.getFechaInicial()!=null)
				{
					desde = utServices.convertStringToDate(filtros.getFechaInicial(), "yyyy-MM-dd");
					desde = utServices.justDate(desde);				
				}
				if (filtros.getFechaFinal()!=null)
				{
					hasta = utServices.convertStringToDate(filtros.getFechaFinal(), "yyyy-MM-dd");
					hasta = utServices.justDate(desde);				
				}
				if (hasta==desde)
					week = utServices.getWeekDates(desde);
				else
				{
					week = new DateRangeVO();
					week.setStartDate(desde);
					week.setEndDate(hasta);
				}
			}else
			{
				week = utServices.getWeekDates(desde);				
			}
			week.setsDesde(utServices.convertDateToString(week.getStartDate(), "yyyy-MM-dd"));
			week.setsHasta(utServices.convertDateToString(week.getEndDate(), "yyyy-MM-dd"));
			return new ResponseEntity<>(week, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar los registros");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}		
	}
	
	@RequestMapping(value="cartera", method = RequestMethod.POST)
	ResponseEntity<?> cartera(Principal principal ,@RequestBody FiltrosOrgVO filtros) {
		try
		{
			List<EstadoCarteraVO> result = credServices.calculoCartera(filtros);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar los registros");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}		
	}

	@RequestMapping(value="carteraDetalle", method = RequestMethod.POST)
	ResponseEntity<?> carteraDetalle(Principal principal ,@RequestBody FiltrosOrgVO filtros) {
		try
		{
			List<CreditoVO> result = credServices.detalleCartera(filtros);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar los registros");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}		
	}

}
