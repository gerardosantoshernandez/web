package com.crenx.controller;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.IndicadorCobranza;
import com.crenx.data.domain.entity.IndicadorCobranzaPeriodo;
import com.crenx.data.domain.vo.DateRangeVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.InidicadorRutaVO;
import com.crenx.services.CajaServices;
import com.crenx.services.CreditoServices;
import com.crenx.services.UtilityServices;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api/caja")
public class CajaRestController {
	@Autowired
	private CajaServices cajaServices;
	@Autowired
	private UtilityServices utServices;
	@Autowired
	private CreditoServices credServices;

	@RequestMapping(value="indicadores", method = RequestMethod.POST)
	ResponseEntity<?> calculaIndicadorCobranzaDiario(@RequestBody String fechaProceso) {
		try
		{
			Date dFechaProceso = utServices.convertStringToDate(fechaProceso, "yyyy-MM-dd");
			List<IndicadorCobranza> result = cajaServices.calculoIndicadorCobranzaDia(dFechaProceso);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}

	@RequestMapping(value="indicadoressemana", method = RequestMethod.GET)
	ResponseEntity<?> calculaIndicadorCobranzaSemana() {
		try
		{
			DateRangeVO semana = utServices.getWeekDates(new Date());
			List<IndicadorCobranzaPeriodo> result = cajaServices.calculoIndicadorCobranzaSemana(semana);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}

	@RequestMapping(value="operacionesmes", method = RequestMethod.POST)
	ResponseEntity<?> calculaOperacionesMensulaes(@RequestBody String fechaProceso) {
		try
		{
			Date dFechaProceso = utServices.convertStringToDate(fechaProceso, "yyyy-MM-dd");
			credServices.operacionesMes(dFechaProceso);
			return new ResponseEntity<>(fechaProceso, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}
	
}
