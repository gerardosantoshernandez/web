package com.crenx.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.services.ClienteServices;

@RestController
@RequestMapping("/api/rutacliente/movil")
public class ClienteRutaRestController {
	
	@Autowired
	private ClienteServices clienteServices;
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> rutaClientes(Principal principal, @RequestBody FiltrosOrgVO filtros)
	{
		try {
			return new ResponseEntity<>(clienteServices.obtenerCliente(filtros), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Error al consultar una Ruta.");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}
		
	}
	

}
