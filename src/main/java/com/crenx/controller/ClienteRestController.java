
package com.crenx.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Credito;
import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.TablaAmortizacion;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.CreditoRepository;
import com.crenx.data.domain.repository.DomicilioRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.ClienteModelMap;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CreditoBRVO;
import com.crenx.data.domain.vo.CreditoModelMap;
import com.crenx.data.domain.vo.CreditoRestVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.CreditosUnifVO;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.PagoVO;
import com.crenx.data.domain.vo.TablaAmortizacionModelMap;
import com.crenx.data.domain.vo.TablaAmortizacionVO;
import com.crenx.data.domain.vo.VisitaVO;
import com.crenx.security.UserAuthentication;
import com.crenx.services.CatalogoServices;
import com.crenx.services.ClienteServices;
import com.crenx.services.CreditoServices;
import com.crenx.services.DocumentoServices;
import com.crenx.services.UtilityServices;
import com.crenx.util.Constantes;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;

@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController {

	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private OrgRepository orgRepository;
	@Autowired
	private DomicilioRepository domRepository;
	@Autowired
	private CreditoRepository credRepository;
	@Autowired
	private CreditoServices credServices;
	@Autowired
	private CatalogoServices catServices;
	@Autowired
	private ClienteServices clienteServices;
	@Autowired
	private DocumentoServices doctoService;
	@Autowired
	private UtilityServices utilidades;
		
	@RequestMapping(value = "detalleCredito", method = RequestMethod.POST)
	ResponseEntity<?> detalleCredito(Principal principal, @RequestBody String idCredito) {
		try {		
			CreditoVO credito = credServices.getCreditoVOById(idCredito);
			return new ResponseEntity<>(credito, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@RequestMapping(value = "aprobarcredito", method = RequestMethod.POST)
	ResponseEntity<?> aprobarCredito(Principal principal, @RequestBody CreditoVO credito) {
		try {	
			CreditoBRVO retVal = credServices.aprobarCredito(credito, principal.getName());
			return new ResponseEntity<>(retVal, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	@RequestMapping(value = "entregarcredito", method = RequestMethod.POST)
	ResponseEntity<?> entregarCredito(Principal principal, @RequestBody CreditoVO credito) {
		try {		
			
			UserAuthentication profile = (UserAuthentication)principal;
			System.out.println("ruta--->"+profile.getDetails().getPerfil().getIdTitOrg());
			CreditoBRVO retVal = credServices.aplicarCreditoAprobado(credito, principal.getName(),profile.getDetails().getPerfil().getIdTitOrg());
			return new ResponseEntity<>(retVal, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	
	//FIXME: Eliminar este método
	@RequestMapping(value = "savevisita", method = RequestMethod.POST)
	ResponseEntity<?> guardaVisita(Principal principal, @RequestBody VisitaVO visita) {
		try {
			credServices.guardaVisita(visita);
			return new ResponseEntity<>(visita, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> clientes(Principal principal, @RequestBody FiltrosOrgVO filtros) {
		generateDefaultClient();
		return new ResponseEntity<>(clienteServices.filtrarClientes(filtros,"FIG_CLIENTE"), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="getClientesTraspado", method = RequestMethod.POST)
	ResponseEntity<?> getClientesTraspaso(Principal principal, @RequestBody FiltrosOrgVO filtros) {
		generateDefaultClient();
		return new ResponseEntity<>(clienteServices.filtrarClientesTraspaso(filtros,"FIG_CLIENTE"), HttpStatus.CREATED);
	}
	
	// FIXME Agregar validaciones adicionales el RFC (Nombres, Genero, Fecha de
	// Nacimiento)
	@RequestMapping(value = "savecliente", method = RequestMethod.POST)
	ResponseEntity<?> agregar(Principal principal, @RequestBody ClienteVO input) {

		try {
			clienteServices.saveCliente(input);
			return new ResponseEntity<>(input, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@SuppressWarnings("serial")
	@RequestMapping(value = "codeudores", method = RequestMethod.POST)
	ResponseEntity<?> codeudores(Principal principal, @RequestBody String idCliente) {
		Cliente parent = clienteRepository.findOne(idCliente);
		Collection<Cliente> coDeudores = Lists.newArrayList(parent.getCodeudores());
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteModelMap());
		Type listType = new TypeToken<List<ClienteVO>>() {
		}.getType();
		List<ClienteVO> listaCodeudores = mapper.map(coDeudores, listType);
		// Collection<NameValueVO> items = parent.codeudorToValueList();
		return new ResponseEntity<>(listaCodeudores, HttpStatus.CREATED);
	}

	@SuppressWarnings("serial")
	@RequestMapping(value = "pagos", method = RequestMethod.POST)
	ResponseEntity<?> pagosCredito(Principal principal, @RequestBody String idCredito) {
		try {
			Credito credito = credServices.getCreditoById(idCredito);
			//Collection<TablaAmortizacion> returnList = credito.getPagos();
			List<TablaAmortizacion> returnList  = credito.getPagos();
			ModelMapper mapper = new ModelMapper();
			mapper.addMappings(new TablaAmortizacionModelMap());
			Type listType = new TypeToken<List<TablaAmortizacionVO>>() {}.getType();
			ArrayList<TablaAmortizacionVO> listaPagos = mapper.map(returnList, listType);
			return new ResponseEntity<>(listaPagos, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);

		}
	}

	@RequestMapping(value = "loadPago", method = RequestMethod.POST)
	ResponseEntity<?> loadPago(Principal principal, @RequestBody String idPago) {
		try {
			
			PagoVO pago = credServices.loadPago(idPago);
			return new ResponseEntity<>(pago, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);

		}
	}
	
	@RequestMapping(value = "deletepago", method = RequestMethod.POST)
	ResponseEntity<?> deletePago(Principal principal, @RequestBody String idPago) {
		try {
			NameValueVO retVal = new NameValueVO("","");
			credServices.deletePago(principal, idPago);
			return new ResponseEntity<>(retVal, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);

		}
	}

	@RequestMapping(value = "detailcte/{clienteId}", method = RequestMethod.GET)
	ClienteVO leerCliente(Principal principal, @PathVariable String clienteId) {
		Cliente clienteDB = clienteRepository.findOne(clienteId);
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new ClienteModelMap());
		mapper.map(clienteDB, ClienteVO.class);
		// FIXME: Crear un ¡a configuracion de maper
		ClienteVO vo = mapper.map(clienteDB, ClienteVO.class);
		//clienteToClienteVO(clienteDB);
		return vo;
	}

	@RequestMapping(value = "carteracreditos", method = RequestMethod.POST)
	ResponseEntity<?> carteraCreditos(Principal principal, @RequestBody FiltrosOrgVO filtros) {
		try
		{
			List<CreditoVO> returnList = credServices.carteraCreditos(filtros);
			return new ResponseEntity<>(returnList, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();

			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar consultar los registros");
			status.setSourceMessage(ex.getMessage());
			status.setMotivoRechazo(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
		
	}
	
	@SuppressWarnings("serial")
	@RequestMapping(value = "creditos/{clienteId}", method = RequestMethod.GET)
	ResponseEntity<?> creditosCliente(Principal principal, @PathVariable String clienteId) {
		Collection<Credito> returnList = null;
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new CreditoModelMap());
		returnList = Lists.newArrayList(credRepository.findByClienteIdClienteOrderByFechaEmisionDesc(clienteId));
		Type listType = new TypeToken<List<CreditoVO>>() {
		}.getType();
		ArrayList<CreditoVO> listaCreditos = mapper.map(returnList, listType);
		return new ResponseEntity<>(listaCreditos, HttpStatus.CREATED);
	}

	
	@RequestMapping(value = "savecredito", method = RequestMethod.POST)
	ResponseEntity<?> saveCreditos(Principal principal, @RequestBody CreditoVO input) {
		try {			
			credServices.crearCredito(input, principal.getName(), new Date());
			return new ResponseEntity<>(input, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();

			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			status.setMotivoRechazo(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@RequestMapping(value = "restcredito", method = RequestMethod.POST)
	ResponseEntity<?> restCreditos(Principal principal, @RequestBody CreditoRestVO input) {
		try {
			credServices.restructuraCredito(principal.getName(), input);
			return new ResponseEntity<>(input, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();

			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar registrar los movimientos");
			status.setSourceMessage(ex.getMessage());
			status.setMotivoRechazo(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@RequestMapping(value = "savepago", method = RequestMethod.POST)
	ResponseEntity<?> savePago(Principal principal, @RequestBody PagoVO input) {
		try {
			Credito credito = credServices.getCreditoById(input.getIdCredito());
			if (!credito.getEstatusCredito().getClaveInterna().equals("CRED_ACTIVO")) {
				throw new Exception("El crédito no está activo");
			}
			credServices.crearPago(credito, input, input.getIdDispositivo(), principal.getName());
			// Se recupera la información actual del crédito después del pago
			// Y se regresa al cliente como información adicional
			input.setCredito(credServices.getCreditoVOById(credito.getIdCredito()));
			return new ResponseEntity<>(input, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar guardar el registro");
			status.setSourceMessage(ex.getMessage());
			status.setMotivoRechazo(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "upload")
	public ResponseEntity<?> handleFileUpload(final Principal principal, @RequestParam("id") final String idCliente,
			@RequestParam("file") final MultipartFile file, @RequestParam("idDocumento") final String idTipoDocumento,
			@RequestParam(name = "idCredito", required = false) final String idCredito,
			RedirectAttributes redirectAttributes) {
		try {
			Cliente cliente = null;
			// Referencia al Cliente
			if ((cliente = clienteRepository.findOne(idCliente)) == null) {
				throw new IllegalArgumentException("No existe el cliente: " + idCliente);
			}
			Credito credito = null;
			NameValueVO propiedadCredito = null;
			// Referencia al crédito del cliente
			if (StringUtils.isNotBlank(idCredito)) {
				if ((credito = credServices.getCreditoById(idCredito)) == null) {
					throw new IllegalArgumentException("No existe el credito: " + idCredito);
				}
				propiedadCredito = new NameValueVO(credito.getIdCredito(), Constantes.IDCREDITIO);
			}
			this.doctoService.salvarDocumento(file, cliente.getIdCliente(), idTipoDocumento,
					(propiedadCredito == null) ? null : Lists.newArrayList(propiedadCredito),Constantes.IDCLIENTE, true);
		} catch (Exception e) {
			MultiValueMap headers = new MultiValueMap();
			headers.put("Error", e.getMessage());
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("{}", HttpStatus.OK);
	}

	private void generateDefaultClient() {

		if (clienteRepository.count() == 0) {

			Cliente cliente = new Cliente();
			cliente.setApellidoMaterno("Roa");
			cliente.setApellidoPaterno("Cadena");
			cliente.setCorreoElectronico("salvador.cadena@gmail.com");
			cliente.setCreacion(new Date());
			cliente.setCurp("CARS641101SFASDADAD");
			cliente.setCveFiscal("CARS641101SFASDADAD");
			cliente.setDocIdentificacion("12345678");
			cliente.setFechaNacimiento(new Date());
			Catalogo tipoFigura = catServices.getCatalogoCveInterna("FIG_CLIENTE");
			cliente.setTipoFigura(tipoFigura);

			Catalogo tipoGenero = catServices.getCatalogoByNombre("Masculino");
			cliente.setGenero(tipoGenero);

			cliente.setNombre("Salvador");
			Catalogo tipoDocId = catServices.getCatalogoByNombre("INE/IFE");
			cliente.setTipoDocId(tipoDocId);

			Domicilio domLab = new Domicilio("03020", "Rochester", "49", "piso 2", "Nápoles", "Benito Juárez", "D.F.",
					"México");
			Domicilio domRes = new Domicilio("03020", "Rochester", "49", "piso 2", "Nápoles", "Benito Juárez", "D.F.",
					"México");
			domRepository.save(domLab);
			domRepository.save(domRes);

			cliente.setDomicilioLaboral(domLab);
			cliente.setDomicilioResidencial(domRes);
			Catalogo entidades = catServices.getCatalogoByNombre("Ciudad de México");
			cliente.setEntidadNacimiento(entidades);

			cliente.setNombreEmpresa("Isol");
			cliente.setNombreRefFamiliar("Juan");
			cliente.setNombreRefPersonal("Pedro");
			Catalogo ocupacion = catServices.getCatalogoByNombre("Contador");
			cliente.setOcupacion(ocupacion);
			Catalogo pais = catServices.getCatalogoByNombre("México");
			cliente.setPaisNacimiento(pais);
			Collection<Organizacion> org = Lists.newArrayList(orgRepository.findByClave("PRUEBA"));
			cliente.setRuta(org.iterator().next());

			cliente.setTelCelular("5585804207");
			cliente.setTelLab("5555233962");
			cliente.setTelRefFamiliar("11077863");
			cliente.setTelRefPersonal("09809809809");
			cliente.setTelResidencia("098098099090");
			clienteRepository.save(cliente);
			// Co-Deudor
			Cliente coDeudor = new Cliente();
			coDeudor.setApellidoMaterno("Jimenez");
			coDeudor.setApellidoPaterno("Fuentes");
			coDeudor.setCorreoElectronico("araceli.fuentes@gmail.com");
			coDeudor.setCreacion(new Date());
			coDeudor.setCurp("FUJA641101SFASDADAD");
			coDeudor.setCveFiscal("FUJA641101SFASDADAD");
			coDeudor.setDocIdentificacion("87654321");
			coDeudor.setFechaNacimiento(new Date());
			tipoFigura = catServices.getCatalogoCveInterna("FIG_CODEUDOR");
			coDeudor.setTipoFigura(tipoFigura);

			coDeudor.setGenero(tipoGenero);

			coDeudor.setNombre("Araceli");
			coDeudor.setTipoDocId(tipoDocId);

			coDeudor.setDomicilioLaboral(domLab);
			coDeudor.setDomicilioResidencial(domRes);
			coDeudor.setEntidadNacimiento(entidades);

			coDeudor.setNombreEmpresa("Crenx");
			coDeudor.setNombreRefFamiliar("Javier");
			coDeudor.setNombreRefPersonal("Ramiro");
			coDeudor.setOcupacion(ocupacion);
			coDeudor.setPaisNacimiento(pais);

			coDeudor.setRuta(cliente.getRuta());

			coDeudor.setTelCelular("5585801864");
			coDeudor.setTelLab("5555236992");
			coDeudor.setTelRefFamiliar("11077863");
			coDeudor.setTelRefPersonal("09809809809");
			coDeudor.setTelResidencia("098098099090");
			coDeudor.setParent(cliente);
			clienteRepository.save(coDeudor);
		}
	}

//	private ClienteVO clienteToClienteVO(Cliente cli) {
//	ClienteVO vo = new ClienteVO();
//	vo.setIdCliente(cli.getIdCliente());
//	vo.setApellidoMaterno(cli.getApellidoMaterno());
//	vo.setApellidoPaterno(cli.getApellidoPaterno());
//	vo.setCorreoElectronico(cli.getCorreoElectronico());
//	vo.setCurp(cli.getCurp());
//	vo.setCveFiscal(cli.getCveFiscal());
//	vo.setDocIdentificacion(cli.getDocIdentificacion());
//	vo.setDomicilioLaboral(cli.getDomicilioLaboral());
//	vo.setDomicilioResidencial(cli.getDomicilioResidencial());
//	// vo.setFechaNacimiento(cli.getFechaNacimiento());
//	vo.setIdEntidadNacimiento(cli.getEntidadNacimiento().getIdCatalogo());
//	vo.setIdGenero(cli.getGenero().getIdCatalogo());
//	vo.setIdOcupacion(cli.getOcupacion().getIdCatalogo());
//	vo.setIdPaisNacimiento(cli.getPaisNacimiento().getIdCatalogo());
//	/*
//	 * Organizacion ruta = new Organizacion();
//	 * ruta.setIdOrg(cli.getRuta().getIdOrg());
//	 * vo.setIdRuta(cli.getRuta().getIdOrg());
//	 */
//	vo.setIdTipoDocumentoIdentidad(cli.getTipoDocId().getIdCatalogo());
//	vo.setIdTipoFigura(cli.getTipoFigura().getIdCatalogo());
//	vo.setNombre(cli.getNombre());
//	vo.setNombreEmpresa(cli.getNombreEmpresa());
//	vo.setNombreRefFamiliar(cli.getNombreRefFamiliar());
//	vo.setNombreRefPersonal(cli.getNombreRefPersonal());
//	vo.setTelCelular(cli.getTelCelular());
//	vo.setTelLab(cli.getTelLab());
//	vo.setTelRefFamiliar(cli.getTelRefFamiliar());
//	vo.setTelRefPersonal(cli.getTelRefPersonal());
//	vo.setTelResidencia(cli.getTelResidencia());
//	return vo;
//}

/*	private Cliente clienteVOToCliente(ClienteVO vo) {
	Cliente cliente = new Cliente();
	cliente.setIdCliente(vo.getIdCliente());
	cliente.setApellidoMaterno(vo.getApellidoMaterno());
	cliente.setApellidoPaterno(vo.getApellidoPaterno());
	cliente.setCorreoElectronico(vo.getCorreoElectronico());
	cliente.setCreacion(new Date());
	cliente.setCurp(vo.getCurp());
	cliente.setCveFiscal(vo.getCveFiscal());
	cliente.setDocIdentificacion(vo.getDocIdentificacion());
	// cliente.setFechaNacimiento(vo.getFechaNacimiento());
	Collection<Catalogo> tipoFigura = Lists.newArrayList(catRepository.findOne(vo.getIdTipoFigura()));
	cliente.setTipoFigura(tipoFigura.iterator().next());

	Collection<Catalogo> tipoGenero = Lists.newArrayList(catRepository.findOne(vo.getIdGenero()));
	cliente.setGenero(tipoGenero.iterator().next());

	cliente.setNombre(vo.getNombre());
	Collection<Catalogo> tipoDocId = Lists.newArrayList(catRepository.findOne(vo.getIdTipoDocumentoIdentidad()));
	cliente.setTipoDocId(tipoDocId.iterator().next());

	cliente.setDomicilioLaboral(vo.getDomicilioLaboral());
	cliente.setDomicilioResidencial(vo.getDomicilioResidencial());
	Collection<Catalogo> entidades = Lists.newArrayList(catRepository.findOne(vo.getIdEntidadNacimiento()));
	cliente.setEntidadNacimiento(entidades.iterator().next());

	cliente.setNombreEmpresa(vo.getNombreEmpresa());
	cliente.setNombreRefFamiliar(vo.getNombreRefFamiliar());
	cliente.setNombreRefPersonal(vo.getNombreRefPersonal());
	Collection<Catalogo> ocupacion = Lists.newArrayList(catRepository.findOne(vo.getIdOcupacion()));
	cliente.setOcupacion(ocupacion.iterator().next());
	Collection<Catalogo> pais = Lists.newArrayList(catRepository.findOne(vo.getIdPaisNacimiento()));
	cliente.setPaisNacimiento(pais.iterator().next());

	Collection<Organizacion> org = Lists.newArrayList(orgRepository.findOne(vo.getIdRuta()));
	cliente.setRuta(org.iterator().next());

	cliente.setTelCelular(vo.getTelCelular());
	cliente.setTelLab(vo.getTelLab());
	cliente.setTelRefFamiliar(vo.getTelRefFamiliar());
	cliente.setTelRefPersonal(vo.getTelRefPersonal());
	cliente.setTelResidencia(vo.getTelResidencia());
	return cliente;

}*/
	
	@RequestMapping(value="cancelarCredito", method = RequestMethod.POST)
	ResponseEntity<?> cancelarGasto(Principal principal, @RequestBody CreditoVO credito){
		try{
			credServices.cancelaCredito(credito);
			return new ResponseEntity<>(credito, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al registrar los datos");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);			
		}	
	}
	
	@RequestMapping(value="reporteRecompras", method = RequestMethod.POST)
	ResponseEntity<?> getReporteRecompras(Principal principal, @RequestBody FiltrosOrgVO filtros) {

		return new ResponseEntity<>(credServices.filtrarCreditosRecompra(filtros), HttpStatus.CREATED);
	}
	

	@RequestMapping(value="getRFC", method = RequestMethod.POST)
	ResponseEntity<?> getRFC(@RequestBody ClienteVO cliente){
		String apaterno = cliente.getApellidoPaterno();
		String amaterno = cliente.getApellidoMaterno();
		String nombre = cliente.getNombre();
		String fechaNacimiento = cliente.getFechaNacimiento();
		String rfc = utilidades.validateRFC(apaterno, amaterno, nombre, fechaNacimiento);
		Object objRFC = new Object(){
			public String valor = rfc;
		};
		return  new ResponseEntity<>(objRFC,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="getCURP", method = RequestMethod.POST)
	ResponseEntity<?> getCURP(@RequestBody ClienteVO cliente){
		String apaterno = cliente.getApellidoPaterno();
		String amaterno = cliente.getApellidoMaterno();
		String nombre = cliente.getNombre();
		String fechaNacimiento = cliente.getFechaNacimiento();
		String genero = cliente.getIdGenero();
		String entidad = cliente.getIdEntidadNacimiento();
		
		String curp = utilidades.validateCURP(apaterno, amaterno, nombre, fechaNacimiento, genero, entidad);
		Object objCURP = new Object(){
			public String valor = curp;
		};
		return  new ResponseEntity<>(objCURP,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="getCreditosActivos", method = RequestMethod.POST)
	ResponseEntity<?> getCreditosActivos(Principal principal, @RequestBody FiltrosOrgVO filtros) {
		return new ResponseEntity<>(credServices.carteraCreditosActivos(filtros), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="getClientesSinCreditoA", method = RequestMethod.POST)
	ResponseEntity<?> getClientesSinCreditoA(Principal principal, @RequestBody FiltrosOrgVO filtros) {
		return new ResponseEntity<>(clienteServices.getClientesCandTitular(filtros,"FIG_CLIENTE"), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "unifCreditos", method = RequestMethod.POST)
	ResponseEntity<?> unifCreditos(Principal principal, @RequestBody CreditosUnifVO input) {
		try {
			credServices.unificaCreditos(principal.getName(), input);
			return new ResponseEntity<>(input, HttpStatus.CREATED);
		} catch (Exception ex) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error al intentar registrar los movimientos");
			status.setSourceMessage(ex.getMessage());
			status.setMotivoRechazo(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
