package com.crenx.controller;

import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Parametros;
import com.crenx.data.domain.repository.ParametrosRepository;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.services.CatalogoServices;
import com.google.common.collect.Lists;

@RestController
@RequestMapping("/api/parametros")
public class ParametrosRestController {

	@Autowired
	private ParametrosRepository paramRepository;
	@Autowired
	private CatalogoServices catServices;
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> parametros(Principal principal ,@RequestBody FiltrosOrgVO input) {
		Collection<Parametros> parametros = Lists.newArrayList(this.paramRepository.findAll());
		return new ResponseEntity<>(parametros, HttpStatus.CREATED);
	}
	
	@RequestMapping( value="getParam",  method = RequestMethod.POST)
	ResponseEntity<?> getParam(Principal principal ,@RequestBody String input) {
		Parametros param = catServices.getParametroByName(input);
		return new ResponseEntity<>(param, HttpStatus.CREATED);
	}

}
