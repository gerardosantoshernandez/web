package com.crenx.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.IndicadorCobranza;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.TaskVO;
import com.crenx.services.ActivitiServices;
import com.crenx.services.CajaServices;

@RestController
@RequestMapping("/api/process")
public class ProcessRestController {
	@Autowired
	private ActivitiServices bpmServices;
	
	@RequestMapping(value="mistareas", method = RequestMethod.GET)
	ResponseEntity<?> getMyTasks(Principal principal) {
		try
		{
			List<TaskVO> tareas = bpmServices.getMyTasks(principal.getName());
			return new ResponseEntity<>(tareas, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}
	@RequestMapping(value="taskDetails/{taskId}", method = RequestMethod.GET)
	ResponseEntity<?> getTaskDetail(Principal principal, @PathVariable String taskId) {
		try
		{
			TaskVO tarea = bpmServices.getTask(taskId, principal.getName());
			return new ResponseEntity<>(tarea, HttpStatus.CREATED);
		}catch(Exception ex)
		{
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(ex.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);						
		}
	}
	
	
}
