package com.crenx.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltroMovimientoCajaVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.security.UserAuthentication;
import com.crenx.services.CreditoServices;
import com.crenx.services.DatosMaestrosServices;

@RestController
@RequestMapping("/api/movil")
public class DatosMaestrosRestController {
	@Autowired
	private DatosMaestrosServices datosMaestrosServices;
	@Autowired
	private CreditoServices creditoServices;
	
	@RequestMapping(value = "consultar",method = RequestMethod.POST)
	public ResponseEntity<?> obtenerDatosMaestros(Principal principal){
		try{
			UserAuthentication perfil = (UserAuthentication)principal;

			return new ResponseEntity<>(datosMaestrosServices.obtenerDatosMaestros(perfil), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="creditosPorRuta", method= RequestMethod.POST)
	public ResponseEntity<?> obtenerCreditosPorRuta(@RequestBody FiltrosOrgVO filtros){
		try{
			return new ResponseEntity<>(datosMaestrosServices.obtenerCreditosPorRuta(filtros),HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar los créditos");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}		
	}

	
	@RequestMapping(value="creditosPorRuta/{idRuta}", method= RequestMethod.GET)
	public ResponseEntity<?> obtenerCreditosPorRuta(@PathVariable String idRuta){
		try{
			System.out.println("En creditos popr ruta : "+idRuta);
			return new ResponseEntity<>(datosMaestrosServices.obtenerCreditosPorRuta(idRuta),HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar los créditos");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}	
	}
	
	@RequestMapping(value="creditosDeClientesPorRuta/{idOrg}", method= RequestMethod.GET)
	public ResponseEntity<?> obtenerCreditosDesdeClientesPorRuta(@PathVariable String idOrg){
		try{
			return new ResponseEntity<>(datosMaestrosServices.obtenerCreditosDesdeClientesPorRuta(idOrg),HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar los créditos");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="visitas/{idOrg}", method= RequestMethod.GET)
	public ResponseEntity<?> obtenerVisitasPorCredito(@PathVariable String idOrg){
		try{
			return new ResponseEntity<>(datosMaestrosServices.obtenerVisitas(idOrg), HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar las visitas");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@RequestMapping(value = "movimientosCaja",method = RequestMethod.POST)
	public ResponseEntity<?> consultarMovimientosCaja(Principal principal ,@RequestBody FiltrosOrgVO input){
		try{
		return new ResponseEntity<>(creditoServices.consultarMovimientosCaja(input), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Obtiene los totales de cargo y abono de los movimientos caja. El
	 * parámetro de este recurso es un filtro que contempla: ruta, tipo de
	 * operación , tipo de movimiento y rango de fecha.
	 * 
	 * 
	 * Es importante considerar el manejo de fechas. El filtro contiene las
	 * fechas en String en el formato YYYY-MM-DD, cualquier otro formato debera
	 * provocar una excepción. El formato anterior no contiene el tiempo, esto
	 * es debido a que la consulta considerará por default que la fecha inicio
	 * se toma al inicio de día (0 hrs, 0 min y 0 seg) y la fecha fin a finde
	 * día(23 hrs, 59 min y 59 seg). Si las dos fechas se encuentran vacias o
	 * NULL la consulta de totales se realizará sólo por los otros criterios que
	 * considera el filtro. Si sólo una de las fechas viene vacia se considerará
	 * la fecha con valor como inicio y fin.
	 * 
	 * 
	 * @param principal
	 * @param filtro
	 * @return
	 */
	@RequestMapping(value = "totalesMovimientosCaja", method = RequestMethod.POST)
	public ResponseEntity<?> consultarTotalesMovimientosCaja(Principal principal,
			@RequestBody FiltroMovimientoCajaVO filtro) {
		try {
			return new ResponseEntity<>(datosMaestrosServices.getTotalesMovimientoCaja(filtro), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "concentradoCaja", method = RequestMethod.POST)
	public ResponseEntity<?> getConcentradoCaja(Principal principal,
			@RequestBody FiltrosOrgVO filtro) {
		try {
			return new ResponseEntity<>(datosMaestrosServices.getConcentradoCaja(filtro), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "reporteTraspasos", method = RequestMethod.POST)
	public ResponseEntity<?> getReporteTraspasos(Principal principal,
			@RequestBody FiltrosOrgVO filtro) {
		try {
			return new ResponseEntity<>(datosMaestrosServices.getTraspasoClientes(filtro), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Ocurrio un error al consultar");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
		}
	}
	
}
