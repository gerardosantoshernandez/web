package com.crenx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crenx.data.domain.entity.Visita;
import com.crenx.data.domain.vo.ErrorVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;

import com.crenx.data.domain.vo.VisitaVO;
import com.crenx.services.VisitasServices;

@RestController
@RequestMapping("/api/visitas")
public class VisitasController {
	
	@Autowired
	private VisitasServices vservice;
	
	@RequestMapping(value="datos", method = RequestMethod.POST)
	ResponseEntity<?> getVisitas(@RequestBody  FiltrosOrgVO filtros) {
		try {
			List<VisitaVO> result = vservice.getVisitas(filtros);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (Exception e) {
			ErrorVO status = new ErrorVO();
			status.setRunTimeError(true);
			status.setErrMessage("Sucedió un error: ");
			status.setSourceMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
		}

	}

}
