package com.crenx.pentahoo;

import java.io.File;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.pentaho.reporting.engine.classic.core.ClassicEngineBoot;
import org.pentaho.reporting.engine.classic.core.ItemBand;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.SubReport;
import org.pentaho.reporting.engine.classic.core.TableDataFactory;
import org.pentaho.reporting.engine.classic.core.filter.DataSource;
import org.pentaho.reporting.engine.classic.core.wizard.RelationalAutoGeneratorPreProcessor;
import org.pentaho.reporting.libraries.resourceloader.Resource;
import org.pentaho.reporting.libraries.resourceloader.ResourceManager;

import com.crenx.data.domain.entity.Domicilio;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Pago;
import com.crenx.data.domain.vo.CajaVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.DetalleCreditoPagosVO;
import com.crenx.data.domain.vo.EstadoCarteraVO;
import com.crenx.data.domain.vo.EstadoCuentaVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.MovimientoCajaVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import com.crenx.data.domain.vo.TraspasoVO;
import com.crenx.data.domain.vo.VisitaVO;

public class GeneradorReportes {
	
	public MasterReport getReporteCartera(FiltrosOrgVO filtros, List<EstadoCarteraVO> result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		final Object[] columnNames = new Object[] { "gerencia", "ruta", "noCreditos1", "enTiempo", "noCreditos2",
				"preventiva", "noCreditos3", "riesgo", "noCreditos4", "vencida", "region", "division" };
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;
		for (EstadoCarteraVO est : result) {

			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
															// el reporte
				if (j == 0)
					datos[i][j] = est.getGerencia();
				if (j == 1)
					datos[i][j] = est.getRuta();
				if (j == 2)
					datos[i][j] = est.getNumCarteraSana();
				if (j == 3)
					datos[i][j] = est.getCarteraSana();
				if (j == 4)
					datos[i][j] = est.getNumCarteraPreventiva();
				if (j == 5)
					datos[i][j] = est.getCarteraPreventiva();
				if (j == 6)
					datos[i][j] = est.getNumCarteraRiesgo();
				if (j == 7)
					datos[i][j] = est.getCarteraRiesgo();
				if (j == 8)
					datos[i][j] = est.getNumCarteraVencida();
				if (j == 9)
					datos[i][j] = est.getCarteraVencida();
				if (j == 10)
					datos[i][j] = est.getRegion();
				if (j == 11)
					datos[i][j] = est.getDivision();
			}
			i++;

		}

		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);
		ClassicEngineBoot.getInstance().start();
		
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteCartera2.prpt").getPath());
		File file = new File(reportPath);

		try {
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return report;
	}
	
	public MasterReport getReporteCobranza(FiltrosOrgVO filtros, List<DetalleCreditoPagosVO>result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
				final Object[] columnNames = new Object[] { "clave", "producto", "cliente", "estatus", "monto", "saldo", "pago",
				"plazo", "vencidos", "ruta", "gerencia", "region", "division" };
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;

		for (DetalleCreditoPagosVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos

				if (j == 0)
					datos[i][j] = est.getCredito().getId();
				if (j == 1)
					datos[i][j] = est.getCredito().getProducto();
				if (j == 2)
					datos[i][j] = est.getCliente().getNombre()==null?"": est.getCliente().getNombre() 
							+ " "+ (est.getCliente().getApellidoPaterno()==null?"":est.getCliente().getApellidoPaterno()) 
							+ " "+ (est.getCliente().getApellidoMaterno()==null?"":est.getCliente().getApellidoMaterno());
				if (j == 3)
					datos[i][j] = est.getCredito().getEstatusCredito();
				if (j == 4)
					datos[i][j] = est.getCredito().getMonto();
				if (j == 5)
					datos[i][j] = est.getCredito().getSaldo();
				if (j == 6)
					datos[i][j] = est.getCredito().getPago();
				if (j == 7)
					datos[i][j] = est.getCredito().getPlazo();
				if (j == 8)
					datos[i][j] = est.getNumeroVencidos();
				if (j == 9)
					datos[i][j] = est.getCliente().getRuta();
				if (j == 10)
					datos[i][j] = est.getGerencia();
				if (j == 11)
					datos[i][j] = est.getRegion();
				if (j == 12)
					datos[i][j] = est.getDivision();
			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteCobranza.prpt").getPath());
		File file = new File(reportPath);

		try{
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			
		}
		return report;
	}
	
	public MasterReport getReporteCaja(FiltrosOrgVO filtros, List<MovimientoCajaVO>result ){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		String fInicial = filtros.getFechaInicial();
		String fFinal = filtros.getFechaFinal();
		
		final Object[] columnNames = new Object[] { "unidad", "tipoMovimiento", "tipoOperacion", "tipoGasto", "fecha",
				"usuario", "referencia", "cargo", "abono", "fInicio", "fFinal", "division", "region", "gerencia","estatus","obs","ruta"};
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;

		for (MovimientoCajaVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getNombreOrg();
				if (j == 1)
					datos[i][j] = est.getTipoMov();
				if (j == 2)
					datos[i][j] = est.getTipoOperacion();
				if (j == 3)
					datos[i][j] = est.getTipoGasto();
				if (j == 4)
					datos[i][j] = est.getFechaSistema();
				if (j == 5)
					datos[i][j] = est.getIdUsuario();
				if (j == 6)
					datos[i][j] = est.getReferencia()+"- "+est.getReferenciaLarga();
				if (j == 7)
					datos[i][j] = est.getCargo();
				if (j == 8)
					datos[i][j] = est.getAbono();
				if (j == 9)
					datos[i][j] = fInicial;
				if (j == 10)
					datos[i][j] = fFinal;
				if (j == 11)
					datos[i][j] = est.getDivision();
				if (j == 12)
					datos[i][j] = est.getRegion();
				if (j == 13)
					datos[i][j] = est.getGerencia();
				if(j == 14)
					datos[i][j]= est.getEstatusMovimiento();
				if(j == 15)
					datos[i][j]= est.getObservaciones();
				if(j==16)
					datos[i][j]= est.getRuta();

			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteCaja.prpt").getPath());
		File file = new File(reportPath);

		try{
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
	
	public MasterReport getReporteResumen(FiltrosOrgVO filtros, List<ResumenEmpresaVO> result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;

		final Object[] columnNames = new Object[] { "gerencia", "ruta", "abono", "comision", "desembolso", "caja",
				"cartera", "region", "division","fInicio","fFin" };
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;
		for (ResumenEmpresaVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getGerencia();
				if (j == 1)
					datos[i][j] = est.getRuta();
				if (j == 2)
					datos[i][j] = est.getAbono();
				if (j == 3)
					datos[i][j] = est.getComision();
				if (j == 4)
					datos[i][j] = est.getDesembolso();
				if (j == 5)
					datos[i][j] = est.getCaja();
				if (j == 6)
					datos[i][j] = est.getCartera();
				if (j == 7)
					datos[i][j] = est.getRegion();
				if (j == 8)
					datos[i][j] = est.getDivision();
				if (j == 9)
					datos[i][j] = filtros.getFechaInicial();
				if (j == 10)
					datos[i][j] = filtros.getFechaFinal();
			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		manager.registerDefaults();

		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteResumen.prpt").getPath());
		File file = new File(reportPath);

		try{
			
				Resource res = manager.createDirectly(file, MasterReport.class);

				report = (MasterReport) res.getResource();
				report.setDataFactory(new TableDataFactory("Default", reportTableModel));

		}
		catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
	
	public MasterReport getReporteBaseNomina(FiltrosOrgVO filtros, List<NominaPeriodoVO> result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		final Object[] columnNames = new Object[] { "division", "region", "gerencia", "ruta", "semanas", "cveEmpleado",
				"nombreCompleto", "cobranza", "baseClientesNuevos", "baseCobranza", "baseNormalidad",
				"factorCobranza", "factorNormalidad", "totalNomina", "baseNomina", "baseAsimilados", "descuentoMoto",
				"descuentoPrestamo","anio","periodo","noperiodo" };
		final Object[][] datos = new Object[result.size()][columnNames.length];
		String periodo;
		if (filtros.getPeriodo().equals("1"))
			periodo = "Semanal";
		else
			periodo = "Quincenal";
	 
		int i = 0;
		for (NominaPeriodoVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getDivision();
				if (j == 1)
					datos[i][j] = est.getRegion();
				if (j == 2)
					datos[i][j] = est.getGerencia();
				if (j == 3)
					datos[i][j] = est.getRuta();
				if (j == 4)
					datos[i][j] = est.getSemanas();
				if (j == 5)
					datos[i][j] = est.getCveEmpleado();
				if (j == 6)
					datos[i][j] = est.getNombreCompleto();
				if (j == 7)
					datos[i][j] = est.getCobranzaTotal();
				if (j == 8)
					datos[i][j] = est.getBaseClientesNuevos();
				if (j == 9)
					datos[i][j] = est.getCobranza();
				if (j == 10)
					datos[i][j] = est.getBaseNormalidad();
				if (j == 11)
					datos[i][j] = est.getFactorCobranza();
				if (j == 12)
					datos[i][j] = est.getFactorNormalidad();
				if (j == 13)
					datos[i][j] = est.getTotalNomina();
				if (j == 14)
					datos[i][j] = est.getBaseNomina();
				if (j == 15)
					datos[i][j] = est.getBaseAsimilados();
				if (j == 16)
					datos[i][j] = est.getDescuentoMoto();
				if (j == 17)
					datos[i][j] = est.getDescuentoPrestamo();
				if (j == 18)
					datos[i][j] = filtros.getAnio();
				if (j == 19)
					datos[i][j] = periodo;
				if (j == 20)
					datos[i][j] = filtros.getNoPeriodo();
			}
			i++;
			
			try {
				final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

				ClassicEngineBoot.getInstance().start();
				manager.registerDefaults();
				String reportPath = URLDecoder
						.decode(this.getClass().getClassLoader().getResource("reportTemplates/prueba3.prpt").getPath());
				File file = new File(reportPath);
				Resource res = manager.createDirectly(file, MasterReport.class);
				report = (MasterReport) res.getResource();
				report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			}catch(Exception e){
				e.printStackTrace();
			}

		}
		return report;
	}

	public MasterReport getReporteVisitas(FiltrosOrgVO filtros, List<VisitaVO> result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		final Object[] columnNames = new Object[] { "rutaCredito","idCredito","fechaVisita","idCliente"
				,"nombreCliente", "motivo","visito","unidadVisita","fInicial","fFinal" };
		final Object[][] datos = new Object[result.size()][columnNames.length];
		
		int i = 0;
		for (VisitaVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getRutaCliente();
				if (j == 1)
					datos[i][j] = est.getIdCredito();
				if (j == 2)
					datos[i][j] = est.getFechaVisita();
				if (j == 3)
					datos[i][j] = est.getIdCliente();
				if (j == 4)
					datos[i][j] = est.getNombreCliente();
				if (j == 5)
					datos[i][j] = est.getIdMotivo();
				if (j == 6)
					datos[i][j] = est.getIdUsuario();
				if (j == 7)
					datos[i][j] = est.getIdOrg();
				if (j == 8)
					datos[i][j] = filtros.getFechaInicial();
				if (j == 9)
					datos[i][j] = filtros.getFechaFinal();
			}
			i++;
		}
			try {
				final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

				ClassicEngineBoot.getInstance().start();
				manager.registerDefaults();
				String reportPath = URLDecoder
						.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteVisitas.prpt").getPath());
				File file = new File(reportPath);
				Resource res = manager.createDirectly(file, MasterReport.class);
				report = (MasterReport) res.getResource();
				report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			}catch(Exception e){
				e.printStackTrace();
			}

		
		return report;
	}
	
	public MasterReport getEstadoCuenta(EstadoCuentaVO result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		final Object[] columnNames = new Object[] {"tipoProducto","nombreComercial","fechaCorte", "pagoMinimo","montoTotal", "nombreCliente"
				,"noContrato","dirCliente","fInicial","fFinal","cat",
				"diasPeriodo","saldoInicial","totComisiones","intOrdCarg","intMorCarg",
				"tasaOrdinaria","totIntCarg","saldoFinP","totImpuestos","totPagos",
				"montoIntOrd","pagoVen","montoIntMor","saldoVencido","saldoInsol", "fechaEmi"};
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		final Object[][] datos = new Object[1][columnNames.length];
		
		datos[0][0] = result.getCredito().get(0).getProducto().getFamiliaProducto().getNombre();//tipoProducto
		datos[0][1] = result.getProductos().get(0).getNombre();//nombreComercial
		datos[0][2] = df.format(result.getCredito().get(0).getFechaUltimoPago());//fechaCorte
		datos[0][3] = result.getCredito().get(0).getPago();//pagoMinimo
		datos[0][4] = result.getCredito().get(0).getMonto();//montoTotal
		datos[0][5] = result.getCliente().get(0).getNombreCompleto();//nombreCliente
		datos[0][6] = result.getCredito().get(0).getId();//noContrato
		Domicilio dir = result.getCliente().get(0).getDomicilioResidencial();
		if(dir!=null){
			String domicilio =  "Calle: "+(dir.getCalle()==null?"":dir.getCalle())
					+" Col: "+(dir.getColonia()==null?"":dir.getColonia())
					+" No.Ext: "+(dir.getNumExt()==null?"":dir.getNumExt())
					+" C.P.:"+(dir.getCodigoPostal()==null?"":dir.getCodigoPostal())+" , "
					+ dir.getEstado();
			datos[0][7] = domicilio;//dirCliente
		}
		
		datos[0][8] = df.format(result.getCredito().get(0).getFechaEmision());//fINicial
		datos[0][9] = df.format(result.getCredito().get(0).getFechaUltimoPago());//fFinal
		datos[0][10] = result.getProductos().get(0).getCat();//cat
		datos[0][11] = result.getDiasPeriodo();//diasPeriodo
		datos[0][12] = result.getCredito().get(0).getMonto();//saldoInicial
		datos[0][13] = result.getSumaComisionesCobradas();//totComisiones
		datos[0][14] = result.getTotalIntereses();//intOrdCarg
		datos[0][15] = result.getInteresesMoratoriosCargados();//intMorCarg
		datos[0][16] = result.getProductos().get(0).getTasa();//tasaOrdinaria
		System.out.println("Tasa: "+result.getProductos().get(0).getTasa());
		datos[0][17] = result.getTotalIntereses();//totIntCarg
		datos[0][18] = result.getCredito().get(0).getSaldo();//saldoFinP
		datos[0][19] = result.getSumaIVA();//totImpuestos
		datos[0][20] = result.getCredito().get(0).getTotPagos();//totPagos
		datos[0][21] = result.getTotalIntereses();//montoIntOrd
		datos[0][22] = result.getCredito().get(0).getSaldoInsoluto();//pagoVen
		datos[0][23] = result.getCredito().get(0).getMonto();//montoIntMor
		datos[0][24] = result.getCredito().get(0).getMonto();//saldoVencido
		datos[0][25] = result.getCredito().get(0).getMonto();//saldoInsol
		datos[0][26] = df.format(result.getCredito().get(0).getFechaEmision());//fechaEmision
		
		//SUBREPORTE DETALLE MOVIMIENTO
		//SubReport detalleMovimientos = new SubReport();
		
		
		List<MovimientoCaja> movimientos = result.getMovimientoCaja();
		System.out.println("Movimientos de caja: "+movimientos.size());
		final Object[] columnNamesDM = new Object[] {"fechaMov", "descMov", "cargoMov", "abonoMov"};
		final Object[][] datosDM = new Object[movimientos.size()][columnNamesDM.length];
		
		int i = 0;
		for (MovimientoCaja mc : movimientos) {
			for(int j = 0; j<columnNamesDM.length; j++){
				if(j==0) datosDM[i][j] = df.format(mc.getFechaEmision());
				if(j==1) datosDM[i][j] = mc.getTipoOperacion().getNombre();
				if(j==2) datosDM[i][j] = mc.getCargo();
				if(j==3) datosDM[i][j] = mc.getAbono();
			
				}
			i++;
			}
		final TableModel tableModelMov = new DefaultTableModel(datosDM, columnNamesDM);
		//detalleMovimientos.setDataFactory(new TableDataFactory("detalleMovimientos", tableModelMov));
		
		
		
		//SUBREPORTE COMISIONES COBRADAS
	//	SubReport comisionesCobradas = new SubReport();
		List<Pago> listPagos = result.getPagos();
		int numValidos = 0;
		
		for(Pago p: listPagos){
			if(p.getComision()>0)
				numValidos++;
		}
		
		final Object[] columnNamesCC = new Object[] {"fechaCom", "descCom", "montoCom"};
		final Object[][] datosCC = new Object[numValidos][columnNamesCC.length];
		
		System.out.println("comisiones validas: "+ numValidos);
		i = 0;
		for (Pago p : listPagos) {
			for(int j = 0; j<columnNamesCC.length; j++){
				if(p.getComision()>0){
					if(j==0) datosDM[i][j] = df.format(p.getFechaPago());
					if(j==1) datosDM[i][j] = p.getNoRecibo();
					if(j==2) datosDM[i][j] = p.getComision();
				}
				}
			i++;
			}
		
		final TableModel tableModelComisiones = new DefaultTableModel(datosCC, columnNamesCC);
		
		//SUBREPORTE CARGOS OBJETADOS
		//SubReport cargosObj = new SubReport();
		final Object[] columnNamesCO = new Object[] {"fechaCargo", "descCargo", "montoCargo"};
		final Object[][] datosCO = new Object[1][columnNamesCO.length];
		datosCO[0][0]="";
		datosCO[0][1]="";
		datosCO[0][2]="";
		final TableModel tableModelCargos = new DefaultTableModel(datosCO, columnNamesCO);
		
		//SUBREPORTE PAGOS
		//SubReport pagos = new SubReport();
		List<Pago> lPagos = result.getPagos();
		System.out.println("Pagos: "+lPagos.size());
		final Object[] columnNamesPag = new Object[] {"fechaPago", "descPago", "montoPago"};
		final Object[][] datosPag = new Object[lPagos.size()][columnNamesPag.length];
		i = 0;
		for (Pago p : lPagos) {
			for(int j = 0; j<columnNamesPag.length; j++){
				if(j==0) datosPag[i][j] = df.format(p.getFechaPago());
				if(j==1) datosPag[i][j] = "Pago";
				if(j==2) datosPag[i][j] = p.getAbono();
			
				}
			i++;
			}
		
		final TableModel tableModelPagos = new DefaultTableModel(datosPag, columnNamesPag);
		//pagos.setDataFactory(new TableDataFactory("Default", tableModelPagos));
		
	
		try {
			ClassicEngineBoot.getInstance().start();
			manager.registerDefaults();
			String reportPath = URLDecoder
					.decode(this.getClass().getClassLoader().getResource("reportTemplates/estadoCuenta.prpt").getPath());
			File file = new File(reportPath);
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			final TableModel reportTableModel = new  DefaultTableModel(datos, columnNames);
			
			report.setDataFactory(new TableDataFactory("queryExterno", reportTableModel));
			report.getItemBand().getSubReport(0).setDataFactory(new TableDataFactory("comisiones",tableModelComisiones));
			report.getItemBand().getSubReport(1).setDataFactory(new TableDataFactory("cargos",tableModelCargos));
			report.getItemBand().getSubReport(2).setDataFactory(new TableDataFactory("pagos",tableModelPagos));
			
			SubReport detalleMov= (SubReport)report.getItemBand().getElement(0);
			detalleMov.setDataFactory(new TableDataFactory("movimientos", tableModelMov));

		}catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
	
	public MasterReport getReporteDetalleCartera(FiltrosOrgVO filtros, List<CreditoVO> result){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		final Object[] columnNames = new Object[] { "ruta", "cliente", "credito", "producto", "sana", "preventiva",
				"riesgo", "vencida", "incobrable","gerencia", "region", "division","fup","fep","dven"};
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;
		for (CreditoVO est : result) {
			
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getRutaCliente();
				if (j == 1)
					datos[i][j] = est.getNombreCliente();
				if (j == 2)
					datos[i][j] = est.getId();
				if (j == 3)
					datos[i][j] = est.getProducto();
				if (j == 4 && est.getTipoCartera().equals("sana"))
					datos[i][j] = est.getSaldo();
				if (j == 5 && est.getTipoCartera().equals("preventiva"))
					datos[i][j] = est.getSaldo();
				if (j == 6 && est.getTipoCartera().equals("riesgosa"))
					datos[i][j] = est.getSaldo();
				if (j == 7 && est.getTipoCartera().equals("vencida"))
					datos[i][j] = est.getSaldo();
				if (j == 8 && est.getTipoCartera().equals("inco"))
					datos[i][j] = est.getSaldo();
				if (j == 9)
					datos[i][j] = est.getGerencia();
				if (j == 10)
					datos[i][j] = est.getRegion();
				if (j == 11)
					datos[i][j] = est.getDivision();
				if (j == 12)
					datos[i][j] = est.getFechaUltimoPago();
				if (j == 13)
					datos[i][j] = est.getFechaPagoNoPagado();
				if (j == 14)
					datos[i][j] = est.getDiasAtraso();
				
			}
			i++;

		}

		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);
		ClassicEngineBoot.getInstance().start();
		
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteCarteraDetalle.prpt").getPath());
		File file = new File(reportPath);

		try {
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return report;
	}

	
	public MasterReport getReporteCajaConcentrado(FiltrosOrgVO filtros, List<CajaVO>result ){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		String fInicial = filtros.getFechaInicial();
		String fFinal = filtros.getFechaFinal();
		
		final Object[] columnNames = new Object[] { "unidad", "fechaC","cargos", "abonos", "fInicio", "fFinal","saldoI","saldoF"};
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;

		for (CajaVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getNombre();
				if (j == 1)
					datos[i][j] = est.getFechaCorte();
				if (j == 2)
					datos[i][j] = est.getTotalCargos();
				if (j == 3)
					datos[i][j] = est.getTotalAbonos();
				if (j == 4)
					datos[i][j] = fInicial;
				if (j == 5)
					datos[i][j] = fFinal;
				if (j == 6)
					datos[i][j] = est.getSaldoInicial();
				if (j == 7)
					datos[i][j] = est.getSaldoFinal();
				
			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteCajaConcentrado.prpt").getPath());
		File file = new File(reportPath);

		try{
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
	
	public MasterReport getReporteTraspasos(FiltrosOrgVO filtros, List<TraspasoVO>result ){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		String fInicial = filtros.getFechaInicial();
		String fFinal = filtros.getFechaFinal();
		
		final Object[] columnNames = new Object[] {"nombreCliente","rutaActual","fechaCreacion","rutaOrigen",
				"rutaDestino","fechaTraspaso","motivo","usuario","fInicio","fFinal","usuarioCreador","monto"};
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;

		for (TraspasoVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getCliente();
				if (j == 1)
					datos[i][j] = est.getRutaActual();
				if (j == 2)
					datos[i][j] = est.getFechaCreacion();
				if (j == 3)
					datos[i][j] = est.getRutaOrigen();
				if (j == 4)
					datos[i][j] = est.getRutaDestino();
				if (j == 5)
					datos[i][j] = est.getFechaOperacion();
				if (j == 6)
					datos[i][j] = est.getMotivo();
				if (j == 7)
					datos[i][j] = est.getUsuario();
				if (j == 8)
					datos[i][j] = fInicial;
				if (j == 9)
					datos[i][j] = fFinal;
				if (j == 10)
					datos[i][j] = est.getUsuarioCreador();
				if (j == 11)
					datos[i][j] = est.getMonto();
				
			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteTraspasos.prpt").getPath());
		File file = new File(reportPath);

		try{
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
	
	public MasterReport getReporteRecompra(FiltrosOrgVO filtros, List<CreditoVO>result ){
		ResourceManager manager = new ResourceManager();
		manager.registerDefaults();
		MasterReport report=null;
		
		String fInicial = filtros.getFechaInicial();
		String fFinal = filtros.getFechaFinal();
		
		final Object[] columnNames = new Object[] {"nombreCliente","rutaCliente","credito","estatus",
				"saldo","rutaCredito","fechaRecompra","fInicio","fFinal","ruta","gerencia","region","division"};
		final Object[][] datos = new Object[result.size()][columnNames.length];
		int i = 0;

		for (CreditoVO est : result) {
			for (int j = 0; j < columnNames.length; j++) {// numero de atributos
				if (j == 0)
					datos[i][j] = est.getNombreCliente();
				if (j == 1)
					datos[i][j] = est.getRutaCliente();
				if (j == 2)
					datos[i][j] = String.valueOf(est.getId());
				if (j == 3)
					datos[i][j] = est.getEstatusCredito();
				if (j == 4)
					datos[i][j] = est.getSaldo();
				if (j == 5)
					datos[i][j] = est.getRuta();
				if (j == 6)
					datos[i][j] = est.getFechaEmision();
				if (j == 7)
					datos[i][j] = fInicial;
				if (j == 8)
					datos[i][j] = fFinal;
				if (j == 9)
					datos[i][j] = est.getRutaCliente();
				if (j == 10)
					datos[i][j] = est.getGerencia();
				if (j == 11)
					datos[i][j] = est.getRegion();
				if (j == 12)
					datos[i][j] = est.getDivision();
								
			}
			i++;
		}
		final TableModel reportTableModel = new DefaultTableModel(datos, columnNames);

		ClassicEngineBoot.getInstance().start();
		manager.registerDefaults();
		
		String reportPath = URLDecoder
				.decode(this.getClass().getClassLoader().getResource("reportTemplates/reporteRecompra.prpt").getPath());
		File file = new File(reportPath);

		try{
			Resource res = manager.createDirectly(file, MasterReport.class);
			report = (MasterReport) res.getResource();
			report.setDataFactory(new TableDataFactory("Default", reportTableModel));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return report;
	}
}
