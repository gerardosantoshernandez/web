package com.crenx.pentahoo;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.awt.Image;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.pentaho.reporting.engine.classic.core.ClassicEngineBoot;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.TableDataFactory;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.html.HtmlReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.ExcelReportUtil;
import org.pentaho.reporting.engine.classic.core.wizard.RelationalAutoGeneratorPreProcessor;
//import org.pentaho.reporting.engine.classic.extensions.datasources.hibernate.HQLDataFactory;
import org.pentaho.reporting.libraries.resourceloader.*;

import org.pentaho.reporting.libraries.resourceloader.ResourceCreationException;
import org.pentaho.reporting.libraries.resourceloader.ResourceException;
import org.pentaho.reporting.libraries.resourceloader.ResourceKeyCreationException;
import org.pentaho.reporting.libraries.resourceloader.ResourceLoadingException;
import org.pentaho.reporting.libraries.resourceloader.ResourceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.crenx.data.domain.vo.CajaVO;
import com.crenx.data.domain.vo.ClienteVO;
import com.crenx.data.domain.vo.CreditoVO;
import com.crenx.data.domain.vo.DetalleCreditoPagosVO;
import com.crenx.data.domain.vo.EstadoCarteraVO;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.MovimientoCajaVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.data.domain.vo.NominaPeriodoVO;
import com.crenx.data.domain.vo.ResumenEmpresaVO;
import com.crenx.data.domain.vo.TraspasoVO;
import com.crenx.data.domain.vo.VisitaVO;
import com.crenx.services.CajaServices;
import com.crenx.services.CreditoServices;
import com.crenx.services.DatosMaestrosServices;
import com.crenx.services.NominaServices;
import com.crenx.services.VisitasServices;
import com.google.common.collect.Lists;
import com.lowagie.text.pdf.codec.Base64.OutputStream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.*;

@RestController
@RequestMapping("/report")
public class ReportController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CreditoServices credServices;

	@Autowired
	private DatosMaestrosServices datosMaestrosServices;

	@Autowired
	private CreditoServices creditoServices;

	@Autowired
	private CajaServices cajaServices;

	@Autowired
	private NominaServices nominaServices;

	@Autowired
	private VisitasServices vservice;

	@RequestMapping(value = "/reporteBaseNomina", method = RequestMethod.POST)
	public String callback(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		List<NominaPeriodoVO> result = nominaServices.consultarNominaPeriodo(filtros);
		try {
			response.setContentType("text/html;charset=UTF-8");
			GeneradorReportes generador = new GeneradorReportes();
			MasterReport report = generador.getReporteBaseNomina(filtros, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteBaseNominaExcel", method = RequestMethod.POST)
	public byte[] callback2(@RequestBody FiltrosOrgVO filtros, HttpServletResponse response) {
		ByteArrayOutputStream baos = null;
		filtros.setTipoReporte("excel");
		List<NominaPeriodoVO> result = nominaServices.consultarNominaPeriodo(filtros);

		try {
			response.setHeader("Pragma", "public");
			response.setHeader("Content-Disposition", "attachment; filename=Report.xls");
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setContentType("UTF-8");
			response.setCharacterEncoding("UTF-8");

			GeneradorReportes generador = new GeneradorReportes();
			MasterReport report = generador.getReporteBaseNomina(filtros, result);
			baos = new ByteArrayOutputStream();
			ExcelReportUtil.createXLS(report, baos);

			baos.flush();
			baos.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteCartera", method = RequestMethod.POST)
	public String reporteCartera(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {

		List<EstadoCarteraVO> result = credServices.calculoCartera(filtros);
		if (result != null) {
			GeneradorReportes generador = new GeneradorReportes();
			MasterReport report = generador.getReporteCartera(filtros, result);
			response.setContentType("text/html;charset=UTF-8");
			try {
				HtmlReportUtil.createStreamHTML(report, response.getOutputStream());
			} catch (ReportProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@RequestMapping(value = "/reporteDetalleCartera", method = RequestMethod.POST)
	public String reporteDetalleCartera(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		List<CreditoVO> result = credServices.detalleCartera(filtros);
		if (result != null) {
			GeneradorReportes generador = new GeneradorReportes();
			MasterReport report = generador.getReporteDetalleCartera(filtros, result);
			response.setContentType("text/html;charset=UTF-8");
			try {
				HtmlReportUtil.createStreamHTML(report, response.getOutputStream());
			} catch (ReportProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@RequestMapping(value = "/reporteDetalleCarteraExcel", method = RequestMethod.POST)
	public byte[] reporteDetalleCarteraExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<CreditoVO> result = credServices.detalleCartera(filtros);
		GeneradorReportes generador = new GeneradorReportes();
		MasterReport report = generador.getReporteDetalleCartera(filtros, result);
		response.setContentType("text/html;charset=UTF-8");
		try {
			ExcelReportUtil.createXLS(report, baos);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteCarteraExcel", method = RequestMethod.POST)
	public byte[] reporteCarteraExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<EstadoCarteraVO> result = credServices.calculoCartera(filtros);
		GeneradorReportes generador = new GeneradorReportes();
		MasterReport report = generador.getReporteCartera(filtros, result);
		response.setContentType("text/html;charset=UTF-8");
		try {
			ExcelReportUtil.createXLS(report, baos);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteCaja", method = RequestMethod.POST)
	public String reporteCaja(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO input) {
		response.setContentType("text/html;charset=UTF-8");
		GeneradorReportes generador = new GeneradorReportes();
		List<MovimientoCajaVO> result = (List<MovimientoCajaVO>) creditoServices.consultarMovimientosCaja(input);
		System.out.println("MovTotales: " + result.size());
		if (result.size() == 0)
			return "";
		try {
			MasterReport report = generador.getReporteCaja(input, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteCajaExcel", method = RequestMethod.POST)
	public byte[] reporteCajaExcel(HttpServletResponse response, @RequestBody FiltrosOrgVO input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GeneradorReportes generador = new GeneradorReportes();
		List<MovimientoCajaVO> result = (List<MovimientoCajaVO>) creditoServices.consultarMovimientosCaja(input);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteCaja(input, result);
			ExcelReportUtil.createXLS(report, baos);

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteResumenExcel", method = RequestMethod.POST)
	public byte[] ReporteResumenExcel(HttpServletResponse response, @RequestBody FiltrosOrgVO filtros) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GeneradorReportes generador = new GeneradorReportes();
		filtros.setTipoReporte("excel");
		List<ResumenEmpresaVO> result = cajaServices.calculoResumenEmpresa(filtros);
		try {
			MasterReport report = generador.getReporteResumen(filtros, result);
			ExcelReportUtil.createXLS(report, baos);

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteResumen", method = RequestMethod.POST)
	public String reporteResumen(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		response.setContentType("text/html;charset=UTF-8");

		List<ResumenEmpresaVO> result = cajaServices.calculoResumenEmpresa(filtros);
		try {
			GeneradorReportes generador = new GeneradorReportes();
			MasterReport report = generador.getReporteResumen(filtros, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/reporteCobranza", method = RequestMethod.POST)
	public String reporteCobranza(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		GeneradorReportes generador = new GeneradorReportes();
		response.setContentType("text/html;charset=UTF-8");
		List<DetalleCreditoPagosVO> result = datosMaestrosServices.obtenerCreditosPorRuta(filtros);
		try {
			MasterReport report = generador.getReporteCobranza(filtros, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteCobranzaExcel", method = RequestMethod.POST)
	public byte[] reporteCobranzaExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		GeneradorReportes generador = new GeneradorReportes();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<DetalleCreditoPagosVO> result = datosMaestrosServices.obtenerCreditosPorRuta(filtros);
		try {
			MasterReport report = generador.getReporteCobranza(filtros, result);
			ExcelReportUtil.createXLS(report, baos);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteVisitas", method = RequestMethod.POST)
	public String reporteVisitas(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		GeneradorReportes generador = new GeneradorReportes();
		response.setContentType("text/html;charset=UTF-8");
		List<VisitaVO> result = vservice.getVisitas(filtros);
		if (result.size() == 0)
			return null;

		try {
			MasterReport report = generador.getReporteVisitas(filtros, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteVisitasExcel", method = RequestMethod.POST)
	public byte[] reporteVisitasExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO filtros) {
		GeneradorReportes generador = new GeneradorReportes();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<VisitaVO> result = vservice.getVisitas(filtros);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteVisitas(filtros, result);
			ExcelReportUtil.createXLS(report, baos);
		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteCajaConcentrado", method = RequestMethod.POST)
	public String reporteCajaConcentrado(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO input) {
		response.setContentType("text/html;charset=UTF-8");
		GeneradorReportes generador = new GeneradorReportes();

		List<CajaVO> result = (List<CajaVO>) datosMaestrosServices.getConcentradoCaja(input);
		System.out.println("MovTotales: " + result.size());
		try {
			MasterReport report = generador.getReporteCajaConcentrado(input, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteCajaConcentradoExcel", method = RequestMethod.POST)
	public byte[] reporteCajaConcentradoExcel(HttpServletResponse response, @RequestBody FiltrosOrgVO input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GeneradorReportes generador = new GeneradorReportes();
		List<CajaVO> result = (List<CajaVO>) datosMaestrosServices.getConcentradoCaja(input);

		try {
			MasterReport report = generador.getReporteCajaConcentrado(input, result);
			ExcelReportUtil.createXLS(report, baos);

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteTraspasos", method = RequestMethod.POST)
	public String reporteTraspasos(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO input) {
		response.setContentType("text/html;charset=UTF-8");
		GeneradorReportes generador = new GeneradorReportes();

		List<TraspasoVO> result = (List<TraspasoVO>) datosMaestrosServices.getTraspasoClientes(input);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteTraspasos(input, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteTraspasosExcel", method = RequestMethod.POST)
	public byte[] reporteTraspasosExcel(HttpServletResponse response, @RequestBody FiltrosOrgVO input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GeneradorReportes generador = new GeneradorReportes();
		List<TraspasoVO> result = (List<TraspasoVO>) datosMaestrosServices.getTraspasoClientes(input);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteTraspasos(input, result);
			ExcelReportUtil.createXLS(report, baos);

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	@RequestMapping(value = "/reporteRecompra", method = RequestMethod.POST)
	public String reporteRecompra(HttpServletRequest request, HttpServletResponse response,
			@RequestBody FiltrosOrgVO input) {
		response.setContentType("text/html;charset=UTF-8");
		GeneradorReportes generador = new GeneradorReportes();

		List<CreditoVO> result = (List<CreditoVO>) credServices.filtrarCreditosRecompra(input);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteRecompra(input, result);
			HtmlReportUtil.createStreamHTML(report, response.getOutputStream());

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/reporteRecompraExcel", method = RequestMethod.POST)
	public byte[] reporteRecompraExcel(HttpServletResponse response, @RequestBody FiltrosOrgVO input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GeneradorReportes generador = new GeneradorReportes();

		List<CreditoVO> result = (List<CreditoVO>) credServices.filtrarCreditosRecompra(input);
		if (result.size() == 0)
			return null;
		try {
			MasterReport report = generador.getReporteRecompra(input, result);
			ExcelReportUtil.createXLS(report, baos);

		} catch (ReportProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

}
