package com.crenx.controller;

import static org.mockito.Mockito.mock;

import java.security.Principal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.crenx.CrenxApplication;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.DocumentoRepository;

/**
 * Pruebas unitarias del Controller ClientRest
 * 
 * @author JCHR
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CrenxApplication.class)
@WebAppConfiguration
public class OrgRestControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	/**
	 * Prueba el envío de el archivo de transferencia cuando se guarda un
	 * fondeo.
	 * 
	 */
	@Test
	public void handlerSaveFondeoUploadTest() {
		try {
			//TODO
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error");
		}
	}
}