package com.crenx.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Principal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.crenx.CrenxApplication;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Cliente;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.PropiedadesDoc;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.ClienteRepository;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.PropiedadesDocRepository;

/**
 * Pruebas unitarias del Controller ClientRest
 * 
 * @author JCHR
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CrenxApplication.class)
@WebAppConfiguration
public class ClientRestControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private CatalogoRepository catalogoRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private DocumentoRepository docRepository;
	@Autowired
	private PropiedadesDocRepository propsRepository;

	/**
	 * Prueba el funcionamiento de la operación UPLOAD. Para su correcta
	 * ejecución modificar a las propiedades del archivo rutas.properties de
	 * acuerdo al FileManger que se desee usar.
	 * 
	 */
	@Test
	public void handlerUploadTest() {
		try {
			// Se crean los recursos de la prueba
			final Principal principal = mock(Principal.class);
			final Catalogo catalogo = catalogoRepository.findByNombre("Cliente");
			// Se crea el cliente de pruebas
			final Cliente testCliente = new Cliente();
			testCliente.setId(1L);
			testCliente.setGenero(catalogo);
			testCliente.setTipoFigura(catalogo);
			clienteRepository.save(testCliente);
			// Se invoca upload
			// MockMvc mockMvc =
			// MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
			final MockMultipartFile firstFile = new MockMultipartFile("file", "jchr.txt", "text/plain",
					"cliente xml".getBytes());
			final MockMvc mockMvc = MockMvcBuilders
					.standaloneSetup(this.webApplicationContext.getBean(ClienteRestController.class)).build();
			mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/api/cliente/upload").file(firstFile).principal(principal)
							.param("id", testCliente.getIdCliente()).param("idDocumento", catalogo.getIdCatalogo()))
					.andExpect(status().isOk());
			// Se valida que se haya creado el registro del Documento
			final List<Documento> docs = docRepository.findByPropiedadesDocsNombreAndPropiedadesDocsValor("IdCliente",
					testCliente.getIdCliente());
			Assert.assertNotNull(docs);
			Assert.assertTrue(docs.size() == 1);
			// Se valida que se haya guardado la referencia de documento
			final List<PropiedadesDoc> props = propsRepository.findByValor(testCliente.getIdCliente());
			Assert.assertNotNull(props);
			Assert.assertTrue(props.size() == 1);
			// Se eliminan los casos de prueba
			docRepository.delete(docs.get(0));
			clienteRepository.delete(testCliente);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error");
		}
	}

	/**
	 * Pruaba el escenario en donde un id cliente no  existe
	 */
	@Test
	public void handlerUploadTestNoFoundCliente() {
		try {
			// Se crean los recursos de la prueba
			final Principal principal = mock(Principal.class);
			final Catalogo catalogo = catalogoRepository.findByNombre("Cliente");
			final MockMultipartFile firstFile = new MockMultipartFile("file", "jchr.txt", "text/plain",
					"cliente xml".getBytes());
			final MockMvc mockMvc = MockMvcBuilders
					.standaloneSetup(this.webApplicationContext.getBean(ClienteRestController.class)).build();
			mockMvc.perform(MockMvcRequestBuilders.fileUpload("/api/cliente/upload").file(firstFile)
					.principal(principal).param("id", "error_id").param("idDocumento", catalogo.getIdCatalogo()))
					.andExpect(status().is5xxServerError())
					.andExpect(jsonPath("$.Error.[0]", is("No existe el cliente: error_id")));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error");
		}
	}
}