package com.crenx.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.Principal;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.crenx.CrenxApplication;
import com.crenx.builder.DocumentoBuilder;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.PropiedadesDocRepository;
import com.crenx.data.domain.vo.FiltroDocumentosVO;
import com.crenx.util.Constantes;
import com.google.gson.Gson;

/**
 * Pruebas unitarias del Controller DocumentosRest
 * 
 * @author JCHR
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CrenxApplication.class)
@WebAppConfiguration
public class DocumentoRestControllerTest {
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private DocumentoRepository doctoRepo;
	@Autowired
	private PropiedadesDocRepository propsDotoRepo;
	private MockMvc mockMvc;
	private Principal principal;
	private DocumentoBuilder doctoBuilder;

	/**
	 * Inicializa los valores de los recursos necesarios antes de cada test
	 */
	@Before
	public void setUp() {
		principal = mock(Principal.class);
		mockMvc = MockMvcBuilders.standaloneSetup(this.webApplicationContext.getBean(DocumentosRestController.class))
				.build();
		doctoBuilder = new DocumentoBuilder("TestDocument", "TestDocument");

	}

	@Test
	public void getDetalleDocumentosCliente() {
		this.doctoBuilder.cleanPropiedades().propiedad(Constantes.TIPO_DOCUMENTO, "Pasaporte")
				.propiedad(Constantes.IDCLIENTE, "ClienteTest");
		Documento docto = this.doctoBuilder.build();
		docto = doctoRepo.save(docto);
		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/api/documentos/obtenerDocCliente/ClienteTest")
					.principal(this.principal)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
					.andExpect(jsonPath("$[0].idDocumento", is(docto.getIdDocumento())))
					.andExpect(jsonPath("$[0].nombre", is("TestDocument")))
					.andExpect(jsonPath("$[0].tipoDocumento", is("Pasaporte"))).andDo(print());
		} catch (Exception exception) {
			exception.printStackTrace();
			Assert.fail("Error");
		} finally {
			this.propsDotoRepo.delete(docto.getPropiedadesDocs());
			this.doctoRepo.delete(docto);
		}
	}

	@Test
	public void getDetalleDocumentosGasto() {
		this.doctoBuilder.cleanPropiedades().propiedad(Constantes.TIPO_DOCUMENTO, "Gasolina")
				.propiedad(Constantes.IDGASTO, "GastoTest");
		Documento docto = this.doctoBuilder.build();
		docto = doctoRepo.save(docto);
		try {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/api/documentos/obtenerDocGasto/GastoTest").principal(this.principal))
					.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
					.andExpect(jsonPath("$[0].idDocumento", is(docto.getIdDocumento())))
					.andExpect(jsonPath("$[0].nombre", is("TestDocument")))
					.andExpect(jsonPath("$[0].tipoDocumento", is("Gasolina"))).andDo(print());
		} catch (Exception exception) {
			exception.printStackTrace();
			Assert.fail("Error");
		} finally {
			this.propsDotoRepo.delete(docto.getPropiedadesDocs());
			this.doctoRepo.delete(docto);
		}
	}

	@Test
	public void getDetalleDocumentosOrg() {
		this.doctoBuilder.cleanPropiedades().propiedad(Constantes.TIPO_DOCUMENTO, "Emergencia")
				.propiedad(Constantes.IDORG, "OrgTest");
		Documento docto = this.doctoBuilder.build();
		docto = doctoRepo.save(docto);

		try {
			mockMvc.perform(
					MockMvcRequestBuilders.get("/api/documentos/obtenerDocOrg/OrgTest").principal(this.principal))
					.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
					.andExpect(jsonPath("$[0].idDocumento", is(docto.getIdDocumento())))
					.andExpect(jsonPath("$[0].nombre", is("TestDocument")))
					.andExpect(jsonPath("$[0].tipoDocumento", is("Emergencia"))).andDo(print());
		} catch (Exception exception) {
			exception.printStackTrace();
			Assert.fail("Error");
		} finally {
			this.propsDotoRepo.delete(docto.getPropiedadesDocs());
			this.doctoRepo.delete(docto);
		}
	}

	@Test
	public void getDetalleDocumentosPorFiltro() {
		final FiltroDocumentosVO filtro = new FiltroDocumentosVO();
		final Gson gson = new Gson();
		this.doctoBuilder.cleanPropiedades().propiedad(Constantes.TIPO_DOCUMENTO, "Emergencia")
				.propiedad(Constantes.IDORG, "OrgTest");
		Documento doctoOrg = this.doctoBuilder.build();
		doctoOrg = doctoRepo.save(doctoOrg);
		filtro.setIdMovCajaTrasferencia("OrgTest");

		this.doctoBuilder.cleanPropiedades().propiedad(Constantes.TIPO_DOCUMENTO, "Pasaporte")
				.propiedad(Constantes.IDCLIENTE, "ClienteTest");
		Documento doctoCliente = this.doctoBuilder.build();
		doctoCliente.setFechaCreacion(DateTime.now().plusDays(1).toDate());
		doctoCliente = doctoRepo.save(doctoCliente);
		filtro.setIdCliente("ClienteTest");
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/api/documentos/obtenerDoctosPorFiltro")
					.principal(this.principal).contentType("application/json").content(gson.toJson(filtro))).andExpect(status().isOk())
					.andExpect(jsonPath("$", hasSize(2)))
					.andExpect(jsonPath("$[0].idDocumento", is(doctoOrg.getIdDocumento())))
					.andExpect(jsonPath("$[0].nombre", is("TestDocument")))
					.andExpect(jsonPath("$[0].tipoDocumento", is("Emergencia")))
					.andExpect(jsonPath("$[1].idDocumento", is(doctoCliente.getIdDocumento())))
					.andExpect(jsonPath("$[1].nombre", is("TestDocument")))
					.andExpect(jsonPath("$[1].tipoDocumento", is("Pasaporte"))).andDo(print());
		} catch (Exception exception) {
			exception.printStackTrace();
			Assert.fail("Error");
		} finally {
			this.doctoRepo.delete(doctoOrg);
			this.doctoRepo.delete(doctoCliente);
		}
	}	
}
