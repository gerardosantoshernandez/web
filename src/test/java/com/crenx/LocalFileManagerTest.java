package com.crenx;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.crenx.business.FileUtils;
import com.crenx.business.LocalFileManager;

/**
 * Prubas unitarias de los File Managers
 * 
 * @author JCHR
 *
 */
public class LocalFileManagerTest {

	/**
	 * Prueba la creación del archivo en Local
	 */
	@Test
	public void testLocalCreateFile() {
		final LocalFileManager manager = new LocalFileManager();
		manager.setWorkDirectory("C:\\Users\\JCHR\\Development\\tmp");
		try {
			// Rutas absolutas
			File file = manager.save("Estoy en Local".getBytes(), "C:\\Users\\JCHR\\Development\\tmp\\crenx",
					"test_Crenx.txt", "text/plain");
			Assert.assertNotNull(file);
			// Rutas relativas
			manager.setRelativePaths(true);
			file = manager.save("Estoy en Local".getBytes(), "crenx", "test1_Crenx.txt", "text/plain");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * Prueba la obtención de un archivo Local
	 */
	@Test
	public void testLocalGetFile() {
		// Creación de recursos
		final LocalFileManager manager = new LocalFileManager();
		manager.setWorkDirectory("C:\\Users\\JCHR\\Development\\tmp");
		try {
			// Rutas absolutas
			File file = manager.get("C:\\Users\\JCHR\\Development\\tmp\\crenx", "test_Crenx.txt");
			Assert.assertNotNull(file);
			Assert.assertArrayEquals(new Object[] { "Estoy en Local" }, new Object[] { FileUtils.fileToString(file) });
			// Rutas relativas
			manager.setRelativePaths(true);
			file = manager.get("crenx", "test1_Crenx.txt");
			Assert.assertNotNull(file);
			Assert.assertArrayEquals(new Object[] { "Estoy en Local" }, new Object[] { FileUtils.fileToString(file) });

		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Prueba el borrado de un archivo local
	 */
	@Test
	public void testLocalDeleteFile() {
		// Creación de recursos
		final LocalFileManager manager = new LocalFileManager();
		manager.setWorkDirectory("C:\\Users\\JCHR\\Development\\tmp");
		try {
			// Rutas absolutas
			manager.delete("C:\\Users\\JCHR\\Development\\tmp\\crenx", "test_Crenx.txt");
			Assert.assertFalse(manager.exists("C:\\Users\\JCHR\\Development\\tmp\\crenx", "test_Crenx.txt"));
			// Rutas relativas
			manager.setRelativePaths(true);
			manager.delete("crenx", "test1_Crenx.txt");
			Assert.assertFalse(manager.exists("crenx", "test1_Crenx.txt"));
		} catch (Exception exception) {
			Assert.fail(exception.getMessage());
		}
	}

	/**
	 * Prueba la creación de un directorio local
	 */
	@Test
	public void testLocalCreateDirectory() {
		// Crea recursos
		final LocalFileManager manager = new LocalFileManager();
		manager.setWorkDirectory("C:\\Users\\JCHR\\Development\\tmp");
		try {
			// Rutas absolutas
			manager.createDirectory("C:\\Users\\JCHR\\Development\\tmp\\crenx", "test");
			Assert.assertTrue(manager.exists("C:\\Users\\JCHR\\Development\\tmp\\crenx", "test"));
			// Rutas relativas
			manager.setRelativePaths(true);
			manager.createDirectory("crenx", "test1");
			Assert.assertTrue(manager.exists("crenx", "test1"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
