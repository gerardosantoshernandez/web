package com.crenx.service.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.crenx.CrenxApplication;
import com.crenx.builder.OrganizacionBuilder;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.services.HelperRepository;
import com.crenx.services.OrganizacionHelperRepository;
import com.google.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CrenxApplication.class)
@WebAppConfiguration
public class OrganizacionRepositoryTest {
	@Autowired
	private OrgRepository orgRepo;
	private OrganizacionBuilder orgBuilder;

	@Before
	public void setUp() {
		orgBuilder = new OrganizacionBuilder("Div1", "TestDiv1", "D");
	}

	@Test
	public void findRutasTest() {
		// Se crea el árbol de prueba
		List<Organizacion> result = null;
		final Organizacion div1 = this.orgRepo.save(orgBuilder.build());
		final Organizacion region1 = this.orgRepo
				.save(this.orgBuilder.clave("Region1").nombre("TestRegion1").tipo("R").parent(div1).build());
		final Organizacion region2 = this.orgRepo
				.save(this.orgBuilder.clave("Region2").nombre("TestRegion2").parent(div1).build());
		final Organizacion gcia1 = this.orgRepo
				.save(this.orgBuilder.clave("Gcia1").nombre("TestGcia1").tipo("G").parent(region1).build());
		final Organizacion gcia2 = this.orgRepo
				.save(this.orgBuilder.clave("Gcia2").nombre("TestGcia2").parent(region1).build());
		final Organizacion gcia3 = this.orgRepo
				.save(this.orgBuilder.clave("Gcia3").nombre("TestGcia3").parent(region2).build());
		final Organizacion ruta1 = this.orgRepo
				.save(this.orgBuilder.clave("Ruta1").nombre("TestRuta1").tipo("RR").parent(gcia1).build());
		final Organizacion ruta2 = this.orgRepo
				.save(this.orgBuilder.clave("Ruta2").nombre("TestRuta2").parent(gcia1).build());
		final Organizacion ruta3 = this.orgRepo
				.save(this.orgBuilder.clave("Ruta3").nombre("TestRuta3").parent(gcia2).build());

		try {
			// Obetner rutas por división
			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE, null, HelperRepository.LIKE_WILDCARD_ZERO_MORE);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			// Obetner rutas por regiónes
			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE,
					Lists.newArrayList(region1.getIdOrg()), null, null, HelperRepository.LIKE_WILDCARD_ZERO_MORE);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE,
					Lists.newArrayList(region2.getIdOrg()), null, null, HelperRepository.LIKE_WILDCARD_ZERO_MORE);
			this.validateFindRutas(result, Lists.newArrayList());

			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE,
					Lists.newArrayList(region1.getIdOrg(), region2.getIdOrg()), null, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			// Obetner rutas por gerencias
			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE, Lists.newArrayList(gcia1.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2"));

			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE, Lists.newArrayList(gcia2.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta3"));

			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE, Lists.newArrayList(gcia3.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList());

			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE,
					Lists.newArrayList(gcia3.getIdOrg(), gcia2.getIdOrg(), gcia1.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			// Obtener rutas por todas las organizaciones
			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null,
					Lists.newArrayList(region1.getIdOrg()), null, Lists.newArrayList(gcia1.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2"));

			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null,
					Lists.newArrayList(region1.getIdOrg()), null, Lists.newArrayList(gcia2.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta3"));

			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null,
					Lists.newArrayList(region1.getIdOrg(), region2.getIdOrg()), null,
					Lists.newArrayList(gcia1.getIdOrg(), gcia2.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null,
					Lists.newArrayList(region2.getIdOrg()), null, Lists.newArrayList(gcia3.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList());

			// Obtener rutas por division y region
			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null,
					Lists.newArrayList(region1.getIdOrg()), null, null, HelperRepository.LIKE_WILDCARD_ZERO_MORE);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2", "TestRuta3"));

			// Obtener rutas por division y gerencia
			result = this.orgRepo.findRutas(Lists.newArrayList(div1.getIdOrg()), null, null,
					HelperRepository.LIKE_WILDCARD_ZERO_MORE, Lists.newArrayList(gcia1.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2"));

			// Obtener rutas por region y gerencia
			result = this.orgRepo.findRutas(null, HelperRepository.LIKE_WILDCARD_ZERO_MORE,
					Lists.newArrayList(region1.getIdOrg()), null, Lists.newArrayList(gcia2.getIdOrg()), null);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta3"));

			final OrganizacionHelperRepository helper = new OrganizacionHelperRepository(this.orgRepo);
			final FiltrosOrgVO filtro = new FiltrosOrgVO();
			filtro.addOrganizacion(div1.getIdOrg(), div1.getNombre(), "D");
			filtro.addOrganizacion(region1.getIdOrg(), region1.getNombre(), "R");
			filtro.addOrganizacion(gcia1.getIdOrg(), gcia1.getNombre(), "G");
			Assert.assertTrue(filtro.getDivisiones().size() == 1);
			Assert.assertTrue(filtro.getRegiones().size() == 1);
			Assert.assertTrue(filtro.getGerencias().size() == 1);
			result = helper.getRutas(filtro);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1", "TestRuta2"));
			Assert.assertTrue(helper.getIds(result).size() == 2);
			filtro.addOrganizacion(ruta1.getIdOrg(), ruta1.getNombre(), "RR");
			result = helper.getRutas(filtro);
			this.validateFindRutas(result, Lists.newArrayList("TestRuta1"));
		} finally {
			this.orgRepo.delete(Lists.newArrayList(div1, region1, region2, gcia1, gcia2, gcia3, ruta1, ruta2, ruta3));
		}
	}

	/**
	 * @param orgs
	 * @param size
	 * @param names
	 */
	private void validateFindRutas(List<Organizacion> orgs, final List<String> nombresRutas) {
		Assert.assertTrue(orgs.size() == nombresRutas.size());
		for (Organizacion org : orgs) {
			Assert.assertTrue(org.getTipoNodo().equals("RR"));
			// Valida los nombres y que estos no se repitan
			Assert.assertTrue(nombresRutas.contains(org.getNombre()));
			nombresRutas.remove(nombresRutas.indexOf(org.getNombre()));
		}
	}
}
