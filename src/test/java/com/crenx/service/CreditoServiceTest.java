package com.crenx.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.crenx.CrenxApplication;
import com.crenx.builder.DocumentoBuilder;
import com.crenx.builder.MovimientoCajaBuilder;
import com.crenx.builder.OrganizacionBuilder;
import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Usuario;
import com.crenx.data.domain.repository.CatalogoRepository;
import com.crenx.data.domain.repository.DocumentoRepository;
import com.crenx.data.domain.repository.MovimientoCajaRepository;
import com.crenx.data.domain.repository.OrgRepository;
import com.crenx.data.domain.repository.UsuarioRepository;
import com.crenx.data.domain.vo.FiltrosOrgVO;
import com.crenx.data.domain.vo.MovimientoCajaVO;
import com.crenx.data.domain.vo.NameValueVO;
import com.crenx.services.CreditoServices;
import com.crenx.util.Constantes;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CrenxApplication.class)
@WebAppConfiguration
public class CreditoServiceTest {

	@Autowired
	private CreditoServices service;
	@Autowired
	MovimientoCajaRepository mcRepository;
	@Autowired
	private DocumentoRepository doctoRepo;
	@Autowired
	private OrgRepository orgRepo;
	@Autowired
	private CatalogoRepository catRepo;
	@Autowired
	private UsuarioRepository usrRepo;
	private DocumentoBuilder doctoBuilder;
	private MovimientoCajaBuilder mcBuilder;
	private OrganizacionBuilder orgBuilder;
	private Organizacion org;
	private Documento docto;
	private MovimientoCaja mc;

	@Before
	public void setUp() {
		doctoBuilder = new DocumentoBuilder("TestDocument", "TestDocument");
		mcBuilder = new MovimientoCajaBuilder();
		orgBuilder = new OrganizacionBuilder("TestOrg", "TestOrganizacion","RR");
	}

	/**
	 * Este test depende de algunos valores ya pre registrados en BD. revise que
	 * existan dichos valores
	 */
	@Test	
	@Transactional
	public void consultarMCTest() {
		final DateTime current = new DateTime();
		final DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");		
		Collection<MovimientoCajaVO> movimientos = null;
		try {
			org = this.orgRepo.save(this.orgBuilder.build());
			final List<NameValueVO> orgs = new ArrayList<NameValueVO>();
			final NameValueVO orgValue = new NameValueVO(org.getIdOrg(), "TestOrganizacion");
			orgs.add(orgValue);
			final Catalogo catMovCargo = this.catRepo.findByClaveInterna("MOV_CAJA_CARGO");
			final Catalogo catTipoOpGasto = this.catRepo.findByClaveInterna("TIPO_OPE_GASTO");
			final Catalogo catActivo = this.catRepo.findByClaveInterna("CAT_TIPO_ACTIVO");
			final List<Usuario> usr = usrRepo.findByNombreUsuario("admin");
			this.mcBuilder.estatusMov(catActivo).fchs(current.toDate(), current.toDate()).ruta(org).tipoMov(catMovCargo)
					.tipoOperacion(catTipoOpGasto).tipoGasto(catMovCargo).usuario(usr.get(0));
			mc = mcRepository.save(this.mcBuilder.build());
			this.doctoBuilder.cleanPropiedades().propiedad(Constantes.IDORG, mc.getIdMovCaja());
			docto = this.doctoRepo.save(this.doctoBuilder.build());
			final FiltrosOrgVO filtro = new FiltrosOrgVO();
			filtro.setRutas(orgs);
			filtro.setFechaInicial(fmt.print(current.minusDays(1)));
			filtro.setFechaFinal(fmt.print(current.plusDays(1)));
			movimientos = this.service.consultarMovimientosCaja(filtro);
		} catch (Exception exception) {			
			Assert.fail(exception.getMessage());
		}
		Assert.assertNotNull(movimientos);
		Assert.assertTrue(movimientos.size() == 1);
		final String idDocto = movimientos.iterator().next().getIdDocto();
		Assert.assertTrue(idDocto.equals(docto.getIdDocumento()));
	}
	
	@AfterTransaction
	public void deleteResources(){
		if (org != null) {
			this.orgRepo.delete(org);
		}
		if (docto != null) {
			this.doctoRepo.delete(docto);		
		}
		if (mc != null) {
			this.mcRepository.delete(mc);
		}
	}
}
