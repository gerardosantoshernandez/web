package com.crenx.builder;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * @author JCHR
 *
 */
public class BuilderUtils {

	/**
	 * @param mandatoryFields
	 * @return
	 */
	public static <T> boolean validMandatoryFields(Class<T> clazz, Object object, final String... mandatoryFields) {
		boolean valid = true;
		if (mandatoryFields != null && mandatoryFields.length > 0 && object != null && clazz != null
				&& object.getClass() == clazz) {
			final Field[] fields = clazz.getDeclaredFields();
			final List<String> mandatoy = Arrays.asList(mandatoryFields);
			for (Field field : fields) {
				field.setAccessible(true);
				try {
					if (mandatoy.contains(field.getName()) && field.get(object) == null) {
						valid = false;
						break;
					}
				} catch (IllegalArgumentException e) {
					throw new IllegalArgumentException("La propiedad " + field.getName() + "del objeto" + clazz
							+ "tiene un valor null y es obligatorio");
				} catch (IllegalAccessException e) {
					// loggear : "La propiedad " + field.getName() + "no puede
					// ser accedida para conocer su valor";
				}
			}
		}
		return valid;
	}
}
