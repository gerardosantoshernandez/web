package com.crenx.builder;

import java.util.Date;

import org.modelmapper.ModelMapper;

import com.crenx.data.domain.entity.Catalogo;
import com.crenx.data.domain.entity.MovimientoCaja;
import com.crenx.data.domain.entity.Organizacion;
import com.crenx.data.domain.entity.Usuario;

/**
 * @author JCHR
 *
 */
public class MovimientoCajaBuilder {

	private MovimientoCaja mc;
	private String[] mandatory = new String[] { "id_ruta", "id_tipo_mov", "id_tipo_operacion", "id_tipo_gasto",
			"id_estatus_mov", "referencia", "autorizador", "fecha_emision", "fecha_sistema", "cargo", "abono",
			"id_usuario", "referenciaLarga", "estatusCorte" };

	/**
	 * 
	 */
	public MovimientoCajaBuilder() {
		this.mc = new MovimientoCaja();
		mc.setReferencia("TestCaja");
		mc.setReferenciaLarga("TestCaja");
		mc.setAbono(0L);
		mc.setAutorizador("TestCaja");
		mc.setCargo(0L);
		mc.setEstatusCorte(Constants.FALSE_BYTE);
		mc.setPendiente(0L);
		mc.setFechaEmision(new Date());
		mc.setFechaSistema(new Date());
	}

	/**
	 * @param referencia
	 * @param referenciaLarga
	 * @return
	 */
	public MovimientoCajaBuilder referencias(final String referencia, final String referenciaLarga) {
		mc.setReferencia(referencia);
		mc.setReferenciaLarga(referenciaLarga);
		return this;
	}

	/**
	 * @param abono
	 * @return
	 */
	public MovimientoCajaBuilder abono(final double abono) {
		mc.setAbono(abono);
		return this;
	}

	/**
	 * @param autorizador
	 * @return
	 */
	public MovimientoCajaBuilder autorizador(final String autorizador) {
		mc.setAutorizador(autorizador);
		return this;
	}

	/**
	 * @param cargo
	 * @return
	 */
	public MovimientoCajaBuilder cargo(final double cargo) {
		this.mc.setCargo(cargo);
		return this;
	}

	/**
	 * @param estatus
	 * @return
	 */
	public MovimientoCajaBuilder estatusCorte(final boolean estatus) {
		mc.setEstatusCorte((estatus) ? Constants.TRUE_BYTE : Constants.FALSE_BYTE);
		return this;
	}

	/**
	 * @param pendiente
	 * @return
	 */
	public MovimientoCajaBuilder pendiente(final double pendiente) {
		this.mc.setPendiente(pendiente);
		return this;
	}

	/**
	 * @param fechaEmision
	 * @param fechaSistema
	 * @return
	 */
	public MovimientoCajaBuilder fchs(final Date fechaEmision, final Date fechaSistema) {
		this.mc.setFechaEmision(fechaEmision);
		this.mc.setFechaSistema(fechaSistema);
		return this;
	}

	/**
	 * @param estatusMovimiento
	 * @return
	 */
	public MovimientoCajaBuilder estatusMov(final Catalogo estatusMovimiento) {
		this.mc.setEstatusMovimiento(estatusMovimiento);
		return this;
	}

	/**
	 * @param usr
	 * @return
	 */
	public MovimientoCajaBuilder usuario(final Usuario usr) {
		this.mc.setIdUsuario(usr.getIdUsuario());
		return this;
	}

	/**
	 * @param ruta
	 * @return
	 */
	public MovimientoCajaBuilder ruta(final Organizacion ruta) {
		this.mc.setRuta(ruta);
		return this;
	}

	/**
	 * @param estatusMovimiento
	 * @return
	 */
	public MovimientoCajaBuilder tipoGasto(final Catalogo tipoGasto) {
		this.mc.setTipoGasto(tipoGasto);
		return this;
	}

	/**
	 * @param estatusMovimiento
	 * @return
	 */
	public MovimientoCajaBuilder tipoMov(final Catalogo tipoMovimiento) {
		this.mc.setTipoMovimiento(tipoMovimiento);
		return this;
	}

	/**
	 * @param estatusMovimiento
	 * @return
	 */
	public MovimientoCajaBuilder tipoOperacion(final Catalogo tipoOperacion) {
		this.mc.setTipoOperacion(tipoOperacion);
		return this;
	}

	/**
	 * @return
	 */
	public MovimientoCaja build() {
		/*final MovimientoCaja mc = new MovimientoCaja();		
		mc.setReferencia(this.mc.getReferencia());
		mc.setReferenciaLarga(this.mc.getReferenciaLarga());
		mc.setAbono(this.mc.getAbono());
		mc.setAutorizador(this.mc.getAutorizador());
		mc.setCargo(this.mc.getCargo());
		mc.setEstatusCorte(this.mc.getEstatusCorte());
		mc.setPendiente(this.mc.getPendiente());
		mc.setFechaEmision(this.mc.getFechaEmision());
		mc.setFechaSistema(this.mc.getFechaSistema());
		mc.setEstatusMovimiento(this.mc.getEstatusMovimiento());
		mc.setIdUsuario(this.mc.getIdUsuario());
		mc.setRuta(this.mc.getRuta());
		mc.setTipoGasto(this.mc.getTipoGasto());
		mc.setTipoMovimiento(this.mc.getTipoMovimiento());
		mc.setTipoOperacion(this.mc.getTipoOperacion());*/
		final ModelMapper mapper = new ModelMapper();
		final MovimientoCaja mc = new MovimientoCaja();
		mapper.map(this.mc, mc);
		BuilderUtils.validMandatoryFields(MovimientoCaja.class, mc, this.mandatory);
		return mc;
	}
}
