package com.crenx.builder;

import org.modelmapper.ModelMapper;

import com.crenx.data.domain.entity.Ruta;

/**
 * @author JCHR
 *
 */
public class RutaBuilder {

	private Ruta ruta;

	/**
	 * 
	 */
	public RutaBuilder() {
		this.ruta = new Ruta();
		ruta.setCreaClientes(Constants.FALSE_BYTE);
		ruta.setFacturar(Constants.FALSE_BYTE);
		ruta.setPermiteAbonos(Constants.FALSE_BYTE);
		ruta.setMensajeFactura("TestRuta");
	}

	/**
	 * @return
	 */
	public RutaBuilder creaClientes(boolean crea) {
		this.ruta.setCreaClientes((crea) ? Constants.TRUE_BYTE : Constants.FALSE_BYTE);
		return this;
	}

	/**
	 * @return
	 */
	public RutaBuilder factura(final boolean factura) {
		this.ruta.setFacturar((factura) ? Constants.TRUE_BYTE : Constants.FALSE_BYTE);
		return this;
	}

	/**
	 * @return
	 */
	public RutaBuilder bonos(final boolean bono) {
		this.ruta.setFacturar((bono) ? Constants.TRUE_BYTE : Constants.FALSE_BYTE);
		return this;
	}

	/**
	 * @param idDomicilio
	 * @return
	 */
	public RutaBuilder domicilio(final String idDomicilio) {
		this.ruta.setIdDomicilio(idDomicilio);
		return this;
	}

	/**
	 * @param cveFiscal
	 * @return
	 */
	public RutaBuilder cveFiscal(final String cveFiscal) {
		this.ruta.setCveFiscal(cveFiscal);
		return this;
	}

	/**
	 * @return
	 */
	public RutaBuilder tipoCuadre(final String idTipoCuadreCel) {
		this.ruta.setIdTipoCuadreCel(idTipoCuadreCel);
		return this;
	}

	/**
	 * @return
	 */
	public RutaBuilder mensajeFactura(final String mensajeFactura) {
		this.ruta.setMensajeFactura(mensajeFactura);
		return this;
	}

	/**
	 * @return
	 */
	public Ruta build() {
		final ModelMapper mapper = new ModelMapper();
		final Ruta ruta = new Ruta();
		mapper.map(this.ruta,ruta);
		return ruta;
	}

}
