package com.crenx.builder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;

import com.crenx.business.FileUtils;
import com.crenx.data.domain.entity.Documento;
import com.crenx.data.domain.entity.PropiedadesDoc;
import com.google.common.reflect.TypeToken;

/**
 * 
 * Builder para creación de la entidad Documento
 * 
 * @author JCHR
 *
 */
public class DocumentoBuilder {

	private Documento documento;

	/**
	 * @param nombre
	 * @param descripcion
	 */
	public DocumentoBuilder(final String nombre, final String descripcion) {
		if (StringUtils.isBlank(nombre) && StringUtils.isBlank(descripcion)) {
			throw new IllegalArgumentException("Nombre y descripcion no pueden ser null");
		}
		this.documento = new Documento();
		documento.setNombre(nombre);
		documento.setDescripcion(descripcion);
		documento.setFileName(FileUtils.stampWithTime("DocumentoPrueba", null));
		documento.setContentType("text/plain");
		documento.setFechaCreacion(new Date());
		documento.setReferencia("TestDocument");
		documento.setPropiedadesDocs(new ArrayList<PropiedadesDoc>());
	}

	/**
	 * @param nombre
	 * @param stapm
	 * @return
	 */
	public DocumentoBuilder fileName(final String nombre, boolean stamp) {
		documento.setFileName((stamp) ? FileUtils.stampWithTime(nombre, null) : nombre);
		return this;
	}

	/**
	 * @param contentType
	 * @return
	 */
	public DocumentoBuilder contentType(final String contentType) {
		this.documento.setContentType(contentType);
		return this;
	}

	/**
	 * @param fch
	 * @return
	 */
	public DocumentoBuilder fchCreacion(final Date fch) {
		this.documento.setFechaCreacion(fch);
		return this;
	}

	/**
	 * @param nombre
	 * @param valor
	 * @return
	 */
	public DocumentoBuilder propiedad(final String nombre, final String valor) {
		final PropiedadesDoc propiedad = new PropiedadesDoc();
		propiedad.setNombre(nombre);
		propiedad.setValor(valor);
		this.documento.getPropiedadesDocs().add(propiedad);
		return this;
	}

	/**
	 * @return
	 */
	public DocumentoBuilder cleanPropiedades() {
		this.documento.getPropiedadesDocs().clear();
		return this;
	}

	/**
	 * @return
	 */
	@SuppressWarnings("serial")
	public Documento build() {
		final ModelMapper mapper = new ModelMapper();
		final Documento docto= new Documento();
		mapper.map(this.documento, docto);
		final Type listType = new TypeToken<List<PropiedadesDoc>>() {
		}.getType();
		final List<PropiedadesDoc> propiedades = mapper.map(this.documento.getPropiedadesDocs(), listType);
		for (PropiedadesDoc propiedad : propiedades) {
			propiedad.setDocumento(docto);
		}
		docto.setPropiedadesDocs(propiedades);
		return docto;
	}
}
