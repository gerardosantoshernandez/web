package com.crenx.builder;

import org.modelmapper.ModelMapper;

import com.crenx.data.domain.entity.Organizacion;

/**
 * @author JCHR
 *
 */
public class OrganizacionBuilder {

	private Organizacion org;

	/**
	 * 
	 */
	public OrganizacionBuilder(final String clave, final String nombre, final String tipo) {
		org = new Organizacion();
		org.setClave(clave);
		org.setNombre(nombre);
		org.setTipoNodo(tipo);
	}

	/**
	 * @return
	 */
	public Organizacion build() {
		final ModelMapper mapper = new ModelMapper();
		final Organizacion org = new Organizacion();
		mapper.map(this.org,org);
		return  org;
	}
	
	/**
	 * @param parent
	 * @return
	 */
	public OrganizacionBuilder parent(final Organizacion parent){
		this.org.setParent(parent);
		return this;
	}
	
	/**
	 * @param clave
	 * @return
	 */
	public OrganizacionBuilder clave(final String clave){
		this.org.setClave(clave);;
		return this;
	}
	/**
	 * @param nombre
	 * @return
	 */
	public OrganizacionBuilder nombre(final String nombre){
		this.org.setNombre(nombre);
		return this;
	}
	
	/**
	 * @param tipo
	 * @return
	 */
	public OrganizacionBuilder tipo(final String tipo){
		this.org.setTipoNodo(tipo);
		return this;
	}
}
